<?php

return [

    'ADMIN_ROLES' => [ 'Administrator' => '1','Manager' => '3','Owner' => '4'],
    'USER_ROLES' => [ 'User' => '2','Owner' => '4'],
    'CONSTANT_TEXT'=>[
        'BED_SHARING'=>'Bed Sharing',
        'AVAILABLITY_TYPE'=>'Availability Type'
        ]
];

