<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlertUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('username')->after('lastName');
            $table->string('email_verify_token')->after('roleID');
            $table->integer('city_id')->after('gender')->nullable();
            $table->integer('state_id')->after('city_id')->nullable();
            $table->string('firm_name')->after('state_id')->nullable();
            $table->dateTime('email_verified_at')->after('email_verify_token');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
