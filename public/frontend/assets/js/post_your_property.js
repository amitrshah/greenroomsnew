$(function () {
	var i = $('.register'),
		j = $('#registrationModal'),
		k = $('.login'),
		l = $('#loginModal'),
		m = $('.forgotPassword'),
		n = $('#forgotModal'),
		o = $('#verifyNo'),
		p = $('#verifyForm'),
		r = $('#rootwizard'),
		m1 = $('.registerModal'),
		m2 = $('.signIn'),
		z = $('#condition'),
		y = $('.withFurnished'),
		A = {
			f: function () {
				i.on('click', function () {
					j.modal({
						backdrop: 'static',
						keyborad: false,
						focus: true
					});
				}),
					k.on('click', function () {
						l.modal({
							backdrop: 'static',
							keyborad: false,
							focus: true
						});
					}),
					m.on('click', function () {
						l.modal('hide'),
							n.modal({
								backdrop: 'static',
								keyborad: false,
								focus: true
							});
					}),
					o.modal({
						backdrop: 'static',
						keyborad: false,
						focus: true
					}),
					m1.on('click', function () {
						l.modal('hide'),
							j.modal({ backdrop: 'static', keyborad: !1, focus: !0 });
					}),
					m2.on('click', function () {
						j.modal('hide'),
							l.modal({ backdrop: 'static', keyborad: !1, focus: !0 });
					});
			},
			g: function () {
				p.validate({
					rules: {
						enterCode: 'required'
					},
					errorElement: 'em',
					errorPlacement: function () { },
					success: function () { },
					highlight: function (element) {
						$(element)
							.parents('.form-group')
							.addClass('was-validated')
							.removeClass('has-success');
					}
				});
			},
			h: function () {
				$.validator.setDefaults({
					onsubmit: false,
					errorElement: 'em',
					highlight: function (element, errorClass, validClass) {
						$(element)
							.parents('.form-group')
							.addClass('was-validated')
							.removeClass('has-success');
					}
				});
				jQuery.validator.messages.required = '';
				(r1 = $('#rootwizard').find("a[href*='tab1']")),
					(r2 = $('#rootwizard').find("a[href*='tab2']")),
					(r3 = $('#rootwizard').find("a[href*='tab3']")),
					(r4 = $('#rootwizard').find("a[href*='tab4']")),
					(circle = new ProgressBar.Circle('#circle', {
						color: '#FFEA82',
						trailColor: '#eee',
						trailWidth: 0.5,
						duration: 1400,
						easing: 'bounce',
						strokeWidth: 5,
						from: { color: '#f6faf7', a: 0 },
						to: { color: '#fff', a: 1 },
						step: function (state, circle) {
							circle.path.setAttribute('stroke', state.color),
								(value = Math.round(circle.value() * 100));
							if (value === 0) {
								circle.setText('');
							} else {
								circle.setText(value);
							}
						}
					}));
				circle.text.style.fontFamily = '"Roboto", sans-serif;';
				circle.text.style.fontSize = '2.5rem';
				circle.animate(0.1);
				r.bootstrapWizard({
					onInit: function () {
						r1.addClass('pointer-events-none'),
							r2.addClass('pointer-events-none'),
							r3.addClass('pointer-events-none'),
							r4.addClass('pointer-events-none');
					},
					onTabShow: function (tab, navigation, index) {
						if (index == 0) {
							circle.animate(0.01);
							$('.prev').hide(),
								$('.next').show(),
								$('.submitbtn').hide()
						}
						if (index == 1) {
							circle.animate(0.25);
							$('.prev').show()
						}
						if (index == 2) {
							circle.animate(0.50);
							$('.prev').show()
						}
						if (index == 3) {
							circle.animate(1);
							$('.prev').show(),
								$('.next').hide(),
								$('.submitbtn').show()
						}
					},
					onNext: function (tab, navigation, index) {
					},
					onPrevious: function (tab, navigation, index) {
						// circle.animate(1);
					}
				});
			}
		},
		B = {
			a: function () {
				(y.hide()),
					z.on('change', function () {
						"unfurnished" == $(this).val() && (y.hide()),
							"semi_furnished" == $(this).val() && (y.show(), $('.conditionStatus').html(''), $('.conditionStatus').html('2 furnishing')),
							"furnished" == $(this).val() && (y.show(), $('.conditionStatus').html(''), $('.conditionStatus').html('3 furnishing')),
							"fully_furnished" == $(this).val() && (y.show(), $('.conditionStatus').html(''), $('.conditionStatus').html('5 to 6 furnishing'))
					})
			},
			b: function () {
				$('ul.items li').each(function () {
					var counter = 0
					$(this).on('click', 'a', function (e) {
						e.stopPropagation();
						e.stopImmediatePropagation();
						if ($(this).hasClass('addition')) {
							var a = ++counter
							$(this).prev('.counter').text(a);
						}
						if ($(this).hasClass('substraction')) {
							var a = counter--;
							$(this).next('.counter').text(a);
							if (counter <= 0) {
								counter = 0
								return false;
							}
						}
					})
				})
				$('#collapseExample').on('hide.bs.collapse', function () {
					$('.downArrow').removeClass('d-none'),
						$('.upArrow').addClass('d-none');
				})
				$('#collapseExample').on('show.bs.collapse', function () {
					$('.downArrow').addClass('d-none'),
						$('.upArrow').removeClass('d-none');
				})
			},
			c: function () {
				start = moment();
				var end = moment();
				function cb(start, end) {
					$('#reportrange span').html(start.format('MMMM D, YYYY'));
					$("#input_available_from").val(start.format("YYYY-MM-DD"));
				}
				$('#reportrange').daterangepicker({
					singleDatePicker: true,
					startDate: start,
					endDate: end
				}, cb);
				cb(start, end);
			},
			d: function () {
				$('ul.availabilityType li').each(function () {
					$('.availableTypeOkay').hide();
					$(this).on('click', 'a', function (e) {
						/*e.stopPropagation();
						e.stopImmediatePropagation();*/
						$(this).addClass('active');
						if ($(this).hasClass('active')) {
							$(this).removeClass('active');
							$(this).next('.availableTypeOkay').show();
							$(this).css('opacity', '1');
							$(this).parent().prevAll('li').find('.availableTypeOkay').hide()
							$(this).parent().prevAll('li').find('a').css('opacity', '0.3');
							$(this).parent().nextAll('li').find('.availableTypeOkay').hide()
							$(this).parent().nextAll('li').find('a').css('opacity', '0.3');
						}
					})
				})
				jQuery.fn.clickToggle = function (a, b) {
					return this.on("click", function (ev) { [b, a][this.$_io ^= 1].call(this, ev) })
				};
				$('ul.externalAmenity li').each(function () {
					$('.availableTypeOkay').hide();
					$(this).clickToggle(function () {
						$(this).find('.availableTypeOkay').show();
						$(this).find('a').css('opacity', '1');
					}, function () {
						$(this).find('.availableTypeOkay').hide();
						$(this).find('a').css('opacity', '0.3');
					});
				})
				$('.numbersOnly').keyup(function (e) {
					if (/\D/g.test(this.value)) {
						// Filter non-digits from input value.
						this.value = this.value.replace(/\D/g, '');
					}
				});
			}
		}
	A.f(), A.g(), A.h(), B.a(), B.b(), B.c(), B.d();
});
