$(function () {
    var m = $('.forgotPassword'),
        n = $('#forgotModal'),
        a1 = $('.editProfile'),
        a2 = $('#editProfile')
    $('[data-toggle="tooltip"]').tooltip()
    A = {
        f: function () {
            m.on('click', function () {
                n.modal({
                    backdrop: 'static',
                    keyborad: false,
                    focus: true
                });
            });
            a1.on('click', function () {
                a2.modal({
                    backdrop: 'static',
                    keyborad: false,
                    focus: true
                });
            });
        },
        i: function () {
            var n = 300,
                i = '...',
                e = 'More...',
                a = 'Less...';
            $('.more').each(function () {
                var a = $(this).html();
                a.length > n &&
                    ((t = a.substr(0, n)),
                        (h = a.substr(n, a.length - n)),
                        (html =
                            t +
                            '<span class="moreellipses">' +
                            i +
                            '&nbsp;</span><span class="morecontent"><span>' +
                            h +
                            '</span>&nbsp;&nbsp;<a href="javascript:void(0)" class="morelink_search">' +
                            e +
                            '</a></span>'),
                        $(this).html(html));
            }),
                $('.morelink_search').click(function () {
                    return (
                        $(this).hasClass('less')
                            ? ($(this).removeClass('less'), $(this).html(e))
                            : ($(this).addClass('less'), $(this).html(a)),
                        $(this)
                            .parent()
                            .prev()
                            .toggle(),
                        $(this)
                            .prev()
                            .toggle(),
                        !1
                    );
                });
        }
    };
    A.f(), A.i()
});
