$(function () {
    var h = $('#propertyAvailability'),
        m = $('.forgotPassword'),
        n = $('#forgotModal'),
        a1 = $('.editProfile'),
        a2 = $('#editProfile')
    $('[data-toggle="tooltip"]').tooltip()
    A = {
        f: function () {
            m.on('click', function () {
                n.modal({
                    backdrop: 'static',
                    keyborad: false,
                    focus: true
                });
            });
            a1.on('click', function () {
                a2.modal({
                    backdrop: 'static',
                    keyborad: false,
                    focus: true
                });
            });
        },
        h: function () {
            var myConfig = {
                backgroundColor: '#fff',
                type: "ring",
                title: {
                    text: "Quota Distribution",
                    fontFamily: 'Roboto',
                    fontSize: 14,
                    // border: "1px solid black",
                    padding: "15",
                    fontColor: "#1E5D9E",
                },
                "plot": {
                    "value-box": {
                        "text": "%v",
                        "decimals": 0,
                        "placement": "out",
                        "offset-r": "-10",
                        "font-family": "Roboto",
                        "font-size": 30,
                        "font-weight": "normal"
                    }
                },
                plotarea: {
                    backgroundColor: 'transparent',
                    borderWidth: 0,
                    borderRadius: "0 0 0 10",
                    margin: "30 0 0 0"
                },
                legend: {
                    toggleAction: 'remove',
                    backgroundColor: '#FBFCFE',
                    borderWidth: 0,
                    adjustLayout: true,
                    align: 'center',
                    verticalAlign: 'bottom',
                    marker: {
                        type: 'circle',
                        cursor: 'pointer',
                        borderWidth: 0,
                        size: 5
                    },
                    item: {
                        fontColor: "#777",
                        cursor: 'pointer',
                        offsetX: -6,
                        fontSize: 12
                    },
                    mediaRules: [
                        {
                            maxWidth: 500,
                            visible: false
                        }
                    ]
                },
                scaleR: {
                    refAngle: 270
                },
                series: [
                    {
                        text: "Remaining Counts",
                        values: [05],
                        lineColor: "#025159",
                        backgroundColor: "#025159",
                        lineWidth: 1,
                        marker: {
                            backgroundColor: '#E80C60'
                        }
                    },
                    {
                        text: "Used Counts",
                        values: [10],
                        lineColor: "#F22727",
                        backgroundColor: "#F22727",
                        lineWidth: 1,
                        marker: {
                            backgroundColor: '#9B26AF'
                        }
                    },
                    {
                        text: "Visited Property Count",
                        values: [4],
                        lineColor: "#037F8C",
                        backgroundColor: "#037F8C",
                        lineWidth: 1,
                        marker: {
                            backgroundColor: '#9B26AF'
                        }
                    }
                ]
            };
            zingchart.render({
                id: 'chartContainer',
                data: {
                    graphset: [myConfig]
                },
                height: '400',
                width: '100%'
            });
        }
    };
    A.f(), A.h();
});
