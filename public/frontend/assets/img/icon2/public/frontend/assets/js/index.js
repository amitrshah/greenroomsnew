$(function () {
	var a = $('#slideshow > div:gt(0)'),
		b = $('#slideshow > div:first'),
		c = $('#propertyType'),
		d = $('#forMaleOrFemale'),
		e = $('#searchByLoc'),
		f = $('#slideshow'),
		g = $('#responsive'),
		h = $('#propertyAvailability'),
		i = $('.register'),
		j = $('#registrationModal'),
		k = $('.login'),
		l = $('#loginModal'),
		m = $('.forgotPassword'),
		n = $('#forgotModal'),
		m1 = $('.registerModal'),
		m2 = $('.signIn'),
		A = {
			a: function () {
				a.hide(),
					setInterval(function () {
						b.fadeOut(1000)
							.next()
							.fadeIn(1000)
							.end()
							.appendTo('#slideshow');
					}, 1000),
					((categoryData = [
						{
							id: 0,
							text: 'Category'
						},
						{
							id: 1,
							text: 'PG'
						},
						{
							id: 2,
							text: 'Corporate PG(Service App.)'
						},
						{
							id: 3,
							text: 'Hostel'
						}
					]),
						(accomodationFor = [
							{
								id: 0,
								text: 'Accommodation For'
							},
							{
								id: 1,
								text: 'Male'
							},
							{
								id: 2,
								text: 'Female'
							}
						]),
						(searchByLoc = [
							{
								id: 1,
								text: 'Ambawadi',
								continent: 'AHD'
							},
							{
								id: 2,
								text: 'Ambli',
								continent: 'AHD'
							},
							{
								id: 3,
								text: 'Amraiwadi',
								continent: 'AHD'
							},
							{
								id: 4,
								text: 'Anandnagar',
								continent: 'AHD'
							},
							{
								id: 5,
								text: 'Asarwa',
								continent: 'AHD'
							},
							{
								id: 6,
								text: 'Ashram road',
								continent: 'AHD'
							},
							{
								id: 7,
								text: 'Aslali',
								continent: 'AHD'
							},
							{
								id: 8,
								text: 'Ayojan Nagar',
								continent: 'AHD'
							},
							{
								id: 9,
								text: 'Bapunagar',
								continent: 'AHD'
							},
							{
								id: 10,
								text: 'Bavla',
								continent: 'AHD'
							},
							{
								id: 11,
								text: 'Behrampura',
								continent: 'AHD'
							},
							{
								id: 12,
								text: 'Bhadaj',
								continent: 'AHD'
							},
							{
								id: 13,
								text: 'Bhadra',
								continent: 'AHD'
							},
							{
								id: 14,
								text: 'Bhat',
								continent: 'AHD'
							},
							{
								id: 15,
								text: 'Bodakdev',
								continent: 'AHD'
							},
							{
								id: 16,
								text: 'Bopal',
								continent: 'AHD'
							},
							{
								id: 17,
								text: 'C G Road',
								continent: 'AHD'
							},
							{
								id: 18,
								text: 'Chandkheda',
								continent: 'AHD'
							},
							{
								id: 19,
								text: 'Chandlodia',
								continent: 'AHD'
							},
							{
								id: 20,
								text: 'Changodar',
								continent: 'AHD'
							},
							{
								id: 21,
								text: 'Chharodi',
								continent: 'AHD'
							},
							{
								id: 22,
								text: 'Dani Limbada',
								continent: 'AHD'
							},
							{
								id: 23,
								text: 'Dariapur',
								continent: 'AHD'
							},
							{
								id: 24,
								text: 'Dhandhuka',
								continent: 'AHD'
							},
							{
								id: 25,
								text: 'Dudheshwar',
								continent: 'AHD'
							},
							{
								id: 26,
								text: 'Ellis Bridge',
								continent: 'AHD'
							},
							{
								id: 27,
								text: 'Ghatlodia',
								continent: 'AHD'
							},
							{
								id: 28,
								text: 'Ghodasar',
								continent: 'AHD'
							},
							{
								id: 29,
								text: 'Ghuma',
								continent: 'AHD'
							},
							{
								id: 30,
								text: 'Girdhar Nagar',
								continent: 'AHD'
							},
							{
								id: 31,
								text: 'Gomtipur',
								continent: 'AHD'
							},
							{
								id: 32,
								text: 'Gota',
								continent: 'AHD'
							},
							{
								id: 33,
								text: 'Gulbai Tekra',
								continent: 'AHD'
							},
							{
								id: 34,
								text: 'Gurukul',
								continent: 'AHD'
							},
							{
								id: 35,
								text: 'Hansol',
								continent: 'AHD'
							},
							{
								id: 36,
								text: 'Hathijan',
								continent: 'AHD'
							},
							{
								id: 37,
								text: 'Hatkeshwar',
								continent: 'AHD'
							},
							{
								id: 38,
								text: 'Isanpur',
								continent: 'AHD'
							},
							{
								id: 39,
								text: 'Jagatpur',
								continent: 'AHD'
							},
							{
								id: 40,
								text: 'Jamalpur',
								continent: 'AHD'
							},
							{
								id: 41,
								text: 'Jashoda Nagar',
								continent: 'AHD'
							},
							{
								id: 42,
								text: 'Jivrajpark',
								continent: 'AHD'
							},
							{
								id: 43,
								text: 'Jodhpur',
								continent: 'AHD'
							},
							{
								id: 44,
								text: 'Juhapura',
								continent: 'AHD'
							},
							{
								id: 46,
								text: 'Juna Wadaj',
								continent: 'AHD'
							},
							{
								id: 47,
								text: 'Kalapinagar',
								continent: 'AHD'
							},
							{
								id: 48,
								text: 'Kalupur',
								continent: 'AHD'
							},
							{
								id: 49,
								text: 'Kankaria',
								continent: 'AHD'
							},
							{
								id: 50,
								text: 'Kathwada',
								continent: 'AHD'
							},
							{
								id: 51,
								text: 'Keshav Nagar',
								continent: 'AHD'
							},
							{
								id: 52,
								text: 'Khadia',
								continent: 'AHD'
							},
							{
								id: 53,
								text: 'Khamasa',
								continent: 'AHD'
							},
							{
								id: 54,
								text: 'Khanpur',
								continent: 'AHD'
							},
							{
								id: 55,
								text: 'Khokhra',
								continent: 'AHD'
							},
							{
								id: 56,
								text: 'Kubernagar',
								continent: 'AHD'
							},
							{
								id: 57,
								text: 'Lambha',
								continent: 'AHD'
							},
							{
								id: 58,
								text: 'Madhupura',
								continent: 'AHD'
							},
							{
								id: 59,
								text: 'Makarba',
								continent: 'AHD'
							},
							{
								id: 60,
								text: 'Maninagar',
								continent: 'AHD'
							},
							{
								id: 61,
								text: 'Meghani Nagar',
								continent: 'AHD'
							},
							{
								id: 62,
								text: 'Memnagar',
								continent: 'AHD'
							},
							{
								id: 63,
								text: 'Motera',
								continent: 'AHD'
							},
							{
								id: 64,
								text: 'Naranpura',
								continent: 'AHD'
							},
							{
								id: 65,
								text: 'Naroda',
								continent: 'AHD'
							},
							{
								id: 66,
								text: 'Naroda GIDC',
								continent: 'AHD'
							},
							{
								id: 67,
								text: 'Naroda road',
								continent: 'AHD'
							},
							{
								id: 68,
								text: 'Narol',
								continent: 'AHD'
							},
							{
								id: 69,
								text: 'Nava Wadaj',
								continent: 'AHD'
							},
							{
								id: 70,
								text: 'Navjivan',
								continent: 'AHD'
							},
							{
								id: 71,
								text: 'Nikol',
								continent: 'AHD'
							},
							{
								id: 72,
								text: 'Nirnay Nagar',
								continent: 'AHD'
							},
							{
								id: 73,
								text: 'Noblenagar',
								continent: 'AHD'
							},
							{
								id: 74,
								text: 'Odhav',
								continent: 'AHD'
							},
							{
								id: 75,
								text: 'Ognaj',
								continent: 'AHD'
							},
							{
								id: 76,
								text: 'Paldi',
								continent: 'AHD'
							},
							{
								id: 77,
								text: 'Prahlad Nagar',
								continent: 'AHD'
							},
							{
								id: 78,
								text: 'Raikhad',
								continent: 'AHD'
							},
							{
								id: 79,
								text: 'Raipur',
								continent: 'AHD'
							},
							{
								id: 80,
								text: 'Rakhial',
								continent: 'AHD'
							},
							{
								id: 81,
								text: 'Ramdev Nagar',
								continent: 'AHD'
							},
							{
								id: 82,
								text: 'Ranip',
								continent: 'AHD'
							},
							{
								id: 83,
								text: 'Sabarmati',
								continent: 'AHD'
							},
							{
								id: 84,
								text: 'Sadar Bazar',
								continent: 'AHD'
							},
							{
								id: 85,
								text: 'Saijpur Bogha',
								continent: 'AHD'
							},
							{
								id: 86,
								text: 'Sanand',
								continent: 'AHD'
							},
							{
								id: 87,
								text: 'Sanathal',
								continent: 'AHD'
							},
							{
								id: 88,
								text: 'Saraspur',
								continent: 'AHD'
							},
							{
								id: 89,
								text: 'Sarkhej',
								continent: 'AHD'
							},
							{
								id: 91,
								text: 'Satellite',
								continent: 'AHD'
							},
							{
								id: 92,
								text: 'Shah E Alam Roja',
								continent: 'AHD'
							},
							{
								id: 93,
								text: 'Shahibaug',
								continent: 'AHD'
							},
							{
								id: 94,
								text: 'Shahpur',
								continent: 'AHD'
							},
							{
								id: 95,
								text: 'Shela',
								continent: 'AHD'
							},
							{
								id: 96,
								text: 'Shilaj',
								continent: 'AHD'
							},
							{
								id: 97,
								text: 'Sola',
								continent: 'AHD'
							},
							{
								id: 98,
								text: 'Sola Road',
								continent: 'AHD'
							},
							{
								id: 99,
								text: 'Thakkarbapa Nagar',
								continent: 'AHD'
							},
							{
								id: 100,
								text: 'Thaltej',
								continent: 'AHD'
							},
							{
								id: 101,
								text: 'Thaltej Road',
								continent: 'AHD'
							},
							{
								id: 102,
								text: 'Usmanpura',
								continent: 'AHD'
							},
							{
								id: 103,
								text: 'Vasna',
								continent: 'AHD'
							},
							{
								id: 104,
								text: 'Vastral',
								continent: 'AHD'
							},
							{
								id: 105,
								text: 'Vastrapur',
								continent: 'AHD'
							},
							{
								id: 106,
								text: 'Vatva GIDC',
								continent: 'AHD'
							},
							{
								id: 107,
								text: 'Vatva',
								continent: 'AHD'
							},
							{
								id: 108,
								text: 'Vejalpur',
								continent: 'AHD'
							},
							{
								id: 109,
								text: 'Viramgam',
								continent: 'AHD'
							},
							{
								id: 110,
								text: 'Shyamal',
								continent: 'AHD'
							},
							{
								id: 111,
								text: 'Racharda',
								continent: 'AHD'
							},
							{
								id: 112,
								text: 'S P Ring Road',
								continent: 'AHD'
							},
							{
								id: 113,
								text: 'Navrangpura',
								continent: 'AHD'
							},
							{
								id: 114,
								text: 'S G Highway',
								continent: 'AHD'
							},
							{
								id: 115,
								text: 'Palodia',
								continent: 'AHD'
							},
							{
								id: 116,
								text: 'Barejadi',
								continent: 'AHD'
							},
							{
								id: 117,
								text: 'Kali',
								continent: 'AHD'
							},
							{
								id: 118,
								text: 'Sughad',
								continent: 'AHD'
							},
							{
								id: 119,
								text: 'Manipur',
								continent: 'AHD'
							},
							{
								id: 120,
								text: 'Koteshwar',
								continent: 'AHD'
							},
							{
								id: 121,
								text: 'Science City',
								continent: 'AHD'
							},
							{
								id: 122,
								text: '132 Feet Ring Road',
								continent: 'AHD'
							},
							{
								id: 123,
								text: 'Santej',
								continent: 'AHD'
							},
							{
								id: 124,
								text: 'Nandej',
								continent: 'AHD'
							},
							{
								id: 125,
								text: 'Godhavi',
								continent: 'AHD'
							},
							{
								id: 126,
								text: 'Bagodara',
								continent: 'AHD'
							},
							{
								id: 127,
								text: 'Dholera',
								continent: 'AHD'
							},
							{
								id: 128,
								text: 'Tragad',
								continent: 'AHD'
							},
							{
								id: 129,
								text: 'Raska',
								continent: 'AHD'
							},
							{
								id: 130,
								text: 'New Ranip',
								continent: 'AHD'
							},
							{
								id: 131,
								text: 'Chanakyapuri',
								continent: 'AHD'
							},
							{
								id: 132,
								text: 'Rakanpur',
								continent: 'AHD'
							},
							{
								id: 133,
								text: 'Sanand - Nalsarovar Road',
								continent: 'AHD'
							},
							{
								id: 134,
								text: 'Dholka',
								continent: 'AHD'
							},
							{
								id: 136,
								text: 'kheda',
								continent: 'AHD'
							},
							{
								id: 137,
								text: 'Laxmanpura',
								continent: 'AHD'
							},
							{
								id: 138,
								text: 'Bavla Nalsarovar Road',
								continent: 'AHD'
							},
							{
								id: 139,
								text: 'Sanand-Viramgam Road',
								continent: 'AHD'
							},
							{
								id: 140,
								text: 'Mandal',
								continent: 'AHD'
							},
							{
								id: 141,
								text: 'New CG Road',
								continent: 'AHD'
							},
							{
								id: 142,
								text: 'D Colony',
								continent: 'AHD'
							},
							{
								id: 143,
								text: 'Sardar Colony',
								continent: 'AHD'
							},
							{
								id: 144,
								text: 'Ahmedabad-Rajkot-Highway',
								continent: 'AHD'
							},
							{
								id: 145,
								text: 'Nehrunagar',
								continent: 'AHD'
							},
							{
								id: 146,
								text: 'Kotarpur',
								continent: 'AHD'
							},
							{
								id: 147,
								text: 'Mirzapur',
								continent: 'AHD'
							},
							{
								id: 148,
								text: 'Nava Naroda',
								continent: 'AHD'
							},
							{
								id: 149,
								text: 'Narayan Nagar',
								continent: 'AHD'
							},
							{
								id: 150,
								text: 'Narol Road',
								continent: 'AHD'
							},
							{
								id: 151,
								text: 'Lapkaman',
								continent: 'AHD'
							},
							{
								id: 152,
								text: 'Kolat',
								continent: 'AHD'
							},
							{
								id: 153,
								text: 'Mahadev Nagar',
								continent: 'AHD'
							},
							{
								id: 154,
								text: 'Moraiya',
								continent: 'AHD'
							},
							{
								id: 155,
								text: 'New Maninagar',
								continent: 'AHD'
							},
							{
								id: 156,
								text: 'Purshottam Nagar',
								continent: 'AHD'
							},
							{
								id: 157,
								text: 'Gita Mandir',
								continent: 'AHD'
							},
							{
								id: 158,
								text: 'Sachana',
								continent: 'AHD'
							},
							{
								id: 159,
								text: 'Nasmed',
								continent: 'AHD'
							},
							{
								id: 160,
								text: 'Khodiar Nagar',
								continent: 'AHD'
							},
							{
								id: 161,
								text: 'Shantipura',
								continent: 'AHD'
							},
							{
								id: 162,
								text: 'Vinzol',
								continent: 'AHD'
							},
							{
								id: 163,
								text: 'Gokuldham',
								continent: 'AHD'
							},
							{
								id: 164,
								text: 'Geratpur',
								continent: 'AHD'
							},
							{
								id: 165,
								text: 'Sarangpur',
								continent: 'AHD'
							},
							{
								id: 166,
								text: 'South Bopal',
								continent: 'AHD'
							},
							{
								id: 167,
								text: 'Vaishno Devi',
								continent: 'AHD'
							},
							{
								id: 168,
								text: 'Acher',
								continent: 'AHD'
							},
							{
								id: 169,
								text: 'Hebatpur',
								continent: 'AHD'
							},
							{
								id: 170,
								text: 'Devdholera',
								continent: 'AHD'
							},
							{
								id: 171,
								text: 'Lilapur',
								continent: 'AHD'
							},
							{
								id: 172,
								text: 'Mahemdabad',
								continent: 'AHD'
							},
							{
								id: 173,
								text: 'Vishala',
								continent: 'AHD'
							},
							{
								id: 174,
								text: 'Pipali Highway',
								continent: 'AHD'
							},
							{
								id: 175,
								text: 'Ashok Vatika',
								continent: 'AHD'
							},
							{
								id: 176,
								text: 'Nandoli',
								continent: 'AHD'
							},
							{
								id: 177,
								text: 'Khoraj',
								continent: 'AHD'
							},
							{
								id: 178,
								text: 'Kasindra',
								continent: 'AHD'
							},
							{
								id: 179,
								text: 'Nana Chiloda',
								continent: 'AHD'
							}
						])),
					c.select2({
						data: categoryData,
						multiple: false,
						minimumResultsForSearch: Infinity
					}),
					d.select2({
						data: accomodationFor,
						multiple: false,
						minimumResultsForSearch: Infinity
					}),
					e.select2({
						data: searchByLoc,
						placeholder: 'Search By Location',
						multiple: true
					});
			},
			b: function () {
				window.matchMedia(
					' screen and (max-width : 414px) and (orientation : portrait) '
				).matches && f.remove();
				window.matchMedia(' screen and (max-width : 767px)').matches &&
					f.remove();
			},
			c: function () {
				($.fn.parallaxify = function () {
					return this.each(function () {
						var $element = $(this);
						function percentageSeen() {
							var viewportHeight = $(window).height(),
								winScrollTop = $(window).scrollTop(),
								elementOffsetTop = $element.offset().top,
								elementHeight = $element.height();
							var distance = winScrollTop + viewportHeight - elementOffsetTop;
							var percentage =
								distance / ((viewportHeight + elementHeight) / 100);
							if (percentage < 0) return 0;
							else if (percentage > 100) return 100;
							else return percentage;
						}
						$element.css({ 'background-position-y': percentageSeen() + '%' });
						$(window).on('scroll', function () {
							$element.css({ 'background-position-y': percentageSeen() + '%' });
						});
					});
				}),
					$('.parallax').parallaxify();
			},
			d: function () {
				g.lightSlider({
					item: 4,
					loop: false,
					slideMargin: 9,
					slideMove: 2,
					easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
					speed: 600,
					responsive: [
						{
							breakpoint: 800,
							settings: {
								item: 1,
								slideMove: 1,
								slideMargin: 6
							}
						},
						{
							breakpoint: 480,
							settings: {
								item: 1,
								slideMove: 1
							}
						}
					]
				}),
					h.lightSlider({
						item: 4,
						loop: false,
						controls: false,
						slideMove: 2,
						easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
						speed: 600
					});
				window.matchMedia(
					' screen and (min-width : 768px) and (max-width: 991px) '
				).matches &&
					h.lightSlider({
						item: 4,
						loop: false,
						controls: false,
						slideMove: 2,
						easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
						speed: 600
					});
				window.matchMedia('(max-width : 420px)').matches &&
					h.lightSlider({
						item: 1,
						loop: false,
						controls: false,
						slideMove: 2,
						easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
						speed: 600
					});
			},
			f: function () {
				i.on('click', function () {
					j.modal({
						backdrop: 'static',
						keyborad: false,
						focus: true
					});
				});
				k.on('click', function () {
					l.modal({
						backdrop: 'static',
						keyborad: false,
						focus: true
					});
				});
				m.on('click', function () {
					l.modal('hide'),
						n.modal({
							backdrop: 'static',
							keyborad: false,
							focus: true
						});
				}),
					m1.on('click', function () {
						l.modal('hide'),
							j.modal({ backdrop: 'static', keyborad: !1, focus: !0 });
					}),
					m2.on('click', function () {
						j.modal('hide'),
							l.modal({ backdrop: 'static', keyborad: !1, focus: !0 });
					});
			},
			g: function () {
				// Configure/customize these variables.
				var showChar = 210;  // How many characters are shown by default
				var ellipsestext = "...";
				var moretext = "more";
				var lesstext = "less";
				$('.more').each(function () {
					var content = $(this).html();
					if (content.length > showChar) {
						var c = content.substr(0, showChar);
						var h = content.substr(showChar, content.length - showChar);
						var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
						$(this).html(html);
					}
				});
				$(".morelink").click(function () {
					if ($(this).hasClass("less")) {
						$(this).removeClass("less");
						$(this).html(moretext);
					} else {
						$(this).addClass("less");
						$(this).html(lesstext);
					}
					$(this).parent().prev().toggle();
					$(this).prev().toggle();
					return false;
				});
			}
		};
	A.a(), A.b(), A.c(), A.d(), A.f(), A.g()
});
