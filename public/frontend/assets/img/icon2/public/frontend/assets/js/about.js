$(function() {
	var h = $('#propertyAvailability'),
		i = $('.register'),
		j = $('#registrationModal'),
		k = $('.login'),
		l = $('#loginModal'),
		m = $('.forgotPassword'),
		n = $('#forgotModal'),
		m1 = $('.registerModal'),
		m2 = $('.signIn'),
		A = {
			d: function() {
				h.lightSlider({
					item: 4,
					loop: false,
					controls: false,
					slideMove: 2,
					easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
					speed: 600
				});
				window.matchMedia(
					' screen and (min-width : 768px) and (max-width: 991px) '
				).matches &&
					h.lightSlider({
						item: 4,
						loop: false,
						controls: false,
						slideMove: 2,
						easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
						speed: 600
					});
				window.matchMedia('(max-width : 420px)').matches &&
					h.lightSlider({
						item: 1,
						loop: false,
						controls: false,
						slideMove: 2,
						easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
						speed: 600
					});
			},
			f: function() {
				i.on('click', function() {
					j.modal({
						backdrop: 'static',
						keyborad: false,
						focus: true
					});
				});
				k.on('click', function() {
					l.modal({
						backdrop: 'static',
						keyborad: false,
						focus: true
					});
				});
				m.on('click', function() {
					l.modal('hide'),
						n.modal({
							backdrop: 'static',
							keyborad: false,
							focus: true
						});
				});
				m1.on('click', function() {
					l.modal('hide'),
						j.modal({ backdrop: 'static', keyborad: !1, focus: !0 });
				});
				m2.on('click', function() {
					j.modal('hide'),
						l.modal({ backdrop: 'static', keyborad: !1, focus: !0 });
				});
			}
		};
	A.d(), A.f();
});
