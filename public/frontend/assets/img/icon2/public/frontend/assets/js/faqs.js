$(function() {
	var i = $('.register'),
		j = $('#registrationModal'),
		k = $('.login'),
		l = $('#loginModal'),
		m = $('.forgotPassword'),
		n = $('#forgotModal'),
		m1 = $('.registerModal'),
		m2 = $('.signIn'),
		A = {
			f: function() {
				i.on('click', function() {
					j.modal({
						backdrop: 'static',
						keyborad: false,
						focus: true
					});
				}),
					k.on('click', function() {
						l.modal({
							backdrop: 'static',
							keyborad: false,
							focus: true
						});
					}),
					m.on('click', function() {
						l.modal('hide'),
							n.modal({
								backdrop: 'static',
								keyborad: false,
								focus: true
							});
					}),
					m1.on('click', function() {
						l.modal('hide'),
							j.modal({ backdrop: 'static', keyborad: !1, focus: !0 });
					}),
					m2.on('click', function() {
						j.modal('hide'),
							l.modal({ backdrop: 'static', keyborad: !1, focus: !0 });
					});
			}
		};
	A.f();
});
