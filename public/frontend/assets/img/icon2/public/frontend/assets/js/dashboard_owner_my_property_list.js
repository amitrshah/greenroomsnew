$(function() {
	var s = $(".forgotPassword"),
		o = $("#forgotModal"),
		a = $(".editProfile"),
		e = $("#editProfile");
	$('[data-toggle="tooltip"]').tooltip(), A = {
		f: function() {
			s.on("click", function() {
				o.modal({
					backdrop: "static",
					keyborad: !1,
					focus: !0
				})
			}), a.on("click", function() {
				e.modal({
					backdrop: "static",
					keyborad: !1,
					focus: !0
				})
			})
		},
		i: function() {
			var s = 300,
				o = "...",
				a = "More...",
				e = "Less...";
			$(".more").each(function() {
				var e = $(this).html();
				e.length > s && (t = e.substr(0, s), h = e.substr(s, e.length - s), html = t + '<span class="moreellipses">' + o + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="javascript:void(0)" class="morelink_search">' + a + "</a></span>", $(this).html(html))
			}), $(".morelink_search").click(function() {
				return $(this).hasClass("less") ? ($(this).removeClass("less"), $(this).html(a)) : ($(this).addClass("less"), $(this).html(e)), $(this).parent().prev().toggle(), $(this).prev().toggle(), !1
			})
		}
	}, A.f(), A.i()
});
