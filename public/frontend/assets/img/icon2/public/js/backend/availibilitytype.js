$(document).ready(function () {
	var mode = $("#mode").val(); // fetch from dropdown
	
	if(mode=='add'){
		$("#frmavailibilitytype").validate({
			rules: {
				title: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				status: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				}
			},
			messages: {
				title: "Please enter title.",
				status: "Please select status.",
			},			
		});
	}

	if(mode == "edit"){
		$("#frmfacilities").validate({
			rules: {
				title: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				status: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				}
			},
			messages: {
				title: "Please enter title.",
				status: "Please select status.",
			},		
		});
	}
});

function deleteRecord(id){	
	if((id == "") || (id == undefined)){
		return;
	}
	if (confirm("Do you want to delete record!") == true) {
	    window.location = baseUrl+"/availibilitytype/delete/"+id;
	}
}
