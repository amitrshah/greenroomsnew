$(document).ready(function () {
	var mode = $("#mode").val(); // fetch from dropdown
	
	if(mode=='add'){
		$('.select2').select2({
			placeholder:"Select City"
		});

		$("#frmplan").validate({
			rules: {
				planName: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				"city[]": {
					required: true,
				},
				price: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				duration: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				favLimit: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				status: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				}
			},
			messages: {
				planName: "Please enter title.",
				"city[]": "Please select city.",
				favLimit: "Please enter favourite property limit.",
				duration: "Please enter duration.",
				price: "Please enter price.",
				status: "Please select status.",
			},
			/*submitHandler: function(form) {
				$(form).submit();
			}*/
		});
	}

	if(mode == "edit"){
		$('.select2').select2({
			placeholder:"Select City"
		});

		$("#frmplan").validate({
			rules: {
				planName: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				"city[]": {
					required: true,
				},
				price: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				duration: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				favLimit: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				status: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				}
			},
			messages: {
				planName: "Please enter title.",
				"city[]": "Please select city.",
				favLimit: "Please enter favourite property limit.",
				duration: "Please enter duration.",
				price: "Please enter price.",
				status: "Please select status.",
			},
			/*submitHandler: function(form) {
				$(form).submit();
			}*/
		});
	}
});

function deleteRecord(id){
	
	if((id == "") || (id == undefined)){
		return;
	}
	if (confirm("Do you want to delete record!") == true) {
	    window.location = baseUrl+"/plan/delete/"+id;
	}
}

function activatePlan(id,status_field){
	if((id == "") || (id == undefined)){
		return;
	}
	var msg = "";
	if(status_field == 0){
		msg = "Do you want to make plan inactive!";
	} else if(status_field == 1){
		msg = "Do you want to make plan active!";
	}
	if (confirm(msg) == true) {
		window.location = baseUrl+"/plan/update/"+id+'&'+status_field;
	}	
}

function performAllForPlan(){
	if($(".plan").length > 0) {
		if (confirm('Do you want to delete plans!') == true) {
			//var action = $('#action').val();
			var action = 'Delete';
			var users = [];
			$('.plan:checked').each(function(i,e) {
				users.push(e.value);
			});
			var totalCheckedUsers = users.join(',');
			
			$.ajax({
				url: baseUrl+'/plan/update/'+action,
				type: "post",
				data: "plan_id="+totalCheckedUsers,
				headers: {
				  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
				  "X-Requested-With":"XMLHttpRequest"
				},
				beforeSend: function()
				{
					$(".whole-page-overlay").show();
				},
			}).done(function(data){
				//$("#hiddenModel").trigger('click');
				$(".whole-page-overlay").hide();
				location.reload();
			}).fail(function(jqXHR, ajaxOptions, thrownError)
			{
				alert('No response from server.');
			});
		}
	} else {
		alert("Please select an action to perform");
	}
}
