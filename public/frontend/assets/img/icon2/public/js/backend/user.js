$(document).ready(function () {
	var mode = $("#mode").val(); // fetch from dropdown
	if(mode=='add'){
		$("#frmuser").validate({
			rules: {
				name: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				firstName: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				lastName: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				email: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				password:{
					required: true,
					minlength: 6
				},
				role:'required',
				phone:{
					number: true,
					rangelength:[10,10],
					normalizer: function(value) {
						return $.trim(value);
					},
				},
				mobile: {
					required: true,
					number: true,
					rangelength:[10,10],
					normalizer: function(value) {
						return $.trim(value);
					},
				},
				
			},
			messages: {
				firstName: "Please enter first name.",
				lastName: "Please enter last name.",
				name: "Please enter user name.",
				email:{
					required: "Please enter email.",
					email:"Please enter a valid email."
				},
				password:{
					required : "Please enter password.",
					minlength : "Please enter password minimum 6 characters."
				},
				role: "Please select role.",
				phone: {
					number : "Please Enter A Valid Number.",
					rangelength : "Please Enter A Value 10 digits Long."
				},
				mobile: {
					required : "Please enter mobile number.",
					number : "Please Enter A Valid Number.",
					rangelength : "Please Enter A Value 10 digits Long."
				},	
			},
			/*submitHandler: function(form) {
				
				$(form).submit();
			
			}*/


		});


	}
	if(mode == "edit"){
		$("#frmuser").validate({
			rules: {
				name: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				firstName: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				lastName: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				email: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				password:{
					minlength: 6
				},
				role:'required',
				phone:{
					number: true,
					rangelength:[10,10],
					normalizer: function(value) {
						return $.trim(value);
					},
				},
				mobile: {
					required: true,
					number: true,
					rangelength:[10,10],
					normalizer: function(value) {
						return $.trim(value);
					},
				},
			},
			messages: {
				firstName: "Please enter first name.",
				lastName: "Please enter last name.",
				name: "Please enter user name.",
				email:{
					required: "Please enter email.",
					email:"Please enter a valid email."
				},
				password:{
							minlength : "Please enter password minimum 6 characters."
						},
				role: "Please select role.",
				phone: {
							number : "Please Enter A Valid Number.",
							rangelength : "Please Enter A Value 10 digits Long."
						},
				mobile: {
							required : "Please enter mobile.",
							number : "Please Enter A Valid Number.",
							rangelength : "Please Enter A Value 10 digits Long."
						}

				
			},
			/*submitHandler: function(form) {
				
				$(form).submit();
			}*/
		});
	}
});

function deleteRecord(id){
	
	if((id == "") || (id == undefined)){
		return;
	}
	if (confirm("Do you want to delete record!") == true) {
	    window.location = baseUrl+"/user/delete/"+id;
	}
}

function activateRecord(id,status_field){
	if((id == "") || (id == undefined)){
		return;
	}
	var msg = "";
	if(status_field == 0){
		msg = "Do you want to make user inactive!";
	} else if(status_field == 1){
		msg = "Do you want to make user active!";
	}
	if (confirm(msg) == true) {
		window.location = baseUrl+"/user/update/"+id+'&'+status_field;
	}	
}

function performAll(){
	if($(".user").length > 0) {
		if (confirm('Do you want to delete users!') == true) {
			//var action = $('#action').val();
			var action = 'Delete';
			var users = [];
			$('.user:checked').each(function(i,e) {
				users.push(e.value);
			});
			var totalCheckedUsers = users.join(',');
			
			$.ajax({
				url: baseUrl+'/user/update/'+action,
				type: "post",
				data: "user_id="+totalCheckedUsers,
				headers: {
				  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
				  "X-Requested-With":"XMLHttpRequest"
				},
				beforeSend: function()
				{
					$(".whole-page-overlay").show();
				},
			}).done(function(data){
				//$("#hiddenModel").trigger('click');
				$(".whole-page-overlay").hide();
				location.reload();
			}).fail(function(jqXHR, ajaxOptions, thrownError)
			{
				alert('No response from server.');
			});
		}
	} else {
		alert("Please select an action to perform");
	}
}

function randomPassword(length) {
    var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP1234567890!@#$%^&*()_+-=";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}
function pass_generate() {
    $('#password').val(randomPassword(6));
}

function show_password() {
	if($('#password')[0].type === "text"){
		$('#password')[0].type = "password";
	} else {
		$('#password')[0].type = "text";
	}
}

function planDetailsByUserID(userID){
    $('#userID').val('');
    $('#upgradeValue').val('');
    $.ajax({
        url: baseUrl+'/user/edit/plan/upgrade/'+userID,
        type: "get",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
          "X-Requested-With":"XMLHttpRequest"
        },
        beforeSend: function()
        {
            //$(".cssload-loader").show();
        },
    }).done(function(data){
    	console.log(data.planDetails.name);
    	if(data.planDetails){
    		$("#userID").val(data.planDetails.userID);
    		$("#userName").text(data.planDetails.name);
    		$("#cityName").text(data.planDetails.cityName);
    		$("#membershipExpiry").text(data.planDetails.membershipExpiry);
    		$("#remainingFavCount").text(data.planDetails.remainingFavCount);
    		$("#totalFavCount").text(data.planDetails.totalFavCount);
    	}
    	
    	$("#hiddenModel").trigger('click');
    	
    }).fail(function(jqXHR, ajaxOptions, thrownError)
    {
        alert('No response from server.');
    });
}


function upgradeValue(){

    var userID = $('#userID').val();
    var upgradeValue = $('#upgradeValue').val();
    if((userID == "" || userID == undefined) || (upgradeValue == "" || upgradeValue == undefined)){
    	$("#modelMessage").text('Please enter upgrade favourite count.');
    	$("#modelMessage").css("display","block");
    	return false;
    }
    var data = {userID: userID, upgradeValue:upgradeValue}
    $.ajax({
        url: baseUrl+'/user/edit/plan/upgrade/update/',
        type: "post",
        data:data,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
          "X-Requested-With":"XMLHttpRequest"
        },
        beforeSend: function()
        {
            //$(".cssload-loader").show();
        },
    }).done(function(data){
    	location.reload();
    	
    }).fail(function(jqXHR, ajaxOptions, thrownError)
    {
        alert('No response from server.');
    });
}