$(document).ready(function () {
	var mode = $("#mode").val(); // fetch from dropdown
	
	if(mode=='add'){
		$("#frmresidencetype").validate({
			rules: {
				title: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				status: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				}
			},
			messages: {
				title: "Please enter title.",
				status: "Please select status.",
			},
			/*submitHandler: function(form) {
				$(form).submit();
			}*/


		});
	}

	if(mode == "edit"){
		$("#frmresidencetype").validate({
			rules: {
				title: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				status: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				}
			},
			messages: {
				title: "Please enter title.",
				status: "Please select status.",
			},
			/*submitHandler: function(form) {
				$(form).submit();
			}*/
		});
	}
});

function deleteRecord(id){
	
	if((id == "") || (id == undefined)){
		return;
	}
	if (confirm("Do you want to delete record!") == true) {
	    window.location = baseUrl+"/residencetype/delete/"+id;
	}
}
