$(document).ready(function () {
	var mode = $("#mode").val(); // fetch from dropdown
	
	if(mode=='add'){
		$('.select2').select2();

		$("#frmarea").validate({
			rules: {
				areaName: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				pincode: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},				
				city: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				state: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				status: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				}
			},
			messages: {
				areaName: "Please enter title.",
				pincode: "Please enter pincode.",
				city: "Please select city.",
				state: "Please select state.",
				status: "Please select status.",
			},
			/*submitHandler: function(form) {
				$(form).submit();
			}*/
		});
	}

	if(mode == "edit"){
		$('.select2').select2();
		
		$("#frmarea").validate({
			rules: {
				areaName: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				pincode: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},				
				city: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				state: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				status: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				}
			},
			messages: {
				areaName: "Please enter title.",
				pincode: "Please enter pincode.",
				city: "Please select city.",
				state: "Please select state.",
				status: "Please select status.",
			},
			/*submitHandler: function(form) {
				$(form).submit();
			}*/
		});
	}
});
function getCity(){
	$("#area").empty().append("<option value=''>Select Area</option>");
	var stateID = $('#state').val();

	if(stateID == null){
		return;
	}
	$.ajax({
		url: baseUrl+'/property/getallcity/'+stateID,
		type:'GET',
		success: function(result){

	        $("#city").empty().append(result);
	    }

	})
}
function deleteRecord(id){
	
	if((id == "") || (id == undefined)){
		return;
	}
	if (confirm("Do you want to delete record!") == true) {
	    window.location = baseUrl+"/area/delete/"+id;
	}
}
