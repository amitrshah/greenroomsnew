


$(document).ready(function () {
	var mode = $("#mode").val();
	if(mode == "profile"){
		$("#frmprofile").validate({
			rules: {
				name: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				firstName: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				
			},
			messages: {
				firstName: "Please enter first name.",
				name: "Please enter user name.",
			},
			submitHandler: function(form) {
				
				$(form).submit();
			}
		});
	}

	if(mode == "changepassword"){
		$("#frmchangepassword").validate({
			rules: {
				oldPassword: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				password: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				confirmPassword: {
					required: true,
					equalTo : "#password",
					normalizer: function(value) {
						return $.trim(value);
					}
				},			
			},
			messages: {
				oldPassword: "Please enter old password.",
				confirmPassword: "Please enter confirmPassword.",
				password:{
							required : 'Please enter password',
							minlength : "Please enter password minimum 6 characters."
						},
			},
			submitHandler: function(form) {
				
				$(form).submit();
			}
		});
	}
});


function randomPassword(length) {
    var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP1234567890!@#$%^&*()_+-=";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}
function pass_generate() {
    $('#password').val(randomPassword(6));
}
