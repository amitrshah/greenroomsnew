jQuery(document).ready(function ($) {
    $("[data-toggle=popover]").popover({
        html: true,
        content: function () {
            return $('#popover-content').html();
        }
    });
    var mySlider = $("#ex2").slider({
        tooltip: 'always',
        ticks_tooltip: true,
        tooltip_position: 'top'
    });
    var value = mySlider.slider('getValue');
    var mySlider = $("#ex3").slider({
        tooltip: 'always',
        ticks_tooltip: true,
        tooltip_position: 'top'
    });
    console.log(value);
    $('.locality-multiple').select2({
        maximumSelectionLength: 6,
        data: localityArray,
        placeholder: "Select a locality",
        allowClear: true,
        selectOnClose: true
    });
    if ($('#fname').val() == '') {
        $('.needs-validation').addClass('was-validated');
        $('#fname').focus();
        return false;
    } else if ($('#contactnumber').val() == '') {
        $('.needs-validation').addClass('was-validated');
        $('#contactnumber').focus();
        return false;
    } else if ($('#alternatecontactnumber').val() == '') {
        $('.needs-validation').addClass('was-validated');
        $('#alternatecontactnumber').focus();
        return false;
    } else if ($('#firmname').val() == '') {
        $('.needs-validation').addClass('was-validated');
        $('#firmname').focus();
        return false;
    } else if ($('#emailid').val() == '') {
        $('.needs-validation').addClass('was-validated');
        $('#emailid').focus();
        return false;
    } else if ($('#password').val() == '') {
        $('.needs-validation').addClass('was-validated');
        $('#password').focus();
        return false;
    } else if ($('#state').val() == '') {
        $('.needs-validation').addClass('was-validated');
        $('#state').focus();
        return false;
    } else if ($('#city').val() == '') {
        $('.needs-validation').addClass('was-validated');
        $('#city').focus();
        return false;
    }
});