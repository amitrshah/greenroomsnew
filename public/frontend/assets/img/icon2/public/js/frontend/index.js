jQuery(document).ready(function ($) {
    var bnrHight = $('#slideshow img').height() + 'px';
    $('#slideshow').height(bnrHight) + 'px';
    $('.greenroom-banner .content').css('position', 'absolute').css('top', '0').css('z-index', 'z-index').css('left', '0').css('right', '0').css('margin', '0 auto').css('color', '#fff').css('display', 'flex').css('align-items', 'center').css('justify-content', 'center').css('height', bnrHight);
    console.log(bnrHight);

    if (window.matchMedia(" screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : portrait) ").matches) {
        /* the viewport is at least 400 pixels wide */
        var bnrHight = $('#slideshow img').height() + 'px';
        $('#slideshow').hide();
        $('.greenroom-banner').css('height', '400px').css('background', '#ddefe4');
        $('.greenroom-banner .content').css('position', 'absolute').css('top', '0').css('z-index', 'z-index').css('left', '0').css('right', '0').css('margin', '0 auto').css('color', '#fff').css('display', 'flex').css('align-items', 'center').css('justify-content', 'center').css('height', 'inherit');
        console.log(bnrHight);
    } else {
        /* the viewport is less than 400 pixels wide */
    }
    if (window.matchMedia(" screen and (max-width : 414px) and (orientation : portrait) ").matches) {
        /* the viewport is at least 400 pixels wide */
        var bnrHight = $('#slideshow img').height() + 'px';
        $('#slideshow').hide();
        $('.greenroom-banner').css('height', '400px').css('background', '#ddefe4');
        $('.greenroom-banner .content').css('position', 'absolute').css('top', '0').css('z-index', 'z-index').css('left', '0').css('right', '0').css('margin', '0 auto').css('color', '#fff').css('display', 'flex').css('align-items', 'center').css('justify-content', 'center').css('height', 'inherit');
        console.log(bnrHight);
    } else {
        /* the viewport is less than 400 pixels wide */
    }
    // image fade slide show start
    $("#slideshow > div:gt(0)").hide();
    setInterval(function () {
        $('#slideshow > div:first')
            .fadeOut(1000)
            .next()
            .fadeIn(1000)
            .end()
            .appendTo('#slideshow');
    }, 6000);
    $('.js-example-basic-multiple').select2({
        maximumSelectionLength: 3,
        data: localityArray,
        placeholder: "Select a locality",
        allowClear: true,
        selectOnClose: true
    });
    $('#CityName').select2({
        data: CityNameArray,
        selectOnClose: true
    });
    var text = "";
    var totalLength = $('.titleBox .title').text().length;
    console.log(totalLength)
    var i;
    for (i = 1; i <= totalLength; i++) {
        $('.titleBox .lines').append('<hr class="hr">');
    }
    // Parallax image start
    $.fn.parallaxify = function () {
        return this.each(function () {
            var $element = $(this);

            function percentageSeen() {
                var viewportHeight = $(window).height(),
                    winScrollTop = $(window).scrollTop(),
                    elementOffsetTop = $element.offset().top,
                    elementHeight = $element.height();

                var distance = (winScrollTop + viewportHeight) - elementOffsetTop;
                var percentage = distance / ((viewportHeight + elementHeight) / 100);

                if (percentage < 0) return 0;
                else if (percentage > 100) return 100;
                else return percentage;
            }
            $element.css({ 'background-position-y': percentageSeen() + '%' });
            $(window).on('scroll', function () {
                $element.css({ 'background-position-y': percentageSeen() + '%' });
            });
        });
    };
    $('.parallax').parallaxify();
    $('#responsive').lightSlider({
        item: 4,
        loop: false,
        slideMargin: 9,
        slideMove: 2,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed: 600,
        responsive: [{
            breakpoint: 800,
            settings: {
                item: 3,
                slideMove: 1,
                slideMargin: 6,
            }
        },
        {
            breakpoint: 480,
            settings: {
                item: 1,
                slideMove: 1
            }
        }
        ]
    });
    $('#propertyAvailability').lightSlider({
        item: 4,
        loop: false,
        slideMove: 2,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed: 600,
        responsive: [{
            breakpoint: 800,
            settings: {
                item: 3,
                slideMove: 1,
                slideMargin: 6,
            }
        },
        {
            breakpoint: 480,
            settings: {
                item: 1,
                slideMove: 1
            }
        }
        ]
    });
});