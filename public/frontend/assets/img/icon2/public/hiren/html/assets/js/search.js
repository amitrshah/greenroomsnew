jQuery(document).ready(function ($) {
    var mySlider = $("#ex2").slider({
        tooltip: 'always',
        ticks_tooltip: true,
        tooltip_position: 'top'
    });
    var value = mySlider.slider('getValue');
    console.log(value);
    $('.locality-multiple').select2({
        maximumSelectionLength: 6,
        data: localityArray,
        placeholder: "Select a locality",
        allowClear: true,
        selectOnClose: true
    });
});