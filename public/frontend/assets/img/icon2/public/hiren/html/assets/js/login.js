jQuery(document).ready(function ($) {
    $('.signUp').hide();
    $('#create_account').on('click', function () {
        $('.signin').hide();
        $('.signUp').show();
    })

    $('#sign_in').on('click', function () {
        $('.signin').show();
        $('.signUp').hide();
    })
});