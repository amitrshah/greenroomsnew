jQuery(document).ready(function ($) {
    $('#accommodation').multiselect();
    $('#rootwizard').bootstrapWizard({
        onTabShow: function (tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index + 1;
            var $percent = ($current / $total) * 100;
            $('#rootwizard .progress-bar').css({ width: $percent + '%' });
            $('.submitbtn').addClass('d-none');
            if (index == 2) {
                $('.submitbtn').removeClass('d-none');
                $('.next').addClass('d-none');
            }
        },
        onTabClick: function (tab, navigation, index) {
            return false;
        },
        onNext: function (tab, navigation, index) {
            // 
            if (index == 1) {
                // Make sure we entered the name
                if ($('#ownername').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#ownername').focus();
                    return false;
                } else if ($('#ownernumber').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#ownernumber').focus();
                    return false;
                } else if ($('#address').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#address').focus();
                    return false;
                } else if ($('#landmark').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#landmark').focus();
                    return false;
                } else if ($('#gender').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#gender').focus();
                    return false;
                } else if ($('#accommodation').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#accommodation').focus();
                    return false;
                } else if ($('#property_availability').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#property_availability').focus();
                    return false;
                } else if ($('#property_type').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#property_type').focus();
                    return false;
                } else if ($('#state').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#state').focus();
                    return false;
                } else if ($('#city').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#city').focus();
                    return false;
                }
            }
            if (index == 2) {
                // Make sure we entered the name
                if ($('#locality').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#locality').focus();
                    return false;
                } else if ($('#sharing').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#sharing').focus();
                    return false;
                } else if ($('#property_condition').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#property_condition').focus();
                    return false;
                } else if ($('#property_amenities').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#property_amenities').focus();
                    return false;
                } else if ($('#no_of_rooms').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#no_of_rooms').focus();
                    return false;
                } else if ($('#facing_direction').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#facing_direction').focus();
                    return false;
                } else if ($('#how_old_building').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#how_old_building').focus();
                    return false;
                } else if ($('#floor').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#floor').focus();
                    return false;
                } else if ($('#sq_feet').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#sq_feet').focus();
                    return false;
                } else if ($('#rent').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#rent').focus();
                    return false;
                }
            }
            if (index == 3) {
                // Make sure we entered the name
                if ($('#deposit').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#deposit').focus();
                    return false;
                } else if ($('#extra_charges').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#extra_charges').focus();
                    return false;
                } else if ($('#meals').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#meals').focus();
                    return false;
                } else if ($('#facilities').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#facilities').focus();
                    return false;
                } else if ($('#society_amenities').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#society_amenities').focus();
                    return false;
                } else if ($('#rules').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#rules').focus();
                    return false;
                } else if ($('#notice_period').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#notice_period').focus();
                    return false;
                } else if ($('#upload').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#upload').focus();
                    return false;
                } else if ($('#no_of_unit').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#no_of_unit').focus();
                    return false;
                } else if ($('#description').val() == '') {
                    $('.needs-validation').addClass('was-validated');
                    $('#description').focus();
                    return false;
                }
            }
        },
        onPrevious: function (tab, navigation, index) {
            if (index == 1) {
                $('.submitbtn').addClass('d-none');
                $('.next').removeClass('d-none');
            }
        }
    });
});