$(document).ready(function () {
	var mode = $("#mode").val(); // fetch from dropdown
	if(mode=='add'){
		$("#frmmemberrequirement").validate({
			rules: {
				roleName: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				description: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				}
			},
			messages: {
				roleName: "Please enter role name.",
				description: "Please enter description.",
			},
			/*submitHandler: function(form) {
				$(form).submit();
			}*/
		});
	}

	if(mode == "edit"){
		$("#frmmemberrequirement").validate({
			rules: {
				roleName: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				description: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				}
			},
			messages: {
				roleName: "Please enter role name.",
				description: "Please enter description.",
			},
			/*submitHandler: function(form) {
				$(form).submit();
			}*/
		});
	}
});

function deleteRecord(id){
	
	if((id == "") || (id == undefined)){
		return;
	}
	if (confirm("Do you want to delete record!") == true) {
	    window.location = baseUrl+"/memberrequirement/delete/"+id;
	}
}