function deleteRecord(id){
	
	if((id == "") || (id == undefined)){
		return;
	}
	if (confirm("Do you want to delete record!") == true) {
	    window.location = baseUrl+"/payment/delete/"+id;
	}
}

function activateRecord(id,status_field){
	if((id == "") || (id == undefined)){
		return;
	}
	var msg = "";
	if(status_field == 0){
		msg = "Do you want to make user inactive!";
	} else if(status_field == 1){
		msg = "Do you want to make user active!";
	}
	if (confirm(msg) == true) {
		window.location = baseUrl+"/user/update/"+id+'&'+status_field;
	}	
}

function deleteAllPayment(){
	if($(".payment").length > 0) {
		if (confirm('Do you want to delete payments!') == true) {
			//var action = $('#action').val();
			var action = 'Delete';
			var users = [];
			$('.user:checked').each(function(i,e) {
				users.push(e.value);
			});
			var totalCheckedUsers = users.join(',');
			
			$.ajax({
				url: baseUrl+'/memberpayment/update/'+action,
				type: "post",
				data: "payment_id="+totalCheckedUsers,
				headers: {
				  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
				  "X-Requested-With":"XMLHttpRequest"
				},
				beforeSend: function()
				{
					$(".whole-page-overlay").show();
				},
			}).done(function(data){
				//$("#hiddenModel").trigger('click');
				$(".whole-page-overlay").hide();
				location.reload();
			}).fail(function(jqXHR, ajaxOptions, thrownError)
			{
				alert('No response from server.');
			});
		}
	} else {
		alert("Please select an action to perform");
	}
}

function randomPassword(length) {
    var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP1234567890!@#$%^&*()_+-=";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}
function pass_generate() {
    $('#password').val(randomPassword(6));
}


function planDetailsByUserID(userID){
    $('#userID').val('');
    $('#upgradeValue').val('');
    $.ajax({
        url: baseUrl+'/user/edit/plan/upgrade/'+userID,
        type: "get",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
          "X-Requested-With":"XMLHttpRequest"
        },
        beforeSend: function()
        {
            //$(".cssload-loader").show();
        },
    }).done(function(data){
    	console.log(data.planDetails.name);
    	if(data.planDetails){
    		$("#userID").val(data.planDetails.userID);
    		$("#userName").text(data.planDetails.name);
    		$("#cityName").text(data.planDetails.cityName);
    		$("#membershipExpiry").text(data.planDetails.membershipExpiry);
    		$("#remainingFavCount").text(data.planDetails.remainingFavCount);
    		$("#totalFavCount").text(data.planDetails.totalFavCount);
    	}
    	
    	$("#hiddenModel").trigger('click');
    	
    }).fail(function(jqXHR, ajaxOptions, thrownError)
    {
        alert('No response from server.');
    });
}


function upgradeValue(){

    var userID = $('#userID').val();
    var upgradeValue = $('#upgradeValue').val();
    if((userID == "" || userID == undefined) || (upgradeValue == "" || upgradeValue == undefined)){
    	$("#modelMessage").text('Please enter upgrade favourite count.');
    	$("#modelMessage").css("display","block");
    	return false;
    }
    var data = {userID: userID, upgradeValue:upgradeValue}
    $.ajax({
        url: baseUrl+'/user/edit/plan/upgrade/update/',
        type: "post",
        data:data,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
          "X-Requested-With":"XMLHttpRequest"
        },
        beforeSend: function()
        {
            //$(".cssload-loader").show();
        },
    }).done(function(data){
    	location.reload();
    	
    }).fail(function(jqXHR, ajaxOptions, thrownError)
    {
        alert('No response from server.');
    });
}