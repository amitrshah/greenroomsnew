$(document).ready(function () {
	var mode = $("#mode").val(); // fetch from dropdown
	
	if(mode=='add'){
		$('.select2').select2();

		$("#frmcity").validate({
			rules: {
				cityName: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				state: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				status: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				}
			},
			messages: {
				cityName: "Please enter title.",
				state: "Please select state.",
				status: "Please select status.",
			},
			/*submitHandler: function(form) {
				$(form).submit();
			}*/
		});
	}

	if(mode == "edit"){
		$('.select2').select2();

		$("#frmcity").validate({
			rules: {
				cityName: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				state: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				status: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				}
			},
			messages: {
				cityName: "Please enter title.",
				state: "Please select state.",
				status: "Please select status.",
			},
			/*submitHandler: function(form) {
				$(form).submit();
			}*/
		});
	}
});

function deleteRecord(id){
	
	if((id == "") || (id == undefined)){
		return;
	}
	if (confirm("Do you want to delete record!") == true) {
	    window.location = baseUrl+"/city/delete/"+id;
	}
}
