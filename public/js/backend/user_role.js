$(document).ready(function () {
	var mode = $("#mode").val(); // fetch from dropdown
	if(mode=='add'){
		$("#frmuserrole").validate({
			rules: {
				roleName: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				description: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				}
			},
			messages: {
				roleName: "Please enter role name.",
				description: "Please enter description.",
			},
			/*submitHandler: function(form) {
				$(form).submit();
			}*/
		});
	}

	if(mode == "edit"){
		$("#frmuserrole").validate({
			rules: {
				roleName: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				description: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				}
			},
			messages: {
				roleName: "Please enter role name.",
				description: "Please enter description.",
			},
			/*submitHandler: function(form) {
				$(form).submit();
			}*/
		});
	}
});

function deleteRecord(id){
	
	if((id == "") || (id == undefined)){
		return;
	}
	if (confirm("Do you want to delete record!") == true) {
	    window.location = baseUrl+"/userrole/delete/"+id;
	}
}

function checkcolall(action_id){

	if(action_id == 1){
		if($('input[name="chkcolall_1"]').is(':checked')){
			$("input[name='List[]'").each(function() {
				this.checked = true;
			});
		}
		if($('input[name="chkcolall_1"]').is(':checked') == false){
			$("input[name='List[]'").each(function() {
				this.checked = false;
			});
		}
	}

	if(action_id == 2){
		if($('input[name="chkcolall_2"]').is(':checked')){
			$("input[name='Add[]'").each(function() {
				this.checked = true;
			});
		}
		if($('input[name="chkcolall_2"]').is(':checked') == false){
			$("input[name='Add[]'").each(function() {
				this.checked = false;
			});
		}
	}

	if(action_id == 3){

		if($('input[name="chkcolall_3"]').is(':checked')){
			$("input[name='Edit[]'").each(function() {
				this.checked = true;
			});
		}
		if($('input[name="chkcolall_3"]').is(':checked') == false){
			$("input[name='Edit[]'").each(function() {
				this.checked = false;
			});
		}
	}
	if(action_id == 4){

		if($('input[name="chkcolall_4"]').is(':checked')){
			$("input[name='Delete[]'").each(function() {
				this.checked = true;
			});
		}
		if($('input[name="chkcolall_4"]').is(':checked') == false){
			$("input[name='Delete[]'").each(function() {
				this.checked = false;
			});
		}
	}
}