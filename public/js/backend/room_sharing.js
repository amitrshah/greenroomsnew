$(document).ready(function () {
	var mode = $("#mode").val(); // fetch from dropdown
	if(mode=='add'){
		$("#frmroomsharing").validate({
			rules: {
				title: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				maximum_beds: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				}
			},
			messages: {
				title: "Please enter title.",
				maximum_beds: "Please enter Maximum Beds.",
			},
			/*submitHandler: function(form) {
				$(form).submit();*/
			/*angular.element('.preloader').fadeIn();

				var title = $("#title").val(); // fetch from dropdown
				var maximum_beds = $("#maximum_beds").val(); // fetch from dropdown
				var status = $("#status").val(); // fetch from dropdown
				
				$.ajax({
			        url: baseUrl+"/roomsharing/add",
			        type: "POST",
			        data: {
				 		title : title,
				 		maximum_beds : maximum_beds,
				 		status : status,
			        },
			        success: function(data) {
			        	if(data.id){
			        		window.location = baseUrl+"/roomsharing";
			        	}
			        	if(data.errors){
			        		angular.element('.preloader').fadeOut();
			        		$.each(data.errors, function (key, val) {
						        $("#"+key).css("display","block");
						        $("#"+key).html(val);

						    });
			        	}
			        }
			    });*/
			/*}*/


		});
	}

	if(mode == "edit"){
		$("#frmroomsharing").validate({
			rules: {
				title: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				maximum_beds: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				}
			},
			messages: {
				title: "Please enter title.",
				maximum_beds: "Please enter Maximum Beds.",
			},
			/*submitHandler: function(form) {
				$(form).submit();*/
				/*console.log($('#frmroomsharing').serialize());
				var title = $("#title").val(); // fetch from dropdown
				var maximum_beds = $("#maximum_beds").val(); // fetch from dropdown
				var status = $("#status").val(); // fetch from dropdown
				var id = $("#id").val(); // fetch from dropdown
				
				$.ajax({
			        url: baseUrl+"/roomsharing/update",
			        type: "POST",
			        data: {
			        	id:id,
				 		title : title,
				 		maximum_beds : maximum_beds,
				 		status : status,
			        },
			        success: function(data) {
			        	
			        	if(data.errors){
			        		$.each(data.errors, function (key, val) {
						        $("#"+key).css("display","block");
						        $("#"+key).html(val);

						    });
			        	}else{
			        		
			        		window.location = baseUrl+"/roomsharing/edit/"+id;
			        	}
			        }
			    });*/
			/*}*/
		});
	}
});
//function saveRoomSharing(){
	
	//console.log($('#frmroomsharing').serialize());
	/*var title = $("#title").val(); // fetch from dropdown
	var maximum_beds = $("#maximum_beds").val(); // fetch from dropdown
	var status = $("#status").val(); // fetch from dropdown
	
	$.ajax({
        url: baseUrl+"/roomsharing/add",
        type: "POST",
        data: {
	 		title : title,
	 		maximum_beds : maximum_beds,
	 		status : status,
        },
        success: function(data) {
        	if(data.id){
        		window.location = baseUrl+"/roomsharing";
        	}
        	if(data.errors){
        		$.each(data.errors, function (key, val) {
			        $("#"+key).css("display","block");
			        $("#"+key).html(val);

			    });
        	}
        }
    });*/
//}


/*function updateRoomSharing(){
	
	console.log($('#frmroomsharing').serialize());
	var title = $("#title").val(); // fetch from dropdown
	var maximum_beds = $("#maximum_beds").val(); // fetch from dropdown
	var status = $("#status").val(); // fetch from dropdown
	var id = $("#id").val(); // fetch from dropdown
	
	$.ajax({
        url: baseUrl+"/roomsharing/update",
        type: "POST",
        data: {
        	id:id,
	 		title : title,
	 		maximum_beds : maximum_beds,
	 		status : status,
        },
        success: function(data) {
        	
        	if(data.errors){
        		$.each(data.errors, function (key, val) {
			        $("#"+key).css("display","block");
			        $("#"+key).html(val);

			    });
        	}else{
        		
        		window.location = baseUrl+"/roomsharing/edit/"+id;
        	}
        }
    });
}*/

function deleteRecord(id){
	
	if((id == "") || (id == undefined)){
		return;
	}
	if (confirm("Do you want to delete record!") == true) {
	    window.location = baseUrl+"/roomsharing/delete/"+id;
	}
}
