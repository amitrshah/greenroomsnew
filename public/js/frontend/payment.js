$(document).ready(function() {
    $('.plan-city').select2();
});

function getPlansByCityID() {

    var city = $("#city").val();
    $("#plan_details").hide();
    $.ajax({
        url: baseUrl+'/getplans',
        type: "POST",
        datatype: "html",
        data:{city:city},
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
          "X-Requested-With":"XMLHttpRequest"
        },
        beforeSend: function()
        {
            $(".cssload-loader").show();
        },
    }).done(function(result){
    	$("#plan").empty();
    	$('#plan').append($('<option>').text('Select Plan').attr('value', null));
    	$.each(result, function(i, value) {
            $('#plan').append($('<option>').text(value).attr('value', i));
        });
        $(".cssload-loader").hide();

    }).fail(function(jqXHR, ajaxOptions, thrownError)
    {
        $("#myModalMessage").text('No response from server.');
        $("#hiddenModel").trigger('click');
    });
}

function getPlansDatailsByPlanID() {
	$("#plan_details").hide();
	var plan = $("#plan").val();
	var city = $("#city").val();
    
    $.ajax({
        url: baseUrl+'/getplandetails',
        type: "POST",
        datatype: "html",
        data:{plan:plan,city:city},
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
          "X-Requested-With":"XMLHttpRequest"
        },
        beforeSend: function()
        {
            $(".cssload-loader").show();
        },
    }).done(function(result){
        $("#plan_details").empty().html(result);
        $("#plan_details").show();
    	$("#cityID").val($("#city").val());
    	$("#planID").val($("#plan").val());

    	$(".cssload-loader").hide();

        	//initialize(data);
        //location.hash = page;
    }).fail(function(jqXHR, ajaxOptions, thrownError)
    {
        $("#myModalMessage").text('No response from server.');
        $("#hiddenModel").trigger('click');
    });
}
