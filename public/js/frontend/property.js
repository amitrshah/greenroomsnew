$(document).ready(function () {
	/*$(".upload_img").on("change",function() {
	  readURL(this);
	});*/
	var mode = $("#mode").val(); // fetch from dropdown
	if( mode == 'edit' || mode == 'add'){
		$('.select2').select2();
		$('.select2Meals').select2({placeholder: "Select Meals"});  
		get_map(); // load map     
	}
	if(mode=='add'){
		$("#frmproperty")
        
        // Revalidate your field when it is changed
        
        .validate({
        	errorClass: 'error',
            errorElement: 'label',
            errorPlacement: function(error, e) {
                e.parents('.form-group').append(error);
            },
			rules: {
				title: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				companyName: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				propertyType: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},				
				state: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				condition: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				city: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				area: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				address: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				companyNumber: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				landmark: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				mealsIncludedID: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				availability: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
                                sharingID: {
                                    required: true,
                                    normalizer: function(value) {
                                        return $.trim(value);
                                    }
				},
                                unitavailableID: {
                                    required: true,
                                    normalizer: function(value) {
                                        return $.trim(value);
                                    }
				},
                                rent:{
                                    required: true,
                                    normalizer: function(value) {
                                        return $.trim(value);
                                    }
                                }
			},
			messages: {
				title: "Please enter title",
				companyName: "Please enter company name",		
				propertyType: "Please select property type",		
				condition: "Please select condition",		
				state: "Please select state",		
				city: "Please select city type",		
				area: "Please select area type",		
				address: "Please enter address",		
				landmark: "Please enter landmark",		
				companyNumber: "Please enter company number",	
				mealsIncludedID: "Please select meals",
                                availability: "Please select availability",
                                sharingID: "Please select sharing",
                                unitavailableID:"Please select unit available",
                                rent:"Please enter rent",
			},
			/*submitHandler: function(form) {
				
				$(".new_room_sharing_table_row").each(function() {
				    //var ids = $(this).attr('id');
					//var array_splited_id = ids.split("_");
					//var obj = {};
					//console.log(array_splited_id);
					
					//price = $("#new_"+array_splited_id[1]+"_rs_roomSharing").val();
					//availability = $("#new_"+array_splited_id[1]+"_rs_availability").val();
					//roomSharingID = $("#new_"+array_splited_id[1]+"_rs_price").val();
					
				});
				$(form).submit();
			}*/
		});
	}

	if(mode == 'edit'){
		getMapLocation();
		$("#frmproperty").find('.select2-select')
        
        // Revalidate your field when it is changed
        
        .end().validate({
        	errorClass: 'error',
            errorElement: 'label',
            errorPlacement: function(error, e) {
                e.parents('.form-group').append(error);
            },
            
			rules: {
				title: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				companyName: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				propertyType: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				state: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				condition: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				city: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				area: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				address: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				companyNumber: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				landmark: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				mealsIncludedID: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				},
				sharingID:{
                                    required: true,
                                    normalizer: function(value) {
                                            return $.trim(value);
                                    }
                                }
			},
			messages: {
				title: "Please enter title",
				companyName: "Please enter company name",		
				propertyType: "Please select property type",		
				condition: "Please select condition",		
				state: "Please select state",		
				city: "Please select city type",		
				area: "Please select area type",		
				address: "Please enter address type",		
				landmark: "Please enter landmark",		
				companyNumber: "Please enter company number",	
				mealsIncludedID: "Please select meals",	
                                mealsIncludedID: "Please select meals",
			},
			//submitHandler: function(form) {
				
				/*$(".new_room_sharing_table_row").each(function() {
				    var ids = $(this).attr('id');
					var array_splited_id = ids.split("_");
					var obj = {};
					console.log(array_splited_id);
					
					price = $("#new_"+array_splited_id[1]+"_rs_roomSharing").val();
					availability = $("#new_"+array_splited_id[1]+"_rs_availability").val();
					roomSharingID = $("#new_"+array_splited_id[1]+"_rs_price").val();
					
				});*/
					
				//$(form).submit();
			//}
		});
	}
});

function getMapLocation(){

	//var address = $("#address").val();
        var landmark = $("#landmark").val();
	var areaID = $("#area").val();
	var cityID = $("#city").val();
	var cityName = $("#city option:selected").val();
	var stateName = $("#state option:selected").text();
	
    if(landmark != undefined){ 

        var geocoder = new google.maps.Geocoder();
       
        //geocoder.geocode({address: address+',' + cityName+', '+stateName},
        geocoder.geocode({address: landmark+',' + cityName+', '+stateName},        
        function(results_array, status) {            
            if(results_array.length > 0){
                var lat = results_array[0].geometry.location.lat()
                var lng = results_array[0].geometry.location.lng()
                var address = results_array[0].formatted_address;               
                get_map(lat,lng, address);
				$("#latitude").val(lat);
				$("#longitude").val(lng);

            }
        });
    }
}
function get_map(latitude='28.7041',longitude='77.1025', address){

    //console.log(latitude+"====="+longitude);

    var markers = [{
        "lat": latitude,
        "lng": longitude,
        "address": address
    }];

    var mapOptions = {
        center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
        zoom: 20,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var infoWindow = new google.maps.InfoWindow();
    var latlngbounds = new google.maps.LatLngBounds();
    var geocoder = geocoder = new google.maps.Geocoder();

    var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
    var data = markers[0];
    var myLatlng = new google.maps.LatLng(data.lat, data.lng);
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: data.title,
        //draggable: true,
        draggable: false,
        animation: google.maps.Animation.DROP
    });
    (function (marker, data) {
        google.maps.event.addListener(marker, "click", function (e) {
            // console.log(data)
            infoWindow.setContent(data.address);
            infoWindow.open(map, marker);
        });

        google.maps.event.addListener(marker, "dragend", function (e) {
            var lat, lng, address;
            geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    lat = marker.getPosition().lat();
                    lng = marker.getPosition().lng();
                    address = results[0].formatted_address;
                    $("#latitude").val(lat);
                    $("#longitude").val(lng);
                }
            });
        });
    })(marker, data);

    latlngbounds.extend(marker.position);

    var bounds = new google.maps.LatLngBounds();
    map.setCenter(latlngbounds.getCenter());
    map.fitBounds(latlngbounds);
}

function getCity(){
   
	$("#area").empty().append("<option value=''>Select Area</option>");
	var stateID = $('#state').val();
	/*var cityID = $('#city').val();*/

	if(stateID == null){
		return;
	}
	$.ajax({
		url: baseUrl+'/property/getallcity/'+stateID,
		type:'GET',
		success: function(result){

	        $("#city").empty().append(result);
	    }

	})
}

function getArea(){
	var stateID = $('#state').val();
	var cityID = $('#city').val();
	//var areaID = $('#area').val();
	if(stateID == null || cityID == null){
		return;
	}
	$.ajax({
		url: baseUrl+'/property/getallarea/'+stateID+"/"+cityID,//?stateID='+stateID+"&cityID="+cityID,
		type:'GET',
		//data:{stateID: stateID,cityID:cityID/*,areaID:areaID*/},
		success: function(result){

	        $("#area").empty().append(result);
	    }

	})
}

function deleteRecord(id){
	
	if((id == "") || (id == undefined)){
		return;
	}
	if (confirm("Do you want to delete record!") == true) {
	    window.location = baseUrl+"/property/delete/"+id;
	}
}

id_count = 1;
function addNew(){
	var error_count = 0;
	/*$(".new_room_sharing_table_row").find("input,select").each(function() {
			if($(this).attr('type') != "checkbox"){
			    if($(this).val()=="" || $(this).val()==undefined){
			    	$('#erros_roomsharing').css('display','block');
			    	//$(this).after("<span class='error' for='"+$(this).attr('id')+"'>This field is required.</span>");
			    	error_count+=1;
			    }
			}
	});
	if(error_count > 0){
		//alert(error_count)
		return false;
	}*/
	//console.log(last_room);
	//var length_ = $(".new_room_sharing_table_row").length;
		//$('.select2').select2();
		$('#erros_roomsharing').css('display','none');

		$(".new_room_sharing_table_row:last").clone().find("input,select").each(function() {
			$(this).val('');
			
			var slp_id = "";
		    $(this).attr({
		      	'id': function(_, id) { 
					slp_id = id.split('_');

			      	return "new_"+id_count+"_"+slp_id[2] + "_" + slp_id[3];
		      	},
		      	'name': function(_, name) {
			      	if(slp_id[3] == 'file'){
			      		return "sharing["+id_count+"]["+slp_id[2]+"]["+slp_id[3]+"][]" 
			      	}else{
			      		return "sharing["+id_count+"]["+slp_id[2]+"]["+slp_id[3]+"]" 

			      	}
		      	},
		                 
		      
		    });

		}).end().appendTo("#room_sharing_main_table");
		$(".new_room_sharing_table_row:last").attr('id','new_'+id_count+'_room_sharing_table_row');
		$(".new_room_sharing_table_row:last div .propertyPhoto").parent().remove();
		$(".new_room_sharing_table_row:last input[type=hidden]").remove();
		$(".new_room_sharing_table_row:last label.error").remove();
		$(".new_room_sharing_table_row:last").find("input").each(function() {
			if($(this).attr('type')){
				$(this).removeAttr('checked');
			}
			
		});
	  id_count++;
}

function deleteRow(e)
{
	$('#erros_roomsharing').css('display','none');
	var rows = $(".new_room_sharing_table_row").length;
		if(rows==1){
			
			$(".new_room_sharing_table_row:last").clone().find("input,select").each(function() {
				$(this).val('');
				
				var slp_id = "";
			    $(this).attr({
			      'id': function(_, id) { 
						slp_id = id.split('_');

				      	return "new_"+id_count+"_"+slp_id[2] + "_" + slp_id[3];
			      		},
			      'name': function(_, name) {
			      	if(slp_id[3] == 'file')
			      	{

			      				 return "sharing["+id_count+"]["+slp_id[2]+"]["+slp_id[3]+"][]" 
			      	}else{
			      				 return "sharing["+id_count+"]["+slp_id[2]+"]["+slp_id[3]+"]" 

			      	}
			      				},
			                 
			      
			    });

			}).end().appendTo("#room_sharing_main_table");
			$(".new_room_sharing_table_row:last").attr('id','new_'+id_count+'_room_sharing_table_row');
			$(".photoslabel").remove();
			$(".new_room_sharing_table_row:last div .propertyPhoto").parent().remove();
			$(".new_room_sharing_table_row:last input[type=hidden]").remove();
			$(".new_room_sharing_table_row:last label.error").remove();
			$(".new_room_sharing_table_row:last").find("input").each(function() {
				if($(this).attr('type')){
					$(this).removeAttr('checked');
				}
				
			});
		  id_count++;
		}
	$(e).parents("tr").remove();
   //$("#"+rowID).remove();

}

function randomPassword(length) {
    var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP1234567890!@#$%^&*()_+-=";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}
function pass_generate() {
    $('#password').val(randomPassword(6));
}

var deletePhotosArr = [];
function deletePhotosFunction(e){
	var photoID = $(e).attr('id');
	var array_splited_id = photoID.split("_");
	if(array_splited_id[2] != ""){
		$.ajax({
    		url: baseUrl+'/property/deleteImage/'+array_splited_id[2],
    		type:'GET',
    		success: function(result){
                deletePhotosArr.push(array_splited_id[2]);
    	    }
    	})
	}
	$("#deletePhotos").val(deletePhotosArr);
	$(e).closest('div').parent().remove();
	console.log(deletePhotosArr);
}

	/*$(".upload_img").change(function(){
		alert()
		readURL(this);
	});*/

function readURL(input) {
	if($(".previewphotos").length <= 0){
		$(".upload-img-block").prepend("<div class='previewphotos'></div>");
	}
	if (input.files && input.files[0]) {
		for (var i = 0; i < input.files.length; i++) {
			if(input.files[i].size < '10485760'){
		        var reader = new FileReader();
		        var j = 1;
		        //console.log($(input.files[i].name))
		        reader.onload = function (e) {
					$(".previewphotos").append("<div><img width='233' src='"+e.target.result+"'><button class='delete-previw-button' type='button' onclick='deletePreview(this,"+j+")'>Delete</button></div>")
		            j++;
		        }
		        reader.readAsDataURL(input.files[i]);
	        }
		}
		

    }
}

function deletePreview(input,j){

	//console.log($(input).offsetParent().children().children('.form-group').find('input'));
	var filess=$(input).offsetParent().children().children('.form-group').find('input').attr("name");
			//console.log($("input[name='"+filess+"']")[0])
			var zzz = $("input[name='"+filess+"']")[0].files;

			console.log(zzz/*$("input[name='"+filess+"']")[0].files*/)
			//zzz.splice(1, 1);

			//console.log($("input[name='"+filess+"']")[0].files[0])
			//$("input[name='"+filess+"']")[0].files[0];
			var asd = $("input[name='"+filess+"']")[0].files;
			//asd.each(function(){ alert()})
			var newArray = [];
			for (var i = 0; i <= 2 /*asd.length-1*/; i++) {
				if(i != j){
					newArray[i] = asd[i];
				}

			}
				console.log(newArray);
			$("input[name='"+filess+"']")[0] = newArray;
			console.log($("input[name='"+filess+"']")[0])
	
	//$("#"+filess.attr('id')).text();
	//console.log(filess[event.target.files]);
	//newFileList.splice(index,1);
	//$(input).parent('div').remove();
}