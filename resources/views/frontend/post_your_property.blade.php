@extends('layouts.frontend.app')
@section('content')
    {{--{{dd($result)}}--}}
    <br><br><br><br>
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-9">
                <div class="list_property shadow-sm mt-5 mb-5">
                    <div id="rootwizard">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3">
                                <div class="lsidecolor">
                                    <div id="circle" class="d-flex justify-content-center pt-4 pb-3 mt-3 mb-1">
                                    </div>
                                    <ul class="nav flex-column nav-pills nav-string">
                                        <li><a class="nav-link rounded-0" href="#tab1" data-toggle="tab"><i
                                                        class="far fa-address-card fa-1x mr-2"></i>Basic
                                                Details</a></li>
                                        <li><a class="nav-link  rounded-0" href="#tab2" data-toggle="tab"><i
                                                        class="fas fa-location-arrow fa-1x mr-2"></i>Location</a>
                                        </li>
                                        <li><a class="nav-link rounded-0" href="#tab3" data-toggle="tab"><i
                                                        class="far fa-building fa-1x mr-2"></i>Property
                                                Details</a></li>
                                        <li><a class="nav-link rounded-0" href="#tab4" data-toggle="tab"><i
                                                        class="fas fa-tag fa-1x mr-2"></i>Price &
                                                Features</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-9">
                                <form action="{{route('add_property')}}" method="post" accept-charset="utf-8"
                                      class="needs-validation" novalidate>
                                  <input type="hidden" name="_token" value="{{csrf_token()}}">
                                  <input type="hidden" name="id" value="{{isset($result["propertyID"])?$result["propertyID"]:""}}">
                                    <div class="tab-content">
                                        <div class="tab-pane" id="tab1">
                                            <div class="step1">
                                                <div class="row">
                                                    <div class="col-12 col-xl-6">
                                                        <div class="form-group">
                                                            <label class="text-success">Property
                                                                Category</label>
                                                            <select name="property_type_id" id="propertyCategory"
                                                                    class="form-control custom-form-control select_customarrow rounded-0" required>
                                                                <option>Select</option>
                                                                @foreach($data_array["property_type"] as $value)
                                                                    <option value="{{$value["id"]}}"
                                                                            {{isset($result["propertyTypeID"])?($result["propertyTypeID"]==$value["id"])? "selected":"":""}}>
                                                                        {{$value["title"]}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-xl-6"></div>
                                                    <div class="col-12 col-xl-8">
                                                        <div class="form-group mt-3">
                                                            <label class="text-success">Accommodation
                                                                For</label>
                                                            <div class="radioControl">
                                                                <div class="custom-control custom-radio c-control">
                                                                    <input type="radio" class="custom-control-input"
                                                                           id="male" name="accommodation" value="Male">
                                                                    <label class="custom-control-label font13"
                                                                           for="male">Male</label>
                                                                </div>
                                                                <div class="custom-control custom-radio c-control">
                                                                    <input type="radio" class="custom-control-input"
                                                                           id="Female" name="accommodation" value="Female">
                                                                    <label class="custom-control-label font13"
                                                                           for="Female">Female</label>
                                                                </div>
                                                                <div class="custom-control custom-radio c-control">
                                                                    <input type="radio" class="custom-control-input"
                                                                           id="both_male_female" value="Male & Female"
                                                                           name="accommodation">
                                                                    <label class="custom-control-label font13"
                                                                           for="both_male_female">Both Male &
                                                                        Female</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-xl-12">
                                                        <div class="form-group mt-3">
                                                            <label class="text-success">Suitable
                                                                For</label>
                                                            <div class="radioControl">
                                                                @foreach($data_array["suitable_for"] as $key=>$value)
                                                                    <div class="custom-control custom-radio c-control">
                                                                        <input type="radio" class="custom-control-input"
                                                                               id="{{$key}}"
                                                                               name="suitable_for" value="{{$key}}"
                                                                                {{isset($result["roomSharingID"])? ($result["roomSharingID"]==$key)? "checked":"":""}} required>
                                                                        <label class="custom-control-label font13"
                                                                               for="{{$key}}">{{$value}}
                                                                        </label>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-xl-12">
                                                        <div class="form-group mt-3">
                                                            <label class="text-success">Availability Type</label>

                                                            <ul class="availabilityType">
                                                                {{--{{dd($data_array["availability_type"])}}--}}
                                                                @foreach($data_array["availability_type"] as $value)
                                                                    {{--{{dd($value)}}--}}
                                                                    <li data-value="{{$value["id"]}}" class="availability_type">
                                                                        @php
                                                                            $title=str_replace(" ","_",$value["title"]);
                                                                            $title=strtolower($title);
                                                                        @endphp

                                                                        <a href="javascript:void(0)" data-value="{{$value["id"]}}"
                                                                           class="availability_type1 {{isset($result["availability"])? (($value['id']== $result["availability"]) ? 'chk-trigger' : ''):'' }}">
                                                                            <img src="{{asset('frontend/assets/img/icon/icon_'.$title.'.png')}}"
                                                                                 alt="Low-rise Appt">
                                                                            <span class="title">{{$value["title"]}}</span>
                                                                        </a>
                                                                        <i class="fas fa-check-circle availableTypeOkay"></i>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                            <input type="hidden" name="availability_type" id="availability_type_value" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-xl-6">
                                                        <div class="form-group mt-3">
                                                            <label class="text-success">Availability</label>
                                                            <select name="availability" id="availability"
                                                                    class="form-control custom-form-control select_customarrow rounded-0" required>
                                                                <option>Select</option>
                                                                        @foreach($data_array["availability"] as $value)
                                                                            <option value="{{$value["id"]}}"
                                                                            {{isset($result["numberOfRoom"])?($result["numberOfRoom"]==$value["id"]) ? "selected":"":""}}
                                                                            >{{$value["title"]}}</option>
                                                                            @php
                                                                                if($value["title"]=="5 BHK"){
                                                                                    break;
                                                                                }
                                                                            @endphp
                                                                        @endforeach
                                                                <option>Above 6 BHK</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab2">
                                            <div class="step2">
                                                <div class="row">
                                                    <div class="col-12 col-xl-4">
                                                        <div class="form-group">
                                                            <label class="text-success">City</label>
                                                                <select name="city_id" id="city_id"
                                                                        class="form-control custom-form-control select_customarrow rounded-0" required>
                                                                    <option>Select</option>
                                                                    @foreach($city_list as $value)
                                                                        <option value="{{$value["id"]}}"
                                                                        {{isset($result["cityID"])?($result["cityID"]==$value["id"])?"selected":"":""}}>{{$value["name"]}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    <div class="col-12 col-xl-4">
                                                        <div class="form-group">
                                                            <label class="text-success">Project
                                                                Name</label>
                                                            <input type="text" name="projectName" id="projectName"
                                                                   placeholder="enter project name" name="project_name"
                                                                   class="form-control custom-form-control" value="{{isset($result["address"])?$result["address"]:""}}" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-xl-4">
                                                        <div class="form-group">
                                                            <label class="text-success">Locality</label>
                                                            <input type="text" name="locality" id="locality"
                                                                   placeholder="enter locality" value="{{isset($result["areaID"])?$result["areaID"]:""}}"
                                                                   class="form-control custom-form-control" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-xl-4">
                                                        <div class="form-group  mt-3">
                                                            <label class="text-success">Nearest
                                                                Landmark</label>
                                                            <input type="text" name="nearestLandmark"
                                                                   id="nearestLandmark" value="{{isset($result["landmark"])?$result["landmark"]:""}}"
                                                                   placeholder="enter nearest landmark"
                                                                   class="form-control custom-form-control" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab3">
                                            <div class="step3">
                                                <div class="row">
                                                    <div class="col-12 col-xl-6">
                                                        <div class="form-group">
                                                            <label class="text-success">Bed
                                                                Sharing</label>
                                                            <select name="bed_sharing" id="bed_sharing"
                                                                    class="form-control custom-form-control select_customarrow rounded-0" required>
                                                                <option>Select</option>
                                                               @foreach($data_array["bedsharing"] as $key=>$value)
                                                                    <option value="{{$key}}"
                                                                            {{isset($result["sharingID"]) ?($result["sharingID"]==$key)?"selected":"":""}}>
                                                                        {{$value}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-xl-6"></div>
                                                    <div class="col-12 col-xl-6">
                                                        <div class="form-group mt-3">
                                                            <label class="text-success">Total
                                                                Area</label>
                                                            <div class="row">
                                                                <div class="col-12 col-xl-6">
                                                                    <input type="text" name="totalarea"
                                                                           id="totalarea"
                                                                           placeholder="Enter Total Area"
                                                                           value="{{isset($result["total_square_feet"]) ? $result["total_square_feet"]:""}}"
                                                                           class="form-control  custom-form-control mb-2 mb-sm-0 mb-md-0 mb-lg-0 mb-xl-0" required>
                                                                </div>
                                                                <div class="col-12 col-xl-6">
                                                                    <select id="total_area" name="total_area" required>
                                                                        <option value=""> - Select Area Unit - </option>
                                                                        <option value="1"
                                                                                {{isset($result["area_unit"])?($result["area_unit"]==1)?"selected":"":""}}
                                                                        >Sq.Ft.</option>
                                                                        <option value="9" {{isset($result["area_unit"])?($result["area_unit"]==1)?"selected":"":""}}
                                                                        >Sq.Yards</option>
                                                                        <option value="10.764"
                                                                                {{isset($result["area_unit"])?($result["area_unit"]==1)?"selected":"":""}}
                                                                        >Sq.Meter</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-xl-6"></div>
                                                    <div class="col-12 col-xl-6">
                                                        <div class="form-group mt-3">
                                                            <label class="text-success">Condition</label>
                                                            <select name="condition" id="condition"
                                                                    class="form-control custom-form-control select_customarrow rounded-0" required>
                                                                <option value="select">Select</option>
                                                                @foreach($data_array["condition"] as $key=>$value)
                                                                <option value="{{$key}}" {{isset($result["conditionID"])?($result["conditionID"]==$key)?"selected":"":""}}>{{$value}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-12 col-xl-12">
                                                        <div class="form-group">
                                                            <div class="withFurnished withFurnishedContainer">
                                                                <p class="text-gray font11">Select atleast <span
                                                                            class="conditionStatus">3
                                                                            furnishing</span>
                                                                    avialable in
                                                                    the
                                                                    room:</p>
                                                                <ul class="items">
                                                                    <li>
                                                                        <ul>
                                                                            <li>
                                                                                <div class="block">
                                                                                    <div class="in">
                                                                                        <img src="{{asset('frontend/assets/img/icon/icon_wardrobe.png')}}"
                                                                                             class="img-fluid mr-2"
                                                                                             alt="Wardrobe">
                                                                                        Wordrobe
                                                                                    </div>
                                                                                    <div class="control">
                                                                                        <a href="javascript:void(0)"
                                                                                           class="substraction"
                                                                                           id="minus1"><i
                                                                                                    class="fas fa-minus"></i></a>
                                                                                        <div class="counter">0</div>
                                                                                        <a href="javascript:void(0)"
                                                                                           class="addition"
                                                                                           id="plus1"><i
                                                                                                    class="fas fa-plus"></i></a>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="block">
                                                                                    <div class="in">
                                                                                        <img src="{{asset('frontend/assets/img/icon/icon_single_bed.png')}}"
                                                                                             class="img-fluid mr-2"
                                                                                             alt="Single Bed">
                                                                                        Single Bed
                                                                                    </div>
                                                                                    <div class="control">
                                                                                        <a href="javascript:void(0)"
                                                                                           class="substraction"
                                                                                           id="minus2"><i
                                                                                                    class="fas fa-minus"></i></a>
                                                                                        <div class="counter">0</div>
                                                                                        <a href="javascript:void(0)"
                                                                                           class="addition"
                                                                                           id="plus2"><i
                                                                                                    class="fas fa-plus"></i></a>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="block block_03">
                                                                                    <div class="in">
                                                                                        <img src="{{asset('frontend/assets/img/icon/icon_double_bed.png')}}"
                                                                                             class="img-fluid mr-2"
                                                                                             alt="Double Bed">
                                                                                        Double Beds
                                                                                    </div>
                                                                                    <div class="control">
                                                                                        <a href="javascript:void(0)"
                                                                                           class="substraction"
                                                                                           id="minus3"><i
                                                                                                    class="fas fa-minus"></i></a>
                                                                                        <div class="counter">0</div>
                                                                                        <a href="javascript:void(0)"
                                                                                           class="addition"
                                                                                           id="plus3"><i
                                                                                                    class="fas fa-plus"></i></a>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                    <li>
                                                                        <ul>
                                                                            <li>
                                                                                <div class="block block_04">
                                                                                    <div class="in">
                                                                                        <img src="{{asset('frontend/assets/img/icon/icon_fan.png')}}"
                                                                                             class="img-fluid mr-2"
                                                                                             alt="Fan">
                                                                                        Fan
                                                                                    </div>
                                                                                    <div class="control">
                                                                                        <a href="javascript:void(0)"
                                                                                           class="substraction"
                                                                                           id="minus4"><i
                                                                                                    class="fas fa-minus"></i></a>
                                                                                        <div class="counter">0</div>
                                                                                        <a href="javascript:void(0)"
                                                                                           class="addition"
                                                                                           id="plus4"><i
                                                                                                    class="fas fa-plus"></i></a>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="block block_05">
                                                                                    <div class="in">
                                                                                        <img src="{{asset('frontend/assets/img/icon/icon_light.png')}}"
                                                                                             class="img-fluid mr-2"
                                                                                             alt="Light">
                                                                                        Light
                                                                                    </div>
                                                                                    <div class="control">
                                                                                        <a href="javascript:void(0)"
                                                                                           class="substraction"
                                                                                           id="minus5"><i
                                                                                                    class="fas fa-minus"></i></a>
                                                                                        <div class="counter">0</div>
                                                                                        <a href="javascript:void(0)"
                                                                                           class="addition"
                                                                                           id="plus5"><i
                                                                                                    class="fas fa-plus"></i></a>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="block block_06">
                                                                                    <div class="in">
                                                                                        <img src="{{asset('frontend/assets/img/icon/icon_fridge.png')}}"
                                                                                             class="img-fluid mr-2"
                                                                                             alt="Fridge">
                                                                                        Fridge
                                                                                    </div>
                                                                                    <div
                                                                                            class="control d-flex justify-content-end">
                                                                                        <label class="switch">
                                                                                            <input type="checkbox"
                                                                                                   checked>
                                                                                            <span
                                                                                                    class="slider round"></span>
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                                <a data-toggle="collapse" href="#collapseExample"
                                                                   class="addmore">
                                                                    <i class="fas fa-angle-down downArrow"></i>
                                                                    <i class="fas fa-angle-up upArrow d-none"></i>
                                                                    Add more
                                                                    furnishings</a>
                                                                <div class="collapse" id="collapseExample">
                                                                    <ul class="items">
                                                                        <li>
                                                                            <ul>
                                                                                <li>
                                                                                    <div class="block">
                                                                                        <div class="in">
                                                                                            <img src="{{asset('frontend/assets/img/icon/icon_ac.png')}}"
                                                                                                 class="img-fluid mr-2"
                                                                                                 alt="AC">
                                                                                            AC </div>
                                                                                        <div class="control">
                                                                                            <a href="javascript:void(0)"
                                                                                               class="substraction"><i
                                                                                                        class="fas fa-minus"></i></a>
                                                                                            <div class="counter">0
                                                                                            </div>
                                                                                            <a href="javascript:void(0)"
                                                                                               class="addition"><i
                                                                                                        class="fas fa-plus"></i></a>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="block">
                                                                                        <div class="in">
                                                                                            <img src="{{asset('frontend/assets/img/icon/icon_geyser.png')}}"
                                                                                                 class="img-fluid mr-2"
                                                                                                 alt="Geyser">
                                                                                            Geyser
                                                                                        </div>
                                                                                        <div class="control">
                                                                                            <a href="javascript:void(0)"
                                                                                               class="substraction"><i
                                                                                                        class="fas fa-minus"></i></a>
                                                                                            <div class="counter">0
                                                                                            </div>
                                                                                            <a href="javascript:void(0)"
                                                                                               class="addition"><i
                                                                                                        class="fas fa-plus"></i></a>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="block block_03">
                                                                                        <div class="in">
                                                                                            <img src=""
                                                                                                 class="img-fluid mr-2"
                                                                                                 alt="Stove">
                                                                                            Stove
                                                                                        </div>
                                                                                        <div
                                                                                                class="control d-flex justify-content-end">
                                                                                            <label class="switch">
                                                                                                <input
                                                                                                        type="checkbox">
                                                                                                <span
                                                                                                        class="slider round"></span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                        <li>
                                                                            <ul>
                                                                                <li>
                                                                                    <div class="block block_03">
                                                                                        <div class="in">
                                                                                            <img src=""
                                                                                                 class="img-fluid mr-2"
                                                                                                 alt="Washing Machine">
                                                                                            Washing Machine
                                                                                        </div>
                                                                                        <div
                                                                                                class="control d-flex justify-content-end">
                                                                                            <label class="switch">
                                                                                                <input
                                                                                                        type="checkbox">
                                                                                                <span
                                                                                                        class="slider round"></span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="block block_03">
                                                                                        <div class="in">
                                                                                            <img src=""
                                                                                                 class="img-fluid mr-2"
                                                                                                 alt="Water Purify">
                                                                                            Water Purify
                                                                                        </div>
                                                                                        <div
                                                                                                class="control d-flex justify-content-end">
                                                                                            <label class="switch">
                                                                                                <input
                                                                                                        type="checkbox">
                                                                                                <span
                                                                                                        class="slider round"></span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="block block_03">
                                                                                        <div class="in">
                                                                                            <img src="{{asset('frontend/assets/img/icon/icon_stove.png')}}"
                                                                                                 class="img-fluid mr-2"
                                                                                                 alt="Microwave">
                                                                                            Microwave
                                                                                        </div>
                                                                                        <div
                                                                                                class="control d-flex justify-content-end">
                                                                                            <label class="switch">
                                                                                                <input
                                                                                                        type="checkbox">
                                                                                                <span
                                                                                                        class="slider round"></span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                        <li>
                                                                            <ul>
                                                                                <li>
                                                                                    <div class="block block_03">
                                                                                        <div class="in">
                                                                                            <img src="{{asset('frontend/assets/img/icon/icon_stove.png')}}"
                                                                                                 class="img-fluid mr-2"
                                                                                                 alt="Curtains">
                                                                                            Curtains
                                                                                        </div>
                                                                                        <div
                                                                                                class="control d-flex justify-content-end">
                                                                                            <label class="switch">
                                                                                                <input
                                                                                                        type="checkbox">
                                                                                                <span
                                                                                                        class="slider round"></span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="block block_03">
                                                                                        <div class="in">
                                                                                            <img src="{{asset('frontend/assets/img/icon/icon_water_purifier.png')}}"
                                                                                                 class="img-fluid mr-2"
                                                                                                 alt="Chimney">
                                                                                            Chimney
                                                                                        </div>
                                                                                        <div
                                                                                                class="control d-flex justify-content-end">
                                                                                            <label class="switch">
                                                                                                <input
                                                                                                        type="checkbox">
                                                                                                <span
                                                                                                        class="slider round"></span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="block block_03">
                                                                                        <div class="in">
                                                                                            <img src="{{asset('frontend/assets/img/icon/icon_fan.png')}}"
                                                                                                 class="img-fluid mr-2"
                                                                                                 alt="Exhaust Fan">
                                                                                            Exhaust Fan
                                                                                        </div>
                                                                                        <div
                                                                                                class="control d-flex justify-content-end">
                                                                                            <label class="switch">
                                                                                                <input
                                                                                                        type="checkbox">
                                                                                                <span
                                                                                                        class="slider round"></span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                        <li>
                                                                            <ul>
                                                                                <li>
                                                                                    <div class="block block_03">
                                                                                        <div class="in">
                                                                                            <img src=""
                                                                                                 class="img-fluid mr-2"
                                                                                                 alt="Sofa">
                                                                                            Sofa
                                                                                        </div>
                                                                                        <div
                                                                                                class="control d-flex justify-content-end">
                                                                                            <label class="switch">
                                                                                                <input
                                                                                                        type="checkbox">
                                                                                                <span
                                                                                                        class="slider round"></span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="block block_03">
                                                                                        <div class="in">
                                                                                            <img src="{{asset('frontend/assets/img/icon/icon_water_purifier.png')}}"
                                                                                                 class="img-fluid mr-2"
                                                                                                 alt="Dining Table">
                                                                                            Dining Table
                                                                                        </div>
                                                                                        <div
                                                                                                class="control d-flex justify-content-end">
                                                                                            <label class="switch">
                                                                                                <input
                                                                                                        type="checkbox">
                                                                                                <span
                                                                                                        class="slider round"></span>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="block block_03">
                                                                                    </div>
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-xl-6">
                                                        <div class="form-group mt-3">
                                                            <label class="text-success">Property
                                                                on
                                                                Floor</label>

                                                                    <input type="text" name="propertyOnFloor" id="propertyOnFloor"
                                                                           placeholder="enter property on floor" value="{{isset($result["floorID"])?$result["floorID"]:""}}"
                                                                           class="form-control custom-form-control" required>
                                                            {{--<select name="propertyOnFloor" id="propertyOnFloor"--}}
                                                                    {{--class="form-control custom-form-control select_customarrow rounded-0">--}}
                                                                {{--@foreach($data_array["property_on_floor"] as $value)--}}
                                                                    {{--<option value="{{$value}}">--}}
                                                                        {{--{{$value}}--}}
                                                                    {{--</option>--}}
                                                                    {{--@endforeach--}}
                                                            {{--</select>--}}
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-xl-6">
                                                        <div class="form-group mt-3">
                                                            <label class="text-success">Total
                                                                Floor</label>
                                                            <input type="text" name="totalFloor" id="totalFloor"
                                                                   placeholder="enter locality" value="{{isset($result["total_floor"])?$result["total_floor"]:""}}"
                                                                   class="form-control custom-form-control" required>
                                                            {{--<select name="totalFloor" id="totalFloor"--}}
                                                                    {{--class="form-control custom-form-control select_customarrow rounded-0">--}}
                                                                {{--<option value="select">Select</option>--}}
                                                                {{--@foreach($data_array["total_floor"] as $value)--}}
                                                                    {{--<option value="1">{{$value}}</option>--}}
                                                                {{--@endforeach--}}
                                                            {{--</select>--}}
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-xl-6">
                                                        <div class="form-group mt-3">
                                                            <label class="text-success">Property
                                                                Age</label>
                                                            <select name="propertyAge" id="propertyAge"
                                                                    class="form-control custom-form-control select_customarrow rounded-0" required>
                                                                <option value="select">Select</option>
                                                                @foreach($data_array["property_age"] as $key=>$value)
                                                                    <option value="{{$key}}" {{isset($resutl["old_building"])? ($result["old_building"]==$key)?"selected":"":""}}>{{$value}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-xl-6"></div>
                                                    <div class="col-12 col-xl-6">
                                                        <div class="form-group mt-3">
                                                            <label class="text-success">Available
                                                                From</label>
                                                            <div id="reportrange"
                                                                 class="form-control custom-form-control d-flex justify-content-between" data-target="input_available_from">
                                                                <span></span>
                                                                <i class="far fa-calendar-alt fa-2x" style="font-size:20px"></i>
                                                            </div>
                                                            <input type="hidden" id="input_available_from" name="avaialble_from">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab4">
                                            <div class="step4">
                                                <div class="row">
                                                    <div class="col-12 col-xl-6">
                                                        <div class="form-group">
                                                            <label class="text-success">Expected
                                                                Montly
                                                                Rent</label>
                                                            <input type="text" name="expectedMontlyRent"
                                                                   placeholder="enter expected monthly rent"
                                                                   id="expectedMontlyRent" value="{{isset($result["price"])?$result["price"]:""}}"
                                                                   class="form-control custom-form-control numbersOnly">
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-xl-6"></div>
                                                    <div class="col-12 col-xl-6">
                                                        <div class="form-group">
                                                            <label class="text-success">Security
                                                                Deposit</label>
                                                            <input type="text" name="securityDeposit"
                                                                   placeholder="enter security deposit"
                                                                   id="securityDeposit" value="{{isset($result["deposit"])?$result["deposit"]:""}}"
                                                                   class="form-control custom-form-control numbersOnly">
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-xl-6"></div>
                                                    <div class="col-12 col-xl-9">
                                                        <div class="form-group mt-3">
                                                            <label class="text-success">Extra
                                                                Charges</label>
                                                            {{--{{dd($data_array["extracharge"])}}--}}
                                                            <div class="radioControl">
                                                                @foreach($data_array["extracharge"] as $key=>$value)
                                                                <div class="custom-control custom-radio c-control">
                                                                    <input type="radio" class="custom-control-input"
                                                                           id="{{$value}}"
                                                                           name="extra-charge" value="{{$key}}" required>
                                                                    <label class="custom-control-label font13"
                                                                           for="{{$value}}">
                                                                        {{$value}}
                                                                    </label>
                                                                </div>
                                                                @endforeach
                                                                {{--<div class="custom-control custom-radio c-control">--}}
                                                                    {{--<input type="radio" class="custom-control-input"--}}
                                                                           {{--id="AC_Charge" name="radio-stacked">--}}
                                                                    {{--<label class="custom-control-label font13"--}}
                                                                           {{--for="AC_Charge">AC Charge</label>--}}
                                                                {{--</div>--}}
                                                                {{--<div class="custom-control custom-radio c-control">--}}
                                                                    {{--<input type="radio" class="custom-control-input"--}}
                                                                           {{--id="maintenance" name="radio-stacked">--}}
                                                                    {{--<label class="custom-control-label font13"--}}
                                                                           {{--for="maintenance">Maintenance</label>--}}
                                                                {{--</div>--}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-xl-9">
                                                        <div class="form-group mt-3">
                                                            <label class="text-success">Notice
                                                                Period</label>
                                                            <div class="form-check form-check-inline">

                                                                @foreach($data_array["notice_period_arr"] as $key=>$value)
                                                                <div class="custom-control custom-radio d-flex align-items-center">
                                                                    <input type="radio" class="custom-control-input"
                                                                           id="{{$value}}" name="notice-period" value="{{$key}}"
                                                                            {{isset($result["noticePeriod"])?($result["noticePeriod"]==$key)?"checked":"":""}} required>
                                                                    <label class="custom-control-label font13"
                                                                           for="{{$value}}" >{{$value}}</label>
                                                                </div>
                                                                @endforeach
                                                                {{--<div--}}
                                                                        {{--class="custom-control custom-radio d-flex align-items-center ml-2">--}}
                                                                    {{--<input type="radio" class="custom-control-input"--}}
                                                                           {{--id="30_Days" name="radio-stacked">--}}
                                                                    {{--<label class="custom-control-label font13"--}}
                                                                           {{--for="30_Days">30 Days</label>--}}
                                                                {{--</div>--}}
                                                                {{--<div--}}
                                                                        {{--class="custom-control custom-radio d-flex align-items-center ml-2">--}}
                                                                    {{--<input type="radio" class="custom-control-input"--}}
                                                                           {{--id="60_Days" name="radio-stacked">--}}
                                                                    {{--<label class="custom-control-label font13"--}}
                                                                           {{--for="60_Days">60 Days</label>--}}
                                                                {{--</div>--}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-xl-9">
                                                        <div class="form-group mt-3">
                                                            <label class="text-success">Facing</label>
                                                            <div class="form-check form-check-inline">
                                                                {{--{{dd($result["Facing"])}}--}}
                                                                {{--{{dd($data_array["facing_arr"   ])}}--}}
                                                                @foreach($data_array["facing_arr"] as $value)
                                                                                    {{--{{dd($value)}}--}}
                                                                    <div class="custom-control custom-radio d-flex align-items-center">
                                                                        <input type="radio" class="custom-control-input"
                                                                               id="{{$value}}" name="facing" value="{{$value}}" required
                                                                            {{isset($result["Facing"]) ? ($result["Facing"]==$value) ? "checked":"":""}}>
                                                                        <label class="custom-control-label font13"
                                                                               for="{{$value}}">{{$value}}</label>
                                                                    </div>
                                                                @endforeach
                                                                {{--<div--}}
                                                                        {{--class="custom-control custom-checkbox d-flex align-items-center ml-2">--}}
                                                                    {{--<input type="checkbox"--}}
                                                                           {{--class="custom-control-input" id="south"--}}
                                                                           {{--name="south">--}}
                                                                    {{--<label class="custom-control-label font13"--}}
                                                                           {{--for="south">South</label>--}}
                                                                {{--</div>--}}
                                                                {{--<div--}}
                                                                        {{--class="custom-control custom-checkbox d-flex align-items-center ml-2">--}}
                                                                    {{--<input type="checkbox"--}}
                                                                           {{--class="custom-control-input" id="east"--}}
                                                                           {{--name="east">--}}
                                                                    {{--<label class="custom-control-label font13"--}}
                                                                           {{--for="east">East</label>--}}
                                                                {{--</div>--}}
                                                                {{--<div--}}
                                                                        {{--class="custom-control custom-checkbox d-flex align-items-center ml-2">--}}
                                                                    {{--<input type="checkbox"--}}
                                                                           {{--class="custom-control-input" id="west"--}}
                                                                           {{--name="west">--}}
                                                                    {{--<label class="custom-control-label font13"--}}
                                                                           {{--for="west">West</label>--}}
                                                                {{--</div>--}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-xl-12">
                                                        <div class="form-group mt-3">
                                                            <label class="text-success">Facilities</label>
                                                            <div class="withFurnishedContainer">
                                                                <ul class="externalAmenity">
                                                                        {{--{{dd($data_array["property_facilities"])}}--}}
                                                                    @foreach($data_array["property_facilities"] as $value)
                                                                        @php
                                                                        $array=[];
                                                                        if(isset($result["facilityID"]))
                                                                            $array=explode(",",$result["facilityID"]);
                                                                        @endphp
                                                                    <li data-value="{{$value["id"]}}" class="property_facilities {{ in_array($value['id'],$array) ? 'chk-trigger' : '' }}">
                                                                        @php
                                                                            $title=str_replace(" ","_",$value["title"]);
                                                                            $title=strtolower($title);
                                                                        @endphp
                                                                        <a href="javascript:void(0)">
                                                                            <img src="{{asset('frontend/assets/img/icon/icon_'.$title.'.png')}}"
                                                                                 alt="Maid">
                                                                            <span class="title">{{$value["title"]}}</span>
                                                                        </a>
                                                                        <i class="fas fa-check-circle availableTypeOkay"></i>

                                                                    </li>
                                                                    @endforeach
                                                                    <input type="hidden" name="property_facilities[]" id="property_facilities_value" required>
                                                                    {{--<li>--}}
                                                                        {{--<a href="javascript:void(0)">--}}
                                                                            {{--<img src="{{asset('frontend/assets/img/icon/icon_attach_bathroom.png')}}"--}}
                                                                                 {{--alt="Attach Bathroom">--}}
                                                                            {{--<span class="title">Attach--}}
                                                                                    {{--Bathroom</span>--}}
                                                                        {{--</a>--}}
                                                                        {{--<i--}}
                                                                                {{--class="fas fa-check-circle availableTypeOkay"></i>--}}
                                                                    {{--</li>--}}
                                                                    {{--<li>--}}
                                                                        {{--<a href="javascript:void(0)">--}}
                                                                            {{--<img src="{{asset('frontend/assets/img/icon/icon_attach_balcony.png')}}"--}}
                                                                                 {{--alt="Attach Balconies">--}}
                                                                            {{--<span class="title">Attach--}}
                                                                                    {{--Balconies</span>--}}
                                                                        {{--</a>--}}
                                                                        {{--<i--}}
                                                                                {{--class="fas fa-check-circle availableTypeOkay"></i>--}}
                                                                    {{--</li>--}}
                                                                    {{--<li>--}}
                                                                        {{--<a href="javascript:void(0)">--}}
                                                                            {{--<img src="{{asset('frontend/assets/img/icon/icon_laundry.png')}}"--}}
                                                                                 {{--alt="Laundry">--}}
                                                                            {{--<span class="title">Laundry</span>--}}
                                                                        {{--</a>--}}
                                                                        {{--<i--}}
                                                                                {{--class="fas fa-check-circle availableTypeOkay"></i>--}}
                                                                    {{--</li>--}}
                                                                    {{--<li>--}}
                                                                        {{--<a href="javascript:void(0)">--}}
                                                                            {{--<img src="{{asset('frontend/assets/img/icon/icon_cleaning.png')}}"--}}
                                                                                 {{--alt="Cleaning">--}}
                                                                            {{--<span class="title">Cleaning</span>--}}
                                                                        {{--</a>--}}
                                                                        {{--<i--}}
                                                                                {{--class="fas fa-check-circle availableTypeOkay"></i>--}}
                                                                    {{--</li>--}}
                                                                    {{--<li>--}}
                                                                        {{--<a href="javascript:void(0)">--}}
                                                                            {{--<img src="{{asset('frontend/assets/img/icon/icon_common_bathroom.png')}}"--}}
                                                                                 {{--alt="Common Bathroom">--}}
                                                                            {{--<span class="title">Common--}}
                                                                                    {{--Bathroom</span>--}}
                                                                        {{--</a>--}}
                                                                        {{--<i--}}
                                                                                {{--class="fas fa-check-circle availableTypeOkay"></i>--}}
                                                                    {{--</li>--}}
                                                                    {{--<li>--}}
                                                                        {{--<a href="javascript:void(0)">--}}
                                                                            {{--<img src="{{asset('frontend/assets/img/icon/icon_common_balcony.png')}}"--}}
                                                                                 {{--alt="Common Balcony">--}}
                                                                            {{--<span class="title">Common--}}
                                                                                    {{--Balcony</span>--}}
                                                                        {{--</a>--}}
                                                                        {{--<i--}}
                                                                                {{--class="fas fa-check-circle availableTypeOkay"></i>--}}
                                                                    {{--</li>--}}
                                                                    {{--<li>--}}
                                                                        {{--<a href="javascript:void(0)">--}}
                                                                            {{--<img src="{{asset('frontend/assets/img/icon/icon_water_storage.png')}}"--}}
                                                                                 {{--alt="Water Storage">--}}
                                                                            {{--<span class="title">Water Storage</span>--}}
                                                                        {{--</a>--}}
                                                                        {{--<i--}}
                                                                                {{--class="fas fa-check-circle availableTypeOkay"></i>--}}
                                                                    {{--</li>--}}
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-xl-12">
                                                        <div class="form-group mt-3">
                                                            <label class="text-success">Society
                                                                Amenities</label>
                                                            <div class="withFurnishedContainer">
                                                                <ul class="externalAmenity">
{{--                                                                    {{dd($data_array["property_society_amenity"])}}--}}
                                                                    @foreach($data_array["property_society_amenity"] as $value)
                                                                        @php
                                                                            $array=[];
                                                                            if(isset($result["societyamenityID"]))
                                                                                $array=explode(",",$result["societyamenityID"]);
                                                                        @endphp
                                                                    <li data-value="{{$value["id"]}}" class="society_amenity {{ in_array($value['id'],$array) ? 'chk-trigger' : '' }}">
                                                                        @php
                                                                            $title=str_replace(" ","_",$value["title"]);
                                                                            $title=strtolower($title);
                                                                        if (strpos($title, 'open_') !== false) {
                                                                                $title=str_replace("open_","",$title);
                                                                            }
                                                                        @endphp
                                                                        <a href="javascript:void(0)">
                                                                            <img src="{{asset('frontend/assets/img/icon/icon_'.$title.'.png')}}"
                                                                                 alt="{{$title}}">
                                                                            <span class="title">{{$value["title"]}}</span>
                                                                        </a>
                                                                        <i class="fas fa-check-circle availableTypeOkay"></i>
                                                                    </li>
                                                                    @endforeach
                                                                    <input type="hidden" name="society_amenity[]" id="society_amenity_value" required>
                                                                    {{--<li>--}}
                                                                        {{--<a href="javascript:void(0)">--}}
                                                                            {{--<img src="{{asset('frontend/assets/img/icon/icon_park.png')}}"--}}
                                                                                 {{--alt="Park">--}}
                                                                            {{--<span class="title">Park</span>--}}
                                                                        {{--</a>--}}
                                                                        {{--<i--}}
                                                                                {{--class="fas fa-check-circle availableTypeOkay"></i>--}}
                                                                    {{--</li>--}}
                                                                    {{--<li>--}}
                                                                        {{--<a href="javascript:void(0)">--}}
                                                                            {{--<img src="{{asset('frontend/assets/img/icon/icon_club_house.png')}}"--}}
                                                                                 {{--alt="Club House">--}}
                                                                            {{--<span class="title">Club House</span>--}}
                                                                        {{--</a>--}}
                                                                        {{--<i--}}
                                                                                {{--class="fas fa-check-circle availableTypeOkay"></i>--}}
                                                                    {{--</li>--}}
                                                                    {{--<li>--}}
                                                                        {{--<a href="javascript:void(0)">--}}
                                                                            {{--<img src="{{asset('frontend/assets/img/icon/icon_cctv.png')}}"--}}
                                                                                 {{--alt="CCTV Camera">--}}
                                                                            {{--<span class="title">CCTV Camera</span>--}}
                                                                        {{--</a>--}}
                                                                        {{--<i--}}
                                                                                {{--class="fas fa-check-circle availableTypeOkay"></i>--}}
                                                                    {{--</li>--}}
                                                                    {{--<li>--}}
                                                                        {{--<a href="javascript:void(0)">--}}
                                                                            {{--<img src="{{asset('frontend/assets/img/icon/icon_lift.png')}}"--}}
                                                                                 {{--alt="Lift">--}}
                                                                            {{--<span class="title">Lift</span>--}}
                                                                        {{--</a>--}}
                                                                        {{--<i--}}
                                                                                {{--class="fas fa-check-circle availableTypeOkay"></i>--}}
                                                                    {{--</li>--}}
                                                                    {{--<li>--}}
                                                                        {{--<a href="javascript:void(0)">--}}
                                                                            {{--<img src="{{asset('frontend/assets/img/icon/icon_car_parking.png')}}"--}}
                                                                                 {{--alt="Car Parking">--}}
                                                                            {{--<span class="title">Car Parking</span>--}}
                                                                        {{--</a>--}}
                                                                        {{--<i--}}
                                                                                {{--class="fas fa-check-circle availableTypeOkay"></i>--}}
                                                                    {{--</li>--}}
                                                                    {{--<li>--}}
                                                                        {{--<a href="javascript:void(0)">--}}
                                                                            {{--<img src="{{asset('frontend/assets/img/icon/icon_two_wheeler_parking.png')}}"--}}
                                                                                 {{--alt="Two wheeler Parking">--}}
                                                                            {{--<span class="title">Two wheeler--}}
                                                                                    {{--Parking</span>--}}
                                                                        {{--</a>--}}
                                                                        {{--<i--}}
                                                                                {{--class="fas fa-check-circle availableTypeOkay"></i>--}}
                                                                    {{--</li>--}}
                                                                    {{--<li>--}}
                                                                        {{--<a href="javascript:void(0)">--}}
                                                                            {{--<img src="{{asset('frontend/assets/img/icon/icon_security.png')}}"--}}
                                                                                 {{--alt="Water Storage">--}}
                                                                            {{--<span class="title">Security</span>--}}
                                                                        {{--</a>--}}
                                                                        {{--<i--}}
                                                                                {{--class="fas fa-check-circle availableTypeOkay"></i>--}}
                                                                    {{--</li>--}}
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-xl-12">
                                                        <div class="form-group mt-3">
                                                            <label class="text-success">Rules</label>
                                                            <div class="withFurnishedContainer">
                                                                <ul class="externalAmenity">
{{--                                                                    {{dd($data_array["property_rule"])}}--}}
                                                                    @foreach($data_array["property_rule"] as $key=>$value)
                                                                        @php
                                                                        $array=[];
                                                                        if(isset($result["rules"]))
                                                                            $array=explode(",",$result["rules"]);
                                                                        @endphp
                                                                        <li data-value="{{$value["id"]}}" class="rules {{in_array($value['id'],$array) ? 'chk-trigger' : '' }}">
                                                                            @php
                                                                                $title=str_replace(" ","_",$value["title"]);
                                                                                $title=strtolower($title);
                                                                            @endphp
                                                                            <a href="javascript:void(0)">
                                                                                <img src="{{asset('frontend/assets/img/icon/icon_'.$title.'.png')}}"
                                                                                     alt="No Smoking">
                                                                                <span class="title">{{$value["title"]}}</span>
                                                                            </a>
                                                                            <i class="fas fa-check-circle availableTypeOkay"></i>
                                                                        </li>
                                                                    @endforeach

                                                                    <input type="hidden" name="rules[]" id="rules_value" required>
                                                                    {{--<li>--}}
                                                                        {{--<a href="javascript:void(0)">--}}
                                                                            {{--<img src="{{asset('frontend/assets/img/icon/icon_no_alcohol.png')}}"--}}
                                                                                 {{--alt="No Drinking">--}}
                                                                            {{--<span class="title">No Drinking</span>--}}
                                                                        {{--</a>--}}
                                                                        {{--<i--}}
                                                                                {{--class="fas fa-check-circle availableTypeOkay"></i>--}}
                                                                    {{--</li>--}}
                                                                    {{--<li>--}}
                                                                        {{--<a href="javascript:void(0)">--}}
                                                                            {{--<img src="{{asset('frontend/assets/img/icon/icon_no_guardian_stay_allow.png')}}"--}}
                                                                                 {{--alt="No Guardian Stay Allow">--}}
                                                                            {{--<span class="title">No Guardian Stay--}}
                                                                                    {{--Allow</span>--}}
                                                                        {{--</a>--}}
                                                                        {{--<i--}}
                                                                                {{--class="fas fa-check-circle availableTypeOkay"></i>--}}
                                                                    {{--</li>--}}
                                                                    {{--<li>--}}
                                                                        {{--<a href="javascript:void(0)">--}}
                                                                            {{--<img src="{{asset('frontend/assets/img/icon/icon_no_nonveg.png')}}"--}}
                                                                                 {{--alt=" No Non-Vegetarian">--}}
                                                                            {{--<span class="title">No--}}
                                                                                    {{--Non-Vegetarian</span>--}}
                                                                        {{--</a>--}}
                                                                        {{--<i--}}
                                                                                {{--class="fas fa-check-circle availableTypeOkay"></i>--}}
                                                                    {{--</li>--}}
                                                                    {{--<li>--}}
                                                                        {{--<a href="javascript:void(0)">--}}
                                                                            {{--<img src="{{asset('frontend/assets/img/icon/icon_no_late_night_entry.png')}}"--}}
                                                                                 {{--alt="No Late Night Entry">--}}
                                                                            {{--<span class="title">No Late Night--}}
                                                                                    {{--Entry</span>--}}
                                                                        {{--</a>--}}
                                                                        {{--<i--}}
                                                                                {{--class="fas fa-check-circle availableTypeOkay"></i>--}}
                                                                    {{--</li>--}}
                                                                    {{--<li></li>--}}
                                                                    {{--<li></li>--}}
                                                                    {{--<li></li>--}}
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-xl-4">
                                                        <div class="form-group">
                                                            <label class="text-success">Occupancy Status</label>
                                                            <select name="occupancy_status" class="form-control custom-form-control select_customarrow rounded-0" required>
                                                                <option value="Available" {{isset($result["occupancy"])?($result["occupancy"]=="Available") ?"selected":"":""}}>Available</option>
                                                                <option value="Occupied" {{isset($result["occupancy"])?($result["occupancy"]=="Occupied") ?"selected":"":""}}>Occupied</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-xl-12">
                                                        <div class="form-group mt-3">
                                                            <label class="text-success">Description</label>
                                                            <textarea class="form-control rounded-0" cols="5"
                                                                      rows="5" name="description">{{isset($result["description"])?$result["description"]:""}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="pager wizard d-flex justify-content-between wizards">
                                            <li class="previous"><button type="button"
                                                                         class="btn btn-primary btn-sm rounded-0 prev shadow-sm">Previous</button>
                                            </li>
                                            <li>
                                                <ul class="d-flex justify-content-end">
                                                    <li class="next">
                                                        <button type="button"
                                                                class="btn btn-info btn-sm rounded-0 shadow-sm">Next</button>
                                                    </li>
                                                    <li>
                                                        <input type="submit"
                                                           class="btn btn-success btn-sm rounded-0 submitbtn shadow-sm" value="submit">
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                <div class="list_property_features">
                    <div class="card border-light rounded-0 shadow-sm">
                        <div class="card-header border-success">Advertise your property with us in 4 simple steps.
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><i class="far fa-address-card fa-1x mr-2"></i>Basic Details
                            </li>
                            <li class="list-group-item"><i class="fas fa-location-arrow fa-1x mr-2"></i>Location
                            </li>
                            <li class="list-group-item"><i class="far fa-building fa-1x mr-2"></i>Property
                                Details</li>
                            <li class="list-group-item"><i class="fas fa-tag fa-1x mr-2"></i>Price &
                                Features</li>
                        </ul>
                    </div>
                </div>
                <div class="help_info m-0">
                    <p>If you're stuck on the form and don't know what to do next, you can email us at <a
                                href="mailto:services@99acres.com" class="text-primary">services@99acres.com</a> or call
                        us at 1800 41 99099 (IND Toll-Free). For international numbers click here.</p>
                    <p>Get your property verified. Verified properties get 80% more responses from interested and
                        genuine buyers.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal For Login Verification Start Here -->
    <div class="modal" tabindex="-1" role="dialog" id="verifyNo" data-keyboard="false">
        <div class="modal-dialog modal-lg modal_custom_new modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="row">
                    <div class="col-12 col-sm-4 col-md-12 col-lg-4 col-xl-4 pr-lg-0 order-1 order-sm-0 order-md-1 order-lg-0 order-xl-0">
                        <div class="specification">
                            <h1 class="title">
                                Why Greenroom?
                            </h1>

                            <div class="card rounded-0 mb-4 mb-sm-4 mb-md-0 md-lg-4 md-xl-4 mt-3 shadow-sm">
                                <!-- <div class="card-header bg-light p-2 pl-3 font13">Facilities</div> -->
                                <div class="card-body text-secondary p-0">
                                    <ul class="perks">
                                        <li class="d-flex justify-content-between align-items-center">
                                            No Brokerage
                                            <!-- <img src="assets/img/icon/icon_05.png" alt=""> -->
                                        </li>
                                        <li class="d-flex justify-content-between align-items-center">
                                            Increased Rental Yield
                                        </li>
                                        <li class="d-flex justify-content-between align-items-center">
                                            Furnishing Support
                                        </li>
                                        <li class="d-flex justify-content-between align-items-center">
                                            Verified Tenant
                                        </li>
                                        <li class="d-flex justify-content-between align-items-center">
                                            Timely Rent
                                        </li>
                                        <li class="d-flex justify-content-between align-items-center">
                                            Free Listing
                                        </li>
                                        <li class="d-flex justify-content-between align-items-center">
                                            Online Dashboard
                                        </li>
                                        <li class="d-flex justify-content-between align-items-center">
                                            Property Management
                                        </li>
                                        <li class="d-flex justify-content-between align-items-center">
                                            Tenant Management
                                        </li>
                                        <li class="d-flex justify-content-between align-items-center">
                                            24x7 Customer Assistance
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <ul class="mt-0 mt-sm-4 mt-md-4 mt-lg-4 mt-xl-4 d-none">
                                <li>No Brokerage</li>
                                <li>Increased Rental Yield</li>
                                <li>Furnishing Support</li>
                                <li>Verified Tenant</li>
                                <li>Timely Rent</li>
                                <li>Free Listing</li>
                                <li>Online Dashboard</li>
                                <li>Property Management</li>
                                <li>Tenant Management</li>
                                <li>24x7 Customer Assistance</li>
                            </ul>

                        </div>
                    </div>
                    <div
                            class="col-12 col-sm-8 col-md-12 col-lg-8 col-xl-8 order-0 order-sm-1 order-md-0 order-lg-1 order-xl-1 pl-xl-0">
                        <div class="modal-header">
                            <h6 class="modal-title">
                                Verify Mobile No.
                            </h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p class="text-primary mb-2">Enter verification code sent to your mobile no.
                                91-9974408232</p>
                            <form id="verifyForm" method="post" class="needs-validation" action="#" novalidate>
                                <div class="form-group">
                                    <label class="is-small">Enter Code</label>
                                    <input type="text" class="form-control rounded-0" id="enterCode"
                                           name="enterCode" placeholder="Enter Code" required />
                                    <div class="invalid-feedback">Verification Code is required</div>
                                </div>
                                <div class="form-group">
                                    <a href="javascript:void(0)" class="resend_code">Resend Code</a>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-lg btn-success btn-block shadow-sm">
                                            Verify
                                        </button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <p class="verify_via_missed_call"><a href="javascript:void(0)">Click here to
                                        Verify</a> via a missed call
                                    from your number (91-9974408232)</p>
                                <p class="text-center mt-2 mb-2">-Or-</p>
                                <div class="row">
                                    <div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2"></div>
                                    <div class="col-12 col-sm-4 col-md-8 col-lg-8 col-xl-8">
                                        <button type="button" data-dismiss="modal"
                                                class="btn btn-lg btn-primary btn-block shadow-sm">
                                            Skip, will do later
                                        </button>
                                    </div>
                                    <div class="col-12 col-sm-4 col-md-2 col-lg-4 col-xl-2"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal For Login Verification Start Here -->
@endsection

@section('footer')
    <script>
        $(document).ready(function(){
            var facilities=[];
            var society_aminity=[];
            var rule=[];

//            fruitvegbasket.push(veggies);

            $(document).on("click",".property_facilities",function(){
                var value=$(this).attr("data-value");
                facilities.push(value);
                $("#property_facilities_value").val(facilities);
            });

            $(document).on("click",".society_amenity",function(){
                var value=$(this).attr("data-value");
                society_aminity.push(value);
                $("#society_amenity_value").val(society_aminity);
            });

            $(document).on("click",".rules",function(){
                var value=$(this).attr("data-value");
                rule.push(value);
                $("#rules_value").val(rule);
            });
//
            $(document).on("click",".availability_type a",function(){
               var value=$(this).parent().attr("data-value");
//               alert(value);
                $("#availability_type_value").val(value);
            });

            $(".chk-trigger").trigger("click");
        });

    </script>
    @endsection
