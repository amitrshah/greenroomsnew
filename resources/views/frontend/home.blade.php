@extends('layouts.frontend.app')
@section('content')

    <!-- /header -->
    <!-- banner Start Here -->
    <div class="greenroom-banner">
        <div id="slideshow">
            <div>
                <img src="{{asset('frontend/assets/img/bnr01.jpg')}}" alt="" class="img-fluid greenroom-img"/>
            </div>
            <div>
                <img src="{{asset('frontend/assets/img/bnr02.jpg')}}" alt="" class="img-fluid greenroom-img"/>
            </div>
            <div>
                <img src="{{asset('frontend/assets/img/bnr03.jpg')}}" alt="" class="img-fluid greenroom-img"/>
            </div>
            <div>
                <img src="{{asset('frontend/assets/img/bnr04.jpg')}}" alt="" class="img-fluid greenroom-img"/>
            </div>
        </div>
        <div class="content">
            <div class="contentContainer">
                <div class="content-row">
                    <h1 class="title">Search For Accommodation</h1>
                    <h4 class="title">
                        Easiest way to find a PG, corporate PG, hostel for long and short
                        stay
                    </h4>
                </div>
                <div class="content-row">
                    <form action="{{route("search")}}" method="GET" id="form-home-search">
                    <div class="homeSearchArea">
                        <div class="category_name property_type">
                            <select name="propertyType[]" id="propertyTypeId" class="select_customarrow form-control js-select2" multiple="multiple">
                                @foreach($search_property_type as $item)
								<option value="{{$item->propertyTypeID}}">{{$item->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="category_name for_mal_or_female">
                            <select name="forMaleOrFemale[]" id="forMaleOrFemaleId" class="select_customarrow form-control js-select2">
								<option value="">Accomodation For</option>
								@foreach($accomodation_for as $key=>$item)
								<option value="{{$key}}">{{$item}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="category_name search_by_loc">
                            <select name="searchByLoc[]" id="searchByLoc" class="select_customarrow" placeHolder="Search By Location" multiple="multiple">
                                @foreach($ahmedabad_loc as $item)
                                    <option value="{{$item->areaID}}">{{$item->areaName}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="search_icon">
                            <a href="javascript:void(0)" id="btn-search-submit" form-id="form-home-search"> 
							<img src="{{asset('frontend/assets/img/icon/icon_search.svg')}}" class="icon-search" alt=""/>
										</a>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- <div class="d-flex justify-content-center">
                <a href="javascript:void(0)" title="List Your Property"
                    class="btn btn-lg btn-success mt-4 list_ur_properties">List
                    Your Property</a>
            </div> -->
    </div>
    <!-- banner End Here -->
    <!-- middle content start -->
    <div class="why_choose_greenroom">
        <!-- why choose greenroom? start -->
        <div class="titleBox spc">
            <h1 class="title">Why Choose Greenroom?</h1>
            <div class="lines"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul class="greenroom-spec">
                        <li>
                            <div class="circle">
                                <img src="{{asset('frontend/assets/img/icon/icon_50.png')}}" alt=""/>
                            </div>
                            <span class="title">Ample of Options</span>
                            <div class="clearfix mb-2"></div>
                            <span class="more">Ample of option to select proper accommodation. We have Compile
                                    Larger Database of
                                    PG, Corporate
                                    PG(Service App.) and Hostel of entire city in which we provide our services, so it
                                    will help you to choice proper
                                    accommodation according to your requirement.</span>
                        </li>
                        <li>
                            <div class="circle">
                                <img src="{{asset('frontend/assets/img/icon/icon_51.png')}}" alt=""/>
                            </div>
                            <span class="title">No Brokerage</span>
                            <div class="clearfix mb-2"></div>
                            <span class="more">We have started these services as a nonprofit base organization. So,
                                    we are not
                                    taking charge from property seeker
                                    to use our portal database for select proper accommodation according to their
                                    requirement. We directly connect you
                                    to verified owners to save brokerage.</span>
                        </li>
                        <li>
                            <div class="circle">
                                <img src="{{asset('frontend/assets/img/icon/icon_52.png')}}" alt=""/>
                            </div>
                            <span class="title">Admin Support</span>
                            <div class="clearfix mb-2"></div>
                            <span class="more">Our admin staff communicates with you and suggests how to get right
                                    accommodation
                                    by using greenroom and also give
                                    suggestion which option is appropriate according to your requirement.</span>
                        </li>
                        <li>
                            <div class="circle">
                                <img src="{{asset('frontend/assets/img/icon/icon_53.png')}}" alt=""/>
                            </div>
                            <span class="title">Pick Up & Drop Service</span>
                            <div class="clearfix mb-2"></div>
                            <span class="more">If you are new in city and not familiar with different location in
                                    which you
                                    searching accommodation in that case
                                    you hire (Book) our pickup & drop service. In this service our service person
                                    schedules your property visit and also
                                    suggests you which option is suitable according to your requirement. And after
                                    visiting accommodation drop you at
                                    your covenant place.</span>
                        </li>
                        <li>
                            <div class="circle">
                                <img src="{{asset('frontend/assets/img/icon/icon_54.png')}}" alt=""/>
                            </div>
                            <span class="title">Documentation Support</span>
                            <div class="clearfix mb-2"></div>
                            <span class="more">After finalization of accommodation, if your PG owner not gives you
                                    agreement
                                    support. Than we will support you in
                                    making agreement and police verification if required. If your company requires
                                    payment receipt for housing allowance
                                    we will provide payment receipt format in our website so you can easily download
                                    it.</span>
                        </li>
                        <!-- <li>
                            <div class="circle">
                                <img src="assets/img/icon/icon_move_in_out.png" alt="">
                            </div>
                            <span>Easy <br>Move in / out</span>
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>
        <!-- why choose greenroom? end -->
    </div>
    <!-- middle content end -->
    <!-- refer & earn start-->
    <div class="refer">
        <div class="parallax">
            <div class="container">
                <div class="content">
                    <h1>Help your friends <br/>who are looking for <br/>a house</h1>
                    <a href="javascript:void(0)" title="List Your Property"
                       class="btn btn-lg btn-success mt-4 refer_earn">REFER & EARN</a>
                </div>
            </div>
            <div class="transperant"></div>
        </div>
    </div>
    <!-- refer & earn end-->
    <!-- How it works Start-->
    <div class="why_choose_greenroom">
        <div class="titleBox">
            <h1 class="title">How Greenroom Works?</h1>
            <div class="lines"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul class="greenroom-spec greenroom-works">
                        <li>
                            <a href="how_it_works.html">
                                <div class="circle">
                                    <img src="{{asset('frontend/assets/img/icon/icon_search.png')}}" alt=""/>
                                </div>
                                <span class="title">Search by location</span>
                            </a>
                        </li>
                        <li>
                            <a href="how_it_works.html">
                                <div class="circle">
                                    <img src="{{asset('frontend/assets/img/icon/icon_shortlist.png')}}" alt=""/>
                                </div>
                                <span class="title">Connect with us</span>
                            </a>
                        </li>
                        <li>
                            <a href="how_it_works.html">
                                <div class="circle">
                                    <img src="{{asset('frontend/assets/img/icon/icon_contact.png')}}" alt=""/>
                                </div>
                                <span class="title">Schedule a visit on call</span>
                            </a>
                        </li>
                        <li>
                            <a href="how_it_works.html">
                                <div class="circle">
                                    <img src="{{asset('frontend/assets/img/icon/icon_schedule.png')}}" alt=""/>
                                </div>
                                <span class="title">Book PG</span>
                            </a>
                        </li>
                        <li>
                            <a href="how_it_works.html">
                                <div class="circle">
                                    <img src="{{asset('frontend/assets/img/icon/icon_move_in.png')}}" alt=""/>
                                </div>
                                <span class="title">Move in</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- How it works End-->
    <!-- Are you looking for pg start-->
    <div class="areyou">
        <div class="container">
            <div class="row d-flex">
                <div class="col-12 col-lg-6 col-md-6 order-1 order-sm-1 order-md-0 order-lg-0 order-xl-0">
                    <div class="cnt lside">
                        <div>
                            <h1>
                                Are you for <span>PG, Corporate PG</span><span> & Hostel?</span>
                            </h1>
                            <ul class="benifits">
                                <li>Visit Website and view property according to your requirement.</li>
                                <li>Register with us it's simple.</li>
                                <li>Put your selected option into Shortlist List (favorite list).</li>
                                <li>You can visit property by yourself.</li>
                                <li>If you want help, our admin staff will provide help you in select property
                                    according to your requirement.
                                </li>
                                <li>You can hire pickup drop facilities.</li>
                                <li>You can share your requirement through post requirement function.</li>
                                <li>If you don't have time to find best accommodation than just call us on
                                    999-888-2000
                                </li>
                            </ul>
                            <a href="javascript:void(0)" title="Post Your Rerequirement"
                               class="btn btn-lg btn-success text-uppercase shadow-sm">Post Your Requirement</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 col-md-6 order-0 order-sm-0 order-md-1 order-lg-1 order-xl-1">
                    <div class="img lside-r">
                        <img src="{{asset('frontend/assets/img/bnr10.jpg')}}" alt/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Are you looking for pg end-->
    <!-- Are you pg owners start-->
    <div class="areyou ruowner mt-3">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 col-md-6">
                    <div class="img rside-l">
                        <img src="{{asset('frontend/assets/img/04.jpg')}}" alt/>
                    </div>
                </div>
                <div class="col-12 col-lg-6 col-md-6">
                    <div class="cnt rside">
                        <h1>Are you <span>PG Owner?</span></h1>
                        <ul class="benifits">
                            <li>Post Your Property Advertisement For Free.</li>
                            <li>As an owner you can list your property in a few minutes.</li>
                            <li>Call us on 999-888-2000 to get help for property listing.</li>
                            <li>Our highly trained customer support team will help you.</li>
                            <li>If you don't have time to post your property than send you property details and good
                                quality photos of your property on given number.
                            </li>
                            <li>If you are not able to click good quality photos of your property, our executive
                                come at your place and click photographs than these photographs will post in our
                                portal.
                            </li>
                            <li>You can manage your property by yourself with the function Edit, Active and
                                Inactive.
                            </li>
                            <li>We promote your property in different ways online and offline.</li>
                            <li>Advertise your property to millions of customers instantly.</li>
                            <li>Only verified and genuine tenants you will get.</li>
                            <li>We will take care of owner satisfaction as well as client satisfaction.</li>
                        </ul>
                        <a href="post_your_property.html" title="Post Your Property"
                           class="btn btn-lg btn-success text-uppercase shadow-sm ml-4">Post Your Property</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Are you pg owners end-->
    <!-- Trending Properties Start-->
    <!-- <div class="trending_properties">
        <div class="titleBox">
            <h1 class="title">Trending Properties</h1>
            <div class="lines"></div>
        </div>
        <div class="clearfix"></div>
        <div class="property_list">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xl-12 col-md-12">
                        <div class="owl-carousel" id="responsive">
                            <div class="block">
                                <div class="item rounded">
                                    <div class="thumb">
                                        <img src="assets/img/thumb/thumb_01.jpg" class="" alt="">
                                    </div>
                                    <div class="content">
                                        <div class="title">Boys PG in 3rd Floor</div>
                                        <span class="place">Ghodasar, Ahmedabad</span>
                                        <div class="clearfix"></div>
                                        <div class="btm-block">
                                            <div class="price">
                                                <i class="fas fa-rupee-sign"></i> 8,000 <span
                                                    class="onwards">onwords</span>
                                            </div>
                                            <div>
                                                <a href="javascript:void(0)" class="btn btn-sm btn-success"
                                                    title="">Subscribe</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block">
                                <div class="item rounded">
                                    <div class="thumb">
                                        <img src="assets/img/thumb/thumb_01.jpg" class="" alt="">
                                    </div>
                                    <div class="content">
                                        <div class="title">Boys PG in 3rd Floor</div>
                                        <span class="place">Ghodasar, Ahmedabad</span>
                                        <div class="clearfix"></div>
                                        <div class="btm-block">
                                            <div class="price">
                                                <i class="fas fa-rupee-sign"></i> 8,000 <span
                                                    class="onwards">onwords</span>
                                            </div>
                                            <div>
                                                <a href="javascript:void(0)" class="btn btn-sm btn-success"
                                                    title="">Subscribe</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block">
                                <div class="item rounded">
                                    <div class="thumb">
                                        <img src="assets/img/thumb/thumb_01.jpg" class="" alt="">
                                    </div>
                                    <div class="content">
                                        <div class="title">Boys PG in 3rd Floor</div>
                                        <span class="place">Ghodasar, Ahmedabad</span>
                                        <div class="clearfix"></div>
                                        <div class="btm-block">
                                            <div class="price">
                                                <i class="fas fa-rupee-sign"></i> 8,000 <span
                                                    class="onwards">onwords</span>
                                            </div>
                                            <div>
                                                <a href="javascript:void(0)" class="btn btn-sm btn-success"
                                                    title="">Subscribe</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block">
                                <div class="item rounded">
                                    <div class="thumb">
                                        <img src="assets/img/thumb/thumb_01.jpg" class="" alt="">
                                    </div>
                                    <div class="content">
                                        <div class="title">Boys PG in 3rd Floor</div>
                                        <span class="place">Ghodasar, Ahmedabad</span>
                                        <div class="clearfix"></div>
                                        <div class="btm-block">
                                            <div class="price">
                                                <i class="fas fa-rupee-sign"></i> 8,000 <span
                                                    class="onwards">onwords</span>
                                            </div>
                                            <div>
                                                <a href="javascript:void(0)" class="btn btn-sm btn-success"
                                                    title="">Subscribe</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block">
                                <div class="item rounded">
                                    <div class="thumb">
                                        <img src="assets/img/thumb/thumb_01.jpg" class="" alt="">
                                    </div>
                                    <div class="content">
                                        <div class="title">Boys PG in 3rd Floor</div>
                                        <span class="place">Ghodasar, Ahmedabad</span>
                                        <div class="clearfix"></div>
                                        <div class="btm-block">
                                            <div class="price">
                                                <i class="fas fa-rupee-sign"></i> 8,000 <span
                                                    class="onwards">onwords</span>
                                            </div>
                                            <div>
                                                <a href="javascript:void(0)" class="btn btn-sm btn-success"
                                                    title="">Subscribe</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block">
                                <div class="item rounded">
                                    <div class="thumb">
                                        <img src="assets/img/thumb/thumb_01.jpg" class="" alt="">
                                    </div>
                                    <div class="content">
                                        <div class="title">Boys PG in 3rd Floor</div>
                                        <span class="place">Ghodasar, Ahmedabad</span>
                                        <div class="clearfix"></div>
                                        <div class="btm-block">
                                            <div class="price">
                                                <i class="fas fa-rupee-sign"></i> 8,000 <span
                                                    class="onwards">onwords</span>
                                            </div>
                                            <div>
                                                <a href="javascript:void(0)" class="btn btn-sm btn-success"
                                                    title="">Subscribe</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block">
                                <div class="item rounded">
                                    <div class="thumb">
                                        <img src="assets/img/thumb/thumb_01.jpg" class="" alt="">
                                    </div>
                                    <div class="content">
                                        <div class="title">Boys PG in 3rd Floor</div>
                                        <span class="place">Ghodasar, Ahmedabad</span>
                                        <div class="clearfix"></div>
                                        <div class="btm-block">
                                            <div class="price">
                                                <i class="fas fa-rupee-sign"></i> 8,000 <span
                                                    class="onwards">onwords</span>
                                            </div>
                                            <div>
                                                <a href="javascript:void(0)" class="btn btn-sm btn-success"
                                                    title="">Subscribe</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block">
                                <div class="item rounded">
                                    <div class="thumb">
                                        <img src="assets/img/thumb/thumb_01.jpg" class="" alt="">
                                    </div>
                                    <div class="content">
                                        <div class="title">Boys PG in 3rd Floor</div>
                                        <span class="place">Ghodasar, Ahmedabad</span>
                                        <div class="clearfix"></div>
                                        <div class="btm-block">
                                            <div class="price">
                                                <i class="fas fa-rupee-sign"></i> 8,000 <span
                                                    class="onwards">onwords</span>
                                            </div>
                                            <div>
                                                <a href="javascript:void(0)" class="btn btn-sm btn-success"
                                                    title="">Subscribe</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block">
                                <div class="item rounded">
                                    <div class="thumb">
                                        <img src="assets/img/thumb/thumb_01.jpg" class="" alt="">
                                    </div>
                                    <div class="content">
                                        <div class="title">Boys PG in 3rd Floor</div>
                                        <span class="place">Ghodasar, Ahmedabad</span>
                                        <div class="clearfix"></div>
                                        <div class="btm-block">
                                            <div class="price">
                                                <i class="fas fa-rupee-sign"></i> 8,000 <span
                                                    class="onwards">onwords</span>
                                            </div>
                                            <div>
                                                <a href="javascript:void(0)" class="btn btn-sm btn-success"
                                                    title="">Subscribe</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block">
                                <div class="item rounded">
                                    <div class="thumb">
                                        <img src="assets/img/thumb/thumb_01.jpg" class="" alt="">
                                    </div>
                                    <div class="content">
                                        <div class="title">Boys PG in 3rd Floor</div>
                                        <span class="place">Ghodasar, Ahmedabad</span>
                                        <div class="clearfix"></div>
                                        <div class="btm-block">
                                            <div class="price">
                                                <i class="fas fa-rupee-sign"></i> 8,000 <span
                                                    class="onwards">onwords</span>
                                            </div>
                                            <div>
                                                <a href="javascript:void(0)" class="btn btn-sm btn-success"
                                                    title="">Subscribe</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- Trending Properties End-->
    <!-- Property Report Start-->
    <div class="property_availability">
        <div class="container">
            <div class="owl-carousel" id="propertyAvailability">
                <div class="item">
                    <div class="block">
                        <img src="{{asset('frontend/assets/img/icon/icon_happy_customer.png')}}" class="icon" alt=""/>
                        <h1>5000+</h1>
                        <span>Visit</span>
                    </div>
                </div>
                <!-- <div class="item">
                    <div class="block">
                        <img src="assets/img/icon/icon_houses_across_india.png" class="icon" alt="">
                        <h1>16800 +</h1>
                        <span>Houses across in India</span>
                    </div>
                </div> -->
                <!-- <div class="item">
                    <div class="block boderNo">
                        <img src="assets/img/icon/icon_saving_brokerage.png" class="icon" alt="">
                        <h1>322700000 +</h1>
                        <span>Saving made on brokerage</span>
                    </div>
                </div> -->
                <!-- <div class="item">
                    <div class="block">
                        <img src="assets/img/icon/icon_cities_in_india.png" class="icon" alt="">
                        <h1>12 +</h1>
                        <span>Cities in India</span>
                    </div>
                </div> -->
                <div class="item">
                    <div class="block">
                        <img src="{{asset('frontend/assets/img/icon/icon_55.png')}}" class="icon" alt=""/>
                        <h1>1000</h1>
                        <span>PG</span>
                    </div>
                </div>
                <div class="item">
                    <div class="block">
                        <img src="{{asset('frontend/assets/img/icon/icon_56.png')}}" class="icon" alt=""/>
                        <h1>750</h1>
                        <span>Corporate PG</span>
                    </div>
                </div>
                <div class="item">
                    <div class="block">
                        <img src="{{asset('frontend/assets/img/icon/icon_57.png')}}" class="icon" alt=""/>
                        <h1>500</h1>
                        <span>Hostel</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Property Report End-->
    <!-- Protional Videos and Ads Start Here -->
    <div class="promotionalAd">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-6 col-lg-7 col-xl-8">
                    <h1 class="title">Why move in to a Greenroom?</h1>
                    <p>
                        Vibrant community, hassle free stay, and simple budget friendly
                        spaces - Greenroom has it all
                    </p>
                    <iframe class="promovideo" src="https://www.youtube.com/embed/tgbNymZ7vqY"></iframe>
                </div>
                <div class="col-12 col-sm-6 col-lg-5 col-xl-4">
                    <img src="{{asset('frontend/assets/img/bnr11.jpg')}}" class="img-fluid" alt=""/>
                </div>
            </div>
        </div>
    </div>

<style>
.select2-results__option {
  padding-right: 10px;
  vertical-align: middle;
}
.select2-results__option:before {
  content: "";
  display: inline-block;
  position: relative;
  height: 20px;
  width: 20px;
  border: 2px solid #e9e9e9;
  border-radius: 4px;
  background-color: #fff;
  margin-right: 10px;
  vertical-align: middle;
}
.select2-results__option[aria-selected=true]:before {
  font-family:fontAwesome;
  content: "\f00c";
  color: #fff;
  background-color: #f77750;
  border: 0;
  display: inline-block;
  padding-left: 3px;
}
.select2-container--default .select2-results__option[aria-selected=true] {
	background-color: #fff;
}
.select2-container--default .select2-results__option--highlighted[aria-selected] {
	background-color: #eaeaeb;
	color: #272727;
}
.select2-container--default .select2-selection--multiple {
	margin-bottom: 10px;
}
.select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple {
	border-radius: 4px;
}
.select2-container--default.select2-container--focus .select2-selection--multiple {
	border-color: #f77750;
	border-width: 2px;
}
.select2-container--default .select2-selection--multiple {
	border-width: 2px;
}
.select2-container--open .select2-dropdown--below {
	
	border-radius: 6px;
	box-shadow: 0 0 10px rgba(0,0,0,0.5);

}
.select2-selection .select2-selection--multiple:after {
	content: 'hhghgh';
}
/* select with icons badges single*/
.select-icon .select2-selection__placeholder .badge {
	display: none;
}
.select-icon .placeholder {
	display: none;
}
.select-icon .select2-results__option:before,
.select-icon .select2-results__option[aria-selected=true]:before {
	display: none !important;
	/* content: "" !important; */
}
.select-icon  .select2-search--dropdown {
	display: none;
}
</style>


@endsection
@section('footer')
    <script>
        $(document).ready(function(){

			
		$("#propertyTypeId").select2({
			closeOnSelect : false,
			placeholder : "Select Property Type",
			allowHtml: true,
			allowClear: true,
			tags: true // создает новые опции на лету
		});


		$("#forMaleOrFemaleId").select2({
			closeOnSelect : false,
			placeholder : "Accommodation For",
			allowHtml: true,
			tags: true // создает новые опции на лету
		});
		$("#forMaleOrFemaleId").val("");


		$("#searchByLoc").val("");
		$("#searchByLoc").select2({
			closeOnSelect : false,
			placeholder : "Search By Location",
			allowHtml: true,
			allowClear: true,
			tags: true // создает новые опции на лету
		});


			$('.icons_select2').select2({
				width: "100%",
				templateSelection: iformat,
				templateResult: iformat,
				allowHtml: true,
				placeholder: "Placeholder",
				dropdownParent: $( '.select-icon' ),//обавили класс
				allowClear: true,
				multiple: false
			});
	

				function iformat(icon, badge,) {
					var originalOption = icon.element;
					var originalOptionBadge = $(originalOption).data('badge');
				 
					return $('<span><i class="fa ' + $(originalOption).data('icon') + '"></i> ' + icon.text + '<span class="badge">' + originalOptionBadge + '</span></span>');
				}



            $(document).on("click","#signup_btn",function(){
                alert("sign_up");
                var data=$("#signup_modal").serialize();
                if(data) {
                    $.ajax({
                        url: "{{route('do_sign_up')}}",
                        type: "POST",
                        data: data,
                        success: function (response) {
                            if (response.status) {

                                $("#registrationModal").modal("hide");
                                $("#loginModal").modal("show");

                                $("#signup_success").html(response.message);

                            } else {
                                var html = "";
                                $.each(response.message, function (key, item) {
                                    html += item;
                                    html += "<br>";
                                });
                                $("#signup_error").html(html);
                                $("#signup_error").addClass("error");
                            }
                        }
                    });
                }else{
                    html="All field is required";
                    $("#signup_error").html(html);
                }
            });

            $(document).on("click","#login",function(){
//                alert("login");
                var data=$("#login_modal").serialize();
//                    alert(data);
                if(data){
                    $.ajax({
                        url: "{{route('do_login')}}",
                        type: "POST",
                        data: data,
                        success:function(response){
                            console.log(response);

                            if(response.status){
                                location.reload();
                            }else{
                                $("#login_error").html(response.message);
                                $("#login_error").addClass("error");
                            }
                        }
                    });
                }
            });


            {{--$(document).on("click",".search_icon",function(){--}}
                   {{--var type=$("#propertyType").val();--}}
                   {{--var accommodation =$("#forMaleOrFemale").val();--}}
                   {{--var search_area=$("#searchByLoc").val();--}}

                   {{--$.ajax({--}}
                        {{--url:"{{route('search')}}",--}}
                        {{--type:"GET",--}}
                        {{--data:{type:type,accommodation:accommodation,search_area:search_area},--}}
                       {{--success:function(){--}}
                            {{----}}
                       {{--}--}}
                   {{--});--}}
            {{--});--}}
        });
    </script>
@endsection