@extends('layouts.frontend.app')
@section('content')
        <!-- middle content start -->
        <div class="about_us addstyle">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="titleBoxInner">
                            <div class="in">
                                <h1 class="title">About Us</h1>
                                <div class="lines"></div>
                            </div>
                        </div>
                        <p class="text-center description"><span class="stronger">Greenroom</span> is managed by
                            property idea
                            group. We have unique understanding of real estate
                            <br>field. We have young dynamical professional team. It is a professionally managed
                            organization
                            <br>provides online information for corporate, PG and Hostel. It's helpful for property
                            seekers,
                            <br>who is looking for stay.</p>
                    </div>
                </div>
            </div>
            <!-- Property Report Start-->
            <div class="property_availability mt-0">
                <div class="container">
                    <div class="owl-carousel" id="propertyAvailability">
                        <div class="item">
                            <div class="block">
                                <img src="{{asset('frontend/assets/img/icon/icon_happy_customer.png')}}" class="icon" alt="" />
                                <h1>5000+</h1>
                                <span>Visit</span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="block">
                                <img src="{{asset('frontend/assets/img/icon/icon_55.png')}}" class="icon" alt="" />
                                <h1>1000</h1>
                                <span>PG</span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="block">
                                <img src="{{asset('frontend/assets/img/icon/icon_56.png')}}" class="icon" alt="" />
                                <h1>750</h1>
                                <span>Corporate PG</span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="block">
                                <img src="{{asset('frontend/assets/img/icon/icon_57.png')}}" class="icon" alt="" />
                                <h1>500</h1>
                                <span>Hostel</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Property Report End-->
        </div>
        <!-- Protional Videos and Ads End Here -->
    @endsection