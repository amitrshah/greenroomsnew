

    Hello {{ $full_name }},<br>

    Thank you for creating account in {{ env("APP_NAME") }}. Your account is successfully created. Your first step will be activation of your account.

    <br />
    <br />

    Please click <a href="{{ $url }}">HERE</a> to activate your account.
