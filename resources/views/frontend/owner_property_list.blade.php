@extends('layouts.frontend.app')
@section('content')

<br><br><br><br>
<!-- middle content start -->
<div class="dashboardContainer mb-5">
    <div class="dashboardHeader">
        <div class="dashboardContent">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                        <div class="card shadow-sm">
                            <img src="{{asset('frontend/assets/img/bnr10.jpg')}}" alt="" class="img-fluid" />
                            <div class="userInfo">
                                <h1>{{\Auth::user()->name}}</h1>
                                <span>Owner ID {{\Auth::user()->id}}</span>
                            </div>
                            <div class="card-body pt-0">
                                <h4 class="city">City: {{!empty($city_name) ? $city_name:""}}</h4>
                                <h1 class="state mt-1">State: {{($state_name) ? $state_name:""}}</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-9">
                        <h1 class="dbtitle">{{($type)? ucwords($type):"All" }} Property List</h1>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <div class="card shadow-sm mt-4">
                                    <div class="card-body pt-2">
                                        <div class="searchResult">
                                            <div class="row">
                                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                    <div class="refineResult">
                                                        <div>
                                                            <p>
                                                                <span class="total_property_count">{{$total_property}}</span> Property available
                                                            </p>
                                                            <div></div>
                                                        </div>
                                                        <!-- 1st -->
                                                        <div class="">
                                                                        {!! $view !!}
                                                            </div>
                                                        </div>


                                                    {{--{{ $result->links() }}--}}
                                                         {{--<nav aria-label="Page navigation example">--}}
                                                            {{--<ul--}}
                                                                    {{--class="pagination pagination-sm justify-content-center">--}}
                                                                {{--<li class="page-item">--}}
                                                                    {{--<a class="page-link"--}}
                                                                       {{--href="javascript:void(0)"--}}
                                                                       {{--tabindex="-1">Previous</a>--}}
                                                                {{--</li>--}}
                                                                {{--<li class="page-item active"><a--}}
                                                                            {{--class="page-link"--}}
                                                                            {{--href="javascript:void(0)">1</a></li>--}}
                                                                {{--<li class="page-item"><a class="page-link"--}}
                                                                                         {{--href="javascript:void(0)">2</a></li>--}}
                                                                {{--<li class="page-item"><a class="page-link"--}}
                                                                                         {{--href="javascript:void(0)">3</a></li>--}}
                                                                {{--<li class="page-item"><a class="page-link"--}}
                                                                                         {{--href="javascript:void(0)">4</a></li>--}}
                                                                {{--<li class="page-item">--}}
                                                                    {{--<a class="page-link"--}}
                                                                       {{--href="javascript:void(0)">Next</a>--}}
                                                                {{--</li>--}}
                                                            {{--</ul>--}}
                                                        {{--</nav>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Protional Videos and Ads End Here -->
@endsection

@section('footer')
    <script>
        $(document).on("click",".property_delete",function(e){
            e.preventDefault();
            var is_confirm=confirm("Are you sure?");
            var href=$(this).attr("data-href");
            var id=$(this).attr("data-id");
            if(is_confirm){
                $.ajax({
                    type:"POST",
                    url:href,
                    data:{"id":id,"_token":"{{csrf_token()}}"},
                    success:function(id){
                        $("#content-"+id).remove();
                        $("#sub-content-"+id).remove();
                        var total=$(".total_property_count").text();
                        $(".total_property_count").text(total-1);

                    }
                });
            }else{
                return false;
            }
        });
    </script>
@endsection