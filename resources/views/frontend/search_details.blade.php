@extends('layouts.frontend.app')
@section('content')
    {{--{{dd($data)}}--}}
    <!-- banner Start Here -->

    <br><br><br><br><br><br>
    <div class="greenroom-banner greenroom-banner-inner addstyle">
        <div class="content">
            <div class="contentContainer">
                <div class="content-row">
                    <form action="{{route("search")}}" method="GET" id="form-detail-search">
                        <div class="homeSearchArea">
                            <div class="category_name property_type">
                                <select name="propertyType" id="propertyType" class="select_customarrow"></select>
                            </div>
                            <div class="category_name for_mal_or_female">
                                <select name="forMaleOrFemale" id="forMaleOrFemale" class="select_customarrow"></select>
                            </div>
                            <div class="category_name search_by_loc">
                                <select name="searchByLoc[]" id="searchByLoc" class="select_customarrow">
                                    @foreach($ahmedabad_loc_ as $item)
                                        @if(!empty($area_id))
                                            @foreach($area_id as $id)
                                                @if($id==$item->areaID)
                                                    <option value="{{$item->areaID}}" selected>{{$item->areaName}}</option>
                                                @else
                                                    <option value="{{$item->areaID}}">{{$item->areaName}}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                        <option value="{{$item->areaID}}">{{$item->areaName}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="category_name search_icon">
                                <a href="javascript:void(0)" id="btn-search-submit" form-id="form-detail-search"> <img
                                            src="{{asset('frontend/assets/img/icon/icon_search.svg')}}"
                                            class="icon-search" alt=""/></a>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- banner End Here -->
    <!-- middle content start -->
    <div class="prop_head shadow" id="prop_head">
        <div class="container">
            <div class="row bcrumbs">
                <div
                        class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 order-1 order-sm-0 order-md-1 order-lg-0 order-xl-0">
                    <ul class="breadcrumbs">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Search</a></li>
                        <li>{{$data["numberOfRoom"]}} Residential Apartment in Ranip</li>
                    </ul>
                </div>
                <div
                        class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 order-0 order-sm-1 order-md-0 order-lg-1 order-xl-1">
                    <div class="postingInfo">
                        Posted on {{$data["created_at"]}} | Property ID: {{$data["property_code"]}}
                    </div>
                </div>
            </div>
            <div class="row propInfo">
                <div class="col-12 col-sm-12 col-md-9 col-lg-8 col-xl-9 pl-xl-0">
                    <div class="prop_d1">
                        <div class="g1">
                            <h1 class="rent_per_month">
                                <span><i class="fas fa-rupee-sign"></i></span>{{$data["price"]}}
                            </h1>
                            <div class="perMonth">Per Month</div>
                        </div>
                        <div>
                            <h1 class="prop_title">Paying Guest</h1>
                            <div class="prop_sml_title">
                                In Satellite, Ahmedabad West,
                                Ahmedabad
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-3 col-lg-4 col-xl-3 pr-xl-0">
                    <div class="contact_n_shortlist">
                        <button type="button" name="view_contact_number"
                                class="btn btn-lg btn-success  btn-block shadow-sm view_contact_number mb-1">
                            Contact Owner
                        </button>
                        <button type="button" name="view_contact_number"
                                class="btn btn-primary btn-lg btn-block shadow-sm">
                            <i class="fas fa-star"></i> Shortlist
                        </button>
                    </div>
                </div>
            </div>
            <ul class="nav nav_details" id="nav_details">
                <li class="nav-item"><a class="nav-link active" href="#overview">Overview</a></li>
                <li class="nav-item"><a class="nav-link" href="#pgdesc">Description</a></li>
                <li class="nav-item"><a class="nav-link" href="#pgAmenities">Amenities</a></li>
                <li class="nav-item"><a class="nav-link" href="#ownerDetails">Owner Details</a></li>
                <li class="nav-item"><a class="nav-link" href="#location">Location</a></li>
                <li class="nav-item"><a class="nav-link" href="#recommendations">Recommendations</a></li>
            </ul>
        </div>
    </div>
    <div class="container mb-0 mb-sm-5 mb-md-5 mb-lg-5 mb-xl-5">
        <div class="tabContiner">
            <div id="overview" class="section-wrapper">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
                        <div class="gallery shadow-sm">
                            <ul id="imageGallery">
                                <li data-thumb="{{asset('frontend/assets/img/product/01.jpg')}}" data-src="{{asset('assets/img/product/01.jpg')}}">
                                    <img src="{{asset('frontend/assets/img/product/01.jpg')}}" alt="" />
                                </li>
                                <li data-thumb="{{asset('frontend/assets/img/product/02.jpg')}}" data-src="{{asset('assets/img/product/02.jpg')}}">
                                    <img src="{{asset('frontend/assets/img/product/02.jpg')}}" alt="" />
                                </li>
                                <li data-thumb="{{asset('frontend/assets/img/product/03.jpg')}}" data-src="{{asset('assets/img/product/03.jpg')}}">
                                    <img src="{{asset('frontend/assets/img/product/03.jpg')}}" alt="" />
                                </li>
                            </ul>
                        </div>
                        <p class="d-flex justify-content-end text-primary">Total 5 Property Photos</p>
                    </div>
                    <div
                            class="col-12 col-sm-12 col-md-6 col-lg-7 col-xl-7 prop_detail mt-4 mt-sm-4 mt-md-0 mt-lg-0 mt-xl-0">
                        <ul class="property_specification shadow-sm">
                            <li>
                                <ul>
                                    <li>
                                        <h1><i class="fas fa-home"></i> Accommodation for</h1>
                                        <p>{{$data["gender"]}}</p>
                                    </li>
                                    <li>
                                        <h1><i class="far fa-user-circle"></i> Suitable for</h1>
                                        <p>{{$data["suitable_for"]}}</p>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <ul>
                                    <li>
                                        <h1><i class="fas fa-home"></i> Address</h1>
                                        <p>
                                           {{$data["address"]}}
                                        </p>
                                    </li>
                                    <li>
                                        <h1><i class="fas fa-bed"></i> Bed Sharing</h1>
                                        <p>{{$data["sharingID"]}}</p>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <ul>
                                    <li>
                                        <h1><i class="far fa-clock"></i> Availability</h1>
                                        <p>{{$data["numberOfRoom"]}}</p>
                                    </li>
                                    <li>
                                        <h1><i class="fas fa-home"></i> Availability Type</h1>
                                        <p>{{$data["availabilityType"]}}</p>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <ul>
                                    <li>
                                        <h1><i class="fas fa-home"></i> Floor</h1>
                                        <p>
{{$data["property"]["floorID"]}} Of {{$data["property"]["total_floor"]}}
										</p>
                                    </li>
                                    <li>
                                        <h1><i class="fas fa-home"></i> Property Age</h1>
                                        <p>0 to 1 year old</p>
                                    </li>
                                </ul>
                            </li>
							<li>
                                <ul>
                                    <li>
                                        <h1><i class="far fa-clock"></i> Property Status</h1>
                                        <p>{{$data["property"]["status"]}}</p>
                                    </li>
                                    <li>
                                        <h1><i class="fas fa-home"></i> Available From</h1>
                                        <p>{{$data["property"]["avaialble_from"]}}</p>
                                    </li>
                                </ul>
                            </li>
                            <!-- <li>
                                <h1><i class="fas fa-home"></i> Accommodation for</h1>
                                <p>Female</p>
                            </li>
                            <li>
                                <h1><i class="far fa-user-circle"></i> Suitable for</h1>
                                <p>Working Professional</p>
                            </li> -->
                            <!-- <li>
                                <h1><i class="fas fa-home"></i> Address</h1>
                                <p>
                                    3rd Floor, 310, Aayushi Homes, Near Anandnagar Cross Road,
                                    Satellite, Ahmedabad West, Gujarat
                                </p>
                            </li>
                            <li>
                                <h1><i class="fas fa-bed"></i> Bed Sharing</h1>
                                <p>Single</p>
                            </li> -->
                            <!-- <li>
                                <h1><i class="far fa-clock"></i> Availability</h1>
                                <p>4BHK</p>
                            </li>
                            <li>
                                <h1><i class="fas fa-home"></i> Availability Type</h1>
                                <p>High Rise Apartment</p>
                            </li> -->
                            <!-- <li>
                                <h1><i class="fas fa-home"></i> Floor</h1>
                                <p>3<sup>rd</sup> out of 9<sup>th</sup></p>
                            </li>
                            <li>
                                <h1><i class="fas fa-home"></i> Property Age</h1>
                                <p>0 to 1 year old</p>
                            </li> -->
                        </ul>
                    </div>
                </div>
                <div class="mt-3 prop_spec">
                    <div class="section">
                        <h1 class="heading"><span>Notice Period:</span> {{$data["notice_period"]}}</h1>
                    </div>
                    <div class="section">
                        <h1 class="heading"><span>Sqft/Sqyard:</span> {{$data["total_square_feet"]}}</h1>
                    </div>
                    <div class="section">
                        <h1 class="heading"><span>Extra Charges:</span> Maintenence</h1>
                    </div>
                    <div class="section">
                        <h1 class="heading"><span>Facing:</span> {{$data["Facing"]}}</h1>
                    </div>
                </div>
            </div>
            <div id="pgdesc" class="section-wrapper">
                <div class="row mt-3">
                    <div class="col-12">
                        <h1 class="prop_title_new">Description</h1>
                        <p class="font13">{!! $data["description"] !!}
                        </p>
                    </div>
                </div>
            </div>
            {{--{{dd($extra_charge_array)}}--}}
            <div id="pgAmenities" class="section-wrapper">
                <div class="mt-1 prop_info">
                    <h1 class="prop_title_new">Extra Charges</h1>
                    <div class="section">
                        @foreach($extra_charge_array as $charge)
                            <?php    $title__=str_replace(" ","_",strtolower($charge));?>
                        <h1 class="heading available"><img src="{{asset('frontend/assets/img/icon/icon_'.$title__.'.png')}}" alt="Maid" />{{$title__}}</h1>
                            @endforeach
                    </div>
                </div>
                <div class="prop_info">
                    <h1 class="prop_title_new">Interior</h1>
                    <div class="section">
                        @foreach($property_interiors_array as $title)
                            <?php $available=""; ?>
                            {{--@foreach($data["society_aminities"] as $item)--}}
                            @if(in_array($title->title,$data["interiors"]))
                                <?php  $available="available";?>
                            @endif
                            @php
                                $title__=str_replace(" ","_",strtolower($title->title));
                            @endphp
                                <h1 class="heading {{$available}}"><img src="{{asset('frontend/assets/img/icon/icon_'.$title__.'.png')}}" alt="Maid" />{{$title->title}}</h1>
                            @endforeach
                    </div>
                </div>
                <div class="mt-1 prop_info">
                    <h1 class="prop_title_new">Meal</h1>
                    <div class="section">
                        @foreach($meals_array as $title)
                            <?php $available=""; ?>
                            {{--@foreach($data["society_aminities"] as $item)--}}
                            @if(in_array($title,$data["meals"]))
                                <?php  $available="available";?>
                            @endif
                            @php
                                $title__=str_replace(" ","_",strtolower($title));
                            @endphp
                        <h1 class="heading {{$available}}"><img src="{{asset('frontend/assets/img/icon/icon_'.$title__.'.png')}}" alt="Maid" />{{$title}}</h1>
                        @endforeach
                    </div>
                </div>
                <div class="prop_info">
                    <h1 class="prop_title_new">Facilities</h1>
                    <div class="section">
                        {{--{{dd($faciliety_array)}}--}}
                        @foreach($faciliety_array as $title)
                            <?php $available=""; ?>
                            {{--@foreach($data["society_aminities"] as $item)--}}
                            @if(in_array($title->title,$data["facilities"]))
                                    <?php  $available="available";

                                    ?>
                                @endif
                                @php $title__=str_replace(" ","_",strtolower($title->title)); @endphp
                        <h1 class="heading {{$available}}">
                            <img src="{{asset('frontend/assets/img/icon/icon_'.$title__.'.png')}}" alt="Maid" />
                            {{$title->title}}
                        </h1>
                            @endforeach

                    </div>
                </div>
                <div class="prop_info">
                    <h1 class="prop_title_new">Society Amenities</h1>
                    <div class="section">
                        @foreach($society_aminities_array as $title)
                            <?php $available=""; ?>
                        {{--@foreach($data["society_aminities"] as $item)--}}
                        @if(in_array($title->title,$data["society_aminities"]))
                               <?php  $available="available";

                                ?>

                        @endif
                            @php
                                 $title__=str_replace(" ","_",strtolower($title->title));
                            @endphp


                        <h1 class="heading {{$available}}">
                            <img src="{{asset('frontend/assets/img/icon/icon_'.$title__.'.'.'png')}}" alt="Gym" />
                            {{$title->title}}</h1>
                        {{--@endforeach--}}
                            @endforeach
                    </div>
                </div>
                <div class="prop_info">
                    <h1 class="prop_title_new">Rules</h1>
                    <?php
                        $rules=explode(',',$data["rules"]);
//                        dd($rules);
                            ?>
                    <div class="section">
                        @foreach($rules as $key=>$rule)
                        <h1 class="heading available"><img src="{{asset('frontend/assets/img/icon/icon_no_smoking.png')}}"
                                                           alt="No Smoking" />{{$rule}}
                        </h1>
                        @endforeach
                        {{--<h1 class="heading available"><img src="{{asset('frontend/assets/img/icon/icon_no_alcohol.png')}}"--}}
                                                           {{--alt="No Drinking" /> No--}}
                            {{--Drinking</h1>--}}
                        {{--<h1 class="heading available"><img src="{{asset('frontend/assets/img/icon/icon_no_guardian_stay_allow.png')}}"--}}
                                                           {{--alt="No Guardian Stay Allow" /> No Guardian Stay Allow</h1>--}}
                        {{--<h1 class="heading available"><img src="{{asset('frontend/assets/img/icon/icon_non_veg_not_allow.png')}}"--}}
                                                           {{--alt="No Non-Veg Cook Allow" /> No Non-Veg Cook Allow</h1>--}}
                        {{--<h1 class="heading available"><img src="{{asset('frontend/assets/img/icon/icon_no_late_night_entry.png')}}"--}}
                                                           {{--alt="No Late Night Entry" /> No Late Night Entry</h1>--}}
                    </div>
                </div>
            </div>
            <div id="ownerDetails" class="section-wrapper">
                <div class="prop_info">
                    <h1 class="prop_title_new mb-3">Owner Details</h1>
                    <div class="row">
                        <div class="col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                            <h1 class="prop_title_new">{{$data["name"]->name}}</h1>
                            <p class="pt-0 mt-0">Capital Realtors</p>
                            <button type="button" name="view_contact_number"
                                    class="btn btn-lg btn-success  btn-block shadow-sm view_contact_number mb-1">View
                                Contact Number</button>
                        </div>
                        <div class="col-12 col-sm-9 col-md-9 col-lg-9 col-xl-9">
                        </div>
                    </div>
                </div>
            </div>

            @if(!empty($data["latitude"]) && !empty($data["longitude"]))
                <div id="location" class="section-wrapper">
                    <div class="prop_info">
                        <h1 class="prop_title_new mb-3">Location</h1>
                        <div class="mapouter">
                            <div class="gmap_canvas">
                                <iframe id="gmap_canvas"
                                        src="https://www.google.com/maps?q={{$data["latitude"]}},{{$data["longitude"]}}&ie=UTF8&iwloc=&output=embed"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div id="recommendations" class="section-wrapper">
                <div class="prop_info">
                    <h1 class="prop_title_new mb-3">Recommendation</h1>
                    <div class="searchResult">
                        <div class="row">
                            <div class="col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                                <div class="refineResult">
                                    <div class="card rounded-0 mb-3">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <img src="{{asset('frontend/assets/img/thumb/thumb_01.jpg')}}" class="img-fluid"
                                                         alt="" />
                                                </div>
                                                <div class="col-12">
                                                    <div class="rside pt-2">
                                                        <a href="javascript:void(0)" class="title is-smaller"
                                                           title="">3
                                                            BHK
                                                            Residential
                                                            Apartment in Ranip</a>
                                                        <p><i class="fas fa-rupee-sign"></i> 22,000 Onwards </p>
                                                        <button type="button" name="view_contact_number"
                                                                class="btn btn-sm btn-success btn-block shadow-sm m-0">Enquire
                                                            Now</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                                <div class="refineResult">
                                    <div class="card rounded-0 mb-3">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <img src="{{asset('assets/img/thumb/thumb_01.jpg')}}" class="img-fluid"
                                                         alt="" />
                                                </div>
                                                <div class="col-12">
                                                    <div class="rside pt-2">
                                                        <a href="javascript:void(0)" class="title is-smaller"
                                                           title="">3
                                                            BHK
                                                            Residential
                                                            Apartment in Ranip</a>
                                                        <p><i class="fas fa-rupee-sign"></i> 22,000 Onwards </p>
                                                        <button type="button" name="view_contact_number"
                                                                class="btn btn-sm btn-success btn-block shadow-sm m-0">Enquire
                                                            Now</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a href="javascript:void(0)" class="goToTop"><i class="fas fa-arrow-alt-circle-up"></i></a>
    <!-- Protional Videos and Ads End Here -->
    {{--<footer class="footer">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">--}}
                    {{--<h1 class="m-title">Search By Localities</h1>--}}
                    {{--<ul class="specs">--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)" title="">Mens PG Hostels in Bodakdev</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)" title="">Mens PG Hostels in Satellite</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)" title="">Mens PG Hostels in Prahlad Nagar</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)" title="">Mens PG Hostels in Navrangpura</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)" title="">Mens PG Hostels in Gurukul</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">--}}
                    {{--<h1 class="m-title d-none d-sm-block d-md-block d-lg-block d-xl-block">--}}
                        {{--&nbsp;--}}
                    {{--</h1>--}}
                    {{--<ul class="specs">--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)" title="">Mens PG Hostels in Bodakdev</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)" title="">Mens PG Hostels in Satellite</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)" title="">Mens PG Hostels in Prahlad Nagar</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)" title="">Mens PG Hostels in Navrangpura</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)" title="">Mens PG Hostels in Gurukul</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">--}}
                    {{--<h1 class="m-title d-none d-sm-block d-md-block d-lg-block d-xl-block">--}}
                        {{--&nbsp;--}}
                    {{--</h1>--}}
                    {{--<ul class="specs">--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)" title="">Mens PG Hostels in Bodakdev</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)" title="">Mens PG Hostels in Satellite</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)" title="">Mens PG Hostels in Prahlad Nagar</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)" title="">Mens PG Hostels in Navrangpura</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)" title="">Mens PG Hostels in Gurukul</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">--}}
                    {{--<h1 class="m-title d-none d-sm-block d-md-block d-lg-block d-xl-block">--}}
                        {{--&nbsp;--}}
                    {{--</h1>--}}
                    {{--<ul class="specs">--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)" title="">Mens PG Hostels in Bodakdev</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)" title="">Mens PG Hostels in Satellite</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)" title="">Mens PG Hostels in Prahlad Nagar</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)" title="">Mens PG Hostels in Navrangpura</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)" title="">Mens PG Hostels in Gurukul</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="row">--}}
                {{--<div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">--}}
                    {{--<h1 class="m-title mt-4 mb-2">Corporate</h1>--}}
                    {{--<ul class="specs">--}}
                        {{--<li><a href="index.html" title="">Home</a></li>--}}
                        {{--<li><a href="about.html" title="">About Us</a></li>--}}
                        {{--<li><a href="about_greenroom.html" title="">About Greenroom</a></li>--}}
                        {{--<li><a href="search.html" title="">Search</a></li>--}}
                        {{--<li><a href="how_it_works.html" title="">How It Works?</a></li>--}}
                        {{--<li><a href="mission_vision.html" title="">Mission & Vision</a></li>--}}
                        {{--<li class="login">--}}
                            {{--<a href="javascript:void(0)" title="">Login</a>--}}
                        {{--</li>--}}
                        {{--<li class="register">--}}
                            {{--<a href="javascript:void(0)" title="">Sign Up</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">--}}
                    {{--<h1 class="m-title mt-4 mb-2">Contact With Us</h1>--}}
                    {{--<ul class="specs">--}}
                        {{--<li><a href="contact.html" title="">Contact Us</a></li>--}}
                        {{--<li><a href="faqs.html" title="">FAQ's</a></li>--}}
                        {{--<li>--}}
                            {{--<a href="tnc.html" title="">Greenroom Terms of Use</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</footer>--}}
    {{--<footer class="ftr">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">--}}
                    {{--<p>Copyright © 2018 Greenroom Technologies Pvt. Ltd.</p>--}}
                {{--</div>--}}
                {{--<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">--}}
                    {{--<ul class="socialIcons">--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)"><i class="fab fa-facebook-square"></i></a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)"><i class="fab fa-twitter-square"></i></a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)"><i class="fab fa-linkedin"></i></a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)"><i class="fab fa-google-plus-square"></i></a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</footer>--}}
    <!-- Modal For Register Start Here -->
    <div class="modal shadow" tabindex="-1" role="dialog" id="registrationModal" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalCenterTitle">
                        Register for a free Greenroom Account
                    </h6>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> -->
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label class="is-small">Full Name</label>
                            <input type="text" class="form-control" id="fullname" placeholder="Enter Full Name" />
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    <label class="is-small">Username</label>
                                    <input type="text" class="form-control rounded-0" id="userName"
                                           placeholder="Enter Username" />
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    <label class="is-small">Email/Phone</label>
                                    <input type="text" class="form-control rounded-0" id="emailOrPhone"
                                           placeholder="Enter email or phone" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    <label class="is-small">Password</label>
                                    <input type="password" class="form-control rounded-0" id="password"
                                           placeholder="Enter Password" />
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    <label class="is-small">Confirm Password</label>
                                    <input type="text" class="form-control rounded-0" id="confirmPssword"
                                           placeholder="Enter confirm password" />
                                </div>
                            </div>
                        </div>
                        <hr class="mt-1" />
                        <p class="terms p-0 mt-0 text-primary">
                            Enter verification code sent to your mobile no. 91-9974408232
                        </p>
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    <label class="is-small">Enter Code</label>
                                    <input type="text" class="form-control rounded-0" id="entercode"
                                           placeholder="Enter Password" />
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">

                            </div>
                        </div>
                        <hr class="mt-1" />
                        <div class="form-group">
                            <p class="terms">
                                By clicking below, you agree to &nbsp;<a href="javascript:void(0)"
                                                                         class="text-primary">Terms & Conditions</a>
                            </p>
                        </div>
                        <button type="button" class="btn btn-lg btn-success shadow-sm mr-2">
                            Register Now!
                        </button>
                        <button type="button" data-dismiss="modal"
                                class="btn btn-secondary btn-secondary shadow-sm">
                            Cancel
                        </button>
                        <a href="javascript:void(0)" class="signIn d-block">Already registered? Sign in</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal For Register End Here -->
    <!-- Modal For Login Start Here -->
    <div class="modal shadow" tabindex="-1" role="dialog" id="loginModal" data-keyboard="false">
        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="loginTitle">
                        Login
                    </h6>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> -->
                    <a href="javascript:void(0)" class="registerModal">Not a member? Sign up</a>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label class="is-small">Username/Email</label>
                            <input type="text" class="form-control rounded-0" id="emailName"
                                   placeholder="Enter username or email" />
                        </div>
                        <div class="form-group">
                            <label class="is-small">Password</label>
                            <input type="password" class="form-control rounded-0" id="pwd"
                                   placeholder="Enter password" />
                        </div>
                        <button type="button" class="btn btn-lg btn-success shadow-sm">Login</button>
                        <button type="button" data-dismiss="modal"
                                class="btn btn-lg btn-secondary  shadow-sm">Cancel</button>
                        <div class="usefulInfo">
                            <a href="javascript:void(0)" class="forgotPassword">Forgot Password</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal For Login End Here -->
    <!-- Modal For Forgot Start Here -->
    <div class="modal shadow" tabindex="-1" role="dialog" id="forgotModal" data-keyboard="false">
        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="forgotTitle">
                        Forgot Password
                    </h6>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> -->
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label class="is-small">Username/Email</label>
                            <input type="text" class="form-control rounded-0" id="userEmailName"
                                   placeholder="Enter username or email" />
                        </div>
                        <button type="button" class="btn btn-lg btn-success shadow-sm">
                            Retrieve
                        </button>
                        <button type="button" data-dismiss="modal" class="btn btn-lg btn-secondary shadow-sm">
                            Cancel
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal For Login End Here -->
    <!-- Modal For Forgot Start Here -->
    <div class="modal" tabindex="-1" role="dialog" id="contactModal" data-keyboard="false">
        <div class="modal-dialog modal-custom modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="contactTitle">
                        Contact PG Owner
                    </h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="card">
                            <div class="card-body p-0">
                                <ul class="list-group">
                                    <li class="list-group-item border-top-0 border-left-0 border-right-0 p-2">
                                        <label class="is-small">Name</label>
                                        <input type="text" class="form-control" id="yourName"
                                               placeholder="Enter Your Name" />
                                    </li>
                                    <li class="list-group-item border-left-0 border-right-0 p-2">
                                        <label class="is-small">Email</label>
                                        <input type="text" class="form-control" id="yourEmail"
                                               placeholder="Enter Your Email" />
                                    </li>
                                    <li class="list-group-item border-left-0 border-right-0 p-2">
                                        <label class="is-small">Phone</label>
                                        <input type="text" class="form-control" id="yourPhone"
                                               placeholder="Enter Your Phone" />
                                    </li>
                                    <li class="list-group-item border-left-0 border-right-0 p-2">
                                        <label class="is-small">Tell Us more</label>
                                        <select class="form-control select_customarrow" name="planning"
                                                id="planning">
                                            <option>Planning to be PG Tenant within</option>
                                            <option>Within 7 Days</option>
                                            <option>Within 15 Days</option>
                                            <option>Within 30 Days</option>
                                            <option>Within 3 Months</option>
                                        </select>
                                    </li>
                                    <li class="list-group-item border-left-0 border-right-0 p-2">
                                        <label class="is-small">Interested in Site Visit?</label>
                                        <select class="form-control select_customarrow" name="siteVisit"
                                                id="siteVisit">
                                            <option>Select</option>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>
                                    </li>
                                    <li class="list-group-item border-left-0 border-right-0 p-2">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="chk" />
                                            <label class="custom-control-label is-small" for="chk">I agree to be
                                                contacted by greenroom and others for
                                                similar PG or related services</label>
                                        </div>
                                    </li>
                                    <li class="list-group-item border-left-0 border-right-0 p-0 pr-2 pl-2">
                                        <button type="button" class="btn btn-primary btn-lg btn-block shadow-sm">
                                            Get PG Owner Details
                                        </button>
                                    </li>
                                    <li class="list-group-item border-left-0 border-right-0 p-2">
                                        <p class="d-flex tnc">
                                            By submitting I accept greenroom &nbsp;<a
                                                    href="javascript:void(0)">Terms & Conditions</a>
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal For Login End Here -->
    @endsection
