@extends('layouts.frontend.app')
@section('content')

    <br><br><br><br>

    <!-- middle content start -->
    <div class="dashboardContainer mb-5">
        <div class="dashboardHeader">
            <div class="dashboardContent">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                            <div class="card shadow-sm">
                                <img src="{{asset('frontend/assets/img/bnr10.jpg')}}" alt="" class="img-fluid"/>
                                <div class="userInfo">
                                    <h1>{{\Auth()->user()->name}}</h1>
                                    <span>Owner ID {{\Auth()->user()->id}}</span>
                                </div>
                                <div class="card-body pt-0">
                                    <h4 class="city">City: {{!empty($city_name)?$city_name:"-"}}</h4>
                                    <h1 class="state mt-1">State: {{!empty($state_name)?$state_name:"-"}}</h1>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-9">
                            <div class="d-flex justify-content-between align-middle">
                                <h1 class="dbtitle">My Profile</h1>
                                <button type="button" class="btn btn-light btn-sm editProfileBtn" id="editProfile">
                                    Edit Profile
                                </button>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="card shadow-sm mt-4 myProfile">

                                        <form class="card-body p-0" id="owner_profile_update_form">
                                            <div id="signup_error" style="color: red">

                                            </div>

                                            <div id="signup_success">

                                            </div>
                                            <ul class="list-group list-group-flush">
                                                <li class="list-group-item">
                                                    <span class="feildName">Name</span>
                                                    <div class="fields">{{\Auth()->user()->name}}</div>
                                                    <div class="editField">
                                                        <input type="text" class="form-control"
                                                               value="{{\Auth::user()->name}}" name="fname" id="fname">

                                                    </div>
                                                </li>
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                <input type="hidden" name="type" value="profile">
                                                <li class="list-group-item">
                                                    <span class="feildName">Contact Number</span>
                                                    <div class="fields">{{\Auth()->user()->phone}}</div>
                                                    <div class="editField">
                                                        <input type="text" class="form-control"
                                                               value="{{\Auth()->user()->phone}}" name="phone">
                                                    </div>
                                                </li>
                                                <li class="list-group-item">
                                                    <span class="feildName">Alternate Contact Number</span>
                                                    <div class="fields">{{\Auth::user()->phone}}</div>
                                                    <div class="editField">
                                                        <input type="text" class="form-control"
                                                               value="{{\Auth()->user()->phone}}" name="phone">
                                                    </div>
                                                </li>
                                                <li class="list-group-item">
                                                    <span class="feildName">Gender</span>
                                                    <div class="fields">{{\Auth()->user()->gender}}</div>
                                                    <div class="editField">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio"
                                                                   name="gender" id="inlineRadio1"
                                                                   value="Male" {{(\Auth()->user()->gender=="Male")?"checked":""}}>
                                                            <label class="form-check-label"
                                                                   for="inlineRadio1">Male</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio"
                                                                   name="gender" id="inlineRadio2"
                                                                   value="Female" {{(\Auth()->user()->gender=="Female")?"checked":""}}>
                                                            <label class="form-check-label"
                                                                   for="inlineRadio2">Female</label>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="list-group-item">
                                                    <span class="feildName">Firm Name</span>
                                                    <div class="fields">{{\Auth()->user()->firm_name}}</div>
                                                    <div class="editField">
                                                        <input type="text" class="form-control"
                                                               value="{{\Auth()->user()->firm_name}}" name="cname">
                                                    </div>
                                                </li>
                                                <li class="list-group-item">
                                                    <span class="feildName">Email</span>
                                                    <div class="fields">{{\Auth::user()->email}}</div>
                                                    <div class="editField">
                                                        <input type="email" class="form-control"
                                                               value="{{\Auth()->user()->email}}" name="email">
                                                    </div>
                                                </li>
                                                <li class="list-group-item">
                                                    <span class="feildName">Password</span>
                                                    <div class="fields">........</div>
                                                    <div class="editField">
                                                        <input type="password" class="form-control"
                                                         name="password">
                                                    </div>
                                                </li>
                                                <li class="list-group-item">
                                                    <span class="feildName">State</span>
                                                    <div class="fields">{{$state_name}}</div>
                                                    <div class="editField">
                                                        <select class="form-control" name="state_id">
                                                            <option>Select</option>
                                                            @foreach($state_list as $state)
                                                                <option
                                                                        value="{{$state["id"]}}" {{($state["id"]==\Auth()->user()->state_id) ? "selected":""}}
                                                                >{{$state["name"]}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </li>
                                                <li class="list-group-item">
                                                    <span class="feildName">City</span>
                                                    <div class="fields">{{$city_name}}</div>
                                                    <div class="editField">
                                                        <select class="form-control" name="city_id">
                                                            <option>Select</option>
                                                            @foreach($city_list as $city)
                                                                <option
                                                                        value="{{$city["id"]}}"
                                                                        {{($city["id"]==\Auth()->user()->city_id) ? "selected":""}}>
                                                                    {{$city["name"]}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </li>
                                                <li class="list-group-item submitBtn">
                                                    <span class="feildName">&nbsp;</span>
                                                    <div class="fields">&nbsp;</div>
                                                    <div class="editField">
                                                        <button type="button" class="btn btn-success"
                                                                id="save_profile_owner">Save My
                                                            Profile
                                                        </button>
                                                    </div>
                                                </li>
                                            </ul>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- Protional Videos and Ads End Here -->
@endsection
@section('footer')
    <script>
        $(document).ready(function(){

            $(document).on("click","#save_profile_owner",function(){
                var data=$("#owner_profile_update_form").serialize();

                if(data!="") {
                    $("#save_profile_owner").attr("disabled","disabled");
                    $.ajax({
                        type: "post",
                        url: "{{route('update_owner_profile')}}",
                        data: data,
                        success: function (response) {
                            console.log(response.result);
                            if(response.status==false) {
                                var html = "";
                                $.each(response.message, function (key, item) {
                                    html += item;
                                    html += "<br>";
                                });
                                $("#signup_error").html(html);
                                $("#signup_error").addClass("error");
                                $("#fname").focus();

                            }else{
                                location.reload();
                            }
                            $("#save_profile_owner").removeAttr("disabled");
                        }
                    });
                }else{
                    $("#signup_error").html("All field is required");
                    $("#signup_error").addClass("error");
                }
            });
        });
    </script>
    @endsection