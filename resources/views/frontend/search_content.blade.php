{{--{{dd($property)}}--}}
<div class="card rounded-0 mb-3">
@forelse($property as $item)
    <div class="card-body" id="content-{{$item["id"]}}">
        <div class="row">

            <div class="col-12 col-sm-3 col-md-12 col-lg-3 col-xl-4">
                <img src="{{asset('frontend/assets/img/thumb/thumb_01.jpg')}}" class="img-fluid" alt="" />
				@if(\Auth::check())
					@if($item["createdBy"]==\Auth::user()->id)
					<div class="d-flex justify-content-end">
						<div class="btn-group btn-group-sm"
							 role="group"
							 aria-label="Controller">
							<a href="{{route('post_your_property',$item["id"])}}" class="btn btn-light"
							   data-toggle="tooltip"
							   data-placement="bottom"><i
										class="far fa-edit"></i></a>
							   {{--title="Edit Property"></a>--}}
							{{--<button type="button"--}}
									{{--class="btn btn-light"--}}
									{{--data-toggle="tooltip"--}}
									{{--data-placement="bottom"--}}
									{{--title="Edit Property">--}}
							{{--</button>--}}

							<a data-href="{{route('delete_your_property')}}" data-id="{{$item["id"]}}" class="btn btn-light property_delete"
							   data-toggle="tooltip"
							   data-placement="bottom"
							   title="Delete Property"><i
										class="far fa-trash-alt"></i></a>
						</div>
					</div>
					@endif
				@endif
            </div>
            <div class="col-12 col-sm-9 col-md-12 col-lg-9 col-xl-8">
                <div class="rside pt-2">
                    <a href="{{route('search_details',$item['id'])}}" class="title" title="">{{$item["title"]}}</a>
                    <ul class="specs nav nav-pills nav-fill">
                        <li class="nav-item">
                            <span>Bed Sharing :</span> {{$item["bed_sharing"]}}
                        </li>
                        <li class="nav-item">
                            <span>Landmark :</span> {{$item["landmark"]}}
                        </li>
                        <li class="nav-item">
                            <span>Highlights :</span> 0 to 1 years
                            old/Furnished/3rd out of 9th Floor
                        </li>
                        <li class="nav-item">
                            <span>Accommodation For :</span> {{$item["gender"]}}
                        </li>
                        <li class="nav-item">
                            <span>Suitable For :</span>{{$item["suitable_for"]}}
                        </li>
						<li class="nav-item">
							<span>Property Status:</span> {{$item["property"]["status"]}}
						</li>
						<li class="nav-item">
							<span>Available From:</span>{{($item["property"]["avaialble_from"])?$item["property"]["avaialble_from"]:'-'}}
						</li>
                    </ul>
                    <p>
                        <span class="more">
                           {!!  $item["description"] !!}
                        </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer bg-transparent p-0 pr-1 pl-2" id="sub-content-{{$item["id"]}}">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                <p>Owner Name: {{$item["user_name"]}}</p>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                <p>Posted on {{$item["created_at"]}}</p>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                <button type="button" name="view_contact_number"
                        class="btn btn-success btn-lg btn-block view_contact_number shadow-sm">
                    View Contact Number
                </button>
            </div>
        </div>
    </div>
<div class="row">
</div>
        @empty
        <p>No Data available</p>

@endforelse
</div>
{{$data_array->links()}}