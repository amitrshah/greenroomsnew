@extends('layouts.frontend.app')
@section('content')
    {{--{{dd($search_property_type)}}--}}
        <!-- banner Start Here -->
    <br><br><br><br><br><br>
        <div class="greenroom-banner greenroom-banner-inner addstyle">
            <div class="content">
                <div class="contentContainer">
                    <div class="content-row">
                        <form id="top_search">
                            <div class="homeSearchArea">
                                <div class="category_name property_type">
                                    <select name="propertyType[]" id="propertyTypeId" class="select_customarrow form-control js-select2" multiple="multiple">
                                        @foreach($search_property_ as $item)
                                            <option value="{{$item->propertyTypeID}}" {{isset($property_id)?($property_id==$item->propertyTypeID)?"selected":"":""}}>{{$item->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="category_name for_mal_or_female">
								<select name="forMaleOrFemale" id="forMaleOrFemaleId" class="select_customarrow form-control js-select2">
									<option value="">Accomodation For</option>
									@foreach($data_array["accomodation_for"] as $key=>$item)
										@if($male_female==$key)
											<option value={{$key}} selected="selected">
											{{$item}}
											</option>
										@else
											<option value={{$key}} >{{$item}}</option>
										@endif
									@endforeach
                                    </select>
                                </div>
                                <div class="category_name search_by_loc">
                                    <select name="searchByLoc[]" id="searchByLoc" class="select_customarrow" placeHolder="Search By Location">
                                        @foreach($ahmedabad_loc_ as $item)
                                            @if(!empty($area_id))
                                            @foreach($area_id as $id)
                                                @if($id==$item->areaID)
                                                    <option value="{{$item->areaID}}" selected>{{$item->areaName}}</option>
                                                    @else
                                                    <option value="{{$item->areaID}}">{{$item->areaName}}</option>
                                                @endif
                                            @endforeach
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="category_name search_icon" style="width:10%;">
                                    <a href="javascript:void(0)" title="" id="search_">
                                        <img src="{{asset('frontend/assets/img/icon/icon_search.svg')}}"
                                            class="icon-search" alt="" />
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <!-- banner End Here -->
        <!-- middle content start -->
        <div class="search_strip">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="search_group">
                            <li class="active"><i class="fas fa-home mr-2 property_count"></i>
                                <span id="count_property">{{$count}}</span>&nbsp;Properties</li>
                            <!-- <li><a href="javascript:void(0)"><i class="fas fa-user mr-2"></i> 30+ Dealers</a></li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <div class="container mt-3 mt-sm-5 mt-md-5 mt-lg-5 mt-xl-5 mb-3 mb-sm-5 mb-md-5 mb-lg-5 mb-xl-5">
            <div class="searchResult">
                <form id="filter_sidebar">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3">
                        <h1 class="prop_title_new mb-2 mt-3">Search By</h1>
                        <div id="accordion01" class="accordion border-bottom">
                            <div class="card">
                                <a class="card-header" data-toggle="collapse" href="#filter100">
                                    <span class="card-title">Search (Relevance)</span>
                                    <i class="fas fa-angle-down float-right"></i>
                                </a>
                                <div id="filter100" class="card-body collapse p-0 border-none"
                                    data-parent="#accordion01">
                                    <div class="list-group rounded-0 border-0">
                                        <a href="#" class="list-group-item list-group-item-action filter">Date:Newest</a>
                                        <a href="#" class="list-group-item list-group-item-action filter">Price: Low-High</a>
                                        <a href="#" class="list-group-item list-group-item-action filter">Price: High-Low</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h1 class="prop_title_new mb-2 mt-3">Filter</h1>
                        <div id="accordion" class="accordion border-bottom mb-5 mb-sm-5 mb-md-0 mb-lg-0 mb-xl-0">
                            <div class="card">
                                <a class="card-header" data-toggle="collapse" href="#filter1">
                                    <span class="card-title">Accommodation For</span>
                                    <i class="fas fa-angle-down float-right"></i>
                                </a>
                                <div id="filter1" class="card-body collapse show" data-parent="#accordion">
                                    <ul>
                                        <li>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input accommodation" id="male" name="male" value="male" />
                                                <label class="custom-control-label is-small " for="male">Male</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input accommodation" id="female" name="female" value="female" />
                                                <label class="custom-control-label is-small" for="female">Female</label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <a class="card-header border-top" data-toggle="collapse" href="#filter2">
                                    <span class="card-title">Suitable For</span>
                                    <i class="fas fa-angle-down float-right"></i>
                                </a>
                                <div id="filter2" class="card-body collapse show" data-parent="#accordion">
                                    <ul>
                                        @foreach($data_array["suitable_for"] as $key=>$item)
                                        <li>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input suitable" name="suitable_for[]" id="suitable_for_{{$key}}" value="{{$key}}"/>
                                                <label class="custom-control-label is-small" for="suitable_for_{{$key}}">{{$item}}</label>
                                            </div>
                                        </li>
                                            @endforeach
                                    </ul>
                                </div>
								<!--
                                <a class="card-header border-top" data-toggle="collapse" href="#filter3">
                                    <span class="card-title">Availability</span>
                                    <i class="fas fa-angle-down float-right"></i>
                                </a>
                                <div id="filter3" class="card-body collapse show" data-parent="#accordion">
                                    <ul>
                                        @foreach($data_array["availability"] as $key=>$item)
                                        <li>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" name="availability[]" class="custom-control-input availability" id="availability_id_{{$key}}" value="{{$key}}" />
                                                <label class="custom-control-label is-small" for="availability_id_{{$key}}">{{$item}}</label>
                                            </div>
                                        </li>
                                            @endforeach
                                    </ul>
                                </div>
                                <a class="card-header border-top" data-toggle="collapse" href="#filter4">
                                    <span class="card-title">Availability Type</span>
                                    <i class="fas fa-angle-down float-right"></i>
                                </a>
                                <div id="filter4" class="card-body collapse show" data-parent="#accordion">
                                    <ul>
                                        @foreach($data_array["availability_type"] as $key=>$item)
                                        <li>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input availability_type" name="availability_type[]" id="type_id_{{$key}}" value="{{$key}}" />
                                                <label class="custom-control-label is-small" for="type_id_{{$key}}">{{$item}}</label>
                                            </div>
                                        </li>
                                            @endforeach
                                    </ul>
                                </div>
								-->
								
								<a class="card-header border-top" data-toggle="collapse" href="#filter6">
                                    <span class="card-title">Bed Sharing</span>
                                    <i class="fas fa-angle-down float-right"></i>
                                </a>
                                <div id="filter6" class="card-body collapse show" data-parent="#accordion">
                                    <ul>
                                        @foreach($data_array["bedsharing"] as $key=>$item)
                                        <li>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input bed_sharing" name="bed_sharing[]" id="bed_sharing_id_{{$key}}" value="{{$key}}" />
                                                <label class="custom-control-label is-small" for="bed_sharing_id_{{$key}}">{{$item}}</label>
                                            </div>
                                        </li>
                                            @endforeach
                                    </ul>
                                </div>

                                <a class="card-header border-top" data-toggle="collapse" href="#filter5">
                                    <span class="card-title">Condition</span>
                                    <i class="fas fa-angle-down float-right"></i>
                                </a>
                                <div id="filter5" class="card-body collapse show" data-parent="#accordion">
                                    <ul>
                                        @foreach($data_array["condition"] as $key=>$item)
                                        <li>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input condition" name="condition[]" id="condition_id_{{$key}}" value="{{$key}}" />
                                                <label class="custom-control-label is-small"
                                                    for="condition_id_{{$key}}">{{$item}}</label>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-8 col-lg-9 col-xl-9">
                        <div class="loader">

                        </div>
                        <div class="refineResult">
                            <div>
                                <p>
								  <span id="count_property_header">{{$count}}</span>
									  @if($string=="")
											Properties
										@else
											{{$string}}
										@endif
                                </p>
                                <div></div>
                            </div>
                            <!-- 1st -->

                            <div class="" id="search_content">

                            </div>

                            <div id="pagination_property">

                            </div>
                            {{--<nav aria-label="Page navigation example">--}}
                                {{--<ul class="pagination pagination-sm justify-content-center">--}}
                                    {{--<li class="page-item ">--}}
                                        {{--<a class="page-link" href="#" tabindex="-1">Previous</a>--}}
                                    {{--</li>--}}
                                    {{--<li class="page-item"><a class="page-link" href="#">1</a></li>--}}
                                    {{--<li class="page-item"><a class="page-link" href="#">2</a></li>--}}
                                    {{--<li class="page-item"><a class="page-link" href="#">3</a></li>--}}
                                    {{--<li class="page-item">--}}
                                        {{--<a class="page-link" href="#">Next</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</nav>--}}
                        </div>
                    </div>

                </div>
                </form>

            </div>
        </div>
        <!-- Protional Videos and Ads End Here -->
		

<style>
.select2-results__option {
  padding-right: 10px;
  vertical-align: middle;
}
.select2-results__option:before {
  content: "";
  display: inline-block;
  position: relative;
  height: 20px;
  width: 20px;
  border: 2px solid #e9e9e9;
  border-radius: 4px;
  background-color: #fff;
  margin-right: 10px;
  vertical-align: middle;
}
.select2-results__option[aria-selected=true]:before {
  font-family:fontAwesome;
  content: "\f00c";
  color: #fff;
  background-color: #f77750;
  border: 0;
  display: inline-block;
  padding-left: 3px;
}
.select2-container--default .select2-results__option[aria-selected=true] {
	background-color: #fff;
}
.select2-container--default .select2-results__option--highlighted[aria-selected] {
	background-color: #eaeaeb;
	color: #272727;
}
.select2-container--default .select2-selection--multiple {
	margin-bottom: 10px;
}
.select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple {
	border-radius: 4px;
}
.select2-container--default.select2-container--focus .select2-selection--multiple {
	border-color: #f77750;
	border-width: 2px;
}
.select2-container--default .select2-selection--multiple {
	border-width: 2px;
}
.select2-container--open .select2-dropdown--below {
	
	border-radius: 6px;
	box-shadow: 0 0 10px rgba(0,0,0,0.5);

}
.select2-selection .select2-selection--multiple:after {
	content: 'hhghgh';
}
/* select with icons badges single*/
.select-icon .select2-selection__placeholder .badge {
	display: none;
}
.select-icon .placeholder {
	display: none;
}
.select-icon .select2-results__option:before,
.select-icon .select2-results__option[aria-selected=true]:before {
	display: none !important;
	/* content: "" !important; */
}
.select-icon  .select2-search--dropdown {
	display: none;
}
</style>		
		
		
		
		
    @endsection
@section('footer')

    <script>
        var search_page = 1;
        $('justify-content-end').hide();
        var accomodation=[];
        var suitable=[];
        var availability=[];
        var availability_type=[];
        var condition=[];
        var bed_sharing=[];
        var final_array={};
        $(document).ready(function(){
			
			
			
		$("#propertyTypeId").select2({
			closeOnSelect : false,
			placeholder : "Select Property Type",
			allowHtml: true,
			allowClear: true,
			tags: true // создает новые опции на лету
		});


		$("#forMaleOrFemaleId").select2({
			closeOnSelect : false,
			placeholder : "Accommodation For",
			allowHtml: true,
			tags: true // создает новые опции на лету
		});
		$("#forMaleOrFemaleId").val("");


		$("#searchByLoc").val("");
		$("#searchByLoc").select2({
			closeOnSelect : false,
			placeholder : "Search By Location",
			allowHtml: true,
			allowClear: true,
			tags: true // создает новые опции на лету
		});


			$('.icons_select2').select2({
				width: "100%",
				templateSelection: iformat,
				templateResult: iformat,
				allowHtml: true,
				placeholder: "Placeholder",
				dropdownParent: $( '.select-icon' ),//обавили класс
				allowClear: true,
				multiple: false
			});
	
			function iformat(icon, badge,) {
					var originalOption = icon.element;
					var originalOptionBadge = $(originalOption).data('badge');
				 
					return $('<span><i class="fa ' + $(originalOption).data('icon') + '"></i> ' + icon.text + '<span class="badge">' + originalOptionBadge + '</span></span>');
				}
			
			
			
			
            fetchdata();
            function init(){
                $("#count_property").text("");
                $("#count_property_header").text("");
                $(".property_count").text("");
                $('.loader').removeClass("hidden");
            }

            function fetchdata() {
                init();
                var final_array = {};
                final_array["accommodation"]=getCheckedValues("accommodation");
                final_array["suitable"]=getCheckedValues("suitable");
                final_array["availability"]=getCheckedValues("availability");
                final_array["availability_type"]=getCheckedValues("availability_type");
                final_array["condition"]=getCheckedValues("condition");
                final_array["bed_sharing"]=getCheckedValues("bed_sharing");
                final_array["propertyType"]=$("#propertyTypeId").val();
                final_array["forMaleOrFemale"]=$("#forMaleOrFemaleId").val();
                final_array["searchByLoc"]=$("#searchByLocId").val();

              $(".loader").addClass("show");

                $.ajax({
                    type: "POST",
                    url: "{{route('search_content')}}",
                    data: {_token:"{{ csrf_token() }}",data1:final_array,page:search_page},
                    success: function (response) {
                        console.log(response);
						$(".loader").addClass("hidden");
                        if (response.status) {
                            $("#search_content").html(response.result);
//                            $("#count_property").html("");
                            $("#count_property").html(response.count);
                            $("#count_property_header").html(response.count);

                        }
                    },
					error: function(error) {
						$(".loader").addClass("hidden");
						$("#search_content").html(error.responseJSON.message+" Failed to get result for your filter, please contact admin.");
					}
                });
            }

            $(document).on("click","#search_",function(){
                search_page = 1;
                fetchdata();
            });
//            $(document).on("click",".filter",function(){
//
//            });
            function getCheckedValues(class_name){
                var return_data = [];
                $('.'+class_name).each(function () {
                    if($(this).is(":checked")){
                        return_data.push($(this).val());
                    }
                });
                return return_data;
            }
            function searchData(){
                search_page = 1;
                fetchdata();
            }
            function getPageNumber(str) {
                var result = null,
                    tmp = [];
                str.substr(1)
                    .split("?")[1].split("&")
                    .forEach(function (item) {
                        tmp = item.split("=");console.log(item);
                        if (tmp[0] === "page") result = decodeURIComponent(tmp[1]);
                    });
                return result;
            }
            function filterPage(e){
                e.preventDefault()
                //search_page = $.trim($(this).text());
                search_page = getPageNumber($(this).attr("href"));
                fetchdata();
            }
            $(document).on('click','.accommodation,.suitable,.availability,.availability_type,.condition,.bed_sharing',searchData);
            $(document).on('click','ul.pagination li a',filterPage);
        });
    </script>
    @endsection