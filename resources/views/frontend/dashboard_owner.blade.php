@extends('layouts.frontend.app')
@section('content')

    <br><br><br><br>

    <!-- middle content start -->
    <div class="dashboardContainer mb-5">
        <div class="dashboardHeader">
            <div class="dashboardContent">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                            <div class="card shadow-sm">
                                <img src="{{asset('frontend/assets/img/bnr10.jpg')}}" alt="" class="img-fluid" />
                                <div class="userInfo">
                                    <h1 id="owner_name_text">{{\Auth::user()->name}}</h1>
                                    <span>Owner ID #{{\Auth::user()->id}}</span>
                                </div>
                                <a href="javascript:void(0)" class="editInfo shadow-sm editProfile"
                                   title="Edit Profile">Edit Profile
                                    &nbsp;<i class="fas fa-edit"></i>
                                </a>
                                <div class="card-body pt-0">
                                    <h4 class="city">City: {{!empty($city_name)?$city_name:"-"}}</h4>
                                    <h1 class="state mt-1">State: {{!empty($state_name)?$state_name:"-"}}</h1>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-9">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                    <h1 class="dbtitle">Dashboard</h1>
                                    <a href="{{route('property_list')}}" class="card shadow-sm mt-3">
                                        <div class="perkStatistics">
                                            <h5>My Property List</h5>
                                        </div>
                                        <div class="card-body pt-2">
                                            <div class="statistics">
                                                <h1>{{$property_list}}</h1>
                                                <div class="iconSet">
                                                    <img src="{{asset('frontend/assets/img/icon/icon_peroperty.png')}}" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                    <h1 class="dbtitle d-none d-lg-block d-xl-block">&nbsp;</h1>
                                    <a href="{{route('property_list','Available')}}"
                                       class="card shadow-sm mt-3">
                                        <div class="perkStatistics">
                                            <h5>Available Property List</h5>
                                        </div>
                                        <div class="card-body pt-2">
                                            <div class="statistics">
                                                <h1>{{$available_property_list}}</h1>
                                                <div class="iconSet">
                                                    <img src="{{asset('frontend/assets/img/icon/icon_peroperty_01.png')}}" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                                    <h1 class="dbtitle d-none d-lg-block d-xl-block">&nbsp;</h1>
                                    <a href="{{route('property_list','occupied')}}"
                                       class=" card shadow-sm mt-3">
                                        <div class="perkStatistics">
                                            <h5>Occupied Property List</h5>
                                        </div>
                                        <div class="card-body pt-2">
                                            <div class="statistics">
                                                <h1>{{$occupied_property_list}}</h1>
                                                <div class="iconSet">
                                                    <img src="{{asset('frontend/assets/img/icon/icon_peroperty_02.png')}}" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="card shadow-sm mt-4">
                                        <div class="card-body pt-2">
                                            <div id="myChart" style="width: 100%;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Protional Videos and Ads End Here -->
    <!-- Modal For Edit Profile Start Here -->
    <div class="modal shadow" tabindex="-1" role="dialog" id="editProfile" data-keyboard="false">
        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">
                        Edit Profile
                    </h6>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> -->
                </div>
                <div class="modal-body">
                    <form id="update-owner-profile">

                        <div id="signup_error" style="color: red">

                        </div>

                        <div id="signup_success">

                        </div>

                        <div class="form-group">

                            <label class="is-small">Name</label>
                            <input type="text" class="form-control rounded-0" id="fname" placeholder=""
                                   value="{{\Auth()->user()->name}}" name="fname" />
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                        </div>
                        <div class="form-group">
                            <label class="is-small">Company Name</label>
                            <input type="text" class="form-control rounded-0" id="cname" name="cname" placeholder=""
                                   value="{{\Auth()->user()->firm_name}}" />
                        </div>
                        <div class="form-group">
                            <label class="is-small">Contact No</label>
                            <input type="text" class="form-control rounded-0" id="phone" name="phone" placeholder=""
                                   value="{{\Auth()->user()->phone}}" />
                        </div>
                        <div class="form-group">
                            <label class="is-small">Change Password</label>
                            <input type="password" class="form-control rounded-0" id="password" name="password" placeholder=""
                            />
                        </div>
                        <div class="form-group">
                            <label class="is-small">Confirm Password</label>
                            <input type="password" class="form-control rounded-0" id="cpassword" name="cpassword" placeholder=""
                            />
                        </div>
                        <div class="form-group">
                            <label class="is-small">Email</label>
                            <input type="email" class="form-control rounded-0" id="email" name="email" placeholder=""
                                   value="{{\Auth::user()->email}}" />
                        </div>
                        <button type="button" class="btn btn-lg btn-success shadow-sm" id="save-owner-profile">
                            Save Profile
                        </button>
                        <button type="button" data-dismiss="modal" class="btn btn-lg btn-secondary shadow-sm" id="cancel-owner-update">
                            Cancel
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal For Edit Profile End Here -->
@endsection

@section("footer")
    <script>


        $(document).ready(function(){
            var my_chart={!!   json_encode($chartSeries)!!};
            console.log(my_chart);
            var myConfig = {
                backgroundColor: '#fff',
                type: "ring",
                title: {
                    text: "Quota Distribution",
                    fontFamily: 'Roboto',
                    fontSize: 14,
                    // border: "1px solid black",
                    padding: "15",
                    fontColor: "#1E5D9E",
                },
                "plot": {
                    "value-box": {
                        "text": "%v",
                        "decimals": 0,
                        "placement": "out",
                        "offset-r": "-10",
                        "font-family": "Roboto",
                        "font-size": 30,
                        "font-weight": "normal"
                    }
                },
                plotarea: {
                    backgroundColor: 'transparent',
                    borderWidth: 0,
                    borderRadius: "0 0 0 10",
                    margin: "30 0 0 0"
                },
                legend: {
                    toggleAction: 'remove',
                    backgroundColor: '#FBFCFE',
                    borderWidth: 0,
                    adjustLayout: true,
                    align: 'center',
                    verticalAlign: 'bottom',
                    marker: {
                        type: 'circle',
                        cursor: 'pointer',
                        borderWidth: 0,
                        size: 5
                    },
                    item: {
                        fontColor: "#777",
                        cursor: 'pointer',
                        offsetX: -6,
                        fontSize: 12
                    },
                    mediaRules: [
                        {
                            maxWidth: 500,
                            visible: false
                        }
                    ]
                },
                scaleR: {
                    refAngle: 270
                },
                series: my_chart

            };
            zingchart.render({
                id: 'myChart',
                data: {
                    graphset: [myConfig]
                },
                height: '400',
                width: '100%'
            });

            $(document).on("click","#save-owner-profile",function(){

                var data=$("#update-owner-profile").serialize();
                if(data!="") {
                    $("#save-owner-profile").attr("disabled","disabled");
                    $.ajax({
                        type: "post",
                        url: "{{route('update_owner_profile')}}",
                        data: data,
                        success: function (response) {
                            console.log(response.result);
                            if(response.status==false) {
                                var html = "";
                                $.each(response.message, function (key, item) {
                                    html += item;
                                    html += "<br>";
                                });
                                $("#signup_error").html(html);
                                $("#signup_error").addClass("error");


                            }else{
                                $("#signup_success").html(response.message);
                                $("#owner_name_text").text(response.result.name);
                                $("#cancel-owner-update").trigger("click");
                            }
                            $("#save-owner-profile").removeAttr("disabled");
                        }
                    });
                }else{
                    $("#signup_error").html("All field is required");
                    $("#signup_error").addClass("error");
                }
            });
        });
    </script>
@endsection
