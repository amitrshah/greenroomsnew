@extends('layouts.frontend.app')
@section('content')

<!-- middle content start -->
<br>
<br>
        <div class="about_us aboutGreenroom addstyle">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="titleBoxInner mb-3">
                            <div class="in">
                                <h1 class="title">Meaning Of Greenroom</h1>
                                <div class="lines"></div>
                            </div>
                        </div>
                        <p class="text-center meaningOfGreenroom">A room in a
                            theatre or studio in
                            which
                            performers can relax when they are
                            not performing.</p>
                    </div>
                </div>
            </div>
            <div class="mt-3 row1">
                <div class="container">
                    <div class="row ">
                        <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <h1 class="titles">Our focus as an organization</h1>
                            <p class="meaningOfGreenroom mt-3">is to act as a <span>bridge
                                    between
                                    Paying Guest/ Hostels
                                    owners</span> and <span>accommodation seekers</span> and therefore our focus is
                                on becoming a One Stop
                                Shop for All Accommodation Needs.</p>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <img src="{{asset('frontend/assets/img/01.png')}}" class="img-fluid" alt="" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row2">
                <div class="container">
                    <div class="row ">
                        <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <img src="{{asset('frontend/assets/img/02.png')}}" class="img-fluid" alt="" />
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <h1 class="titles">Greenroom is a <span>Socio-Economic Organization.</span></h1>
                            <p class="meaningOfGreenroom mt-3">In our
                                operation, we do not
                                have more focus on revenue, we have focus on providing quality service at affordable
                                rate to all property seeker in across India</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Property Report Start-->
            <div class="property_availability">
                <div class="container">
                    <div class="owl-carousel" id="propertyAvailability">
                        <div class="item">
                            <div class="block">
                                <img src="{{asset('frontend/assets/img/icon/icon_happy_customer.png')}}" class="icon" alt="" />
                                <h1>5000+</h1>
                                <span>Visit</span>
                            </div>
                        </div>
                        <!-- <div class="item">
                            <div class="block">
                                <img src="assets/img/icon/icon_houses_across_india.png" class="icon" alt="">
                                <h1>16800 +</h1>
                                <span>Houses across in India</span>
                            </div>
                        </div> -->
                        <!-- <div class="item">
                            <div class="block boderNo">
                                <img src="assets/img/icon/icon_saving_brokerage.png" class="icon" alt="">
                                <h1>322700000 +</h1>
                                <span>Saving made on brokerage</span>
                            </div>
                        </div> -->
                        <!-- <div class="item">
                            <div class="block">
                                <img src="assets/img/icon/icon_cities_in_india.png" class="icon" alt="">
                                <h1>12 +</h1>
                                <span>Cities in India</span>
                            </div>
                        </div> -->
                        <div class="item">
                            <div class="block">
                                <img src="{{asset('frontend/assets/img/icon/icon_55.png')}}" class="icon" alt="" />
                                <h1>1000</h1>
                                <span>PG</span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="block">
                                <img src="{{asset('frontend/assets/img/icon/icon_56.png')}}" class="icon" alt="" />
                                <h1>750</h1>
                                <span>Corporate PG</span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="block">
                                <img src="{{asset('frontend/assets/img/icon/icon_57.png')}}" class="icon" alt="" />
                                <h1>500</h1>
                                <span>Hostel</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Property Report End-->
        </div>
        <!-- Protional Videos and Ads End Here -->
@endsection