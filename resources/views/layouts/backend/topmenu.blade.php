@if (@auth()->guard('admin')->user()->id)
<nav class="navbar navbar-expand-sm navbar-dark border-0 rounded-0 shadow greenroom-nav py-0" style="background:#1BB351">
    <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId01" aria-controls="collapsibleNavId01"
        aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
    <div class="collapse navbar-collapse show" id="collapsibleNavId01">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item{{((Request::is('/')||Request::is('admin/dashboard'))?' active':'')}}"> 
                <a class="nav-link"  href="{{ route('admin_home') }}"><i class="fas fa-tachometer-alt"></i>&nbsp;&nbsp;Dashboard</a>
            </li>
            @if(SystemHelper::is_module_permission('User','List'))
                <li class="nav-item{{(Request::segment(2)==='user'?' active':'')}}">
                    <a class="nav-link" href="{{route('admin_user')}}"><i class="fas fa-user"></i>&nbsp;&nbsp;User</a>
                </li>
            @endif
            @if(SystemHelper::is_module_permission('Property','List'))
                <li class="nav-item{{(Request::segment(2)==='property'?' active':'')}}">
                    <a class="nav-link" href="{{route('admin_property')}}"><i class="fas fa-building"></i>&nbsp;&nbsp;Property</a>
                </li>
            @endif
			<!--@if(SystemHelper::is_module_permission('Memberpayment','List'))
                <li class="nav-item{{(Request::segment(2)==='memberpayment'?' active':'')}}">
                    <a class="nav-link" href="{{route('admin_memberpayment')}}"><i class="fas fa-rupee-sign"></i>&nbsp;&nbsp;Member Payment</a>
                </li>
            @endif-->
			@if(SystemHelper::is_module_permission('Memberrequirement','List'))
                <li class="nav-item{{(Request::segment(2)==='memberrequirement'?' active':'')}}">
                    <a class="nav-link" href="{{route('admin_memberrequirement')}}"><i class="fas fa-rupee-sign"></i>&nbsp;&nbsp;Member Requirement</a>
                </li>
            @endif
            
			<!--@if(SystemHelper::is_module_permission('Plan','List'))
                <li class="nav-item{{(Request::segment(2)==='plan'?' active':'') }}">
                    <a class="nav-link" href="{{route('admin_plan')}}"><i class="far fa-calendar-alt"></i>&nbsp;&nbsp;Plan</a>
                </li>
            @endif-->
            @if(SystemHelper::is_module_permission('Userrole','List') || SystemHelper::is_module_permission('Roomsharing','List') || SystemHelper::is_module_permission('Propertytype','List') || SystemHelper::is_module_permission('State','List') || SystemHelper::is_module_permission('City','List') || SystemHelper::is_module_permission('Area','List'))
                <li class="nav-item dropdown{{((Request::segment(2)==='propertytype')||(Request::segment(2)==='userrole')||(Request::segment(2)==='roomsharing')||(Request::segment(2)==='state')||(Request::segment(2)==='city')||(Request::segment(2)==='area'))?' active':''}}">
                    <a href="javascript:void(0)" class="nav-link dropdown-toggle" data-toggle="dropdown" id="dropdownId" aria-haspopup="true" aria-expanded="false"><i class="fas fa-tasks"></i>&nbsp;&nbsp;Master Mgmt.</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownId">
                        @if(SystemHelper::is_module_permission('Userrole','List'))
                            <a class="dropdown-item{{(Request::segment(2)==='userrole'?' active':'')}}" href="{{route('admin_userrole')}}">User Role</a>
                        @endif
                        @if(SystemHelper::is_module_permission('Roomsharing','List'))
                            <a class="dropdown-item{{(Request::segment(2)==='roomsharing'?' active':'')}}" href="{{route('admin_roomsharing')}}">{{config('constants.CONSTANT_TEXT.BED_SHARING')}}</a>
                        @endif
                        @if(SystemHelper::is_module_permission('Propertytype','List'))
                            <a class="dropdown-item{{(Request::segment(2)==='propertytype'?' active':'')}}" href="{{route('admin_propertytype')}}"><!--{{config('constants.CONSTANT_TEXT.AVAILABLITY_TYPE')}}-->
							Property Category
						</a>
                        @endif
						@if(SystemHelper::is_module_permission('Availability','List'))
                            <a class="dropdown-item{{(Request::segment(2)==='availability'?' active':'')}}" href="{{route('admin_availability')}}"><!--{{config('constants.CONSTANT_TEXT.AVAILABLITY_TYPE')}}-->
							Availibility
						</a>
						@endif
						@if(SystemHelper::is_module_permission('Availibilitytype','List'))
                            <a class="dropdown-item{{(Request::segment(2)==='availibilitytype'?' active':'')}}" href="{{route('admin_availibilitytype')}}"><!--{{config('constants.CONSTANT_TEXT.AVAILABLITY_TYPE')}}-->
							Availibility Type
						</a>
                        @endif
						@if(SystemHelper::is_module_permission('Facilities','List'))
                            <a class="dropdown-item{{(Request::segment(2)==='facilities'?' active':'')}}" href="{{route('admin_facilities')}}">Facilities</a>
                        @endif
                        @if(SystemHelper::is_module_permission('Interiors','List'))
                            <a class="dropdown-item{{(Request::segment(2)==='interiors'?' active':'')}}" href="{{route('admin_interiors')}}">Interiors</a>
                        @endif
                        @if(SystemHelper::is_module_permission('Rules','List'))
                            <a class="dropdown-item{{(Request::segment(2)==='rules'?' active':'')}}" href="{{route('admin_rules')}}">Rules</a>
                        @endif
                        @if(SystemHelper::is_module_permission('Societyamenity','List'))
                            <a class="dropdown-item{{(Request::segment(2)==='societyamenity'?' active':'')}}" href="{{route('admin_societyamenity')}}">Society Amenity</a>
                        @endif
                        @if(SystemHelper::is_module_permission('State','List'))
                            <a class="dropdown-item{{(Request::segment(2)==='state'?' active':'')}}" href="{{route('admin_state')}}">State</a>
                        @endif
                        @if(SystemHelper::is_module_permission('City','List'))
                            <a class="dropdown-item{{(Request::segment(2)==='city'?' active':'')}}" href="{{route('admin_city')}}">City</a>
                        @endif
                        @if(SystemHelper::is_module_permission('Area','List'))
                            <a class="dropdown-item{{(Request::segment(2)==='area'?' active':'')}}" href="{{route('admin_area')}}">Area</a>
                        @endif
                    </div>
                </li>
            @endif
			<li class="nav-item">
				<a class="nav-link" href="javascript:" onClick="document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i>&nbsp;&nbsp;Logout</a>
				<form id="logout-form" class="form-horizontal" method="POST" action="{{ route('admin_logout') }}">
					{{ csrf_field() }}
				</form>
			</li>
			
            <!-- <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
                <div class="dropdown-menu" aria-labelledby="dropdownId">
                    <a class="dropdown-item" href="#">Action 1</a>
                    <a class="dropdown-item" href="#">Action 2</a>
                </div>
            </li> -->
        </ul>
    </div>
</nav>
@endif