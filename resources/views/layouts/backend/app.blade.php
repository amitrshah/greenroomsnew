<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') {{ config('app.name', 'Green Rooms') }}</title>
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <link href="{{asset('bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <!-- <link href="{{asset('css/backend/admin.css')}}" rel="stylesheet"> -->
    <!-- <script src="{{ asset('js/app.js') }}"></script>  -->
    <!-- <script src="{{ asset('js/jquery.min.js') }}"></script> -->
    <!-- <script src="{{asset('bootstrap/dist/js/bootstrap.min.js')}}"></script> -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
   
   
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/bootstrap.min.css') }}" />
    
	
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/latolatinfonts.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/backend/style.min.css')}}">

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script>
		var baseUrl = "{{ url('/admin') }}";
        $(function () {
            window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token(),]); ?>;
            $baseUrl = "{{ url('/admin') }}";
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
            });
            if($('.authcontent').length > 0 ){
                $('body').addClass('loginpage');
            }else{
                $('body').removeClass('loginpage');
            }
        });
    </script>
	<style>
	.whole-page-overlay{
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  position: fixed;
  background: rgba(0,0,0,0.6);
  width: 100%;
  height: 100% !important;
  z-index: 1050;
  display: none;
}
.whole-page-overlay .center-loader{
  top: 50%;
  left: 52%;
  position: absolute;
  color: white;
}
</style>
</head>
<body>

<div id="app">
<div class="whole-page-overlay" id="whole_page_loader">
	<img class="center-loader"  style="height:100px;" src="{{asset('images/loader.svg')}}" />
</div>
    <div class="nav_t">
        <nav class="navbar navbar-expand-lg navbar-light bg-light mb-0 border-0 rounded-0" style="background:#fff!important;">
            <a class="navbar-brand" href="#">
                <img src="{{ asset('img/logo.png') }}" class="img-fluid" alt="Greenroom Admin" srcset="{{ asset('img/logo.png') }}">
            </a>
            <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="collapsibleNavId">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    @if (Auth::guard('admin')->check())
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::guard('admin')->user()->name }}</a>
                            <div class="dropdown-menu" aria-labelledby="dropdownId">
								<form id="logout-form" class="form-horizontal" method="POST" action="{{ route('admin_logout') }}">
									{{ csrf_field() }}
									<a class="dropdown-item" href="javascript:" onClick="document.getElementById('logout-form').submit();">Logout</a>
								</form>
                            </div>
                        </li>
                    @else
                        <li><a href="{{ route('admin_login') }}">Login</a></li>
                    @endguest
                </ul>
            </div>
        </nav>
        <div>
            @include('layouts.backend.topmenu')
        </div>
            @yield('content')
    </div>
</div>
</body>
</html>
