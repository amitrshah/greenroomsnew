<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') {{ config('app.name', 'Green Rooms') }}</title>
    
    <!-- google fonts link -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet"> 
    
    <!-- Styles -->
    <link href="{{asset('bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
     <link href="{{asset('css/frontend/style.css')}}" rel="stylesheet">
     <link href="{{asset('css/frontend/perfect-scrollbar.css')}}" rel="stylesheet">
   
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <!-- Styles -->

    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('js/select2.full.js') }}"></script>
    <script src="{{ asset('js/frontend/perfect-scrollbar.min.js') }}"></script>
    <!-- <script src="{{ asset('js/app.js') }}"></script> -->
    <script type="text/javascript" src="{!! asset('js/jquery.validate.js') !!}"></script>

    <script>
         window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
            ]); ?>
         
         var baseUrl = "{{ url('/') }}";
         var userFavCounter = "";
         var userPlanCityID = 0;
         var isPremiumUser = 0;
         var isLoggedIn = @if(Auth::guard('user')->check()) "1" @else "0" @endif;

      </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-4 col-sm-4 col-md-3">
                        <div class="navbar-header">
                            <!-- Branding Image -->
                            <a class="navbar-brand" href="{{ url('/') }}">
                                <i class="fa fa-map" aria-hidden="true"></i>
                                {{ config('app.name', 'Laravel') }}
                            </a>
                            
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-5 col-md-5">
                        <form name="frmsearch" id="frmsearch" action="{{ route('search')}}" method="POST" class="frmsearch row">
                            <!-- <input type="hidden" name="state" id="state"> -->
                            {{ csrf_field() }}
                            <div class="col-xs-6 col-sm-6">
                                <i class="fa fa-map-pin" aria-hidden="true"></i>
                                <!-- <select id="searchBarCity" name="searchBarCity" class="searchBarCity"><option>Select City</option></select> -->
                                <input type="hidden" id="searchBarCity" name="searchBarCity">
                                <input type="text" id="searchBarCityName" name="searchBarCityName" class="searchBarCity" placeholder="Select City" readonly data-toggle="modal" data-target="#myModalForSearch">
                            </div>
                            <div class="col-xs-6 col-sm-6">
                                <select id="searchBarArea" name="searchBarArea" class="searchBarArea"><option>Select Area</option></select> 
                            </div>
                        </form>
                    </div>
                    <div class="col-xs-2 hidden-sm hidden-md hidden-lg mobilemenu-tooggle">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                       
                        <span class="hamb-top"></span>
                        <span class="hamb-middle"></span>
                        <span class="hamb-bottom"></span>

                        </button>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-4">
                        <!-- Collapsed Hamburger -->
                        
                        <div class="collapse navbar-collapse" id="app-navbar-collapse">
                            
                            <!-- Right Side Of Navbar -->
                            <ul class="nav navbar-nav navbar-right">
                                <!-- Authentication Links -->
                                @if (Auth::guard('user')->check())
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                                        {{ Auth::guard('user')->user()->name }} <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="{{ route('profile') }}">
                                                My Profile
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('myfavourite') }}">
                                                My Favourite
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('profile.changePassword') }}">
                                                Change Password
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('user_plans') }}">
                                                Plans
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('payment-history') }}">
                                                Payment History
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('user_logout') }}"
                                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>
                                            <form id="logout-form" action="{{ route('user_logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                                @else
                                <li><a href="{{ route('user_login') }}">Login</a></li>
                                <li><a href="{{ route('user_register') }}">Register</a></li>
                                @endguest
                                <li class="dropdown header-toggle-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        <div class="toggle-icon">
                                            <span class="hamb-top"></span> 
                                            <span class="hamb-middle"></span> 
                                            <span class="hamb-bottom"></span> 
                                        </div>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="{{route('contact-us')}}">Contact Us</a>
                                        </li>
                                        <li>
                                            <a href="{{route('about-us')}}">About Us</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                       
                    </div>
                </div>
            </div>
        </nav>
        @yield('content')
    </div>
    <!-- <script src="{{ asset('js/frontend/topsearchbar.js') }}"></script> -->
    <div class="cssload-loader" style="display: none;">
        <div class="cssload-center"></div>
    </div>
    <!-- Modal For Search City List -->
      <div class="modal fade" id="myModalForSearch" role="dialog">
        <div class="modal-dialog">
        
          <!-- Modal content-->
          <div class="modal-content">
            <!-- <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Modal Header</h4>
            </div> -->
            <div class="modal-body">
              <ul id="myModalUlCityList"></ul>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
          
        </div>
      </div>
    <!-- Scripts -->
    <script type="text/javascript">
        $(document).ready(function() {
            /*$(".menu .menu-btn").click(function () {
                $(".menu ul.navbar-nav").toggleClass("active");
                $(this).toggleClass("open");
            });*/
        /*navigator.geolocation.getCurrentPosition(function (pos) {
            var geocoder = new google.maps.Geocoder();
            var lat = pos.coords.latitude;
            var lng = pos.coords.longitude;
            var latlng = new google.maps.LatLng(lat, lng);

            //reverse geocode the coordinates, returning location information.
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                var result = results[0];
                var state = '';
                console.log(result);

                for (var i = 0, len = result.address_components.length; i < len; i++) {
                    var ac = result.address_components[i];

                    if (ac.types.indexOf('administrative_area_level_2') >= 0) {
                        state = ac.long_name;
                    }
                }
                /*$("#city").val(city);
                $("#state").trigger("change");
            });
        });*/
            //$('.searchBarCity').select2();
            
        });

    function getCity(){
        
        //$("#searchBarCity").empty().append("<option value=''>Select City</option>");
        $("#searchBarArea").empty().append("<option value=''>Select Area</option>");
        //  var stateID = 1;//$('#state').val();
        /*if(stateID == null){
            return;
        }*/
        $.ajax({
            url: baseUrl+'/ajax-city',
            type:'GET',
            beforeSend: function()
            {
                $(".cssload-loader").show();
            },
            /*data:{state : $("#state").val()},*/

            success: function(result){
                @if(!empty($dataArray['searchBarCity']))
                    var searchBarCity = <?php echo $dataArray['searchBarCity']; ?>;
                    //$("#searchBarCity").val(searchBarCity);
                @endif
                $.each(result, function(i, value) {
                    $('#myModalUlCityList').append($('<li data-cityname="'+value+'">').text(value).attr('data-cityid', i));
                    if(searchBarCity == i){
                        $("#searchBarCityName").val(value);
                        $("#searchBarCity").val(searchBarCity);
                    }
                });
                
                $(".cssload-loader").hide();
            }
        });
    }

    
    $('#searchBarCity').change(function(){
        $.ajax({
            url: baseUrl+'/ajax-area',
            type:'GET',
            datatype : "application/json",
            contentType: "text/plain",
            data:{city : $("#searchBarCity").val()},
            beforeSend: function()
            {
                $(".cssload-loader").show();
            },
            success: function(result){

                $("#searchBarArea").empty();
                $('#searchBarArea').append($('<option>').text('Select Area').attr('value', null));
                $.each(result, function(i, value) {
                    $('#searchBarArea').append($('<option>').text(value).attr('value', i));
                });
                $(".cssload-loader").hide();
            }
        })
    });
    $(document).ready(function() {
        $('.searchBarArea').select2();
        
        getCity();

        setTimeout(
            function(){
                $('#myModalForSearch li').click(function(){  
                    $("#searchBarCityName").val($(this).attr('data-cityname'));
                    $("#searchBarCity").val($(this).attr('data-cityid'));
                    ;
                    $('#myModalForSearch').modal('hide');
                    $('#searchBarCity').trigger('change');
                });
            },1000);

        setTimeout(
            function(){
                @if(!empty($dataArray['searchBarCity']))
                var searchBarCity = <?php echo $dataArray['searchBarCity']; ?>;
                $.ajax({
                    url: baseUrl+'/ajax-area',
                    type:'GET',
                    datatype : "application/json",
                    contentType: "text/plain",
                    data:{city : searchBarCity},
                    beforeSend: function()
                    {
                        $(".cssload-loader").show();
                    },
                    success: function(result){
                        $("#searchBarArea").empty();
                        $('#searchBarArea').append($('<option>').text('Select Area').attr('value', null));
                        $.each(result, function(i, value) {
                            $('#searchBarArea').append($('<option>').text(value).attr('value', i));
                        });

                        @if(!empty($dataArray['searchBarArea']))
                            var searchBarArea = <?php echo $dataArray['searchBarArea']; ?>;
                            $("#searchBarArea").val(searchBarArea);
                        @endif
                        
                        $(".cssload-loader").hide();
                    }
                });
                @endif
          }, 10);

        if($('.authcontent').length > 0 ){
            $('body').addClass('loginpage');
        }else{
            $('body').removeClass('loginpage');
        }
    });
    $('#searchBarArea').change(function(){
        $('#frmsearch').submit();
    });
    </script>
</body>
</html>
