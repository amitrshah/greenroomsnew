















<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head><meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
    <meta name="referrer" content="origin">
    <meta name="viewport" content="initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="application-name" content="GreenRoom">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="apple-mobile-web-app-title" content="GreenRoom">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') {{ config('app.name', 'Green Rooms') }}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/bootstrap-grid.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/bootstrap-reboot.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/latolatinfonts.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/all.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/select2.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/lightslider.min.css')}}"/>
    <link rel="stylesheet" type="text/css" media="(orientation: portrait)" href="{{ asset('css/frontend/portrait.min.css')}}" />
    <link rel="stylesheet" type="text/css" media="(orientation: landscape)" href="{{ asset('css/frontend/landscape.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/style.min.css')}}" />
    <script src="{{ asset('js/frontend/jquery-3.3.1.min.js') }}" type="text/javascript" charset="utf-8"></script>
    <script src="{{ asset('js/jquery.validate.js') }}"></script>
    <script type="text/javascript" charset="utf-8">
        window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token(),]); ?>;
        $baseUrl = "{{ url('/') }}";
        $userFavCounter = "";
        $userPlanCityID = 0;
        $isPremiumUser = 0;
        $isLoggedIn = @if(Auth::guard('user')->check()) "1" @else "0" @endif;
    </script>
</head>
<body>
<!-- /header -->
<header id="header" class="header">
    <div class="container">
        <nav class="navbar navbar-expand-xl navbar-collapse-lg navbar-collapse-md navbar-collapse-sm navigate">
            <div>
                <a href="{{ url('/') }}"><img src="{{ asset('img/logo.png')}}" alt="" class="logo"></a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample03" aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">
                <span><i class="fas fa-bars"></i></span>
            </button>
            <div class="collapse navbar-collapse justify-content-sm-end" id="navbarsExample03">
                <ul class="nav greenroom_nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('list-your-property') }}">List Your Property</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('search') }}">Need a PG</a>
                    </li>
                    <?php /*<li class="nav-item">
                        <a class="nav-link" href="{{ route('search') }}">Search</a>
                    </li>*/ ?>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('how-it-works') }}">How it works?</a>
                    </li>
                    <?php if (!isset(Auth::guard('user')->user()->id)){ ?>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('user_login') }}"  @if (\Route::current()->getName() == 'user_login')class = "active"@endif">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('user_register') }}"  @if (\Route::current()->getName() == 'user_register')class = "active"@endif">Signup</a>
                    </li>
                    <?php }else{ ?>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('user_logout') }}"  @if (\Route::current()->getName() == 'user_logout')class = "active"@endif">Logout</a>
                    </li>
                    <?php } ?>
                    <!-- <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="https://example.com" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown04">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li> -->
                </ul>
            </div>
        </nav>
    </div>
</header>
<!-- /header -->
@yield('content')
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">
                <h1 class="m-title m-title-0">Search By Localities</h1>
                <ul class="specs">
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Bodakdev</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Satellite</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Prahlad Nagar</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Navrangpura</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Gurukul</a></li>
                </ul>
            </div>
            <div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">
                <ul class="specs mt-5 xt-0">
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Bodakdev</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Satellite</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Prahlad Nagar</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Navrangpura</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Gurukul</a></li>
                </ul>
            </div>
            <div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">
                <ul class="specs mt-5 xt-0">
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Bodakdev</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Satellite</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Prahlad Nagar</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Navrangpura</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Gurukul</a></li>
                </ul>
            </div>
            <div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">
                <ul class="specs mt-5 xt-0">
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Bodakdev</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Satellite</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Prahlad Nagar</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Navrangpura</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Gurukul</a></li>
                </ul>
            </div>
        </div>
        <div class="row mt-5 xt-0">
            <div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">
                <h1 class="m-title">Corporate</h1>
                <ul class="specs">
                    <li><a href="{{ url('/') }}" title="">Home</a></li>
                    <li><a href="{{ route('about-us') }}" title="">About Us</a></li>
                    <li><a href="{{ route('about-us') }}" title="">Blog</a></li>
                    <li><a href="{{ route('refer-and-earn') }}" title="">Refer and Earn</a></li>
                    <li><a href="{{ route('owners') }}" title="">Owners</a></li>
                    <li><a href="{{ route('affiliate') }}" title="">Affiliate</a></li>
                    <li><a href="{{ route('complaint') }}" title="">Complaint</a></li>
                </ul>
            </div>
            <div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">
                <h1 class="m-title">Contact With Us</h1>
                <ul class="specs">
                    <li><a href="{{ route('contact-us') }}" title="">Contact Us</a></li>
                    <li><a href="{{ route('faqs') }}" title="">FAQ</a></li>
                    <li><a href="{{ route('terms') }}" title="">Greenroom Terms of Use</a></li>
                    <li><a href="{{ route('privacy-policy') }}" title="">Privacy Policy</a></li>
                    <li><a href="{{ route('tenants') }}" title="">Tenants</a></li>
                    <li><a href="{{ route('testimonial') }}" title="">Testimonial</a></li>
                </ul>
            </div>
            <div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">
                <h1 class="m-title">Let us help you</h1>
                <ul class="specs">
                    <li><a href="{{ route('owners-benefits') }}" title="">Owner Benefits</a></li>
                    <li><a href="{{ route('tenants-benefits') }}" title="">Tenants Benefits</a></li>
                    <li><a href="{{ route('why-choose-us') }}" title="">Why Choose Us</a></li>
                    <li><a href="{{ route('how-it-works') }}" title="">How it works</a></li>
                    <li><a href="{{ route('corporate') }}" title="">Corporate</a></li>
                    <li><a href="{{ route('careers') }}" title="">Careers</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<footer class="ftr">
    <div class="container">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="lside">
                <p>Copyright &copy; 2019 Greenroom Technologies Pvt. Ltd.</p>
            </div>
            <div>
                <ul class="socialIcons">
                    <li><i class="fab fa-facebook-square"></i></li>
                    <li><i class="fab fa-twitter-square"></i></li>
                    <li><i class="fab fa-linkedin"></i></li>
                    <li><i class="fab fa-google-plus-square"></i></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<script src="{{ asset('js/frontend/popper.min.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('js/frontend/bootstrap.min.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('js/frontend/select2.full.min.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('js/frontend/locality.json') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('js/frontend/cities.json') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('js/frontend/lightslider.min.js') }}"  type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('js/frontend/index.min.js') }}"  type="text/javascript" charset="utf-8"></script>
</body>
</html>