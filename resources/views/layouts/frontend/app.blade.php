<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head><meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
    <meta name="referrer" content="origin">
    <meta name="viewport" content="initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="application-name" content="GreenRoom">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="apple-mobile-web-app-title" content="GreenRoom">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>@yield('title') {{ config('app.name', 'Green Rooms') }}</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
            rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Heebo:100,300,400,500,700,800,900" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/bootstrap-grid.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/bootstrap-reboot.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/latolatinfonts.min.css')}}" />
    {{--<link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/bootstrap-grid.min.css')}}" />--}}
    {{--<link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/bootstrap-reboot.min.css')}}" />--}}
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/latolatinfonts.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/select2.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/lightslider.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/style.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/style.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/all.min.css')}}" />
    <link rel="stylesheet" type="text/css"
          href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    @yield('header')
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/bootstrap.min.css') }}" />--}}
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/bootstrap-grid.min.css')}}" />--}}
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/bootstrap-reboot.min.css')}}" />--}}
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/latolatinfonts.min.css')}}" />--}}
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/all.min.css')}}" />--}}
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/select2.min.css')}}" />--}}
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/lightslider.min.css')}}"/>--}}
    {{--<link rel="stylesheet" type="text/css" media="(orientation: portrait)" href="{{ asset('css/frontend/portrait.min.css')}}" />--}}
    {{--<link rel="stylesheet" type="text/css" media="(orientation: landscape)" href="{{ asset('css/frontend/landscape.min.css')}}" />--}}
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/style.min.css')}}" />--}}
    <script type="text/javascript" charset="utf-8">
        window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token(),]); ?>;
        $baseUrl = "{{ url('/') }}";
        $userFavCounter = "";
        $userPlanCityID = 0;
        $isPremiumUser = 0;
        $isLoggedIn = @if(Auth::guard('user')->check()) "1" @else "0" @endif;
    </script>
</head>
<body>
<!-- /header -->
<header id="header" class="header fixed-top inner_header shadow-sm">
    <div class="top">
        <div class="container">
            <div>
                <ul class="info mr-0 pr-0">
                    <li>
                        <a href="javascript:void(0)"><i class="fas fa-headset"></i> 9998882000</a>
                    </li>
                    <li>|</li>
                    <li>
                        <a href="javascript:void(0)"><i class="far fa-envelope-open"></i>
                            Contact@greenroom.com</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container bottom">

        @if(\Auth::check())
            <nav class="navbar navbar-expand-lg navbar-expand-xl navbar-collapse-sm">
                <a href="{{route('user_home')}}" class="logo">
                    <img src="{{asset('frontend/assets/img/logo.png')}}" alt="" />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample03"
                        aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">
                    <span><i class="fas fa-bars"></i></span>
                </button>
                <div class="collapse navbar-collapse justify-content-sm-end" id="navbarsExample03">
                    <ul class="nav greenroom_nav">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('user_home')}}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('dashboard_owner')}}">Dashboard</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="{{route('owner_profile')}}">My Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('post_your_property')}}">+ Add Property</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" id="dropdownMenuButton"
                               href="">Manage Your
                                Property</a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{route('property_list')}}">My Property
                                    List</a>
                                <a class="dropdown-item"
                                   href="{{route('property_list',"Available")}}">Available Property List</a>
                                <a class="dropdown-item" href="{{route('property_list',"occupied")}}">Occupied
                                    Property List</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="dashboard_owner_response.html">Response</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route("front_logout")}}">Logout</a>
                        </li>
                    </ul>
                </div>
            </nav>
        @else
        <nav class="navbar navbar-expand-xl navbar-collapse-lg navbar-collapse-sm">
            <a href="{{route('user_home')}}" class="logo">
                <img src="{{asset('frontend/assets/img/logo.png')}}" alt="" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample03"
                    aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">
                <span><i class="fas fa-bars"></i></span>
            </button>
            <div class="collapse navbar-collapse justify-content-sm-end" id="navbarsExample03">
                <ul class="nav greenroom_nav">
                    <li class="{{ Request::path() ===  '/' ? 'active nav-item' : 'nav-item' }}">
                        <a class="nav-link" href="{{route('user_home')}}">Home</a>
                    </li>
                    <li class="{{ Request::path() ===  'about-us' ? 'active nav-item' : 'nav-item' }}">
                        <a class="nav-link" href="{{route('about_us')}}">About Us</a>
                    </li>
                    <li class="{{ Request::path() ===  'about-us-green-room' ? 'active nav-item' : 'nav-item' }}">
                        <a class="nav-link" href="{{route('about_us_green_room')}}">About Greenroom</a>
                    </li>
                    <li class="{{ Request::path() ===  'search' ? 'active nav-item' : 'nav-item' }}">
                        <a class="nav-link" href="{{route('search')}}">Search</a>
                    </li>
                    <li class="{{ Request::path() ===  'post-requirement' ? 'active nav-item' : 'nav-item' }}">
                        <a class="nav-link" href="">Post Requirement</a>
                    </li>
                    <li class="{{ Request::path() ===  'post-your-property' ? 'active nav-item' : 'nav-item' }}">
                        <a class="nav-link" href="{{route('post_your_property')}}">Post Property</a>
                    </li>


                    @if(\Auth::check())
                    <li class="nav-item">
                        <a class="nav-link logout" href="{{route("front_logout")}}">logout</a>
                    </li>
                        @else
                        <li class="nav-item">
                            <a class="nav-link login" href="javascript:void(0)">Login</a>
                        </li><li class="nav-item">
                        <a class="nav-link register" href="javascript:void(0)">Signup</a>
                    </li>
                        @endif
                </ul>
            </div>
        </nav>
        @endif
    </div>
</header>
{{--<header id="header" class="header">--}}
{{--<div class="container">--}}
{{--<nav class="navbar navbar-expand-xl navbar-collapse-lg navbar-collapse-md navbar-collapse-sm navigate">--}}
{{--<div>--}}
{{--<a href="{{ url('/') }}"><img src="{{ asset('img/logo.png')}}" alt="" class="logo"></a>--}}
{{--</div>--}}
{{--<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample03" aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">--}}
{{--<span><i class="fas fa-bars"></i></span>--}}
{{--</button>--}}
{{--<div class="collapse navbar-collapse justify-content-sm-end" id="navbarsExample03">--}}
{{--<ul class="nav greenroom_nav">--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{ route('list-your-property') }}">List Your Property</a>--}}
{{--</li>--}}
{{----}}
{{--<li class="nav-item">--}}
{{--<a class="nav-link" href="{{ route('search') }}">Need a PG</a>--}}
{{--</li>--}}
{{--<?php /*<li class="nav-item">--}}
{{--<a class="nav-link" href="{{ route('search') }}">Search</a>--}}
{{--</li>*/ ?>--}}
{{--<li class="nav-item">--}}
{{--<a class="nav-link" href="{{ route('how-it-works') }}">How it works?</a>--}}
{{--</li>--}}
{{--<?php if (!isset(Auth::guard('user')->user()->id)){ ?>--}}
{{--<li class="nav-item">--}}
{{--<a class="nav-link" href="{{ route('user_login') }}"  @if (\Route::current()->getName() == 'user_login')class = "active"@endif">Login</a>--}}
{{--</li>--}}
{{--<li class="nav-item">--}}
{{--<a class="nav-link" href="{{ route('user_register') }}"  @if (\Route::current()->getName() == 'user_register')class = "active"@endif">Signup</a>--}}
{{--</li>--}}
{{--<?php }else{ ?>--}}
{{--<li class="nav-item">--}}
{{--<a class="nav-link" href="{{ route('user_logout') }}"  @if (\Route::current()->getName() == 'user_logout')class = "active"@endif">Logout</a>--}}
{{--</li>--}}
{{--<?php } ?>--}}
{{--<!-- <li class="nav-item dropdown">--}}
{{--<a class="nav-link dropdown-toggle" href="https://example.com" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>--}}
{{--<div class="dropdown-menu" aria-labelledby="dropdown04">--}}
{{--<a class="dropdown-item" href="#">Action</a>--}}
{{--<a class="dropdown-item" href="#">Another action</a>--}}
{{--<a class="dropdown-item" href="#">Something else here</a>--}}
{{--</div>--}}
{{--</li> -->--}}
{{--</ul>--}}
{{--</div>--}}
{{--</nav>--}}
{{--</div>--}}
{{--</header>--}}
<!-- /header -->
@yield('content')
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">
                <h1 class="m-title">Search By Localities</h1>
                <ul class="specs">
                    <li>
                        <a href="javascript:void(0)" title="">Mens PG Hostels in Bodakdev</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">Mens PG Hostels in Satellite</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">Mens PG Hostels in Prahlad Nagar</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">Mens PG Hostels in Navrangpura</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">Mens PG Hostels in Gurukul</a>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">
                <h1 class="m-title d-none d-sm-block d-md-block d-lg-block d-xl-block">
                    &nbsp;
                </h1>
                <ul class="specs">
                    <li>
                        <a href="javascript:void(0)" title="">Mens PG Hostels in Bodakdev</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">Mens PG Hostels in Satellite</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">Mens PG Hostels in Prahlad Nagar</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">Mens PG Hostels in Navrangpura</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">Mens PG Hostels in Gurukul</a>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">
                <h1 class="m-title d-none d-sm-block d-md-block d-lg-block d-xl-block">
                    &nbsp;
                </h1>
                <ul class="specs">
                    <li>
                        <a href="javascript:void(0)" title="">Mens PG Hostels in Bodakdev</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">Mens PG Hostels in Satellite</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">Mens PG Hostels in Prahlad Nagar</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">Mens PG Hostels in Navrangpura</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">Mens PG Hostels in Gurukul</a>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">
                <h1 class="m-title d-none d-sm-block d-md-block d-lg-block d-xl-block">
                    &nbsp;
                </h1>
                <ul class="specs">
                    <li>
                        <a href="javascript:void(0)" title="">Mens PG Hostels in Bodakdev</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">Mens PG Hostels in Satellite</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">Mens PG Hostels in Prahlad Nagar</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">Mens PG Hostels in Navrangpura</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">Mens PG Hostels in Gurukul</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">
                <h1 class="m-title mt-4 mb-2">Corporate</h1>
                <ul class="specs">
                    <li><a href="index.html" title="">Home</a></li>
                    <li><a href="about.html" title="">About Us</a></li>
                    <li><a href="about_greenroom.html" title="">About Greenroom</a></li>
                    <li><a href="search.html" title="">Search</a></li>
                    <li><a href="how_it_works.html" title="">How It Works?</a></li>
                    <li><a href="mission_vision.html" title="">Mission & Vision</a></li>
                    <li class="login"><a href="javascript:void(0)" title="">Login</a></li>
                    <li class="register"><a href="javascript:void(0)" title="">Sign Up</a></li>
                </ul>
            </div>
            <div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">
                <h1 class="m-title mt-4 mb-2">Contact With Us</h1>
                <ul class="specs">
                    <li><a href="contact.html" title="">Contact Us</a></li>
                    <li><a href="faqs.html" title="">FAQ's</a></li>
                    <li>
                        <a href="tnc.html" title="">Greenroom Terms of Use</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<footer class="ftr">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                <p>Copyright © 2018 Greenroom Technologies Pvt. Ltd.</p>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                <ul class="socialIcons">
                    <li>
                        <a href="javascript:void(0)"><i class="fab fa-facebook-square"></i></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><i class="fab fa-twitter-square"></i></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><i class="fab fa-linkedin"></i></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><i class="fab fa-google-plus-square"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- Modal For Register Start Here -->
<div class="modal shadow" tabindex="-1" role="dialog" id="registrationModal" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" role="document">

        <div class="modal-content">
            <div class="modal-header">

                <h6 class="modal-title" id="exampleModalCenterTitle">
                    Register for a free Greenroom Account
                </h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div id="signup_error" style="color: red">
                </div>
                <form  id="signup_modal">
                    <div class="form-group">
                        <label class="is-small">Full Name</label>
                        <input type="text" name="full_name" class="form-control" id="fullname" placeholder="Enter Full Name" />
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                    </div>

                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label class="is-small">Username</label>
                                <input type="text" name="username" class="form-control rounded-0" id="userName"
                                       placeholder="Enter Username" />
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label class="is-small">Email/Phone</label>
                                <input type="text" name="email" class="form-control rounded-0" id="emailOrPhone"
                                       placeholder="Enter email or phone" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label class="is-small">Password</label>
                                <input type="password" name="password" class="form-control rounded-0" id="password"
                                       placeholder="Enter Password" />
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label class="is-small">Confirm Password</label>
                                <input type="text" name="cpass" class="form-control rounded-0" id="confirmPssword"
                                       placeholder="Enter confirm password" />
                            </div>
                        </div>
                    </div>
                    <hr class="mt-1" />
                    <p class="terms p-0 mt-0 text-primary">
                        Enter verification code sent to your mobile no. 91-9974408232
                    </p>
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label class="is-small">Enter Code</label>
                                <input type="text" class="form-control rounded-0" id="entercode"
                                       placeholder="Enter Password" />
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                        </div>
                    </div>
                    <hr class="mt-1" />
                    <div class="form-group">
                        <p class="terms">
                            By clicking below, you agree to &nbsp;<a href="javascript:void(0)"
                                                                     class="text-primary">Terms & Conditions</a>
                        </p>
                    </div>
                    <button type="button" class="btn btn-lg btn-success shadow-sm mr-2" id="signup_btn">
                        Register Now!
                    </button>
                    <button type="button" data-dismiss="modal"
                            class="btn btn-secondary btn-secondary shadow-sm" >
                        Cancel
                    </button>
                    <a href="javascript:void(0)" class="signIn d-block">Already registered? Sign in</a>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal For Register End Here -->
<!-- Modal For Login Start Here -->
<div class="modal shadow" tabindex="-1" role="dialog" id="loginModal" data-keyboard="false">
    <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">

                <h6 class="modal-title" id="loginTitle">

                    Login
                </h6>
                <a href="javascript:void(0)" class="registerModal">Not a member? Sign up</a>
                {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                    {{--<span aria-hidden="true">&times;</span>--}}
                {{--</button>--}}

            </div>
            <div class="modal-body">
                <div class="">
                    <div id="signup_success" style="color:green">
                    </div>
                    <div id="login_error"  style="color:red">
                    </div>
                <form id="login_modal">
                    <div class="form-group">
                        <label class="is-small">Username/Email</label>
                        <input type="text" name="login" class="form-control rounded-0" id="email"
                               placeholder="Enter username or email" />
                    </div>
                    <div class="form-group">
                        <label class="is-small">Password</label>
                        <input type="password" name="password" class="form-control rounded-0" id="password"
                               placeholder="Enter password" />
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                    </div>
                    <button type="button" class="btn btn-lg btn-success shadow-sm" id="login">
                        Login
                    </button>
                    <button type="button" data-dismiss="modal" class="btn btn-lg btn-secondary shadow-sm">
                        Cancel
                    </button>
                    <div class="usefulInfo">
                        <a href="javascript:void(0)" class="forgotPassword">Forgot Password</a>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal For Login End Here -->
<!-- Modal For Forgot Start Here -->
<div class="modal shadow" tabindex="-1" role="dialog" id="forgotModal" data-keyboard="false">
    <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="forgotTitle">
                    Forgot Password
                </h6>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button> -->
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label class="is-small">Username/Email</label>
                        <input type="text" class="form-control rounded-0" id="userEmailName"
                               placeholder="Enter username or email" />
                    </div>
                    <button type="button" class="btn btn-lg btn-success shadow-sm">
                        Retrieve
                    </button>
                    <button type="button" data-dismiss="modal" class="btn btn-lg btn-secondary shadow-sm">
                        Cancel
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal For Login End Here -->



{{--<footer class="footer">--}}
{{--<div class="container">--}}
{{--<div class="row">--}}
{{--<div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">--}}
{{--<h1 class="m-title m-title-0">Search By Localities</h1>--}}
{{--<ul class="specs">--}}
{{--<li><a href="javascript:void(0)" title="">Mens PG Hostels in Bodakdev</a></li>--}}
{{--<li><a href="javascript:void(0)" title="">Mens PG Hostels in Satellite</a></li>--}}
{{--<li><a href="javascript:void(0)" title="">Mens PG Hostels in Prahlad Nagar</a></li>--}}
{{--<li><a href="javascript:void(0)" title="">Mens PG Hostels in Navrangpura</a></li>--}}
{{--<li><a href="javascript:void(0)" title="">Mens PG Hostels in Gurukul</a></li>--}}
{{--</ul>--}}
{{--</div>--}}
{{--<div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">--}}
{{--<ul class="specs mt-5 xt-0">--}}
{{--<li><a href="javascript:void(0)" title="">Mens PG Hostels in Bodakdev</a></li>--}}
{{--<li><a href="javascript:void(0)" title="">Mens PG Hostels in Satellite</a></li>--}}
{{--<li><a href="javascript:void(0)" title="">Mens PG Hostels in Prahlad Nagar</a></li>--}}
{{--<li><a href="javascript:void(0)" title="">Mens PG Hostels in Navrangpura</a></li>--}}
{{--<li><a href="javascript:void(0)" title="">Mens PG Hostels in Gurukul</a></li>--}}
{{--</ul>--}}
{{--</div>--}}
{{--<div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">--}}
{{--<ul class="specs mt-5 xt-0">--}}
{{--<li><a href="javascript:void(0)" title="">Mens PG Hostels in Bodakdev</a></li>--}}
{{--<li><a href="javascript:void(0)" title="">Mens PG Hostels in Satellite</a></li>--}}
{{--<li><a href="javascript:void(0)" title="">Mens PG Hostels in Prahlad Nagar</a></li>--}}
{{--<li><a href="javascript:void(0)" title="">Mens PG Hostels in Navrangpura</a></li>--}}
{{--<li><a href="javascript:void(0)" title="">Mens PG Hostels in Gurukul</a></li>--}}
{{--</ul>--}}
{{--</div>--}}
{{--<div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">--}}
{{--<ul class="specs mt-5 xt-0">--}}
{{--<li><a href="javascript:void(0)" title="">Mens PG Hostels in Bodakdev</a></li>--}}
{{--<li><a href="javascript:void(0)" title="">Mens PG Hostels in Satellite</a></li>--}}
{{--<li><a href="javascript:void(0)" title="">Mens PG Hostels in Prahlad Nagar</a></li>--}}
{{--<li><a href="javascript:void(0)" title="">Mens PG Hostels in Navrangpura</a></li>--}}
{{--<li><a href="javascript:void(0)" title="">Mens PG Hostels in Gurukul</a></li>--}}
{{--</ul>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="row mt-5 xt-0">--}}
{{--<div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">--}}
{{--<h1 class="m-title">Corporate</h1>--}}
{{--<ul class="specs">--}}
{{--<li><a href="{{ url('/') }}" title="">Home</a></li>--}}
{{--<li><a href="{{ route('about-us') }}" title="">About Us</a></li>--}}
{{--<li><a href="{{ route('about-us') }}" title="">Blog</a></li>--}}
{{--<li><a href="{{ route('refer-and-earn') }}" title="">Refer and Earn</a></li>--}}
{{--<li><a href="{{ route('owners') }}" title="">Owners</a></li>--}}
{{--<li><a href="{{ route('affiliate') }}" title="">Affiliate</a></li>--}}
{{--<li><a href="{{ route('complaint') }}" title="">Complaint</a></li>--}}
{{--</ul>--}}
{{--</div>--}}
{{--<div class="col-12 col-lg-3 col-signup_successsm-12 col-md-3 col-xl-3">--}}
{{--<h1 class="m-title">Contact With Us</h1>--}}
{{--<ul class="specs">--}}
{{--<li><a href="{{ route('contact-us') }}" title="">Contact Us</a></li>--}}
{{--<li><a href="{{ route('faqs') }}" title="">FAQ</a></li>--}}
{{--<li><a href="{{ route('terms') }}" title="">Greenroom Terms of Use</a></li>--}}
{{--<li><a href="{{ route('privacy-policy') }}" title="">Privacy Policy</a></li>--}}
{{--<li><a href="{{ route('tenants') }}" title="">Tenants</a></li>--}}
{{--<li><a href="{{ route('testimonial') }}" title="">Testimonial</a></li>--}}
{{--</ul>--}}
{{--</div>--}}
{{--<div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">--}}
{{--<h1 class="m-title">Let us help you</h1>--}}
{{--<ul class="specs">--}}
{{--<li><a href="{{ route('owners-benefits') }}" title="">Owner Benefits</a></li>--}}
{{--<li><a href="{{ route('tenants-benefits') }}" title="">Tenants Benefits</a></li>--}}
{{--<li><a href="{{ route('why-choose-us') }}" title="">Why Choose Us</a></li>--}}
{{--<li><a href="{{ route('how-it-works') }}" title="">How it works</a></li>--}}
{{--<li><a href="{{ route('corporate') }}" title="">Corporate</a></li>--}}
{{--<li><a href="{{ route('careers') }}" title="">Careers</a></li>--}}
{{--</ul>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</footer>--}}
{{--<footer class="ftr">--}}
{{--<div class="container">--}}
{{--<div class="d-flex justify-content-between flex-wrap">--}}
{{--<div class="lside">--}}
{{--<p>Copyright &copy; 2019 Greenroom Technologies Pvt. Ltd.</p>--}}
{{--</div>--}}
{{--<div>--}}
{{--<ul class="socialIcons">--}}
{{--<li><i class="fab fa-facebook-square"></i></li>--}}
{{--<li><i class="fab fa-twitter-square"></i></li>--}}
{{--<li><i class="fab fa-linkedin"></i></li>--}}
{{--<li><i class="fab fa-google-plus-square"></i></li>--}}
{{--</ul>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</footer>--}}

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="{{asset('frontend/assets/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/popper.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/select2.full.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/lightslider.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/index.js')}}"></script>
<script src="{{asset('frontend/assets/js/search.js')}}"></script>
<script src="{{asset('frontend/assets/js/dashboard_client_shorlisted_property.js')}}"></script>
<script src="{{asset('frontend/assets/js/property_details.js')}}"></script>
<script src="{{asset('frontend/assets/js/search_result.js')}}"></script>
<script src="{{asset('frontend/assets/js/jquery.nav.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="{{asset('frontend/assets/js/progressbar.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/jquery.bootstrap.wizard.min.js')}}"></script>
<script src="{{asset('frontend/assets/js/jquery.mousewheel.min.js')}}"></script>
{{--<script src="{{asset('frontend/assets/js/post_your_property.min.js')}}"></script>--}}
<script src="https://cdn.zingchart.com/zingchart.min.js"></script>
<script> zingchart.MODULESDIR = "https://cdn.zingchart.com/modules/";
    ZC.LICENSE = ["569d52cefae586f634c54f86dc99e6a9", "ee6b7db5b51705a13dc2339db3edaf6d"];</script>
<script src="{{asset('frontend/assets/js/dashboard_owner.js')}}"></script>
<script src="{{asset('frontend/assets/js/dashboard_owner_my_profile.js')}}"></script>
<script src="{{asset('frontend/assets/js/post_your_property.js')}}"></script>

<script>
    $(document).ready(function(){
        $("#btn-search-submit").click(function(){
            var form_id = $(this).attr("form-id");
            $("#"+form_id).submit();
        })
    })
</script>
@yield('footer')

{{--<script src="{{asset('js/frontend/jquery-3.3.1.min.js')}}" type="text/javascript" charset="utf-8"></script>--}}
{{--<script src="{{ asset('js/frontend/popper.min.js') }}" type="text/javascript" charset="utf-8"></script>--}}
{{--<script src="{{ asset('js/frontend/bootstrap.min.js') }}" type="text/javascript" charset="utf-8"></script>--}}
{{--<script src="{{ asset('js/frontend/select2.full.min.js') }}" type="text/javascript" charset="utf-8"></script>--}}
{{--<script src="{{ asset('js/frontend/locality.json') }}" type="text/javascript" charset="utf-8"></script>--}}
{{--<script src="{{ asset('js/frontend/cities.json') }}" type="text/javascript" charset="utf-8"></script>--}}
{{--<script src="{{ asset('js/frontend/lightslider.min.js') }}"  type="text/javascript" charset="utf-8"></script>--}}
{{--<script src="{{ asset('js/frontend/index.min.js') }}"  type="text/javascript" charset="utf-8"></script>--}}

</body>
</html>





