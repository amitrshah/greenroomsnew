<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head><meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') {{ config('app.name', 'Green Rooms') }}</title>
    
    <!-- google fonts link -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet"> 
    
    <!-- Styles -->
<!--    <link href="{{asset('bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
     <link href="{{asset('css/frontend/style.css')}}" rel="stylesheet">
     <link href="{{asset('css/frontend/perfect-scrollbar.css')}}" rel="stylesheet">-->
     
     <!--mehul style-->
     <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/bootstrap-grid.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/bootstrap-reboot.min.css')}}">
    
    <link rel="stylesheet" type="text/css" media="(orientation: portrait)" href="{{ asset('css/frontend/portrait.min.css')}}">
    
    <!--<link rel="stylesheet" type="text/css" media="(max-width: 640px)" href="{{ asset('css/frontend/max-640px.css')}}">
    <link rel="stylesheet" type="text/css" media="(min-width: 640px)" href="{{ asset('css/frontend/min-640px.css')}}">-->
    <link rel="stylesheet" type="text/css" media="(orientation: portrait)" href="{{ asset('css/frontend/portrait.css')}}">
    <link rel="stylesheet" type="text/css" media="(orientation: landscape)" href="{{ asset('css/frontend/landscape.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/latolatinfonts.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/all.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/select2.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/style-1.css')}}">
    
    
    <!--mehul style over-->
   
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <!-- Styles -->

    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('js/select2.full.js') }}"></script>
    <script src="{{ asset('js/frontend/perfect-scrollbar.min.js') }}"></script>
    <!-- <script src="{{ asset('js/app.js') }}"></script> -->
    <script type="text/javascript" src="{!! asset('js/jquery.validate.js') !!}"></script>

    <script>
         window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
            ]); ?>
         
         var baseUrl = "{{ url('/') }}";
         var userFavCounter = "";
         var userPlanCityID = 0;
         var isPremiumUser = 0;
         var isLoggedIn = @if(Auth::guard('user')->check()) "1" @else "0" @endif;

      </script>
</head>
<body>
    <!-- /header -->
<header id="header" class="header">
    <div class="container">
        <nav class="navbar navbar-expand-xl navbar-collapse-lg navbar-collapse-md navbar-collapse-sm navigate">
            <div>
                <a href="{{ url('/') }}"><img src="{{ asset('img/logo.png')}}" alt="" class="logo"></a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample03" aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">
                <span><i class="fas fa-bars"></i></span>
            </button>
            <div class="collapse navbar-collapse justify-content-sm-end" id="navbarsExample03">
                <ul class="nav greenroom_nav">
                    
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('list-your-property') }}">List Your Property</a>
                    </li>
                   
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('search') }}">Need a PG</a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('search') }}">Search</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">How it works?</a>
                    </li>
                    <?php if (!isset(Auth::guard('user')->user()->id)){ ?>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('user_login') }}"  @if (\Route::current()->getName() == 'user_login')class = "active"@endif">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('user_register') }}"  @if (\Route::current()->getName() == 'user_register')class = "active"@endif">Signup</a>
                    </li>
                    <?php } ?>
                    <!-- <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="https://example.com" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown04">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li> -->
                </ul>
            </div>
        </nav>
    </div>
</header>
<!-- /header -->

    <!--<div id="app">-->
<!--        <nav class="navbar navbar-default navbar-static-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-4 col-sm-4 col-md-3">
                        <div class="navbar-header">
                             Branding Image 
                            <a class="navbar-brand" href="{{ url('/') }}">
                                <i class="fa fa-map" aria-hidden="true"></i>
                                {{ config('app.name', 'Laravel') }}
                            </a>
                            
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-5 col-md-5">
                        <form name="frmsearch" id="frmsearch" action="{{ route('search')}}" method="POST" class="frmsearch row">
                             <input type="hidden" name="state" id="state"> 
                            {{ csrf_field() }}
                            <div class="col-xs-6 col-sm-6">
                                <div class="city-input-icon">
                                    <i class="fa fa-map-pin" aria-hidden="true"></i>
                                     <select id="searchBarCity" name="searchBarCity" class="searchBarCity"><option>Select City</option></select> 
                                    <input type="hidden" id="searchBarCity" name="searchBarCity">
                                    <input type="text" id="searchBarCityName" name="searchBarCityName" class="searchBarCity" placeholder="Select City" readonly data-toggle="modal" data-target="#myModalForSearch">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6">
                                <select id="searchBarArea" name="searchBarArea" class="searchBarArea"><option>Select Area</option></select> 
                            </div>
                        </form>
                    </div>
                    <div class="col-xs-2 hidden-sm hidden-md hidden-lg mobilemenu-tooggle">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                       
                        <span class="hamb-top"></span>
                        <span class="hamb-middle"></span>
                        <span class="hamb-bottom"></span>

                        </button>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-4">
                         Collapsed Hamburger 
                        
                        <div class="collapse navbar-collapse" id="app-navbar-collapse">
                            
                             Right Side Of Navbar 
                            <ul class="nav navbar-nav navbar-right">
                                 Authentication Links 
                                @if (Auth::guard('user')->check())
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                                        {{ Auth::guard('user')->user()->name }} <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="{{ route('profile') }}" @if (\Route::current()->getName() == 'profile')class = "active"@endif >
                                                My Profile
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('myfavourite') }}" @if (\Route::current()->getName() == 'myfavourite')class = "active"@endif>
                                                My Favourite
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('profile.changePassword') }}" @if (\Route::current()->getName() == 'profile.changePassword')class = "active"@endif>
                                                Change Password
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('user_plans') }}" @if (\Route::current()->getName() == 'user_plans')class = "active"@endif>
                                                Plans
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('payment-history') }}" @if (\Route::current()->getName() == 'payment-history')class = "active"@endif>
                                                Payment History
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('user_logout') }}"
                                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>
                                            <form id="logout-form" action="{{ route('user_logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                                @else
                                <li><a href="{{ route('user_login') }}"  @if (\Route::current()->getName() == 'user_login')class = "active"@endif >Login</a></li>
                                <li><a href="{{ route('user_register') }}"  @if (\Route::current()->getName() == 'user_register')class = "active"@endif >Register</a></li>
                                @endguest
                                <li class="dropdown header-toggle-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        <div class="toggle-icon">
                                            <span class="hamb-top"></span> 
                                            <span class="hamb-middle"></span> 
                                            <span class="hamb-bottom"></span> 
                                        </div>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="{{route('contact-us')}}" @if (\Route::current()->getName() == 'contact-us')class = "active"@endif >Contact Us</a>
                                        </li>
                                        <li>
                                            <a href="{{route('about-us')}}" @if (\Route::current()->getName() == 'about-us')class = "active"@endif >About Us</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                       
                    </div>
                </div>
            </div>
        </nav>-->

        @yield('content')
        
    <!--</div>-->
    <!-- <script src="{{ asset('js/frontend/topsearchbar.js') }}"></script> -->
<!--    <div class="cssload-loader" style="display: none;">
        <div class="cssload-center"></div>
    </div>-->
    <!-- Modal For Search City List -->
<!--      <div class="modal fade" id="myModalForSearch" role="dialog">
        <div class="modal-dialog">
        
           Modal content
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">What city would you like to move in?</h4>
            </div>
            <div class="my-modal-city-list">
              <ul id="myModalUlCityList"></ul>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
            </div>
          </div>
          
        </div>
      </div>-->



<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">
                <h1 class="m-title m-title-0">Search By Localities</h1>
                <ul class="specs">
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Bodakdev</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Satellite</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Prahlad Nagar</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Navrangpura</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Gurukul</a></li>
                </ul>
            </div>
            <div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">
                <ul class="specs mt-5 xt-0">
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Bodakdev</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Satellite</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Prahlad Nagar</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Navrangpura</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Gurukul</a></li>
                </ul>
            </div>
            <div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">
                <ul class="specs mt-5 xt-0">
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Bodakdev</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Satellite</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Prahlad Nagar</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Navrangpura</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Gurukul</a></li>
                </ul>
            </div>
            <div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">
                <ul class="specs mt-5 xt-0">
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Bodakdev</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Satellite</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Prahlad Nagar</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Navrangpura</a></li>
                    <li><a href="javascript:void(0)" title="">Mens PG Hostels in Gurukul</a></li>
                </ul>
            </div>
        </div>
        <div class="row mt-5 xt-0">
            <div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">
                <h1 class="m-title">Corporate</h1>
                <ul class="specs">
                    <li><a href="{{ url('/') }}" title="">Home</a></li>
                    <li><a href="{{ route('about-us') }}" title="">About Us</a></li>
                    <li><a href="javascript:void(0)" title="">Blog</a></li>
                    <li><a href="javascript:void(0)" title="">Refer and Earn</a></li>
                    <li><a href="javascript:void(0)" title="">Owners</a></li>
                    <li><a href="javascript:void(0)" title="">Affiliate</a></li>
                    <li><a href="javascript:void(0)" title="">Complaint</a></li>
                </ul>
            </div>
            <div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">
                <h1 class="m-title">Contact With Us</h1>
                <ul class="specs">
                    <li><a href="{{ route('contact-us') }}" title="">Contact Us</a></li>
                    <li><a href="{{ route('faqs') }}" title="">FAQ</a></li>
                    <li><a href="{{ route('terms') }}" title="">Greenroom Terms of Use</a></li>
                    <li><a href="javascript:void(0)" title="">Privacy Policy</a></li>
                    <li><a href="javascript:void(0)" title="">Tenants</a></li>
                    <li><a href="javascript:void(0)" title="">Testimonial</a></li>
                </ul>
            </div>
            <div class="col-12 col-lg-3 col-sm-12 col-md-3 col-xl-3">
                <h1 class="m-title">Let us help you</h1>
                <ul class="specs">
                    <li><a href="javascript:void(0)" title="">Owner Benefits</a></li>
                    <li><a href="javascript:void(0)" title="">Tenants Benefits</a></li>
                    <li><a href="javascript:void(0)" title="">Why Choose Us</a></li>
                    <li><a href="javascript:void(0)" title="">How it works</a></li>
                    <li><a href="javascript:void(0)" title="">Corporate</a></li>
                    <li><a href="javascript:void(0)" title="">Careers</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<footer class="ftr">
    <div class="container">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="lside">
                <p>Copyright Â© 2018 Greenroom Technologies Pvt. Ltd.</p>
            </div>
            <div>
                <ul class="socialIcons">
                    <li><i class="fab fa-facebook-square"></i></li>
                    <li><i class="fab fa-twitter-square"></i></li>
                    <li><i class="fab fa-linkedin"></i></li>
                    <li><i class="fab fa-google-plus-square"></i></li>
                </ul>
            </div>
        </div>
    </div>
</footer>


    <!-- Scripts -->
<!--    <script type="text/javascript">
        $(document).ready(function() {
            /*$(".menu .menu-btn").click(function () {
                $(".menu ul.navbar-nav").toggleClass("active");
                $(this).toggleClass("open");
            });*/
        /*navigator.geolocation.getCurrentPosition(function (pos) {
            var geocoder = new google.maps.Geocoder();
            var lat = pos.coords.latitude;
            var lng = pos.coords.longitude;
            var latlng = new google.maps.LatLng(lat, lng);

            //reverse geocode the coordinates, returning location information.
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                var result = results[0];
                var state = '';
                console.log(result);

                for (var i = 0, len = result.address_components.length; i < len; i++) {
                    var ac = result.address_components[i];

                    if (ac.types.indexOf('administrative_area_level_2') >= 0) {
                        state = ac.long_name;
                    }
                }
                /*$("#city").val(city);
                $("#state").trigger("change");
            });
        });*/
            //$('.searchBarCity').select2();
            
        });

    function getCity(){
        
        //$("#searchBarCity").empty().append("<option value=''>Select City</option>");
        $("#searchBarArea").empty().append("<option value=''>Select Area</option>");
        //  var stateID = 1;//$('#state').val();
        /*if(stateID == null){
            return;
        }*/
        $.ajax({
            url: baseUrl+'/ajax-city',
            type:'GET',
            beforeSend: function()
            {
                $(".cssload-loader").show();
            },
            /*data:{state : $("#state").val()},*/

            success: function(result){
                @if(!empty($dataArray['searchBarCity']))
                    var searchBarCity = <?php echo $dataArray['searchBarCity']; ?>;
                    //$("#searchBarCity").val(searchBarCity);
                @endif
                $.each(result, function(i, value) {
                    $('#myModalUlCityList').append($('<li data-cityname="'+value+'">').html("<i class='fa fa-map' aria-hidden='true'></i> "+value).attr('data-cityid', i));
                    if(searchBarCity == i){
                        $("#searchBarCityName").val(value);
                        $("#searchBarCity").val(searchBarCity);
                    }
                });
                
                $(".cssload-loader").hide();
            }
        });
    }

    
    $('#searchBarCity').change(function(){
        $.ajax({
            url: baseUrl+'/ajax-area',
            type:'GET',
            datatype : "application/json",
            contentType: "text/plain",
            data:{city : $("#searchBarCity").val()},
            beforeSend: function()
            {
                $(".cssload-loader").show();
            },
            success: function(result){

                $("#searchBarArea").empty();
                $('#searchBarArea').append($('<option>').text('Select Area').attr('value', null));
                $.each(result, function(i, value) {
                    $('#searchBarArea').append($('<option>').text(value).attr('value', i));
                });
                $(".cssload-loader").hide();
            }
        })
    });
    $(document).ready(function() {
        $('.searchBarArea').select2();
        
        getCity();

        setTimeout(
            function(){
                $('#myModalForSearch li').click(function(){  
                    $("#searchBarCityName").val($(this).attr('data-cityname'));
                    $("#searchBarCity").val($(this).attr('data-cityid'));
                    ;
                    $('#myModalForSearch').modal('hide');
                    $('#searchBarCity').trigger('change');
                });
            },1000);

        setTimeout(
            function(){
                @if(!empty($dataArray['searchBarCity']))
                var searchBarCity = <?php echo $dataArray['searchBarCity']; ?>;
                $.ajax({
                    url: baseUrl+'/ajax-area',
                    type:'GET',
                    datatype : "application/json",
                    contentType: "text/plain",
                    data:{city : searchBarCity},
                    beforeSend: function()
                    {
                        $(".cssload-loader").show();
                    },
                    success: function(result){
                        $("#searchBarArea").empty();
                        $('#searchBarArea').append($('<option>').text('Select Area').attr('value', null));
                        $.each(result, function(i, value) {
                            $('#searchBarArea').append($('<option>').text(value).attr('value', i));
                        });

                        @if(!empty($dataArray['searchBarArea']))
                            var searchBarArea = <?php echo $dataArray['searchBarArea']; ?>;
                            $("#searchBarArea").val(searchBarArea);
                        @endif
                        
                        $(".cssload-loader").hide();
                    }
                });
                @endif
          }, 10);

        if($('.authcontent').length > 0 ){
            $('body').addClass('loginpage');
        }else{
            $('body').removeClass('loginpage');
        }
    });
    $('#searchBarArea').change(function(){
        $('#frmsearch').submit();
    });


    </script>-->
    
<script src="{{ asset('js/frontend/jquery-3.3.1.min.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('js/frontend/bootstrap.min.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('js/frontend/popper.min.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap-wizard/1.2/jquery.bootstrap.wizard.min.js"></script>
<script src="{{ asset('js/frontend/select2.full.min.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('js/frontend/locality.json') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('js/frontend/cities.json') }}" type="text/javascript" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
<script src="{{ asset('js/frontend/list_your_property.min.js') }}" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
    var bnrHight = $('#slideshow img').height() + 'px';
    $('#slideshow').height(bnrHight) + 'px';
    $('.greenroom-banner .content').css('position', 'absolute').css('top', '0').css('z-index', 'z-index').css('left', '0').css('right', '0').css('margin', '0 auto').css('color', '#fff').css('display', 'flex').css('align-items', 'center').css('justify-content', 'center').css('height', bnrHight);
    console.log(bnrHight);

    if (window.matchMedia(" screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : portrait) ").matches) {
        /* the viewport is at least 400 pixels wide */
        var bnrHight = $('#slideshow img').height() + 'px';
        $('#slideshow').hide();
        $('.greenroom-banner').css('height', '400px').css('background', '#ddefe4');
        $('.greenroom-banner .content').css('position', 'absolute').css('top', '0').css('z-index', 'z-index').css('left', '0').css('right', '0').css('margin', '0 auto').css('color', '#fff').css('display', 'flex').css('align-items', 'center').css('justify-content', 'center').css('height', 'inherit');
        console.log(bnrHight);
    } else {
        /* the viewport is less than 400 pixels wide */
    }
    if (window.matchMedia(" screen and (max-width : 414px) and (orientation : portrait) ").matches) {
        /* the viewport is at least 400 pixels wide */
        var bnrHight = $('#slideshow img').height() + 'px';
        $('#slideshow').hide();
        $('.greenroom-banner').css('height', '400px').css('background', '#ddefe4');
        $('.greenroom-banner .content').css('position', 'absolute').css('top', '0').css('z-index', 'z-index').css('left', '0').css('right', '0').css('margin', '0 auto').css('color', '#fff').css('display', 'flex').css('align-items', 'center').css('justify-content', 'center').css('height', 'inherit');
        console.log(bnrHight);
    } else {
        /* the viewport is less than 400 pixels wide */
    }
    // image fade slide show start
    $("#slideshow > div:gt(0)").hide();
    setInterval(function() {
        $('#slideshow > div:first')
            .fadeOut(1000)
            .next()
            .fadeIn(1000)
            .end()
            .appendTo('#slideshow');
    }, 6000);
    $('.js-example-basic-multiple').select2({
        maximumSelectionLength: 3,
        data: localityArray,
        placeholder: "Select a locality",
        allowClear: true,
        selectOnClose: true
    });
    $('#CityName').select2({
        data: CityNameArray,
        selectOnClose: true
    });
    var text = "";
    var totalLength = $('.titleBox .title').text().length;
    console.log(totalLength)
    var i;
    for (i = 1; i <= totalLength; i++) {
        $('.titleBox .lines').append('<hr class="hr">');
    }
    // Parallax image start
    $.fn.parallaxify = function() {
        return this.each(function() {
            var $element = $(this);

            function percentageSeen() {
                var viewportHeight = $(window).height(),
                    winScrollTop = $(window).scrollTop(),
                    elementOffsetTop = $element.offset().top,
                    elementHeight = $element.height();

                var distance = (winScrollTop + viewportHeight) - elementOffsetTop;
                var percentage = distance / ((viewportHeight + elementHeight) / 100);

                if (percentage < 0) return 0;
                else if (percentage > 100) return 100;
                else return percentage;
            }
            $element.css({ 'background-position-y': percentageSeen() + '%' });
            $(window).on('scroll', function() {
                $element.css({ 'background-position-y': percentageSeen() + '%' });
            });
        });
    };
    $('.parallax').parallaxify();
    $('#responsive').lightSlider({
        item: 4,
        loop: false,
        slideMargin: 9,
        slideMove: 2,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed: 600,
        responsive: [{
                breakpoint: 800,
                settings: {
                    item: 3,
                    slideMove: 1,
                    slideMargin: 6,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    item: 1,
                    slideMove: 1
                }
            }
        ]
    });
    $('#propertyAvailability').lightSlider({
        item: 4,
        loop: false,
        slideMove: 2,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed: 600,
        responsive: [{
                breakpoint: 800,
                settings: {
                    item: 3,
                    slideMove: 1,
                    slideMargin: 6,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    item: 1,
                    slideMove: 1
                }
            }
        ]
    });
});
</script>

</body>
</html>
