@extends('layouts.backend.app')
@section('title')
{{ $page_title}}
@stop
@section('content')
<script type="text/javascript" src="{!! asset('js/backend/residence_type.js') !!}"></script>
<div class="container-fluid">
   <div class="row bg-title">
      <div class="col-sm-6">
         <h4 class="page-title">Residence Type</h4>
       </div>
       @if(SystemHelper::is_module_permission('Residencetype','Add'))
       <div class="col-sm-6">
         <div class="text-right">
            <a href="{{ route('admin_add_residencetype')}}"><button class="btn btn-info waves-effect waves-light"><i class="fa fa-plus"></i> ADD Residence Type</button></a>
         </div>
      </div>
      @endif
   </div>
   <div class="row">
      @if (Session::has('message'))
      <div class="col-sm-12">
         <div class="alert {{ Session::get('msgclass') }} alert-dismissable">{{ Session::get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         </div>
      </div>
      @endif
      <div class="col-sm-12">
         <div class="alert alert-dismissable" id="showMessage" style="display: none">
         </div>
      </div>
      <div class="col-sm-12">
         <div class="panel panel-info">
            <div class="panel-wrapper collapse in" aria-expanded="true">
               <div class="panel-body">
                  <form name="frmlist" id="frmlist">
                     <div class="form-body">
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="form-group col-sm-3">
                                 {!! Form::label('title', 'Search by Title', array('class'=>'control-label')) !!}
                                 <input type="text" id="title" name="title" class="form-control m-r-10 m-b-10" class="form-control" placeholder="Search" value="{{ Request::input('title') }}">
                              </div>
                              <div class="form-group col-sm-3">
                                 {!! Form::label('status', 'Search by Status', array('class'=>'control-label')) !!}
                                 {!! Form::select('status', ([''=> 'Select Status','Active'=>'Active', 'Inactive'=>'Inactive']), null, ['class' => 'form-control select2', 'id' => 'status']) !!}
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="form-actions">
                        <button type="submit" class="btn btn-success" > <i class="fa fa-search"></i> Search</button>
                        <button type="button" onclick="window.location = '{{ route("admin_residencetype") }}';"  class="btn btn-default">Reset</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
     <!--  <div class="col-sm-12">
         <div id="filter">
            <form id="filterForm">
               <input type="title" name="title" value="{{ Request::input('title') }}">
               <input type="submit" class="alert alert-success">
            </form>
         </div>  
      </div> -->
      <div class="col-sm-12" >
         <div class="white-box">
            <!-- <div><a href="{{ route('admin_add_residencetype')}}">Add {{ ucfirst(Request::segment(2)) }}</a></div> -->
            <div class="table-responsive">
               <table class="table color-table info-table">
                  <thead>
                     <tr>
                        <th><span>@sortablelink('title', 'Title')</span></th>
                        <th><span>@sortablelink('status', 'Status')</span></th>
                        @if(SystemHelper::is_module_permission('Residencetype','Edit') || SystemHelper::is_module_permission('Residencetype','Delete'))
                        <th><span>Action</span></th>
                        @endif
                     </tr>
                  </thead>
                  <tbody>
                    @foreach ($records as $record)
                     <tr>
                        <td width="25%">
                         {{ $record->title }}
                        </td>
                        <td width="25%">
                         {{ $record->status }}
                        </td>
                        @if(SystemHelper::is_module_permission('Residencetype','Edit') || SystemHelper::is_module_permission('Residencetype','Delete'))
                        <td width="25%">
                           @if(SystemHelper::is_module_permission('Residencetype','Edit'))
                           <a class="alert alert-info" href="{{ url('admin/residencetype/edit/'.base64_encode($record->residenceTypeID))}}">Edit</a>
                           @endif
                           @if(SystemHelper::is_module_permission('Residencetype','Delete'))
                           <a class="alert alert-danger" href="javascript:void(0)" onclick="deleteRecord('{{ base64_encode($record->residenceTypeID) }}')">Delete</a>
                           @endif
                        </td>
                        @endif
                     </tr>
                     @endforeach
                  </tbody>
               </table>
               {!! $records->appends(\Request::except('page'))->render() !!}
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   var type = '{{ Request::segment(2) }}';
</script>
@endsection