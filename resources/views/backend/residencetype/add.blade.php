@extends('layouts.backend.app')
@section('title')
{{ $page_title}}
@stop
@section('content') 
<script type="text/javascript" src="{!! asset('js/jquery.validate.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/backend/residence_type.js') !!}"></script>

<div class="container-fluid">
  <div class="row bg-title">
    <div class="col-sm-6">
        <h4 class="page-title">Add Residence Type</h4>
    </div>
    <div class="col-sm-6">
          <!-- bread cum -->
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="add_form_section">
        <div class="panel panel-info">
          @if (Session::has('message'))
          <div class="alert {{ Session::get('msgclass') }} alert-dismissable">{{ Session::get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          </div>
          @endif
          <div class="panel-wrapper collapse in" aria-expanded="true">
            <div class="panel-body">
              <form name="frmresidencetype" id="frmresidencetype" method="POST" action="{{ route('admin_add_residencetype')}}" novalidate="" class="form-horizontal">
                {{ csrf_field() }}
                {!! Form::hidden('mode',"add", ['id'=>'mode']) !!}
                <div class="form-body">
                    <div class="form-group">
                      <label class="control-label">Title</label>
                      <div class="form_field"> {{Form::text('title',null,['id'=>'title','required','class'=>'form-control'])}}
                        @if($errors->has('label_title'))
                        <div class="error">{{ $errors->first('label_title') }}</div>
                        @endif
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Status</label>
                      <div class="form_field"> 
                        {{Form::select('status', ['Active'=>'Active','Inactive'=>'Inactive'],null,['id'=>'status','required','class'=>'form-control'])}}
                        @if($errors->has('label_status'))
                        <div class="error">{{ $errors->first('label_status') }}</div>
                        @endif
                      </div>
                    </div>
                </div>
                <div class="row">
                  <div class="form-actions">
                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                    <a href="/admin/{{ Request::segment(2) }}">
                    <button type="button" class="btn btn-default">Cancel</button>
                    </a> </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--./row--> 
</div>
@endsection