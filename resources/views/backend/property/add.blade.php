@extends('layouts.backend.app')
@section('title')
{{ $page_title}}
@stop
@section('content') 
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/bootstrap-wysihtml5.css') }}" rel="stylesheet">
<link href="{{ asset('css/bootstrap-multiselect.css') }}" rel="stylesheet">
<script type="text/javascript" src="{!! asset('js/wysihtml5-0.3.0.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/bootstrap-wysihtml5.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/select2.full.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/jquery.validate.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/backend/property.js') !!}"></script>

<div class="container-fluid property-add-edit">
	<div class="row">
		<div class="col-12">
			<div class="card rounded-0 mb-3">
			<div class="card-body">
	
	<div class="row">
		<h1 class="greenroom-title mt-2 ml-3">
			<i class="fas fa-building"></i>
			Add Property
		</h1>
	</div>
	<hr/>	
	<div class="panel panel-primary card-body">
	<div class="">

		@if (Session::has('message'))
		<div class="alert {{ Session::get('msgclass') }} alert-dismissable">{{ Session::get('message') }}
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		</div>
		@endif
		
		<form name="frmproperty" id="frmproperty" method="POST" action="{{ route('admin_add_property')}}" novalidate="" class="form-horizontal" enctype="multipart/form-data">
			{{ csrf_field() }}
			{!! Form::hidden('mode',"add", ['id'=>'mode']) !!}

			<div class="shadowbox">
					<div class="">
							<div class="form-group">
								<label class="col-sm-2 control-label">Property Category</label>
								<!--<div class="col-sm-3"> 
									{!!Form::select('availability', [null=>"Select Category",'1'=>'PG','2'=>'CORPORATE PG(SERVICE APT)','3'=>'HOSTEL'] , null, ['class' => 'form-control ', 'id' => 'availability','required']) !!}                                
									@if($errors->has('label_availability'))
									<div class="error">{{ $errors->first('label_availability') }}</div>
									@endif
									{!! Form::select('propertyType', [null=>"Select Property"] + $dropdowns['propertyTypeDropdown'], null, ['class' => 'form-control select2', 'id' => 'propertyType','required']) !!}
									@if($errors->has('label_propertyType'))
									<div class="error">{{ $errors->first('label_propertyType') }}</div>
									@endif
								</div>-->
								<div class="col-sm-3"> 
									{!! Form::select('propertyType', [null=>"Select Category"] + $dropdowns['propertyTypeDropdown'], null, ['class' => 'form-control', 'id' => 'propertyType','required']) !!}
									@if($errors->has('label_propertyType'))
									<div class="error">{{ $errors->first('label_propertyType') }}</div>
									@endif
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-2 control-label">Date Available From</label>
								<div class="col-sm-2">
									<div class="input-group date">                             
										<input autocomplete="off" type="text" id="avaialble_from" name="avaialble_from" class="input-sm form-control" placeholder="dd-mm-yyyy" value="{{ Request::input('avaialble_from') }}">      
										<span class="input-group-addon"><span style="top:5px!important;left:-5px;" class="glyphicon-calendar glyphicon" id="avaialble_from_pic"></span></span>
									</div>
								</div>
							</div>
					</div>

					<div class="">
						<!--<div class="form-group">
							<label class="col-sm-2 control-label">
								Accommodation For (Property Type)
							</label>
							<div class="col-sm-5 form_field">
								{!! Form::select('propertyType', [null=>"Select Property"] + $dropdowns['propertyTypeDropdown'], null, ['class' => 'form-control select2', 'id' => 'propertyType','required']) !!}
								@if($errors->has('label_propertyType'))
								<div class="error">{{ $errors->first('label_propertyType') }}</div>
								@endif
							</div>
						</div>-->
						<div class="form-group">
							<label class="col-sm-2 control-label">
								Accommodation For
							</label>
							<div class="col-sm-5 form_field">
								{!! Form::select('gender', [null=>"Select",'Male'=>'Male','Female'=>'Female','Male & Female'=>'Male & Female'], null, ['class' => 'form-control', 'id' => 'accommodation','required']) !!}
								@if($errors->has('label_accommodation'))
								<div class="error">{{ $errors->first('label_accommodation') }}</div>
								@endif
							</div>
						</div>
					</div>
					
					<div class="">
						<div class="form-group">
							<label class="col-sm-2 control-label">Suitable for</label>
							<div class="col-sm-5 form_field">
								{!! Form::select("sharingID", [null=>"Select",'1'=>'Students','2'=>'Working Professional','3'=>'Student & Working Professional'], '', ['class' => 'form-control', 'id' => "new_rs_roomSharingID",'required']) !!}
								@if($errors->has('label_roomSharing'))
								<div class="error">{{ $errors->first('label_roomSharing') }}</div>
								@endif
							</div>
						</div>
					</div>
					
					<div class="">
						<div class="form-group">
							<label class="col-sm-2 control-label">Company Name</label>
							<div class="col-sm-5 form_field"> {{Form::text('companyName',null,['id'=>'companyName','class'=>'form-control','required'])}}
								@if($errors->has('label_companyName'))
								<div class="error">{{ $errors->first('label_companyName') }}</div>
								@endif
							</div>
						</div>
					</div>
					
					<div class="">
						<div class="form-group">
							<label class="col-sm-2 control-label">Owner Name</label>
							<div class="col-sm-3 form_field"> {{Form::text('ownerName',null,['id'=>'ownerName','class'=>'form-control','required'])}}
								@if($errors->has('label_ownerName'))
								<div class="error">{{ $errors->first('label_ownerName') }}</div>
								@endif
							</div>
						</div>
					</div>

					<div class="">
						<div class="form-group">
							<label class="col-sm-2 control-label">Contact Number</label>
							<div class="col-sm-3"> {{Form::text('companyNumber',null,['id'=>'companyNumber','class'=>'form-control','required'])}}
								@if($errors->has('label_companyNumber'))
								<div class="error">{{ $errors->first('label_companyNumber') }}</div>
								@endif
							</div>
						</div>
					</div>
					
					<div class="">
						<div class="form-group">
							<label class="col-sm-2 control-label">Address</label>
							<div class="col-sm-8"> {{Form::text('address',null,['id'=>'address','class'=>'form-control','required'])}}
								@if($errors->has('label_address'))
								<div class="error">{{ $errors->first('label_address') }}</div>
								@endif
							</div>
						</div>
					</div>
					
					<div class="">
						<div class="form-group">
							<label class="col-sm-2 control-label">Landmark</label>
							<div class="col-sm-5 form_field"> {{Form::text('landmark',null,['id'=>'landmark','class'=>'form-control','onchange'=>'getMapLocation()','required'])}}
								@if($errors->has('label_landmark'))
								<div class="error">{{ $errors->first('label_landmark') }}</div>
								@endif
							</div>
						</div>
					</div> 
					
					<!--
					<div class="">
						<div class="form-group">
							<label class="col-sm-2 control-label">Unit Available</label>
							<div class="col-sm-2"> {{Form::text('unitavailableID',null,['id'=>'unitavailableID','class'=>'form-control','required'])}}
								@if($errors->has('label_unitavailableID'))
								<div class="error">{{ $errors->first('label_unitavailableID') }}</div>
								@endif
							</div>
						</div>
					</div>-->                    
					
					<div class="">
						<div class="form-group">
							<label class="col-sm-2 control-label">State</label>
							<div class="col-sm-3 form_field">
								{!! Form::select('state', [null=>"Select State"] + $dropdowns['stateDropdown'], null, ['class' => 'form-control select2', 'id' => 'state', 'onchange'=>'getCity()','required']) !!}
								@if($errors->has('label_state'))
								<div class="error">{{ $errors->first('label_state') }}</div>
								@endif
							</div>
						</div>
					</div>

					<div class="">
						<div class="form-group">
							<label class="col-sm-2 control-label">City</label>
							<div class="col-sm-3 form_field">
								{!! Form::select('city', [null=>"Select City"], null, ['class' => 'form-control select2', 'id' => 'city', 'onchange'=>'getArea()','required']) !!}
								@if($errors->has('label_city'))
								<div class="error">{{ $errors->first('label_city') }}</div>
								@endif
							</div>
						</div>
					</div>

					<div class="">
						<div class="form-group">
							<label class="col-sm-2 control-label">Location</label>
							<div class="col-sm-3 form_field">
								{!! Form::select('area', [null=>"Select Area"], null, ['class' => 'form-control select2', 'id' => 'area','required']) !!}
								@if($errors->has('label_area'))
								<div class="error">{{ $errors->first('label_area') }}</div>
								@endif
							</div>
						</div>
					</div>                   
					
					<?php /*
					<div class=" col-sm-3 col-md-3 col-lg-3">
						<div class="form-group">
							<label class="col-sm-2 control-label">Latitude</label>
							<div class="col-sm-5 form_field"> {{Form::text('latitude',null,['id'=>'latitude','class'=>'form-control'])}}
								@if($errors->has('label_latitude'))
								<div class="error">{{ $errors->first('label_latitude') }}</div>
								@endif
							</div>
						</div>
					</div>

					<div class=" col-sm-3 col-md-3 col-lg-3">
						<div class="form-group">
							<label class="col-sm-2 control-label">Longitude</label>
							<div class="col-sm-5 form_field"> {{Form::text('longitude',null,['id'=>'longitude','class'=>'form-control'])}}
								@if($errors->has('label_longitude'))
								<div class="error">{{ $errors->first('label_longitude') }}</div>
								@endif
							</div>
						</div>
					</div>
					*/ ?>
					
					<div class=" ">
						<div class="form-group">
							<label class="col-sm-2 control-label">Rent</label><span id="amountvalue" style="color:#777;padding-left:15px;font-weight: bold;"></span>                            
							<div class="col-sm-2 form_field"> {{Form::text('rent',null,['id'=>'new_0_rs_price','class'=>'form-control','required','min'=>'0'])}}                                                                
							</div>
						</div>
					</div>
					
					<!--mehul change-->
					<div class=" ">
						<div class="form-group">
							<label class="col-sm-2 control-label">Deposit</label><span id="depositvalue" style="color:#777;padding-left:10px;font-weight: bold;"></span>                            
							<div class="col-sm-2 form_field"> {{Form::text('deposit',null,['id'=>'new_0_rs_deposit','class'=>'form-control','required','min'=>'0'])}}                                                                
							</div>
						</div>
					</div>
					<!--mehul change over-->
					
					<div class=" ">
						<div class="form-group">
							<label class="col-sm-2 control-label">Availability</label>
							<div class="col-sm-5 form_field">
								{!! Form::select("numberOfRoom", [null=>"Availability"] + $dropdowns["numberOfRoomDropdown"], '', ['class' => 'form-control', 'id' => "new_rs_numberOfRoom",'required']) !!}
								@if($errors->has('label_numberOfRoom'))
								<div class="error">{{ $errors->first('label_numberOfRoom') }}</div>
								@endif
							</div>
						</div>
					</div>
					
					
					
					<div class=" ">
						<div class="form-group">
							<label class="col-sm-2 control-label">Availability Type</label>
							<div class="col-sm-5 form_field">
								{!! Form::select("availability", [null=>"Availability Type"] + $dropdowns["availableTypeDropdown"], '', ['class' => 'form-control', 'id' => "new_availableType",'required']) !!}
								@if($errors->has('label_availableType'))
								<div class="error">{{ $errors->first('label_availableType') }}</div>
								@endif
							</div>
						</div>
					</div>
					
					<div class=" ">
						<div class="form-group">
							<label class="col-sm-2 control-label">Condition</label>
							<div class="col-sm-5 form_field">
								{!! Form::select('condition', [null=>"Condition"] + $dropdowns['conditionDropdown'], '', ['class' => 'form-control ', 'id' => 'condition','required']) !!}
								@if($errors->has('label_condition'))
								<div class="error">{{ $errors->first('label_condition') }}</div>
								@endif
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="sqfeet" class="col-sm-2 control-label">SqFeet</label>
						<div class="col-sm-3">
							<div class="input-group">						{{Form::text('total_square_feet',null,['id'=>'square_feet','class'=>'form-control',''])}} 
								<span class="input-group-addon col-md-7">
									<select id="sqlftdrp" name="sqlftdrp">
										<option value=""> - Select Area Unit - </option>
										<option value="1">Sq.Ft.</option>
										<option value="9">Sq.Yards</option>
										<option value="10.764">Sq.Meter</option>
									</select>
								</span>
							</div>
						</div>
					</div>

					<div class=" ">
						<div class="form-group">
							<label class="col-sm-2 control-label">Property On Floor</label>
							<div class="col-sm-2 form_field"> {{Form::text('floorID',null,['id'=>'floorID','class'=>'form-control',''])}}
							</div>
						</div>
					</div>

					<div class=" ">
						<div class="form-group">
							<label class="col-sm-2 control-label">Total Floor</label>
							<div class="col-sm-2 form_field"> {{Form::text('totalFloor',null,['id'=>'totalFloor','class'=>'form-control',''])}}
							</div>
						</div>
					</div>
					
					<div class="">
						<div class="form-group">
							<label class="col-sm-2 control-label">Property Age</label>
								<div class="col-sm-2 form_field">
								{{Form::select('old_building', [''=>'Select'] + $dropdowns["propertyAge"],null,['id'=>'building','class'=>'form-control'])}}
								@if($errors->has('label_building'))
								</div>
								<div class="error">{{ $errors->first('label_building') }}</div>
								@endif
						</div>
					</div>

					<div class="">
						<div class="form-group">
							<label class="col-sm-2 control-label">Facing</label>
								<div class="col-sm-2 form_field">
								{!! Form::select("facing", [null=>"Facing"] + $dropdowns["facingDropdown"], '', ['class' => 'form-control', 'id' => "new_rs_facing",'required']) !!}
								</div>
								@if($errors->has('label_facing'))
								<div class="error">{{ $errors->first('label_facing') }}</div>
								@endif
						</div>
					</div>
					
					<div class="">
						<div class="form-group">
							<label class="col-sm-2 control-label">Notice Period</label>
								<div class="col-sm-2 form_field">
								{!! Form::select("noticePeriod", [null=>"Notice Period"] + $dropdowns["noticePeriodDropdown"], '', ['class' => 'form-control', 'id' => "new_rs_noticePeriod",'required']) !!}
								</div>
								@if($errors->has('label_facing'))
								<div class="error">{{ $errors->first('label_noticePeriod') }}</div>
								@endif
						</div>
					</div>
					
					<div class="">
						<div class="form-group">						
							<label class="col-sm-2 control-label">Bed Sharing</label>
							<div class="col-sm-5 form_field">
								{!! Form::select("bedSharingId", [null=>"Bed Sharing"] + $dropdowns["roomSharingDropdown"], '', ['class' => 'form-control', 'id' => "new_rs_roomSharingID",'required']) !!}
								@if($errors->has('label_roomSharing'))
								<div class="error">{{ $errors->first('label_roomSharing') }}</div>
								@endif
							</div>
						</div>
					</div>

					
					<!--
					<div class=" ">
						<div class="form-group map-block">       
							<label class="col-sm-2 control-label">&nbsp;</label>
							<div class="btn-group btn-group-justified" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<button type="button" class="btn btn-default" data-toggle="modal" data-target="#mappopup">View Map</button>
								</div>                                
							</div>
						</div>
					</div> -->
					
					{{Form::hidden('latitude',null,['id'=>'latitude','class'=>'form-control'])}}
					{{Form::hidden('longitude',null,['id'=>'longitude','class'=>'form-control'])}}

					<div class="">
						<div class="form-group">
							<label class="col-sm-2 control-label">Description 1</label>
							<div class="col-sm-5 form_field"> {{Form::textarea('description',null,['id'=>'description','class'=>'form-control','rows'=>'4'])}}
								@if($errors->has('label_description'))
								<div class="error">{{ $errors->first('label_description') }}</div>
								@endif
							</div>
						</div>
					</div>
					
					<hr>

					<div class="col-lg-12">
						<div class="form-group">
							<label class="col-sm-2 control-label">Description 2</label>
						</div>
					</div>
					
					<label class="col-sm-2 control-label">Interior</label>
					<div class="col-12">
						<div class="form-group checkboxrow">
							<div class="col-sm-2"></div>
							<div class="col-sm-10">                                
									@foreach ($dropdowns['interiorschk'] as $slug => $value) 
										<div class="col-sm-4"><input type="checkbox" name="interiorID[]" value="<?php echo $value->interiorsID; ?>"><?php echo $value->title;?></div>
									@endforeach
							</div>
						</div>
					</div>
					
					<label class="col-sm-2 control-label">Facility</label>
					<div class="col-12">
						<div class="form-group checkboxrow">
							<div class="col-sm-2"></div>
							<div class="col-sm-10">                                
									@foreach ($dropdowns['facilitieschk'] as $slug => $value) 
										<div class="col-sm-4"><input type="checkbox" name="facilityID[]" value="<?php echo $value->facilitiesID; ?>"><?php echo $value->title;?></div>
									@endforeach
							</div>
						</div>
					</div>
					
					<label class="col-sm-2 control-label">Society Amenity</label>
					<div class="col-12">
						<div class="form-group checkboxrow">
							<div class="col-sm-2"></div>
							<div class="col-sm-10">
									@foreach ($dropdowns['societyamenitychk'] as $slug => $value) 
										<div class="col-sm-4"><input type="checkbox" name="societyamenityID[]"  value="<?php echo $value->societyamenityID; ?>"><?php echo $value->title;?></div>                                            
									@endforeach
							</div>
						</div>
					</div>
					
					<label class="col-sm-2 control-label">Rules</label>
					<div class="col-12">
						<div class="form-group checkboxrow">
							<div class="col-sm-2"></div>
							<div class="col-sm-10">
									@foreach ($dropdowns['ruleschk'] as $slug => $value)
										<div class="col-sm-4"><input type="checkbox" name="rulesID[]" value="<?php echo $value->rulesID; ?>"><?php echo $value->title;?></div>
									@endforeach
									@if($errors->has('label_rules'))
									<div class="error">{{ $errors->first('label_rules') }}</div>
									@endif
							</div>
						</div>
					</div>
					
						
					<label class="col-sm-2 control-label">Meals</label>
					<div class="col-12">
						<div class="form-group checkboxrow">
							<div class="col-sm-2"></div>
							<div class="col-sm-10">                                
									@foreach ($dropdowns['mealschk'] as $slug => $value) 
										<div class="col-sm-4"><input type="checkbox" name="mealsIncludedID[]" value="<?php echo $slug; ?>"><?php echo $value;?></div>
									@endforeach
							</div>
						</div>
					</div>

					
					
					</div>
				</div>


					<?php /*
					<div class=" ">
						<div class="col-offset-2 form-group checkboxrow">
							<div class="col-sm-3 form_field">                                
								<label class="control-label">Facility</label>
								<select id="facility" name="facilityID[]" multiple="multiple">
									@foreach ($dropdowns['facilitieschk'] as $slug => $value) 
										<option value="<?php echo $value->facilitiesID; ?>"><?php echo $value->title;?></option>
									@endforeach
								</select>
							</div>
							<div class="col-sm-3 form_field">
								<label class="control-label">Society Amenity</label>
								<select id="societyamenity" name="societyamenityID[]" multiple="multiple"> 
									@foreach ($dropdowns['societyamenitychk'] as $slug => $value) 
										<option value="<?php echo $value->societyamenityID; ?>"><?php echo $value->title;?></option>                                            
									@endforeach
								</select>
							</div>
							<div class="col-sm-3 form_field">							
								<label class="control-label">Rules</label>
								<select id="rules" name="rulesID[]" multiple="multiple"> 
									@foreach ($dropdowns['ruleschk'] as $slug => $value)
										<option value="<?php echo $value->rulesID; ?>"><?php echo $value->title;?></option>                                            
									@endforeach
								</select>
								@if($errors->has('label_rules'))
								<div class="error">{{ $errors->first('label_rules') }}</div>
								@endif
							</div>
						</div>
					</div>
					
					
					<div class="">
							<div class="col-sm-2"></div>
							<div class="form-group checkboxrow">
								<!--
								<div class="col-sm-3 form_field">                                
								<label class="control-label">Accommodation For</label>
									<select id="accommodation" name="gender[]" multiple="multiple"> 
										<option value="Male Bachelor">Male Bachelor</option>
										<option value="Male Executive">Male Executive</option>
										<option value="Female Bachelor">Female Bachelor</option>
										<option value="Female Executive">Female Executive</option>                                    
									</select>
									@if($errors->has('label_gender'))
									<div class="error">{{ $errors->first('label_gender') }}</div>
									@endif
								</div>
								-->
								
								<div class="col-sm-2 form_field">
									<label class="control-label">Property Age</label>
										{{Form::select('old_building', [''=>'Select OLD Building','1'=>'0 to 3 years','2'=>'3 to 5 years','3'=>'5 to 10 years','4'=>'10 to 20 years','5'=>'more than 20 years'],null,['id'=>'building','class'=>'form-control '])}}
										@if($errors->has('label_building'))
										<div class="error">{{ $errors->first('label_building') }}</div>
										@endif
								</div>
								
								<div class="col-sm-2 form_field">
									<label class="control-label">Facing</label>
										{!! Form::select("facing", [null=>"Facing"] + $dropdowns["facingDropdown"], '', ['class' => 'form-control', 'id' => "new_rs_facing",'required']) !!}
										@if($errors->has('label_facing'))
										<div class="error">{{ $errors->first('label_facing') }}</div>
										@endif
								</div>
								
								<div class="col-sm-2 form-group">
									<label class="control-label">Notice Period</label>
										{!! Form::select("noticePeriod", [null=>"Notice Period"] + $dropdowns["noticePeriodDropdown"], '', ['class' => 'form-control', 'id' => "new_rs_noticePeriod",'required']) !!}
										@if($errors->has('label_facing'))
										<div class="error">{{ $errors->first('label_noticePeriod') }}</div>
										@endif
								</div>

								
							</div>
					</div>
						
						<div class=" ">
								<div class="col-sm-2"></div>
								
								<div class="col-sm-2 btn-group btn">                                
									<label class="control-label">Meals</label>
									<select id="meals" name="mealsIncludedID[]" multiple="multiple" > 
										<option value="Breakfast">Breakfast</option>
										<option value="Lunch">Lunch</option>
										<option value="Dinner">Dinner</option>
										<option value="Not Included">Not Included</option>
									</select>
								</div>

								<div class="col-sm-2 btn-group btn">                                
								<label class="control-label">Furniture</label>
									<select id="furniture" name="furnitureID[]" multiple="multiple"> 
										@foreach ($dropdowns['furniturechk'] as $slug => $value) 
											<option value="<?php echo $value->furnitureID; ?>"><?php echo $value->title;?></option>
											@endforeach
									</select>
								</div>
						
						
						
						</div>
					</div>
				</div>
				
				*/ ?>
					
					
				<hr>

				<!--<div class=" ">
					<div class="form-group">
						<label class="col-sm-2 control-label">Status</label>
						<div class="col-sm-3 form_field"> 
							{{Form::select('status', ['Active'=>'Active','Inactive'=>'Inactive','Delete'=>'Delete'],'',['id'=>'status','class'=>'form-control'])}}
							@if($errors->has('label_status'))
							<div class="error">{{ $errors->first('label_status') }}</div>
							@endif
						</div>
					</div>
				</div>-->
				
				<div class=" ">
					<div class="form-group">
						<label class="col-sm-2 control-label">Occupancy Status</label>
						<div class="col-sm-3 form_field"> 
							{{Form::select('status', ['Available'=>'Available','Occupied'=>'Occupied'],'',['id'=>'status','class'=>'form-control'])}}
							@if($errors->has('label_oc_status'))
							<div class="error">{{ $errors->first('label_oc_status') }}</div>
							@endif
						</div>
					</div>
				</div>
				
				<div class="">
						<div class="form-group">
							<label class="col-sm-2 control-label">Remarks</label>
							<div class="col-sm-5 form_field"> {{Form::text('remarks','',['id'=>'remarks','class'=>'form-control'])}}
                                @if($errors->has('label_remarks'))
                                <div class="error">{{ $errors->first('label_remarks') }}</div>
                                @endif
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="photosrow">
							<label class="col-sm-2 control-label">Property Photos</label>
							<div class="upload-img-block">
								<div class="col-sm-5 form_field">
									<div class="flie"> 
										<?php $attributes = ['id' => "new_0_rs_file", 'multiple' => 'multiple', 'class' => 'upload_img', 'accept' => "image/x-png, image/gif, image/jpeg, image/bmp"] ?>
										{{ Form::file("sharing[0][rs][file][]", $attributes) }}                                                                   
									</div>
								</div>
							</div>
						</div>
					</div>
					
					
					
					<div class="form-group">
						<div class="float-right form-actions admin-property-actions">
							<button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
							<a href="{{ route('admin_property')}}">
								<button type="button" class="btn btn-default">Cancel</button>
							</a> 
						</div>
					</div>				
			</div>
		</form>
	</div>

	</div>
	</div>
</div>
		</div>
	</div>
</div>
<div class="modal fade" id="mappopup" tabindex="-1">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Map</h4>
            </div>
            <div class="modal-body">
                <div id="dvMap" class="map" style="width: 100%; height: 173px"></div>
            </div>
        </div>
    </div>
</div>
<!--
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">  
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
-->

<script type="text/javascript" src="{!! asset('js/bootstrap-multiselect.js') !!}"></script>
<script type="text/javascript">
//$('#description').wysihtml5();
$(function(){
   $("#avaialble_from").datepicker({ minDate: 0, defaultDate: new Date(), dateFormat: 'dd-mm-yy' });
	$("#avaialble_from").datepicker("setDate", "-0d");

	$("#avaialble_from_pic").click(function(){
		$("#avaialble_from").datepicker("show");
	});	

    //$('#accommodation').multiselect({buttonWidth: '250px'});
    
	//$('#meals').multiselect({buttonWidth: '200px'});
    //$('#furniture').multiselect({buttonWidth: '200px'});
    //$('#facility').multiselect({buttonWidth: '200px'}); 
    //$('#societyamenity').multiselect({buttonWidth: '200px'});
    //$('#rules').multiselect({buttonWidth: '200px'});
	
    $('#new_0_rs_price').keyup(function() {
        var amount = $('#new_0_rs_price').val();        
        if (/\D/g.test(amount))
        {
            this.value = amount.replace(/\D/g, '');
        }
        
        if($.isNumeric(amount)) {
            if (amount >= 10000000)
                amount = (amount / 10000000) + ' Cr';
            else if (amount >= 100000)
                amount = (amount / 100000) + ' Lac';
            else if (amount >= 1000)
                amount = (amount / 1000) + ' Thousand';
            $('#amountvalue').html(amount);
        }            
    });   
//    mehul change
    $('#new_0_rs_deposit').keyup(function() {
        var deposit = $('#new_0_rs_deposit').val();        
        if (/\D/g.test(deposit))
        {
            this.value = deposit.replace(/\D/g, '');
        }
        
        if($.isNumeric(deposit)) {
            if (deposit >= 10000000)
                deposit = (deposit / 10000000) + ' Cr';
            else if (deposit >= 100000)
                deposit = (deposit / 100000) + ' Lac';
            else if (deposit >= 1000)
                deposit = (deposit / 1000) + ' Thousand';
            $('#depositvalue').html(deposit);
        }            
    });
//    mehul change over
});
</script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyB3lU0aAawEEcJ9opbm8vuBuYXnYS91sTc"></script> 
@endsection