@extends('layouts.backend.app')
@section('title')
{{ $page_title}}
@stop
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card rounded-0 mb-3">
                <div class="card-body" style="padding-bottom:0px;">
						<div class="row">
							<div class="col col-xs-12 col-lg-6 col-sm-6 col-md-6 col-xl-6">
								<h1 class="greenroom-title mt-2"><i class="fas fa-building"></i> {{Request::segment(2)}}</h1>
							</div>
							<div class="col col-xs-12 col-lg-6 col-sm-6 col-md-6 col-xl-6">
								@if(SystemHelper::is_module_permission('Property','Add'))
									<div class="d-flex justify-content-end">
										<a href="{{route('admin_add_property')}}" class="btn btn-primary btn-md shadow">
										<i class="fas fa-plus-circle"></i> Add New {{Request::segment(2)}}
										</a>
										&nbsp;
										<button class="btn btn-danger btn-md shadow" onClick="bulkPerform()">
                                        <i class="fas fa-trash"></i>
                                    </button>
									</div>
								@endif
							</div>
						</div>
						<hr/>
						<div class="row">
							@if (Session::has('message'))
								<div class="col-12">
									<div class="alert alert-danger {{ Session::get('msgclass') }} alert-dismissable">{{ Session::get('message') }}
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									</div>
								</div>
							@endif
						</div>
						
						@if(count($records) == 0)
							<div class="col-12">
								<div class="alert alert-danger alert-dismissable">
									No record found.
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								</div>
							</div>
						@endif	
						
					</div>
					
					@if(count($records) > 0)
					<div class="table-responsive-sm card-body" style="padding-top:0px;">
					
					<div class="card rounded-0">
								
                                 <form class="well-filter" name="frmlist" id="frmlist" novalidate>
									<div class="m-3">
										  <div class="col-md-2">
											<div class="form-group">
                                                <!--{!! Form::select('status', ([''=> 'Select Status','Active'=>'Active', 'Inactive'=>'Inactive','Delete'=>'Delete']), 'Active', ['class' => 'form-control', 'id' => 'status']) !!}-->
												{!! Form::select('propertyTypeID', ([''=> 'Select Type']+$dropdown['propertyTypeDropdown']), '', ['class' => 'form-control', 'id' => 'propertyTypeID']) !!}
                                              </div>
                                          </div>
										  
										  <div class="col-md-2">
											<div class="form-group">
                                                <input type="text" id="contact_no" required="required" name="contact_no" value="{{ Request::input('contact_no') }}" class="form-control"placeHolder="Contact No" />
                                             </div>
                                          </div>
                                          
										  <div class="col-md-2">
											<div class="form-group">
                                                <input type="text" id="title" required="required" name="title" value="{{ Request::input('title') }}" class="form-control"placeHolder="Search By Location" />
                                             </div>
                                          </div>
                                          <!--<div class="col-md-2">
											<div class="form-group">
                                                {!! Form::select('city', [''=> 'Select City'] + $dropdown['cityDropdown'], null, ['class' => 'form-control', 'id' => 'city']) !!}
                                              </div>
                                          </div>
                                          <div class="col-md-2">
											<div class="form-group">
                                                {!! Form::select('state', [''=> 'Select State'] + $dropdown['stateDropdown'], null, ['class' => 'form-control', 'id' => 'state','onchange'=>'getCity()']) !!}
                                             </div>
                                          </div>-->
                                          <div class="col-md-2">
											<div class="form-group">
                                                <!--{!! Form::select('status', ([''=> 'Select Status','Active'=>'Active', 'Inactive'=>'Inactive','Delete'=>'Delete']), 'Active', ['class' => 'form-control', 'id' => 'status']) !!}-->
												{!! Form::select('status', ([''=> 'Select Status','Available'=>'Available', 'Occupied'=>'Occupied']), 'Available', ['class' => 'form-control', 'id' => 'status']) !!}
                                              </div>
                                          </div>
										  <!--<div class="col-md-2">
											<div class="form-group">
												{!! Form::select('action', [null=>"Bulk Action"] + ([''=> 'Select Status','Active'=>'Active', 'Inactive'=>'Inactive','Delete'=>'Delete']), null, ['class' => 'form-control', 'id' => 'action', 'onchange'=>'bulkPerform()' ]) !!}
											</div>
										</div>-->
                                       
									   <div class="col-sm-1 form-group">
                                             <button type="submit" class="btn btn-success form-control">Search
											 <!--<i class="fas fa-search-plus"></i>--> 
											 </button>
                                          </div>
										<div class="col-sm-1 form-group">
                       					 <a href='{{ route("admin_property") }}'class="btn btn-primary form-control">
										 Reset
										 <!--<i class="fas fa-sync" aria-hidden="true"></i>-->
										 </a></div>
                                   
									
									</div>
                                 </form>
                                
                    </div>
                
                  <table class="display table table-hover table-bordered table-striped w-full dtr-inline" id="data-user" border="0">
                     <thead class="thead-light">
                           <tr>
						   <th class="no"><input id="propertyCheckAll" type="checkbox" /></th>
                           <th>#</th>
						   <th>Property&nbsp;Code</th>
						   <th width="200px">Date</th>
						   <th>Property Type</th>
						   <th>Accommodation</th>
						   <th>Suitable For</th>
                           <th>@sortablelink('companyName', 'Company Name')</th>
                           <th>@sortablelink('ownerName', 'Owner Name')</th>
						   <th>Contact #</th>
                           <th>@sortablelink('address', 'Address')</th>
						   <th>Landmark</th>
						   <th>Location</th>
						   <th>Rent/Deposit</th>
						   <th>Availability and Availability Type</th>
						   <th>Bed Sharing</th>
						   <th>Condition</th>
						   <!--<th>Bed Sharing</th>-->
						   <th>Description1</th>
						   <th>Sq.Ft/Sq.Yard</th>
						   <th>Description2</th>
						   <th>Property Status</th>
						   <th>Photos</th>
						   <th>Remarks</th>
                           <th colspan="3"></th>
                           </tr>
                     </thead>

                     <tbody>
                           @foreach ($records as $indexKey => $record)
                           <tr>
							  <td><input class="user" name="properties[]" type="checkbox" value="{{ $record->propertyID }}" /></td>
                              <td>{{(($records->currentpage()-1)*$records->perpage())+$indexKey+1}}</td>
                              <td>{{$record->property_code}}</td>
						      <td>{{ date('d-m-Y', strtotime($record->created_at)) }}</td>
                              <td>{{$dropdown['propertyTypeDropdown'][$record->propertyTypeID]}}</td>
							   <td>
								{{$record->gender}}
							   </td>
							   <td>
								{{$dropdown['suitableFor'][$record->sharingID]}}
							   </td>
                              <td>{{$record->companyName}}</td>
                              <td>{{$record->ownerName}}</td>
                              <td>{{$record->companyNumber}}</td>
                              <td>{{$record->address}}</td>
							  <td>{{$record->landmark}}</td>
						   <td>Location</th>
						   <td>{{$record->price}}<br/>{{$record->deposit}}</td>
						   <td>
						   {{ 
							(isset($dropdown['numberOfRoomDropdown'][$record->numberOfRoom]))
							?
							$dropdown['numberOfRoomDropdown'][$record->numberOfRoom]
							:'-' 
						   }}
						   <br/>
						   {{ 
							(isset($dropdown['availableTypeDropdown'][$record->availability]))
							?
							$dropdown['availableTypeDropdown'][$record->availability]
							:'-' 
						   }}
						   </td>
						   <td>
							{{ 
								(isset($dropdown['roomSharingDropdown'][$record->conditionID]))
								?
								$dropdown['roomSharingDropdown'][$record->conditionID]
								:'-' 
							   }}
						   </td>
						   <td>
						   {{ 
							(isset($dropdown['conditionDropdown'][$record->conditionID]))
							?
							$dropdown['conditionDropdown'][$record->conditionID]
							:'-' 
						   }}
						   </td>
						   <!--<td>
						   {{ 
							(isset($dropdown['roomSharingDropdown'][$record->conditionID]))
							?
							$dropdown['roomSharingDropdown'][$record->conditionID]
							:'-' 
						   }}
						   </td>-->
						   <td>{{str_limit($record->description,20,'...')}}</td>
						   <td>{{$record->total_square_feet}}</td>
						   <td>
								<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal_{{$record->propertyID}}">View</button>

								<div id="myModal_{{$record->propertyID}}" class="modal" role="dialog">
								  <div class="modal-dialog">

									<!-- Modal content-->
									<div class="modal-content">
									  <div class="modal-header">
										<h4 class="modal-title">Property Details - {{$record->propertyID}}</h4>
										<button type="button" class="close" data-dismiss="modal">&times;</button>
									  </div>
									  <div class="modal-body">
										<p>												
								<b>Interiors:</b>
								@if ($record->interiorID != "")
								  @foreach(explode(',', $record->interiorID) as $index) 
									@foreach($dropdown['interiorschk'] as $interior)
										@if ($index == $interior->interiorsID)
											{{$interior->title}},
										@endif
									@endforeach
								  @endforeach
								@endif
								<br/>
								<b>Facility:</b>
								@if ($record->facilityID != "")
								  @foreach(explode(',', $record->facilityID) as $index) 
									@foreach($dropdown['facilitieschk'] as $facilities)
										@if ($index == $facilities->facilitiesID)
											{{$facilities->title}},
										@endif
									@endforeach
								  @endforeach
								@endif
								<br/>
								<b>Eminities:</b>
								@if ($record->societyamenityID != "")
								  @foreach(explode(',', $record->societyamenityID) as $index) 
									@foreach($dropdown['societyamenitychk'] as $society)
										@if ($index == $society->societyamenityID)
											{{$society->title}},
										@endif
									@endforeach
								  @endforeach
								@endif
								<br/>
								<b>Rules:</b>
								@if ($record->rules != "")
								  @foreach(explode(',', $record->rules) as $index) 
									@foreach($dropdown['ruleschk'] as $rule)
										@if ($index == $rule->rulesID)
											{{$rule->title}},
										@endif
									@endforeach
								  @endforeach
								@endif
								<br/>
								<b>Meals:</b>
								@if ($record->mealsIncludedID != "")
								  @foreach(explode(',', $record->mealsIncludedID) as $index) 
										@foreach($dropdown['mealsIncludedDropdown'] as $key=>$value)
											@if ($index == $key)
												{{$value}},
											@endif
										@endforeach
									@endforeach
								@endif
								<br/>
								<!--<b>Bed Sharing</b>: {{ 
								(isset($dropdown['roomSharingDropdown'][$record->conditionID]))
								?
								$dropdown['roomSharingDropdown'][$record->conditionID]
								:'-' 
							   }}<br/>-->
							   
							   <b>Property Floor</b>: {{ 
								(isset($record->floorID))
								?
								$record->floorID
								:'-' 
							   }}&nbsp;of {{ 
								(isset($record->total_floor))
								?
								$record->total_floor
								:'-' 
							   }} Floor<br/>
							   
							   <!--<b>Property On Floor</b>: {{ 
								(isset($record->floorID))
								?
								$record->floorID
								:'-' 
							   }}<br/>
							   <b>Total Floor</b>: {{ 
								(isset($record->total_floor))
								?
								$record->total_floor
								:'-' 
							   }}<br/>-->
								<b>Property age:</b>{{
									(isset($dropdown['propertyAge'][$record->old_building]))
										?
										$dropdown['propertyAge'][$record->old_building]
										:'-' 
									   }}
								<br/>
								<b>Facing:</b>
								@if ($record->Facing != "")
									{{$record->Facing}}
								@endif
								<br/>
								<b>Notice Period</b>
								{{
									(isset($dropdown['noticePeriodDropdown'][$record->noticePeriod]))
										?
										$dropdown['noticePeriodDropdown'][$record->noticePeriod]
										:'-' 
									   }}
								<br/>
										
										</p>
									  </div>
									  <div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									  </div>
									</div>

								  </div>
								</div>
								
						   </td>
						   <td>{{$record->status}}</td>
						   <td>{{$record->image_count}}</td>
						   <td>{{$record->remarks}}</td>
                              <!--<td>{{$record->areaName}}</td>
                              <td>{{$record->cityName}}</td>
                              <td>{{$record->stateName}}</td> -->
                              <td colspan="3">
                                 <div class="d-flex justify-content-center">
                                    <!--<div class="btn-group" role="group" aria-label="">
                                       <a class="btn btn-outline-primary btn-sm rounded-0 border-right-0" href="{{ route('admin_edit_user',base64_encode($record->userID))}}" data-toggle="tooltip" data-placement="top" title="Admin"><i class="fas fa-lg fa-user-secret"></i>{{$record->createdBy}}</a>
                                    </div>-->
                                    @if($record->status == 'Available')
                                       <div class="btn-group" role="group" aria-label="">
                                          <div class="btn btn-outline-success btn-sm rounded-0 border-right-0" data-toggle="tooltip" data-placement="top" title="Available Property, Mark it Occupied" onclick="activateProperty('{{base64_encode($record->propertyID)}}','0')">
                                             <i class="fas fa-lg fa-user-circle"></i>
                                          </div>
                                       </div>
									@endif
									@if($record->status == 'Occupied')
										<div class="btn-group" role="group" aria-label="">
                                          <div class="btn btn-outline-success btn-sm rounded-0 border-right-0" data-toggle="tooltip" data-placement="top" title="Occupied Property, Mark it Available" onclick="activateProperty('{{base64_encode($record->propertyID)}}','1')">
                                             <i class="fas fa-lg fa-user-ninja"></i>
                                          </div>
                                       </div>
									@endif	
									@if($record->status == 'Delete')
                                       <div class="btn-group" role="group" aria-label="">
                                          <div class="btn btn-outline-success btn-sm rounded-0 border-right-0" data-toggle="tooltip" data-placement="top" title="Active Property" onclick="activateProperty('{{base64_encode($record->propertyID)}}','1')">
                                             <i class="fas fa-lg fa-undo"></i>
                                          </div>
                                       </div>
									@endif
									@if($record->status == 'Active')
                                       <div class="btn-group" role="group" aria-label="">
                                          <div class="btn btn-outline-success btn-sm rounded-0 border-right-0" data-toggle="tooltip" data-placement="top" title="Active Property" onclick="activateProperty('{{base64_encode($record->propertyID)}}','0')">
                                             <i class="fas fa-lg fa-check"></i>
                                          </div>
                                       </div>
                                    @endif
									@if($record->status == 'Inactive')
                                       <div class="btn-group" role="group" aria-label="">
                                          <div class="btn btn-outline-danger btn-sm rounded-0 border-right-0 disabled" data-toggle="tooltip" data-placement="top" title="Inactive Property" onclick="activateProperty('{{base64_encode($record->propertyID)}}','1')">
                                             <i class="fas fa-lg fa-times"></i>
                                          </div>
                                       </div>
                                    @endif
                                    @if(SystemHelper::is_module_permission('Property','Edit') || SystemHelper::is_module_permission('Property','Delete'))
                                       <div class="btn-group d-flex justify-content-center" role="group" aria-label="Basic example">
                                          @if(SystemHelper::is_module_permission('Property','Edit'))
                                             <a class="btn btn-outline-info btn-sm rounded-0 border-left-0 border-right-0" href="{{ url('admin/property/edit/'.base64_encode($record->propertyID))}}" data-toggle="tooltip" data-placement="top" title="Edit Property"><i class="fas fa-lg fa-edit"></i></a>
                                          @endif
                                          @if(SystemHelper::is_module_permission('Property','Delete'))
                                             <a class="btn btn-outline-danger btn-sm rounded-0 border-left-0" href="javascript:void(0)" onclick="deleteRecord('{{ base64_encode($record->propertyID) }}')" data-toggle="tooltip" data-placement="top" title="Delete Property"><i class="fas fa-lg fa-trash-alt"></i></a>
                                          @endif
                                       </div>
                                    @endif
                                 </div>
                              </td>
                           </tr>
                           @endforeach
                     </tbody>
                  </table>
               </div>
			   @endif
			   
               {!! $records->appends(\Request::except('page'))->render() !!}
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{!! asset('js/backend/property.js') !!}"></script>
<script>
   $('[data-toggle="tooltip"]').tooltip();
   var type = '{{ Request::segment(2) }}';
   
   $("#propertyCheckAll, #userBottomCheckAll").click(function () {
		$(".user").attr('checked', this.checked);
	});
	
	$('.user').click(function(){
		if($(".user").length == $(".user:checked").length) {
			$("#propertyCheckAll").prop("checked", true);
		}else {
			$("#propertyCheckAll").prop("checked", false);            
		}
	});
</script>
@endsection