@extends('layouts.backend.app')
@section('title')
{{ $page_title}}
@stop
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="bg-title">
            <div class="col-sm-12">
                <h4 class="page-title">{{ ucfirst(Request::segment(2)) }}</h4>
            </div>
            <!-- <div class="col-sm-6">
                breadcrumbs
            </div> -->
        </div>
    </div>
	
    <div class="row">
		<div class="col-xl-2 col-lg-2">
			<div class="card card-stats mb-4 mb-xl-0">
				<div class="card-body">
					<div class="row">
						<div class="col">
							<h5 class="card-auto text-uppercase text-muted mb-0">Active Data</h5>
							<span class="fa-2x font-weight-bold mb-0"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row mt-2">
		@php($active_total = 0)
		@foreach ($data['property_active'] as $indexKey => $record)
		
		@php($active_total += $record->typeCount)

		<div class="col-xl-2 col-lg-2">
			<div class="card card-stats mb-4 mb-xl-0">
				<div class="card-body">
					<div class="row">
						<div class="col">
							<h5 class="card-auto text-uppercase text-muted mb-0" style="min-height:20px;font-size:12px;">{{$record->title}}</h5>
							<span class="fa-2x font-weight-bold mb-0">
							<a href="{{ url('admin/property/?status=Available&propertyTypeID='.$record->propertyTypeID)}}">
								{{$record->typeCount}}
							</a>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		@endforeach
		<div class="col-xl-2 col-lg-2">
			<div class="card card-stats mb-4 mb-xl-0">
				<div class="card-body">
					<div class="row">
						<div class="col">
							<h5 class="card-auto text-uppercase text-muted mb-0" style="min-height:20px;font-size:12px;">Total Properties</h5>
							<span class="fa-2x font-weight-bold mb-0">	
							<a href="{{ url('admin/property/?status=Available')}}">
							{{$active_total}}
							</a>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-2 col-lg-2">
			<div class="card card-stats mb-4 mb-xl-0">
				<div class="card-body">
					<div class="row">
						<div class="col">
							<h5 class="card-auto text-uppercase text-muted mb-0" style="min-height:20px;font-size:12px;">Total Clients</h5>
							<span class="fa-2x font-weight-bold mb-0">
							<a href="{{ url('admin/user/?role=2&status=Active')}}">
								{{$data['active_userCount']}}
							</a>
							</span>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<div class="col-xl-2 col-lg-2">
			<div class="card card-stats mb-4 mb-xl-0">
				<div class="card-body">
					<div class="row">
						<div class="col">
							<h5 class="card-auto text-uppercase text-muted mb-0" style="min-height:20px;font-size:12px;">Total Owners</h5>
							<span class="fa-2x font-weight-bold mb-0">
							<a href="{{ url('admin/user/?role=4&status=Active')}}">
								{{$data['active_ownerCount']}}
							</a>
							</span>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>

	
    <div class="row mt-2">
		<div class="col-xl-2 col-lg-2">
			<div class="card card-stats mb-4 mb-xl-0">
				<div class="card-body">
					<div class="row">
						<div class="col">
							<h5 class="card-auto text-uppercase text-muted mb-0">Deleted Data</h5>
							<span class="fa-2x font-weight-bold mb-0"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row mt-2">
		@php($deleted_total = 0)
		@foreach ($data['property_delete'] as $indexKey => $record)
		
		@php($deleted_total += $record->typeCount)
		
		<div class="col-xl-2 col-lg-2">
			<div class="card card-stats mb-4 mb-xl-0">
				<div class="card-body">
					<div class="row">
						<div class="col">
							<h5 class="card-auto text-uppercase text-muted mb-0" style="min-height:20px;font-size:12px;">{{$record->title}}</h5>
							<span class="fa-2x font-weight-bold mb-0">
							<a href="{{ url('admin/property/?status=Occupied&propertyTypeID='.$record->propertyTypeID)}}">
								{{$record->typeCount}}
							</a>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		@endforeach
		<div class="col-xl-2 col-lg-2">
			<div class="card card-stats mb-4 mb-xl-0">
				<div class="card-body">
					<div class="row">
						<div class="col">
							<h5 class="card-auto text-uppercase text-muted mb-0" style="min-height:20px;font-size:12px;">Total Properties</h5>
							<span class="fa-2x font-weight-bold mb-0">
							<a href="{{ url('admin/property/?status=Occupied')}}">
								{{$deleted_total}}
							</a>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-2 col-lg-2">
			<div class="card card-stats mb-4 mb-xl-0">
				<div class="card-body">
					<div class="row">
						<div class="col">
							<h5 class="card-auto text-uppercase text-muted mb-0" style="min-height:20px;font-size:12px;">Total Clients</h5>
							<span class="fa-2x font-weight-bold mb-0">
							<a href="{{ url('admin/user/?role=2&status=Delete')}}">
								{{$data['delete_userCount']}}
							</a>
							</span>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<div class="col-xl-2 col-lg-2">
			<div class="card card-stats mb-4 mb-xl-0">
				<div class="card-body">
					<div class="row">
						<div class="col">
							<h5 class="card-auto text-uppercase text-muted mb-0" style="min-height:20px;font-size:12px;">Total Owners</h5>
							<span class="fa-2x font-weight-bold mb-0">
							<a href="{{ url('admin/user/?role=4&status=Delete')}}">
								{{$data['delete_ownerCount']}}
							</a>
							</span>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
	
	{{--
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-lg-4">
                <div class="admin-property-block">
					<table class="table table-striped">
					<thead>
					  <tr>
						<th colspan="3">PROPERTIES</th>
					  </tr>
					</thead>
					<tbody>
					  <tr>
						<td>Active</td>
						<td>
							@if(isset($data['active_propertyCount']))
								{{ $data['active_propertyCount'] }}
							@else
								0
							@endif
						</td>
						<td><a href="{{ route('admin_property')}}?status=Active">View Properties</a></td>
					  </tr>
					  <tr>
						<td>Inactive</td>
						<td>
							@if(isset($data['inactive_propertyCount']))
								{{ $data['inactive_propertyCount'] }}
							@else
								0
							@endif
						</td>
						<td><a href="{{ route('admin_property')}}?status=Inactive">View Properties</a></td>
					  </tr>
					  <tr>
						<td>Deleted</td>
						<td>
							@if(isset($data['deleted_propertyCount']))
								{{ $data['deleted_propertyCount'] }}
							@else
								0
							@endif
						</td>
						<td><a href="{{ route('admin_property')}}?status=Delete">View Properties</a></td>
					  </tr>
					  <tr>
						<td>All</td>
						<td>
							@if(isset($data['total_propertyCount']))
								{{ $data['total_propertyCount'] }}
							@else
								0
							@endif
						</td>
						<td>
							<a href="{{ route('admin_property')}}">View Properties</a>
						</td>
					  </tr>
					</tbody>
				  </table>
                </div>
            </div>
			
			<div class="col-xs-12 col-sm-6 col-lg-4">
                <div class="admin-property-block">
					<table class="table table-striped">
					<thead>
					  <tr>
						<th colspan="3">Users</th>
					  </tr>
					</thead>
					<tbody>
					  <tr>
						<td>Active</td>
						<td>
							@if(isset($data['active_userCount']))
								{{ $data['active_userCount'] }}
							@else
								0
							@endif
						</td>
						<td><a href="{{ route('admin_user')}}?status=Active">View Users</a></td>
					  </tr>
					  <tr>
						<td>Inactive</td>
						<td>
							@if(isset($data['inactive_userCount']))
								{{ $data['inactive_userCount'] }}
							@else
								0
							@endif
						</td>
						<td><a href="{{ route('admin_user')}}?status=Inactive">View Users</a></td>
					  </tr>
					  <tr>
						<td>Deleted</td>
						<td>
							@if(isset($data['deleted_userCount']))
								{{ $data['deleted_userCount'] }}
							@else
								0
							@endif
						</td>
						<td><a href="{{ route('admin_user')}}?status=Delete">View Users</a></td>
					  </tr>
					  <tr>
						<td>All</td>
						<td>
							@if(isset($data['total_userCount']))
								{{ $data['total_userCount'] }}
							@else
								0
							@endif
						</td>
						<td>
							<a href="{{ route('admin_user')}}">View Users</a>
						</td>
					  </tr>
					</tbody>
				  </table>
                </div>
            </div>
			
			</div>
			<div class="row">
			
			<div class="col-sm-6 col-lg-4">
                <div class="admin-property-block">
					<table class="table table-striped">
					<thead>
					  <tr>
						<th colspan="3">Users by Type</th>
					  </tr>
					</thead>
					<tbody>
					  <tr>
						<td>Admin</td>
						<td>
							@if(isset($data['admin_userCount']))
								{{ $data['admin_userCount'] }}
							@else
								0
							@endif
						</td>
						<td><a href="{{ route('admin_user')}}?role=1">View Users</a></td>
					  </tr>
					  <tr>
						<td>Manager</td>
						<td>
							@if(isset($data['manager_userCount']))
								{{ $data['manager_userCount'] }}
							@else
								0
							@endif
						</td>
						<td><a href="{{ route('admin_user')}}?role=3">View Users</a></td>
					  </tr>
					  <tr>
						<td>Owner</td>
						<td>
							@if(isset($data['owner_userCount']))
								{{ $data['owner_userCount'] }}
							@else
								0
							@endif
						</td>
						<td><a href="{{ route('admin_user')}}?role=4">View Users</a></td>
					  </tr>
					  <tr>
						<td>User</td>
						<td>
							@if(isset($data['user_userCount']))
								{{ $data['user_userCount'] }}
							@else
								0
							@endif
						</td>
						<td>
							<a href="{{ route('admin_user')}}?role=2">View Users</a>
						</td>
					  </tr>
					  <tr>
						<td>All</td>
						<td>
							@if(isset($data['total_userCount']))
								{{ $data['total_userCount'] }}
							@else
								0
							@endif
						</td>
						<td>
							<a href="{{ route('admin_user')}}">View Users</a>
						</td>
					  </tr>
					</tbody>
				  </table>
                </div>
            </div>
			
			<div class="col-xs-12 col-sm-6 col-lg-4">
                <div class="admin-property-block">
					<table class="table table-striped">
					<thead>
					  <tr>
						<th colspan="3">Properties by Type</th>
					  </tr>
					</thead>
					<tbody>
					  <tr>
						<td>Villa</td>
						<td>
							@if(isset($data['villa_propertyCount']))
								{{ $data['villa_propertyCount'] }}
							@else
								0
							@endif
						</td>
						<td><a href="{{ route('admin_property')}}?role=1">View Properties</a></td>
					  </tr>
					  <tr>
						<td>PG</td>
						<td>
							@if(isset($data['pg_propertyCount']))
								{{ $data['pg_propertyCount'] }}
							@else
								0
							@endif
						</td>
						<td><a href="{{ route('admin_property')}}">View Properties</a></td>
					  </tr>
					  <tr>
						<td>Service Apartment</td>
						<td>
							@if(isset($data['service_app_propertyCount']))
								{{ $data['service_app_propertyCount'] }}
							@else
								0
							@endif
						</td>
						<td><a href="{{ route('admin_property')}}">View Properties</a></td>
					  </tr>
					  <tr>
						<td>Home</td>
						<td>
							@if(isset($data['home_propertyCount']))
								{{ $data['home_propertyCount'] }}
							@else
								0
							@endif
						</td>
						<td>
							<a href="{{ route('admin_property')}}">View Properties</a>
						</td>
					  </tr>
					  <tr>
						<td>All</td>
						<td>
							@if(isset($data['total_propertyCount']))
								{{ $data['total_propertyCount'] }}
							@else
								0
							@endif
						</td>
						<td>
							<a href="{{ route('admin_property')}}">View Properties</a>
						</td>
					  </tr>
					</tbody>
				  </table>
                </div>
            </div>
        </div>
		--}}
		{{--
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <div class="white-box admin-property-block">
                    <a href="#">
                        <h3 class="box-title">RECENT PROPERTIES</h3>
                    </a>
                    <div id="no-more-tables">
                        <div class="table-responsive">
                            <table class="table tabledesign">
                                <thead>
                                    <tr>
                                        <th width="2%"><span>#</span></th>
                                        <th width="12%"><span>Title</span></th>
                                        <th width="14%"><span>Company Name</span></th>
                                        <th width="14%"><span>Company Number</span></th>
                                        <th width="13%"><span>Address</span></th>
                                        <th width="10%"><span>Area</span></th>
                                        <th width="10%"><span>City</span></th>
                                        <th width="9%"><span>State</span></th>
                                        <th width="10%" ><span>Created By</span></th>
                                        <th width="7%" ><span>Status</span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($data['properties'] as $key => $value)
                                    <tr>
                                        <td>
                                        {{ (($data['properties']->currentpage()-1)*$data['properties']->perpage())+$key+1}}
                                        </td>
                                        <td>
                                        <a href="{{ route('admin_edit_property',base64_encode($value->propertyID))}}">{{ $value->title }}</a>
                                        </td>
                                        <td>
                                        {{ $value->companyName }}
                                        </td>
                                        <td>
                                        {{ $value->companyNumber }}
                                        </td>
                                        <td>
                                        {{ $value->address }}
                                        </td>
                                        <td>
                                        {{ $value->areaName }}
                                        </td>
                                        <td>
                                        {{ $value->cityName }}
                                        </td>
                                        <td>
                                        {{ $value->stateName }}
                                        </td>
                                        <td >
                                        <a class="creayedbybtn" href="{{ route('admin_edit_user',base64_encode($value->userID))}}">  {{ $value->createdBy }}
                                        </a>
                                        </td>
                                        <td >
                                            @if($value->status == 'Active')
                                                <span class="activebtn">{{ $value->status }}</span>
                                            @else
                                                <span class="inactivebtn">{{ $value->status }}</span>
                                            @endif
                                        </td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td>No records found.</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>--}}
</div>
@endsection
