@extends('layouts.backend.app')
@section('title')
{{ $page_title}}
@stop
@section('content') 
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<script type="text/javascript" src="{!! asset('js/select2.full.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/jquery.validate.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/backend/area.js') !!}"></script>

<div class="container">
  <div class="page-title">
   
        <h3>Add Area</h3>
  
  </div>

      <div class="add_form_section">
   
          @if (Session::has('message'))
          <div class="alert {{ Session::get('msgclass') }} alert-dismissable">{{ Session::get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          </div>
          @endif
       
              <form name="frmarea" id="frmarea" method="POST" action="{{ route('admin_add_area')}}" novalidate="" class="form-horizontal">
                {{ csrf_field() }}
                {!! Form::hidden('mode',"add", ['id'=>'mode']) !!}
                    <div class="shadowbox">
                  <div class="row">  <div class="col-sm-4 col-md-3">
                    <label class="control-label">Title</label>   </div>
                     <div class="col-sm-8 col-md-9">
                    <div class="form-group"> {{Form::text('areaName',null,['id'=>'areaName','required','class'=>'form-control'])}}
                      @if($errors->has('label_areaName'))
                      <div class="error">{{ $errors->first('label_areaName') }}</div>
                      @endif
                    </div></div>
                  </div>
                  <div class="row">  <div class="col-sm-4 col-md-3">
                    <label class="control-label">Pincode</label>   </div>
                     <div class="col-sm-8 col-md-9">
                    <div class="form-group"> {{Form::number('pincode',null,['id'=>'pincode','required','class'=>'form-control'])}}
                      @if($errors->has('label_pincode'))
                      <div class="error">{{ $errors->first('label_pincode') }}</div>
                      @endif
                    </div></div>
                  </div>
                  <div class="row">  <div class="col-sm-4 col-md-3">
                    <label class="control-label">State</label>   </div>
                     <div class="col-sm-8 col-md-9">
                    <div class="form-group">
                      {!! Form::select('state', [null=>"Select State"] + $stateDropdown, "null", ['class' => 'form-control select2', 'id' => 'state', 'onchange'=>'getCity()','required']) !!}
                      @if($errors->has('label_state'))
                      <div class="error">{{ $errors->first('label_state') }}</div>
                      @endif
                    </div></div>
                  </div>
                  <div class="row">  <div class="col-sm-4 col-md-3">
                    <label class="control-label">City</label>   </div>
                     <div class="col-sm-8 col-md-9">
                    <div class="form-group">
                      {!! Form::select('city', [null=>"Select City"], "null", ['class' => 'form-control select2', 'id' => 'city','required']) !!}
                      @if($errors->has('label_city'))
                      <div class="error">{{ $errors->first('label_city') }}</div>
                      @endif
                    </div></div>
                  </div>
                  <div class="row">  <div class="col-sm-4 col-md-3">
                    <label class="control-label">Status</label>   </div>
                     <div class="col-sm-8 col-md-9">
                    <div class="form-group"> {{Form::select('status', ['Active'=>'Active','Inactive'=>'Inactive'],null,['id'=>'status','required','class'=>'form-control'])}}
                      @if($errors->has('label_status'))
                      <div class="error">{{ $errors->first('label_status') }}</div>
                      @endif
                    </div></div>
                  </div>
                </div>
                <div class="">
                  <div class="form-actions">
                    <button type="submit"  class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                    <a href="/admin/{{ Request::segment(2) }}">
                    <button type="button" class="btn btn-default">Cancel</button>
                    </a> </div>
                </div>
              </form>
            </div>
    
  <!--./row--> 
</div>
<script type="text/javascript">
  /*$(document).ready(function () {

  });
  function addPlan(){
    
  }*/
</script>
@endsection