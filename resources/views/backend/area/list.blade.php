@extends('layouts.backend.app')
@section('title')
{{ $page_title}}
@stop
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card rounded-0 mb-3">
                <div class="card-body" style="padding-bottom:0px;">	
                    <div class="row">
                        <div class="col col-lg-6 col-sm-6 col-md-6 col-xl-6">
                            <h1 class="greenroom-title mt-2"><i class="fab fa-font-awesome-flag"></i> Area</h1>
                        </div>
                        <div class="col col-lg-6 col-sm-6 col-md-6 col-xl-6">
                            @if(SystemHelper::is_module_permission('Area','Add'))
                                <div class="d-flex justify-content-end">
                                    <a href="{{ route('admin_add_area') }}" class="btn btn-primary btn-md shadow">
                                    <i class="fas fa-plus-circle"></i> Add New Area
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        @if (Session::has('message'))
                            <div class="col-12">
                                <div class="alert alert-danger {{ Session::get('msgclass') }} alert-dismissable">{{ Session::get('message') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        @endif
                    </div>
                    </div>
                    
                    <div class="table-responsive-sm card-body" style="padding-top:0px;">
					<div class="card rounded-0">
                                 <form class="well-filter" name="frmlist" id="frmlist" novalidate>
									<div class="m-3">
                                 
                                          <div class="col-md-2">
                                             <div class="form-group">
                                                <input class="form-control" placeHolder="Area Name" type="text"id="areaName"name="areaName"required="required"value="{{Request::input('areaName')}}"/>
                                             </div>
                                          </div>
                                          <div class="col-md-2">
                                             <div class="form-group">
                                                <input  class="form-control" placeHolder="City Name" type="text"id="cityName"name="cityName"required="required"value="{{Request::input('cityName')}}"/>
                                             </div>
                                          </div>
                                          <div class="col-md-2">
                                            <div class="form-group">
                                                <input  class="form-control" placeHolder="State Name" type="text"id="stateName"name="stateName"required="required"value="{{Request::input('stateName')}}"/>
                                             </div>
                                          </div>
                                       
                                   
                                       
                                          <div class="col-md-2">
                                            <div class="form-group">
                                                <input  class="form-control" placeHolder="Pincode"  type="text"id="pincode"name="pincode"required="required"value="{{Request::input('pincode')}}"/>
                                             </div>
                                          </div>
                                          <div class="col-md-2">
                                            <div class="form-group">
                                                {!!Form::select('status',([''=>'Status','Active'=>'Active','Inactive'=>'Inactive']),null,['class'=>'form-control','id'=>'status'])!!}
                                                
                                             </div>
                                          </div>
                                       
                                       
									   
									    <div class="col-md-1 form-group">
											<button type="submit" class="btn btn-success form-control"><i class="fas fa-search-plus"></i> </button>
										</div>
										<div class="col-md-1 form-group">
											<a href='{{ route("admin_area") }}'class="btn btn-primary form-control"><i class="fas fa-sync" aria-hidden="true"></i></a>
										</div>
									   
									   
                                    
                                 </form>
                                </div>
								
								<form name="frmuserlist" id="frmuserlist" method="post">	
                    <table class="display table table-hover table-bordered table-striped w-full dtr-inline" id="data-user" border="1">
                        <thead class="thead-light">
                    
                                <th>#</th>
                                <th>@sortablelink('areaName','Title')</th>
                                <th>@sortablelink('cityName','City')</th>
                                <th>@sortablelink('stateName','State')</th>
                                <th>@sortablelink('pincode','Pincode')</th>
                                <th></th>
                            </thead>
                            <tbody>
                                @foreach ($records as $indexKey => $record)
                                <tr>
                                    <td>{{(($records->currentpage()-1)*$records->perpage())+$indexKey+1}}</td>
                                    <td>{{$record->areaName}}</td>
                                    <td>{{$record->cityName}}</td>
                                    <td>{{$record->stateName}}</td>
                                    <td>{{$record->pincode}}</td>
                                    <td>
                                        <div class="d-flex justify-content-center">
                                            @if($record->status == 'Active')
                                                <div class="btn-group" role="group" aria-label="">
                                                    <div class="btn btn-outline-success btn-sm rounded-0 border-right-0" data-toggle="tooltip" data-placement="top" title="Active">
                                                    <i class="fas fa-lg fa-check"></i>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="btn-group" role="group" aria-label="">
                                                    <div class="btn btn-outline-danger btn-sm rounded-0 border-right-0 disabled" data-toggle="tooltip" data-placement="top" title="Inactive">
                                                    <i class="fas fa-lg fa-times"></i>
                                                    </div>
                                                </div>
                                            @endif
                                            @if(SystemHelper::is_module_permission('Area','Edit')||SystemHelper::is_module_permission('Area','Delete'))
                                                <div class="btn-group d-flex justify-content-center" role="group" aria-label="">
                                                    @if(SystemHelper::is_module_permission('Area','Edit'))
                                                    <a class="btn btn-outline-info btn-sm rounded-0 border-left-0 border-right-0" href="{{ url('admin/area/edit/'.base64_encode($record->areaID))}}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-lg fa-edit"></i></a>
                                                    @endif
                                                    @if(SystemHelper::is_module_permission('Area','Delete'))
                                                    <a class="btn btn-outline-danger btn-sm rounded-0 border-left-0" href="javascript:void(0)" onclick="deleteRecord('{{base64_encode($record->areaID)}}')" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fas fa-lg fa-trash-alt"></i></a>
                                                    @endif
                                                </div>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </form>
                    </div>
                    {!! $records->appends(\Request::except('page'))->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"src="{!!asset('js/backend/area.js')!!}"></script>
<script>var type='{{Request::segment(2)}}';$('[data-toggle="tooltip"]').tooltip();</script>
@endsection