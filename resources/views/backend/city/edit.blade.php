@extends('layouts.backend.app')
@section('title')
{{ $page_title}}
@stop
@section('content') 
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<script type="text/javascript" src="{!! asset('js/select2.full.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/jquery.validate.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/backend/city.js') !!}"></script>
<div class="container">
  <div class="page-title">

        <h3>Edit City</h3>

  </div>

      <div class="add_form_section">

          @if (Session::has('message'))
          <div class="alert {{ Session::get('msgclass') }} alert-dismissable">{{ Session::get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          </div>
          @endif
 
              <form name="frmcity" id="frmcity" method="POST" action="{{ route('admin_edit_city')}}" class="form-horizontal">
                {{ csrf_field() }}
                {!! Form::hidden('cityID', base64_encode($data->cityID), ['id'=>'cityID']) !!}
                {!! Form::hidden('mode',"edit", ['id'=>'mode']) !!}
             <div class="shadowbox">
                    <div class="row"> <div class="col-sm-4 col-md-3">
                      <label class="control-label">Title</label>
                      </div>
                     <div class="col-sm-8 col-md-9">
                      <div class="form-group"> {{Form::text('cityName',$data->cityName,['id'=>'cityName','required','class'=>'form-control'])}}
                        @if($errors->has('label_cityName'))
                        <div class="error">{{ $errors->first('label_cityName') }}</div>
                        @endif
                      </div></div>
                    </div>
                    <div class="row">     <div class="col-sm-4 col-md-3">
                        <label class="control-label">State</label> </div>
                     <div class="col-sm-8 col-md-9">
                        <div class="form-group">
                          {!! Form::select('state', [null=>"Select State"] + $data->stateDropdown, "$data->stateID", ['class' => 'form-control select2', 'id' => 'state','required']) !!}
                          @if($errors->has('label_state'))
                          <div class="error">{{ $errors->first('label_state') }}</div>
                          @endif
                        </div> </div>
                      </div>
                    <div class="row">     <div class="col-sm-4 col-md-3">
                      <label class="control-label">Status</label> </div>
                     <div class="col-sm-8 col-md-9">
                      <div class="form-group"> {{Form::select('status', ['Active'=>'Active','Inactive'=>'Inactive'],$data->status,['id'=>'status','required','class'=>'form-control'])}}
                        @if($errors->has('label_status'))
                        <div class="error">{{ $errors->first('label_status') }}</div>
                        @endif
                      </div> </div>
                    </div>
                </div>
                <div class="">
                  <div class="form-actions">
                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                    <a href="/admin/{{ Request::segment(2) }}">
                    <button type="button" class="btn btn-default">Cancel</button>
                    </a> </div>
                </div>
              </form>
            </div>
   
  <!--./row--> 
</div>
<script type="text/javascript">
  /*$(document).ready(function () {

  });
  */
</script>
@endsection