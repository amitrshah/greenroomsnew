@extends('layouts.backend.app')
@section('title')
{{ $page_title}}
@stop
@section('content') 
<script type="text/javascript" src="{!! asset('js/jquery.validate.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/backend/facilities.js') !!}"></script>

<div class="container">
    <div class="page-title">
        <h3>Edit Interior</h3>
    </div>
    <div class="add_form_section">
        @if (Session::has('message'))
        <div class="alert {{ Session::get('msgclass') }} alert-dismissable">{{ Session::get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        </div>
        @endif

        <form name="frmfacilities" id="frmfacilities" method="POST" action="{{ route('admin_edit_facilities')}}" class="form-horizontal">
            {{ csrf_field() }}
            {!! Form::hidden('facilitiesID', base64_encode($data->facilitiesID), ['id'=>'facilitiesID']) !!}
            {!! Form::hidden('mode',"edit", ['id'=>'mode']) !!}
            <div class="shadowbox">
                <div class="row">        
                    <div class="col-sm-4 col-md-3">
                        <label class="control-label">Title</label>
                    </div>
                    <div class="col-sm-8 col-md-9">
                        <div class="form-group"> {{Form::text('title',$data->title,['id'=>'title','required','class'=>'form-control'])}}
                            @if($errors->has('label_title'))
                            <div class="error">{{ $errors->first('label_title') }}</div>
                            @endif
                        </div>
                    </div>  </div>
                <div class="row">        <div class="col-sm-4 col-md-3">
                        <label class="control-label">Status</label>  </div>
                    <div class="col-sm-8 col-md-9">
                        <div class="form-group"> {{Form::select('status', ['Active'=>'Active','Inactive'=>'Inactive'],$data->status,['id'=>'status','required','class'=>'form-control'])}}
                            @if($errors->has('label_status'))
                            <div class="error">{{ $errors->first('label_status') }}</div>
                            @endif
                        </div>  </div>
                </div>
            </div>
            <div class="">
                <div class="form-actions">
                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                    <a href="{{ route('admin_facilities')}}">
                        <button type="button" class="btn btn-default">Cancel</button>
                    </a> </div>
            </div>
        </form>
    </div>

    <!--./row--> 
</div>
<script type="text/javascript">
        /*$(document).ready(function () {
         
         });
         */
</script>
@endsection