@extends('layouts.backend.app')
@section('title')
{{ $page_title}}
@stop
@section('content') 
<script type="text/javascript" src="{!! asset('js/jquery.validate.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/backend/user_role.js') !!}"></script>

<div class="container">
  <div class="page-title">

        <h3>Edit User Permission</h3>
  
  </div>

      <div class="add_form_section">
    
          @if (Session::has('message'))
          <div class="alert {{ Session::get('msgclass') }} alert-dismissable">{{ Session::get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          </div>
          @endif
       
              <form name="frmuserrole" id="frmuserrole" method="POST" action="{{ route('admin_update_role_permission')}}"  class="form-horizontal searchbox">
                {{ csrf_field() }}
                {!! Form::hidden('roleID', $roleID, ['id'=>'roleID']) !!}
                {!! Form::hidden('mode',"manage", ['id'=>'mode']) !!}
                 <table class="table tabledesign">
                    <thead>
                      <tr>
                        <th>Module</th>
                        @foreach ($actions as $action)
                        <th>   <div class="checkboxtbl"> {{Form::checkbox('chkcolall_'.$action->actionID, $action->actionID, false, ['onclick'=>'checkcolall('.$action->actionID.')'])}}
                        <label><span></span> {{$action->actionName}} </label></div></th>
                        @endforeach </tr>
                    </thead>
                    <tbody>
                    
                    @foreach($modules as $module)
                    <tr>
                      <td>
                         {{$module->moduleName}} </td>
                      @foreach ($actions as $action)
                      <td> 
                          <div class="checkboxtbl"> 
                        {{Form::checkbox($action->actionName.'[]', $module->moduleID.'_'.$action->actionID, ((isset($per_sel_arr[$module->moduleID][$action->actionName])) ? true : false), ['class'=>'col_'.$action->actionID.' row_'.$module->moduleID])}} 
                         <label><span></span></label>
                       </div>
                      </td>
                      @endforeach </tr>
                    @endforeach
                      </tbody>
                    
                  </table>
                <div class="">
                  <div class="form-actions">
                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                    <a href="{{ route('admin_userrole') }}">
                    <button type="button" class="btn btn-default">Cancel</button>
                    </a> </div>
                </div>
              </form>
            </div>
   
  <!--./row--> 
</div>
<script type="text/javascript">
  /*$(document).ready(function () {

  });
  */
</script>
@endsection