@extends('layouts.backend.app')
@section('title')
{{ $page_title}}
@stop
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card rounded-0 mb-3">
                <div class="card-body" style="padding-bottom:0px;">	
                    <div class="row">
                        <div class="col col-xs-12 col-lg-6 col-sm-6 col-md-6 col-xl-6">
                            <h1 class="greenroom-title mt-2"><i class="fas fa-list-ol"></i> User Role</h1>
                        </div>
                        <div class="col col-xs-12 col-lg-6 col-sm-6 col-md-6 col-xl-6">
                            @if(SystemHelper::is_module_permission('Userrole','Add'))
                                <div class="d-flex justify-content-end">
                                    <a href="{{ route('admin_add_userrole') }}" class="btn btn-primary btn-md shadow">
                                        <i class="fas fa-plus-circle"></i> Add New {{Request::segment(2)}}
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        @if (Session::has('message'))
                            <div class="col-12">
                                <div class="alert alert-danger {{ Session::get('msgclass') }} alert-dismissable">{{ Session::get('message') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        @endif
                    </div>
                    </div>

					<div class="table-responsive-sm card-body" style="padding-top:0px;">
						<div class="card rounded-0">
                        
                                 <form name="frmlist" id="frmlist" novalidate>
                                    <div class="m-3">
									<div class="form-group">
                                       
                                          <div class="col-md-2">
											<div class="form-group">
                                                    <input type="text" id="roleName"  required="required" name="roleName" value="{{ Request::input('roleName') }}" placeHolder="Search By" class="form-control"/>
                                                </div>
                                          </div>
                                          <div class="col-md-2">
											<div class="form-group">
                                                    {!!Form::select('status',([''=>'Status','Active'=>'Active','Inactive'=>'Inactive']),null,['class'=>'form-control','id'=>'status'])!!}
                                                    
                                                </div>
                                          </div>
                                       
 									   <div class="col-md-1 form-group">
											<button type="submit" class="btn btn-success form-control"><i class="fas fa-search-plus"></i> </button>
										</div>
										<div class="col-md-1 form-group">
											<a href='{{ route("admin_userrole") }}'class="btn btn-primary form-control"><i class="fas fa-sync" aria-hidden="true"></i></a>
										</div>
									   
									   
                                    </div>
                                    </div>
                                 </form>
                                
                </div>
				
                <form name="frmuserlist" id="frmuserlist" method="post">	
                    <table class="display table table-hover table-bordered table-striped w-full dtr-inline" id="data-user" border="1">
                        <thead class="thead-light">
                            <tr>
                                <!--<th class="no"><input id="userroleCheckAll" type="checkbox" /></th>-->
                                <th>#</th>
                                <th>@sortablelink('roleName','Title')</th>
                                <th>@sortablelink('description','Description')</th>
                            @if(SystemHelper::is_module_permission('Userrole','Edit')||SystemHelper::is_module_permission('Userrole','Delete'))
                                <th></th>
                            @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($records as $indexKey=>$record)
                            <tr>
                                <!--<td><input class="userrole" name="userrole[]" type="checkbox" value="{{ $record->id }}" /></td>-->
                                <td>{{(($records->currentpage()-1)*$records->perpage())+$indexKey+1}}</td>
                                <td>{{$record->roleName}}</td>
                                <td>{{$record->description}}</td>
                                <td>
                                    <div class="d-flex justify-content-center">
                                        @if($record->status=='Active')
                                            <div class="btn-group" role="group" aria-label="">
                                                <div class="btn btn-outline-success btn-sm rounded-0 border-right-0" data-toggle="tooltip" data-placement="top" title="Active">
                                                <i class="fas fa-lg fa-user-circle"></i>
                                                </div>
                                            </div>
                                        @else
                                            <div class="btn-group" role="group" aria-label="">
                                                <div class="btn btn-outline-secondary btn-sm rounded-0 border-right-0 disabled" data-toggle="tooltip" data-placement="top" title="Inactive">
                                                <i class="fas fa-lg fa-user-circle"></i>
                                                </div>
                                            </div>
                                        @endif
                                        @if(SystemHelper::is_module_permission('Userrole','Edit')||SystemHelper::is_module_permission('Userrole','Delete'))
                                        <div class="btn-group shadow-sm" role="group" aria-label="Basic example">
                                                @if(SystemHelper::is_module_permission('Userrole','Edit'))
                                                <a class="btn btn-outline-info btn-sm rounded-0 border-left-0 border-right-0" href="{{url('admin/userrole/edit/'.base64_encode($record->roleID))}}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-lg fa-edit"></i></a>
                                                @endif
                                                @if(SystemHelper::is_module_permission('Userrole','Delete'))
                                                <a class="btn btn-outline-danger btn-sm rounded-0 border-left-0 border-right-0" href="javascript:void(0)" onclick="deleteRecord('{{base64_encode($record->roleID)}}')" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fas fa-lg fa-trash-alt"></i></a>
                                                @endif
                                                @if(SystemHelper::is_module_permission('Userrole','Edit'))
                                                <a class="btn btn-outline-dark btn-sm rounded-0 border-left-0" href="{{url('admin/userrole/manage/'.base64_encode($record->roleID))}}" data-toggle="tooltip" data-placement="top" title="Manage Permission"><i class="fas fa-lg fa-tasks"></i></a>
                                                @endif
                                            </div>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </form>
                {!! $records->appends(\Request::except('page'))->render() !!}
            </div>
       
    </div>
</div>
<script type="text/javascript"src="{!!asset('js/backend/user_role.js')!!}"></script>
<script>var type='{{Request::segment(2)}}';$('[data-toggle="tooltip"]').tooltip();</script>
<script>
    //$('#data-user').DataTable();
	
	$("#userroleCheckAll, #userroleBottomCheckAll").click(function () {
		$(".userrole").prop('checked', this.checked);
	});
	
	$('.userrole').click(function(){
		if($(".userrole").length == $(".userrole:checked").length) {
			$("#userroleCheckAll").prop("checked", true);
			$("#userroleBottomCheckAll").prop("checked", true);
		}else {
			$("#userroleCheckAll").prop("checked", false);            
			$("#userroleBottomCheckAll").prop("checked", false);            
		}
	});
</script>
@endsection