@extends('layouts.backend.app')
@section('title')
{{ $page_title}}
@stop
@section('content')
<script type="text/javascript" src="{!! asset('js/jquery.validate.js') !!}"></script>
<div class="container authcontent">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form id="frmemail" class="form-horizontal" method="POST" action="{{ route('admin_password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                 

                            <div class="">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="E-Mail Address">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="">
                                <button type="submit" class="btn btn-primary">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#frmemail").validate({
        rules: {
            email: {
                required: true,
                normalizer: function(value) {
                    return $.trim(value);
                }
            },
        },
        messages: {
            email:{
                required:"Please enter email.",
            },
        },
    });
</script>
@endsection
