@extends('layouts.backend.app')
@section('title')
{{ $page_title}}
@stop
@section('content')
<script type="text/javascript" src="{!! asset('js/jquery.validate.js') !!}"></script>
<div class="container authcontent">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">Admin Login</div>
                <div class="panel-body">
                    <form id="frmlogin" class="form-horizontal" method="POST" action="{{ route('admin_authenticate') }}">
                        {{ csrf_field() }}
                        @if (Session::get('status'))
                            <div class="alert alert-dismissable alert-danger">
                                <ul>
                                    <li>{!! Session::get('status') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                       

                            <div class="">
                                <i class="fa fa-envelope-o"></i> <input id="mobile" type="text" class="form-control" name="mobile" value="{{ old('mobile') }}" required autofocus placeholder="Mobile">

                                @if ($errors->has('mobile'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
             

                            <div class=""> <i class="fa fa-lock"></i>
                                <input id="password" type="password" class="form-control" name="password" required placeholder="Password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                         <div class="remebercheckbox">
                             
                                   
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>     <label> <span></span> Remember Me
                                    </label>
                        
                            </div>
                             <a class="btn btn-link" href="{{ route('admin_password.request') }}">
                                    Forgot Your Password?
                                </a>
                        </div>

                        <div class="form-group">
                            <div class="">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#frmlogin").validate({
        rules: {
            mobile: {
                required: true,
                normalizer: function(value) {
                    return $.trim(value);
                }
            },
            password:{
                required: true,
            },
        },
        messages: {
            mobile:{
                required:"Please enter mobile.",
            } ,
            password:{
                    required : "Please enter password.",
                },
        },
    });
</script>
@endsection
