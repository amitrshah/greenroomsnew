@extends('layouts.backend.app')
@section('title')
{{ $page_title}}
@stop
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card rounded-0 mb-3">
                <div class="card-body" style="padding-bottom:0px;">	
                    <div class="row">
                        <div class="col col-xs-12 col-lg-6 col-sm-6 col-md-6 col-xl-6">
                            <h1 class="greenroom-title mt-2"><i class="far fa-building"></i> Property Category</h1>
                        </div>
                        <div class="col col-xs-12 col-lg-6 col-sm-6 col-md-6 col-xl-6">
                            @if(SystemHelper::is_module_permission('Propertytype','Add'))
                                <div class="d-flex justify-content-end">
                                    <a href="{{ route('admin_add_propertytype') }}" class="btn btn-primary btn-md shadow">
                                    <i class="fas fa-plus-circle"></i> Add New Property Category
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        @if (Session::has('message'))
                            <div class="col-12">
                                <div class="alert alert-danger {{ Session::get('msgclass') }} alert-dismissable">{{ Session::get('message') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        @endif
                    </div>
                    </div>
                <div class="table-responsive-sm card-body" style="padding-top:0px;">
					<div class="card rounded-0">
					
                    
                                 <form class="well-filter" name="frmlist" id="frmlist" novalidate>
                                    <div class="m-3">
										<div class="col-md-2">
											<div class="form-group">
                                                <input type="text"id="title"required="required"class="form-control" placeHolder="Search By" name="title"value="{{Request::input('title')}}"/>
                                             </div>
                                          </div>
                                          <div class="col-md-2">
											<div class="form-group">
                                                {!!Form::select('status',([''=>'Status','Active'=>'Active','Inactive'=>'Inactive']),null,['class'=>'form-control','id'=>'status'])!!}
                                                
                                             </div>
                                          </div>
                                      
									   <div class="col-md-1 form-group">
											<button type="submit" class="btn btn-success form-control"><i class="fas fa-search-plus"></i> </button>
										</div>
										<div class="col-md-1 form-group">
											<a href='{{ route("admin_propertytype") }}'class="btn btn-primary form-control"><i class="fas fa-sync" aria-hidden="true"></i></a>
										</div>
                                    </div>
                                 </form>
                                
								

                                <form name="frmuserlist" id="frmuserlist" method="post">	
                    <table class="display table table-hover table-bordered table-striped w-full dtr-inline" id="data-user" border="1">
                        <thead class="thead-light">
                                        <th>#</th>
                                        <th>@sortablelink('title','Title')</th>
                                        <th></th>
                                    </thead>
                                    <tbody>
                                        @foreach ($records as $indexKey => $record)
                                        <tr>
                                            <td>{{(($records->currentpage()-1)*$records->perpage())+$indexKey+1}}</td>
                                            <td>{{$record->title}}</td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                   @if($record->status == 'Active')
                                                      <div class="btn-group" role="group" aria-label="">
                                                         <div class="btn btn-outline-success btn-sm rounded-0 border-right-0" data-toggle="tooltip" data-placement="top" title="Active">
                                                            <i class="fas fa-lg fa-check"></i>
                                                         </div>
                                                      </div>
                                                   @else
                                                      <div class="btn-group" role="group" aria-label="">
                                                         <div class="btn btn-outline-danger btn-sm rounded-0 border-right-0 disabled" data-toggle="tooltip" data-placement="top" title="Inactive">
                                                            <i class="fas fa-lg fa-times"></i>
                                                         </div>
                                                      </div>
                                                   @endif
                                                   @if(SystemHelper::is_module_permission('Propertytype','Edit')||SystemHelper::is_module_permission('Propertytype','Delete'))
                                                      <div class="btn-group d-flex justify-content-center" role="group" aria-label="">
                                                         @if(SystemHelper::is_module_permission('Propertytype','Edit'))
                                                            <a class="btn btn-outline-info btn-sm rounded-0 border-left-0 border-right-0" href="{{ url('admin/propertytype/edit/'.base64_encode($record->propertyTypeID))}}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-lg fa-edit"></i></a>
                                                         @endif
                                                         @if(SystemHelper::is_module_permission('Propertytype','Delete'))
                                                            <a class="btn btn-outline-danger btn-sm rounded-0 border-left-0" href="javascript:void(0)" onclick="deleteRecord('{{base64_encode($record->propertyTypeID)}}')" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fas fa-lg fa-trash-alt"></i></a>
                                                         @endif
                                                      </div>
                                                   @endif
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {!! $records->appends(\Request::except('page'))->render() !!}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"src="{!!asset('js/backend/property_type.js')!!}"></script>
<script>var type='{{Request::segment(2)}}';$('[data-toggle="tooltip"]').tooltip();</script>
@endsection