@extends('layouts.backend.app')
@section('title')
{{ $page_title}}
@stop
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card rounded-0 mb-3">
                <div class="card-body" style="padding-bottom:0px;">
                    <div class="row">
                        <div class="col col-xs-12 col-lg-6 col-sm-6 col-md-6 col-xl-6">
                            <h1 class="greenroom-title mt-2"><i class="fas fa-home"></i>&nbsp;Member Requirement</h1>
                        </div>
                        <div class="col col-xs-12 col-lg-6 col-sm-6 col-md-6 col-xl-6">
                            <div class="d-flex justify-content-end">
                                    <a href="{{ route('admin_add_memberrequirement') }}" class="btn btn-primary btn-md shadow">
                                        <i class="fas fa-plus-circle"></i> Add New Requirement
                                    </a>
									&nbsp;
									<button class="btn btn-danger btn-md shadow" onClick="deleteAllPayment()">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        @if (Session::has('message'))
                            <div class="col-12">
                                <div class="alert alert-danger {{ Session::get('msgclass') }} alert-dismissable">{{ Session::get('message') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        @endif
                    </div>
                    </div>
                    
				<div class="table-responsive-sm card-body" style="padding-top:0px;">
					<div class="card rounded-0">
                            
                                    <!--<form class="well-filter" name="frmlist" id="frmlist" novalidate>
										<div class="m-3">
                                        
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        {!! Form::select('plan', [''=> 'Plan Name'] + $dropdown['planDropdown'], null, ['class' => 'form-control', 'id' => 'plan']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input type="text" id="userName" name="userName"  value="{{ Request::input('userName') }}" required="required" class="form-control" placeHolder="Username">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input type="text" id="orderID" name="orderID"  value="{{ Request::input('orderID') }}" required="required" class="form-control" placeHolder="Order Id">
                                                        
                                                    </div>
                                                </div>
                                     
                                               <div class="col-md-2">
                                                    <div class="form-group">
                                                        {!! Form::select('city', [''=> 'City'] + $dropdown['cityDropdown'], null, ['class' => 'form-control', 'id' => 'City']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        {!! Form::select('status', [''=> 'Status'] + $dropdown['tnxStatsDropdown'], null, ['class' => 'form-control', 'id' => 'status']) !!}
                                                    </div>
                                                </div>
                                            
                                       
										
										<div class="col-md-1 form-group">
											<button type="submit" class="btn btn-success form-control"><i class="fas fa-search-plus"></i> </button>
										</div>
										<div class="col-md-1 form-group">
											<a href='{{ route("admin_memberpayment") }}'class="btn btn-primary form-control"><i class="fas fa-sync" aria-hidden="true"></i></a>
										</div> 
										
										
                                        
										
										</div>
									</form>-->
                                
                        </div>
                   
                    <form name="frmuserlist" id="frmuserlist" method="post">	
                    <table class="display table table-hover table-bordered table-striped w-full dtr-inline" id="data-user" border="1">
                        <thead class="thead-light">
                            <tr>
							<!--<th class="no"><input id="paymentCheckAll" type="checkbox" /></th>-->
                            <th>#</th>
                            <th>@sortablelink('client_id', 'Client #')</th>
                            <th>@sortablelink('name', 'Name')</th>
                            <th>@sortablelink('contact_no', 'Contact Number')</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>Looking for</th>
                            <th>I want to Finalize PG in</th>
                            <th>Requirement Description</th>
                            <th>Total fav. Count</th>
                            <th>Remaining Count</th>
                            <th>Total Property Visit</th>
                            <th>Visited Property Link</th>
                            <th>Executive Name</th>
                            <th>Remarks</th>
                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($records as $indexKey => $record)
                            <tr>
								<!--<td><input class="payment" name="payments[]" type="checkbox" value="{{ $record->paymentDetailsID }}" /></td>-->
                                <td>{{(($records->currentpage()-1)*$records->perpage())+$indexKey+1}}</td>
                                <td>{{$record->client_id}}</td>
                                <td>{{$record->name}}</td>
                                <td>{{$record->contact_no}}</td>
                                <td>{{$record->email}}</td>
                                <td>{{$record->gender}}</td>
                                
                                <td>{{$dropdown['availablebhkDropdown'][$record->looking_for]}}</td>
                                <td>{{$dropdown['noticePeriodDropdown'][$record->final_pg_in]}}</td>
								
                                <td>{{$record->requirment_description}}</td>
                                <td>{{$record->total_fav_count}}</td>
                                <td>{{$record->remaining_count}}</td>
                                <td>{{$record->total_visited_count}}</td>
                                <td>{{$record->visited_count}}</td>
                                <td>{{$record->executive_name}}</td>
                                <td>{{$record->remarks}}</td>
                                <td>		@if(SystemHelper::is_module_permission('Memberrequirement','Edit')||SystemHelper::is_module_permission('Memberrequirement','Delete'))
										<div class="btn-group d-flex justify-content-center" role="group" aria-label="">
											@if(SystemHelper::is_module_permission('Memberrequirement','Edit'))
											<a class="btn btn-outline-info btn-sm rounded-0 border-left-0 border-right-0" href="{{ url('admin/memberrequirement/edit/'.base64_encode($record->id))}}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-lg fa-edit"></i></a>
											@endif
											@if(SystemHelper::is_module_permission('Memberrequirement','Delete'))
											<a class="btn btn-outline-danger btn-sm rounded-0 border-left-0" href="javascript:void(0)" onclick="deleteRecord('{{base64_encode($record->id)}}')" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fas fa-lg fa-trash-alt"></i></a>
											@endif
										</div>
									@endif
								</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </form>
					
                </div>
                </div>
            
        </div>
    </div>
</div>
<script type="text/javascript"src="{!!asset('js/backend/payment.js')!!}"></script>
<script>
    var type = '{{ Request::segment(2) }}';
    $('[data-toggle="tooltip"]').tooltip();
    //$('#data-user').DataTable();
	
	$("#paymentCheckAll, #paymentBottomCheckAll").click(function () {
		$(".payment").attr('checked', this.checked);
	});
	
	$('.payment').click(function(){
		if($(".payment").length == $(".payment:checked").length) {
			$("#paymentCheckAll").prop("checked", true);
			$("#paymentBottomCheckAll").prop("checked", true);
		}else {
			$("#paymentCheckAll").prop("checked", false);            
			$("#paymentBottomCheckAll").prop("checked", false);            
		}
	});
</script>
@endsection