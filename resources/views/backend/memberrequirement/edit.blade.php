@extends('layouts.backend.app')
@section('title')
{{ $page_title}}
@stop
@section('content') 
<script type="text/javascript" src="{!! asset('js/jquery.validate.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/backend/memberrequirement.js') !!}"></script>

<div class="container-fluid property-add-edit">
	<div class="row">
		<div class="col-12">
			<div class="card rounded-0 mb-3">
			<div class="card-body">
	
	<div class="row">
		<h1 class="greenroom-title mt-2 ml-3">
			<i class="fas fa-building"></i>
			Edit Requirement
		</h1>
	</div>
	<hr/>

      <div class="add_form_section">

          @if (Session::has('message'))
          <div class="alert {{ Session::get('msgclass') }} alert-dismissable">{{ Session::get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          </div>
          @endif
       
              <form name="frmmemberrequirement" id="frmmemberrequirement" method="POST" action="{{ route('admin_add_memberrequirement')}}" novalidate="" class="form-horizontal">
 			    {{ csrf_field() }}
                {!! Form::hidden('id', base64_encode($data->id), ['id'=>'id']) !!}
                {!! Form::hidden('mode',"edit", ['id'=>'mode']) !!}
				
                <div class="shadowbox">
                    <div class="row">         
					<div class="col-sm-4 col-md-3">
                      <label class="control-label">Name</label>     </div>
                     <div class="col-sm-8 col-md-4">
                      <div class="form-group"> {{Form::text('name',$data->name,['id'=>'name','required','class'=>'form-control'])}}
                        @if($errors->has('label_name'))
                        <div class="error">{{ $errors->first('label_name') }}</div>
                        @endif
                      </div></div>
                    </div>
					
					<div class="row">         <div class="col-sm-4 col-md-3">
                      <label class="control-label">Contact No.</label>     </div>
                     <div class="col-sm-8 col-md-2">
                      <div class="form-group"> {{Form::text('contact_no',null,['id'=>'contact_no','required','class'=>'form-control','maxLength'=>10])}}
                        @if($errors->has('label_contact_no'))
                        <div class="error">{{ $errors->first('label_contact_no') }}</div>
                        @endif
                      </div></div>
                    </div>
					
					<div class="row">         <div class="col-sm-4 col-md-3">
                      <label class="control-label">Email</label>     </div>
                     <div class="col-sm-8 col-md-4">
                      <div class="form-group"> {{Form::text('email',null,['id'=>'email','required','class'=>'form-control'])}}
                        @if($errors->has('label_email'))
                        <div class="error">{{ $errors->first('label_email') }}</div>
                        @endif
                      </div></div>
                    </div>
					
					<div class="row">         <div class="col-sm-4 col-md-3">
                      <label class="control-label">Gender</label>     </div>
                     <div class="col-sm-8 col-md-9">
                      <div class="form-group">
						{!! Form::radio('gender', 'Male', true ,['id' => 'male']) !!}
						{!! Form::label('male', 'Male') !!}
					
						{!! Form::radio('gender', 'Female', false,['id' => 'female']) !!}
						{!! Form::label('female', 'Female') !!}
						
                        <div class="error">{{ $errors->first('label_gender') }}</div>
                      </div></div>
                    </div>
					
					<div class="row">         <div class="col-sm-4 col-md-3">
                      <label class="control-label">Looking for</label>     </div>
                     <div class="col-sm-8 col-md-2">
                      <div class="form-group"> 						
						{!! Form::select('looking_for', [null=>"Looking For"] + $dropdowns['propertyTypeDropdown'], null, ['class' => 'form-control', 'id' => 'looking_for','required']) !!}
						@if($errors->has('label_looking_for'))
						<div class="error">{{ $errors->first('label_looking_for') }}</div>
						@endif
						</div>
					  </div>
                    </div>
					
					<div class="row">         <div class="col-sm-4 col-md-3">
                      <label class="control-label">Final pg in</label>     </div>
                     <div class="col-sm-8 col-md-2">
                      <div class="form-group"> 						{!! Form::select("final_pg_in", [null=>"Notice Period"] + $dropdowns["noticePeriodDropdown"], '', ['class' => 'form-control', 'id' => "final_pg_in",'required']) !!}
						@if($errors->has('label_final_pg_in'))
						<div class="error">{{ $errors->first('label_final_pg_in') }}</div>
						@endif
                      </div></div>
                    </div>

					<div class="row">         <div class="col-sm-4 col-md-3">
                      <label class="control-label">Requirement Description</label>     </div>
                     <div class="col-sm-8 col-md-4">
                      <div class="form-group"> {{Form::textarea('requirment_description',null,['id'=>'requirment_description','required','class'=>'form-control','rows'=>3])}}
                        @if($errors->has('label_requirment_description'))
                        <div class="error">{{ $errors->first('label_requirment_description') }}</div>
                        @endif
                      </div></div>
                    </div>
					
					
					<div class="row">         <div class="col-sm-4 col-md-3">
                      <label class="control-label">Total fav count</label>     </div>
                     <div class="col-sm-8 col-md-1">
                      <div class="form-group"> {{Form::text('total_fav_count',null,['id'=>'total_fav_count','required','class'=>'form-control'])}}
                        @if($errors->has('label_total_fav_count'))
                        <div class="error">{{ $errors->first('label_total_fav_count') }}</div>
                        @endif
                      </div></div>
                    </div>
					
					<div class="row">         <div class="col-sm-4 col-md-3">
                      <label class="control-label">Remaining count</label>     </div>
                     <div class="col-sm-8 col-md-1">
                      <div class="form-group"> {{Form::text('remaining_count',null,['id'=>'remaining_count','required','class'=>'form-control'])}}
                        @if($errors->has('label_remaining_count'))
                        <div class="error">{{ $errors->first('label_remaining_count') }}</div>
                        @endif
                      </div></div>
                    </div>
					
					<div class="row">         <div class="col-sm-4 col-md-3">
                      <label class="control-label">Total Visited count</label>     </div>
                     <div class="col-sm-8 col-md-1">
                      <div class="form-group"> {{Form::text('total_visited_count',null,['id'=>'total_visited_count','required','class'=>'form-control'])}}
                        @if($errors->has('label_total_visited_count'))
                        <div class="error">{{ $errors->first('label_total_visited_count') }}</div>
                        @endif
                      </div></div>
                    </div>
					
					<div class="row">         <div class="col-sm-4 col-md-3">
                      <label class="control-label">Visited count</label>     </div>
                     <div class="col-sm-8 col-md-1">
                      <div class="form-group"> {{Form::text('visited_count',null,['id'=>'visited_count','required','class'=>'form-control'])}}
                        @if($errors->has('label_visited_count'))
                        <div class="error">{{ $errors->first('label_visited_count') }}</div>
                        @endif
                      </div></div>
                    </div>
					
					<div class="row">         <div class="col-sm-4 col-md-3">
                      <label class="control-label">Executive name</label>     </div>
                     <div class="col-sm-8 col-md-4">
                      <div class="form-group"> {{Form::text('executive_name',null,['id'=>'executive_name','required','class'=>'form-control'])}}
                        @if($errors->has('label_executive_name'))
                        <div class="error">{{ $errors->first('label_executive_name') }}</div>
                        @endif
                      </div></div>
                    </div>
					
					<div class="row">         <div class="col-sm-4 col-md-3">
                      <label class="control-label">Remarks</label>     </div>
                     <div class="col-sm-8 col-md-4">
                      <div class="form-group"> {{Form::textarea('remarks',null,['id'=>'requirment_description','required','class'=>'form-control','rows'=>3])}}
                        @if($errors->has('label_remarks'))
                        <div class="error">{{ $errors->first('label_remarks') }}</div>
                        @endif
                      </div></div>
                    </div>
					
					
					
                    <div class="row">         <div class="col-sm-4 col-md-3">
                      <label class="control-label">Status</label>     </div>
                     <div class="col-sm-8 col-md-2">
                      <div class="form-group"> 
                        {{Form::select('status', ['1'=>'Active','0'=>'Inactive'],null,['id'=>'status','required','class'=>'form-control'])}}
                        @if($errors->has('label_status'))
                        <div class="error">{{ $errors->first('label_status') }}</div>
                        @endif
                      </div>
                    </div></div>
                </div>
                <div class="">
                  <div class="form-actions">
                    <button type="submit"  class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
					<a href="{{ route('admin_propertytype')}}">
                    <button type="button" class="btn btn-default">Cancel</button>
                    </a> </div>
                </div>
              </form>
            </div>
    
	
	
	
  <!--./row--> 
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
  /*$(document).ready(function () {

  });
  function addPlan(){
    
  }*/
</script>
@endsection