@extends('layouts.backend.app')
@section('title')
{{ $page_title}}
@stop
@section('content') 
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<script type="text/javascript" src="{!! asset('js/jquery.validate.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/select2.full.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/backend/plan.js') !!}"></script>
<div class="container">
  <div class="row page-title"> 
        <h3>Edit Plan</h3> 
  </div>

      <div class="add_form_section">

          @if (Session::has('message'))
          <div class="alert {{ Session::get('msgclass') }} alert-dismissable">{{ Session::get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          </div>
          @endif
     
              <form name="frmplan" id="frmplan" method="POST" action="{{ route('admin_edit_plan')}}" class="form-horizontal">
                {{ csrf_field() }}
                {!! Form::hidden('planID', base64_encode($data->planID), ['id'=>'planID']) !!}
                {!! Form::hidden('mode',"edit", ['id'=>'mode']) !!}
                         <div class="col-sm-6">
               <div class="shadowbox">
                    <div class="row"> <div class="col-sm-4 col-md-3">
                      <label class="control-label">Title*</label></div>
                      <div class="col-sm-8 col-md-9">
                      <div class="form-group"> {{Form::text('planName',$data->planName,['id'=>'planName','required','class'=>'form-control'])}}
                        @if($errors->has('label_planName'))
                        <div class="error">{{ $errors->first('label_planName') }}</div>
                        @endif
                      </div>
                       </div>
                    </div>
                    <div class="row">    <div class="col-sm-4 col-md-3">
                      <label class="control-label">City*</label>
                      </div>
                     <div class="col-sm-8 col-md-9">
                      <div class="form-group">
                        {!! Form::select('city[]', [null=>"Select City"] + $data->cityDropdown, explode(',',$data->cityID), ['class' => 'form-control select2', 'id' => 'city','required', "multiple"=>"multiple"]) !!}
                        @if($errors->has('label_city'))
                        <div class="error">{{ $errors->first('label_city') }}</div>
                        @endif
                      </div>
                       </div>
                    </div>
                    <div class="row">    <div class="col-sm-4 col-md-3">
                      <label class="control-label">Tier</label>
                      </div>
                     <div class="col-sm-8 col-md-9">
                      <div class="form-group"> {{Form::number('tier',$data->tier,['id'=>'tier','class'=>'form-control'])}}
                        @if($errors->has('label_tier'))
                        <div class="error">{{ $errors->first('label_tier') }}</div>
                        @endif
                      </div>
                    </div>
                     </div>
                    <div class="row">    <div class="col-sm-4 col-md-3">
                      <label class="control-label">Price*</label>
                      </div>
                     <div class="col-sm-8 col-md-9">
                      <div class="form-group"> {{Form::number('price',$data->price,['id'=>'price','required','class'=>'form-control'])}}
                        @if($errors->has('label_price'))
                        <div class="error">{{ $errors->first('label_price') }}</div>
                        @endif
                      </div>
                    </div>
                  </div> </div>
                </div>
                          <div class="col-sm-6">
               <div class="shadowbox">
                    <div class="row">    <div class="col-sm-4 col-md-3">
                      <label class="control-label">Duration [In Days]*</label>
                      </div>
                     <div class="col-sm-8 col-md-9">
                      <div class="form-group"> {{Form::number('duration',$data->duration,['id'=>'duration','required','class'=>'form-control'])}}
                        @if($errors->has('label_duration'))
                        <div class="error">{{ $errors->first('label_duration') }}</div>
                        @endif
                      </div>
                    </div>
                     </div>
                    <div class="row">    <div class="col-sm-4 col-md-3">
                      <label class="control-label">Favourite Property Limit*</label>
                      </div>
                     <div class="col-sm-8 col-md-9">
                      <div class="form-group"> {{Form::number('favLimit',$data->favLimit,['id'=>'favLimit','required','class'=>'form-control'])}}
                        @if($errors->has('label_favLimit'))
                        <div class="error">{{ $errors->first('label_favLimit') }}</div>
                        @endif
                      </div>
                    </div>
                     </div>
                    <div class="row">    <div class="col-sm-4 col-md-3">
                      <label class="control-label">Status</label>
                      </div>
                     <div class="col-sm-8 col-md-9">
                      <div class="form-group"> {{Form::select('status', ['Active'=>'Active','Inactive'=>'Inactive'],$data->status,['id'=>'status','required','class'=>'form-control'])}}
                        @if($errors->has('label_status'))
                        <div class="error">{{ $errors->first('label_status') }}</div>
                        @endif
                      </div>
                    </div>
                     </div>
                  </div>
                </div>
               
                <div class="col-sm-12">
                  <div class="form-actions">
                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                    <a href="/admin/{{ Request::segment(2) }}">
                    <button type="button" class="btn btn-default">Cancel</button>
                    </a> </div>
                </div>
              </form>
          
        </div>
 
  <!--./row--> 
</div>
<script type="text/javascript">
  /*$(document).ready(function () {

  });
  */
</script>
@endsection