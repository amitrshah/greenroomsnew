@extends('layouts.backend.app')
@section('title')
{{ $page_title}}
@stop
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card rounded-0 mb-3">
                <div class="card-body" style="padding-bottom:0px;">
                    <div class="row">
                        <div class="col col-xs-12 col-lg-6 col-sm-6 col-md-6 col-xl-6">
                            <h1 class="greenroom-title mt-2"><i class="far fa-calendar-alt"></i> {{Request::segment(2)}}</h1>
                        </div>
                        <div class="col col-xs-12 col-lg-6 col-sm-6 col-md-6 col-xl-6">
                            @if(SystemHelper::is_module_permission('Plan','Add'))
                                <div class="d-flex justify-content-end">
                                    <a href="{{ route('admin_add_plan') }}" class="btn btn-primary btn-md shadow">
                                        <i class="fas fa-plus-circle"></i> Add New {{Request::segment(2)}}
                                    </a>
									&nbsp;
									<button class="btn btn-danger btn-md shadow" onClick="performAllForPlan()">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </div>
                            @endif
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        @if (Session::has('message'))
                            <div class="col-12">
                                <div class="alert alert-danger {{ Session::get('msgclass') }} alert-dismissable">{{ Session::get('message') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        @endif
					</div>
					</div>

                    
                    <div class="table-responsive-sm card-body" style="padding-top:0px;">
                        <div class="card rounded-0">
                                 <form class="well-filter" name="frmlist" id="frmlist" novalidate>
									<div class="m-3">
                                    
                                          <div class="col-md-2">
											<div class="form-group">
                                                <input type="text"id="planName"required="required" name="planName"value="{{Request::input('planName')}}" placeHolder="Search By" class="form-control"/>
                                             </div>
                                          </div>
                                          <div class="col-md-2">
											<div class="form-group">
                                                {!!Form::select('status',([''=>'Status','Active'=>'Active','Inactive'=>'Inactive']),null,['class'=>'form-control','id'=>'status'])!!}
                                                <span class="select-highlight"></span>
                                                <span class="select-bar"></span>
                                             </div>
                                          </div>
                                       </div>
									   <div class="col-md-1 form-group">
											<button type="submit" class="btn btn-success form-control"><i class="fas fa-search-plus"></i> </button>
										</div>
										<div class="col-md-1 form-group">
											<a href='{{ route("admin_plan") }}'class="btn btn-primary form-control"><i class="fas fa-sync" aria-hidden="true"></i></a>
										</div>
                                    </div>
                                 </form>
                        
                
                
                    <form name="frmuserlist" id="frmuserlist" method="post">	
                    <table class="display table table-hover table-bordered table-striped w-full dtr-inline" id="data-user" border="1">
                        <thead class="thead-light">
							<th class="no"><input id="planCheckAll" type="checkbox" /></th>
                            <th>#</th>
                            <th>@sortablelink('planName','Title')</th>
                            <th>@sortablelink('price','Price')</th>
                            <th>@sortablelink('cityName','City')</th>
                            <th>@sortablelink('duration','Duration [In Days]')</th>
                            <th>@sortablelink('favLimit','Favourite Property Limit')</th>
                            <th>@sortablelink('tier','Tier')</th>
                            <th>@sortablelink('created_at','Added Date')</th>
                            <th></th>
                        </thead>
                        <tbody>
                            @foreach ($records as $indexKey => $record)
                            <tr>
								<td><input class="plan" name="plans[]" type="checkbox" value="{{ $record->planID }}" /></td>
                                <td>{{(($records->currentpage()-1)*$records->perpage())+$indexKey+1}}</td>
                                <td>{{$record->planName}}</td>
                                <td>{{$record->price}}</td>
                                <td>{{$record->cityName}}</td>
                                <td>{{$record->duration}}</td>
                                <td>{{$record->favLimit}}</td>
                                <td>{{$record->tier or '-'}}</td>
                                <td>
									@if($record->created_at)
										<span>
										{{
											date('d-m-Y', strtotime($record->created_at))}}
										</span>
									@else
										<span>-</span>
									@endif
								</td>
                                <td>
                                    <div class="d-flex justify-content-center">
                                        @if($record->status == 'Active')
                                            <div class="btn-group" role="group" aria-label="">
                                                <div class="btn btn-outline-success btn-sm rounded-0 border-right-0" data-toggle="tooltip" data-placement="top" title="Active" onclick="activatePlan('{{base64_encode($record->planID)}}','0')">
                                                <i class="fas fa-lg fa-check"></i>
                                                </div>
                                            </div>
                                        @else
                                            <div class="btn-group" role="group" aria-label="">
                                                <div class="btn btn-outline-danger btn-sm rounded-0 border-right-0 disabled" data-toggle="tooltip" data-placement="top" title="Inactive" onclick="activatePlan('{{base64_encode($record->planID)}}','1')">
                                                <i class="fas fa-lg fa-times"></i>
                                                </div>
                                            </div>
                                        @endif
                                        @if(SystemHelper::is_module_permission('Plan','Edit')||SystemHelper::is_module_permission('Plan','Delete'))
                                            <div class="btn-group d-flex justify-content-center" role="group" aria-label="">
                                                @if(SystemHelper::is_module_permission('Plan','Edit'))
                                                <a class="btn btn-outline-info btn-sm rounded-0 border-left-0 border-right-0" href="{{ url('admin/plan/edit/'.base64_encode($record->planID))}}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-lg fa-edit"></i></a>
                                                @endif
                                                @if(SystemHelper::is_module_permission('Plan','Delete'))
                                                <a class="btn btn-outline-danger btn-sm rounded-0 border-left-0" href="javascript:void(0)" onclick="deleteRecord('{{base64_encode($record->planID)}}')" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fas fa-lg fa-trash-alt"></i></a>
                                                @endif
                                            </div>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {!! $records->appends(\Request::except('page'))->render() !!}
            </div>
            </div>
			</div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{!!asset('js/backend/plan.js')!!}"></script>
<script>
    var type = '{{Request::segment(2)}}';
    $('[data-toggle="tooltip"]').tooltip();
	
	$("#planCheckAll, #planBottomCheckAll").click(function () {
		$(".plan").attr('checked', this.checked);
	});
	
	$('.plan').click(function(){
		if($(".plan").length == $(".plan:checked").length) {
			$("#planCheckAll").prop("checked", true);
			$("#planBottomCheckAll").prop("checked", true);
		}else {
			$("#planCheckAll").prop("checked", false);            
			$("#planBottomCheckAll").prop("checked", false);            
		}
	});
	
	
</script>
@endsection