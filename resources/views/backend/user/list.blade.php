@extends('layouts.backend.app')
@section('title')
{{ $page_title}}
@stop
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card rounded-0 mb-3">
                <div class="card-body" style="padding-bottom:0px;">	
                    <div class="row">
                        <div class="col col-xs-12 col-lg-6 col-sm-6 col-md-6 col-xl-6">
                            <h1 class="greenroom-title mt-2"><i class="fas fa-user-plus"></i> {{Request::segment(2)}}</h1>
                        </div>
                        <div class="col col-xs-12 col-lg-6 col-sm-6 col-md-6 col-xl-6">
                            @if(SystemHelper::is_module_permission('User','Add'))
                                <div class="d-flex justify-content-end">
                                    <a href="{{ route('admin_add_user') }}" class="btn btn-primary btn-md shadow">
                                        <i class="fas fa-plus-circle"></i> Add New {{Request::segment(2)}}
                                    </a>
									&nbsp;
									<button class="btn btn-danger btn-md shadow" onClick="performAll()">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </div>
                            @endif
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        @if (Session::has('message'))
                            <div class="col-12">
                                <div class="alert alert-danger {{ Session::get('msgclass') }} alert-dismissable">{{ Session::get('message') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="table-responsive-sm card-body" style="padding-top:0px;">
					<div class="card rounded-0">
							<form class="well-filter" name="frmlist" id="frmlist" novalidate>
									<div class="m-3">
										<div class="col-md-2">
											<div class="form-group">
												{!! Form::select('role', [null=>"Select Role"] + $userRoleDropDown, null, ['class' => 'form-control', 'id' => 'role']) !!}
											</div>
										</div>
										
										<div class="col-md-2">
											<div class="form-group">
												<input type="text" id="name"  required="required" name="name" value="{{ Request::input('name') }}" placeHolder="Name" class="form-control"/>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<input type="text" id="mobile"  required="required" name="mobile" value="{{ Request::input('mobile') }}" placeHolder="Mobile" class="form-control"/>
											</div>
										</div>
										<!--
										<div class="col-md-3">
											<div class="form-grp">
												<input type="text" id="email" required="required" name="email" value="{{ Request::input('email') }}">
												{!! Form::label('email', 'Search by Email', array('class'=>'control-label')) !!}<i class="bar"></i>
											</div>
										</div>-->
										<div class="col-md-2">
											<div class="form-group">
												{!! Form::select('status', [null=>"Select Status"] + $userStatus, '', ['class' => 'form-control', 'id' => 'status']) !!}
											</div>
										</div>
										<!--<div class="col-md-2">
											<div class="form-group">
												{!! Form::select('action', [null=>"Bulk Action"] + $userStatus, null, ['class' => 'form-control', 'id' => 'action', 'onchange'=>'performAll()' ]) !!}
											</div>
										</div>-->
										<div class="col-md-1 form-group">
											<button type="submit" class="btn btn-success form-control"><i class="fas fa-search-plus"></i> </button>
										</div>
										<div class="col-md-1 form-group">
											<a href='{{ route("admin_user") }}'class="btn btn-primary form-control"><i class="fas fa-sync" aria-hidden="true"></i></a>
										</div>
									</div>
							</form>
                                
						</div>
					<form name="frmuserlist" id="frmuserlist" method="post">	
                    <table class="display table table-hover table-bordered table-striped w-full dtr-inline" id="data-user" border="1">
                        <thead class="thead-light">
                            <!-- <tr>
                                <th>#</th>
                                <th>@sortablelink('name','Name')</th>
                                <th>@sortablelink('email','Email')</th>
                                <th>@sortablelink('role','Role')</th>
                                <th>@sortablelink('premiumUser', 'Premium User')</th>
                                <th>@sortablelink('remainingFavCount', 'Remaining Fav Count')</th>
                                <th>@sortablelink('totalFavCount', 'Total Fav Count')</th>
                                <th>@sortablelink('membershipExpiry', 'Expire Date')</th>
                                <th></th>
                            </tr> -->
                            <tr>
                                <th class="no"><input id="userCheckAll" type="checkbox" /></th>
                                <th>#</th>
                                <th>User Code</th>
                                <th>Joining Date</th>
                                <th>Name</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th>Role</th>
                                <!--<th>Premium User</th>
                                <th>Remaining Fav Count</th>
                                <th>Total Fav Count</th>-->
                                <th>Expire Date</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($records as $indexKey => $record)
                                <tr>
                                    <td><input class="user" name="users[]" type="checkbox" value="{{ $record->id }}" /></td>
                                    <td>{{(($records->currentpage()-1)*$records->perpage())+$indexKey+1}}</td>
                                    <td>{{ $record->user_code }}</td>
                                    <td>{{ date('d-m-Y', strtotime($record->created_at)) }}</td>
                                    <td>{{ $record->firstName }} {{ $record->lastName }}</td>
                                    <td>{{ $record->mobile }}</td>
                                    <td>{{ $record->email }}</td>
                                    <td>{{ $record->role }}</td>
                                    <!--<td>@if($record->premiumUser == 1) Yes @else No @endif</td>
                                    <td>{{ $record->remainingFavCount or '0' }}</td>
                                    <td>{{ $record->totalFavCount or '0' }}</td>-->
                                    <td>
									
										@if($record->membershipExpiry)
											<span>
											{{
												date('d-m-Y', strtotime($record->membershipExpiry))}}
											</span>
										@else
											<span>-</span>
										@endif
									
									
									</td>
                                    @if(SystemHelper::is_module_permission('User','Edit') || SystemHelper::is_module_permission('User','Delete'))
                                        <td>
                                            <div class="d-flex justify-content-center">
                                            @if($record->status == 'Active')
                                                <div class="btn-group" role="group" aria-label="">
                                                    <div class="btn btn-outline-success btn-sm rounded-0 border-right-0" data-toggle="tooltip" data-placement="top" title="Make Inactive" onclick="activateRecord('{{base64_encode($record->id)}}','0')">
                                                    <i class="fas fa-lg fa-check"></i>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="btn-group" role="group" aria-label="">
                                                    <div class="btn btn-outline-danger btn-sm rounded-0 border-right-0 disabled" data-toggle="tooltip" data-placement="top" title="Make Active" onclick="activateRecord('{{base64_encode($record->id)}}','1')">
                                                    <i class="fas fa-lg fa-times"></i>
                                                    </div>
                                                </div>
                                            @endif
                                            @if(SystemHelper::is_module_permission('User','Edit')||SystemHelper::is_module_permission('User','Delete'))
                                                <div class="btn-group d-flex justify-content-center" role="group" aria-label="">
                                                    @if(SystemHelper::is_module_permission('User','Edit'))
                                                        <a class="btn btn-outline-info btn-sm rounded-0 border-left-0 border-right-0" href="{{ url('admin/user/edit/'.base64_encode($record->id))}}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-lg fa-edit"></i></a>
                                                    @endif
                                                    @if(SystemHelper::is_module_permission('User','Delete'))
														@if($record->status == 'Delete')
															<a class="btn btn-outline-danger btn-sm rounded-0 border-left-0" href="javascript:void(0)" onclick="activateRecord('{{base64_encode($record->id)}}','1')" data-toggle="tooltip" data-placement="top" title="Restore"><i class="fas fa-lg fa-undo"></i></a>
														@else
															<a class="btn btn-outline-danger btn-sm rounded-0 border-left-0" href="javascript:void(0)" onclick="deleteRecord('{{base64_encode($record->id)}}')" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fas fa-lg fa-trash-alt"></i></a>
														@endif
                                                    @endif
                                                    
													{{-- 	@if(SystemHelper::is_module_permission('User','Edit') && !empty($record->premiumUser == 1) && !empty($record->planCityID))
                                                    <a class="btn btn-outline-success btn-sm rounded-0 border-left-0" href="javascript:void(0)" onclick="planDetailsByUserID('{{ base64_encode($record->id) }}')" data-toggle="tooltip" data-placement="top" title="Upgrade"><i class="fas fa-sync"></i></a>
                                                    @endif --}}
                                                </div>
                                            @endif
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
						<!--<tfoot class="thead-light">
                            <tr>
                                <th class="no"><input id="userBottomCheckAll" type="checkbox" /></th>
                                <th>#</th>
                                <th>User Code</th>
                                <th>Joining Date</th>
                                <th>Name</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Premium User</th>
                                <th>Remaining Fav Count</th>
                                <th>Total Fav Count</th>
                                <th>Expire Date</th>
                                <th></th>
                            </tr>
                        </tfoot>-->
                    </table>
					</form>
					</div>
                </div>
                <!-- {!! $records->appends(\Request::except('page'))->render() !!} -->
			</div>
		</div>
	</div>
</div>
    <!-- Modal For Upgrade user favCount -->
    <input type="hidden" id="hiddenModel" data-toggle="modal" data-target="#myModalUpgrade">
    <div class="modal fade adminpopup" id="myModalUpgrade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">User Plan Upgrade</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="modelMessage" class="alert alert-danger" style="display: none"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table tabledesign">
                                    <tbody>
                                        <tr>
                                            <td><b>Name</b></td>
                                            <td><span id="userName"></span></td>
                                        </tr>
                                        <tr>
                                            <td><b>Plan City</b></td>
                                            <td><span id="cityName"></span></td>
                                        </tr>
                                        <tr>
                                            <td><b>Plan Expire On</b></td>
                                            <td><span id="membershipExpiry"></span></td>
                                        </tr>
                                        <tr>
                                            <td><b>Remaining Favourite Count</b></b></td>
                                            <td><span id="remainingFavCount"></span></td>
                                        </tr>
                                        <tr>
                                            <td><b>Total Favourite Count</b></td>
                                            <td><span id="totalFavCount"></span></td>
                                        </tr>
                                        <tr>
                                            <td><b>Upgrade Favourite Count</b></td>
                                            <td>
                                                {!! Form::hidden('userID', null, ['id'=>'userID']) !!}
                                                <input type="number" name="upgradeValue" id="upgradeValue">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button type="button" onclick="upgradeValue()" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>-->
<script type="text/javascript"src="{!!asset('js/backend/user.js')!!}"></script>
<script>
    var type = '{{ Request::segment(2) }}';
    $('[data-toggle="tooltip"]').tooltip();
    //$('#data-user').DataTable();
	
	$("#userCheckAll, #userBottomCheckAll").click(function () {
		$(".user").attr('checked', this.checked);
	});
	
	$('.user').click(function(){
		if($(".user").length == $(".user:checked").length) {
			$("#userCheckAll").prop("checked", true);
			$("#userBottomCheckAll").prop("checked", true);
		}else {
			$("#userCheckAll").prop("checked", false);            
			$("#userBottomCheckAll").prop("checked", false);            
		}
	});
	
	
</script>
@endsection