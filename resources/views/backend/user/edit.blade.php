@extends('layouts.backend.app')
@section('title')
{{ $page_title}}
@stop
@section('content')
<script type="text/javascript" src="{!! asset('js/jquery.validate.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/backend/user.js') !!}"></script>
<div class="container-fluid myprofile">

	<div class="row">
			<div class="col-12">
		
			<div class="card rounded-0 mb-3">
			<div class="card-body">
	
	<div class="row">
		<h1 class="greenroom-title mt-2 ml-3">
			<i class="fas fa-user"></i>
			Edit User
		</h1>
	</div>
	<hr/>	
    
    <div class="add_form_section">
        @if (Session::has('message'))
        <div class="alert {{ Session::get('msgclass') }} alert-dismissable">{{ Session::get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        </div>
        @endif
	</div>
	
        <div class="">
            <form name="frmuser" id="frmuser" method="POST" action="{{ route('admin_update_user')}}" novalidate="" class="form-horizontal">
                {{ csrf_field() }}
                {!! Form::hidden('id', base64_encode($data->id), ['id'=>'id']) !!}
                {!! Form::hidden('mode',"edit", ['id'=>'mode']) !!}
                <div class="shadowbox">
                    <div class="col-sm-6">
                        {{--<div class="row">
                            <div class="col-sm-4 col-md-3">
                                <label class="control-label">User Name</label>
                            </div>
                            <div class="col-sm-8 col-md-9">
                                <div class="form-group">
                                    {{Form::text('name',$data->name,['id'=>'name','required','class'=>'form-control'])}}
                                    @if($errors->has('label_name'))
                                    <div class="error">{{ $errors->first('label_name') }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>--}}
                        <div class="row">
                            <div class="col-sm-4 col-md-3">
                                <label class="control-label">Name</label>
                            </div>
                            <div class="col-sm-8 col-md-9">
                                <div class="form-group">
                                    {{Form::text('firstName',$data->firstName,['id'=>'firstName','required','class'=>'form-control'])}}
                                    @if($errors->has('label_firstName'))
                                    <div class="error">{{ $errors->first('label_firstName') }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
						{{--
                        <div class="row">
                            <div class="col-sm-4 col-md-3">
                               <label class="control-label">Last Name</label>
                            </div>
                            <div class="col-sm-8 col-md-9">
                                <div class="form-group">
                                    {{Form::text('lastName',$data->lastName,['id'=>'lastName','required','class'=>'form-control'])}}
                                    @if($errors->has('label_lastName'))
                                    <div class="error">{{ $errors->first('label_lastName') }}</div>
                                    @endif
                                </div>
                             </div>
                        </div>--}}
						
						<div class="row">
                            <div class="col-sm-4 col-md-3">
                                <label class="control-label">Contact No.</label>
                            </div>
                            <div class="col-sm-8 col-md-9">
                                <div class="form-group">
                                    {{Form::tel('mobile',$data->mobile,['id'=>'mobile','required','class'=>'form-control','placeHolder'=>'9999999999','maxLength'=>10])}}
                                    @if($errors->has('label_mobile'))
                                    <div class="error">{{ $errors->first('label_mobile') }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
						
						<div class="row">
                            <div class="col-sm-4 col-md-3">
                                <label class="control-label">Password</label>
                            </div>
                            <div class="col-sm-8 col-md-9 pl-0">
                                <div class="form-group col-sm-5 col-md-5">
                                    {{Form::text('password',null,['id'=>'password','class'=>'form-control'])}}
										{{--<button type="button" onClick="pass_generate();" class="btn btn-default">Password Generator</button> --}}
                                    @if($errors->has('label_password'))
                                    <div class="error">{{ $errors->first('label_password') }}</div>
                                    @endif
                                </div>
								<div class="col-sm-4 col-md-4">
                                    <button type="button" onClick="show_password(this);" class="btn btn-default">Show</button>
                                </div>
                            </div>
                        </div>
						
                        <div class="row">
                            <div class="col-sm-4 col-md-3">
                                <label class="control-label">Email</label>
                            </div>
                            <div class="col-sm-8 col-md-9">
                                <div class="form-group">
                                    {{Form::email('email',$data->email,['id'=>'email','required','class'=>'form-control'])}}
                                    @if($errors->has('label_email'))
                                    <div class="error">{{ $errors->first('label_email') }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        
                    
                        
                        {{--<div class="row">
                            <div class="col-sm-4 col-md-3">
                                <label class="control-label">Phone</label>
                            </div>
                            <div class="col-sm-8 col-md-9">
                                <div class="form-group">
                                    {{Form::number('phone',$data->phone,['id'=>'phone','class'=>'form-control'])}}
                                    @if($errors->has('label_phone'))
                                    <div class="error">{{ $errors->first('label_phone') }}</div>
                                    @endif
                                </div>
                            </div>
                        </div> --}}
						
                        <div class="row">
                            <div class="col-sm-4 col-md-3">
                                <label class="control-label">Role</label>
                            </div>
                            <div class="col-sm-8 col-md-9">
                                <div class="form-group">
                                    {!! Form::select('role', [null=>"Select Role"] + $userRoleDropDown, $data->roleID, ['class' => 'form-control', 'id' => 'role']) !!}
                                    @if($errors->has('label_role'))
                                    <div class="error">{{ $errors->first('label_role') }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                       
						
						<div class="row">
                            <div class="col-sm-4 col-md-3">
                                <label class="control-label">Expiry Date</label>
                            </div>
                            <div class="col-sm-8 col-md-9">
                                <div class="form-group">
									@if($data->membershipExpiry)
										{{Form::text('membershipExpiry',date('d-m-Y', strtotime($data->membershipExpiry)),['id'=>'membershipExpiry','class'=>'form-control', 'placeHolder'=>'DD-MM-YYYY'])}}
										@if($errors->has('label_expiry_date'))
										<div class="error">{{ $errors->first('label_expiry_date') }}</div>
										@endif
									@else												{{Form::text('membershipExpiry',$data->membershipExpiry,['id'=>'membershipExpiry','class'=>'form-control', 'placeHolder'=>'DD-MM-YYYY'])}}
										@if($errors->has('label_expiry_date'))
										<div class="error">{{ $errors->first('label_expiry_date') }}</div>
										@endif										
									@endif
                                </div>
                            </div>
                        </div>
						
						<div class="row">
                            <div class="col-sm-4 col-md-3">
                                <label class="control-label">Remarks</label>
                            </div>
                            <div class="col-sm-8 col-md-9">
                                <div class="form-group">                                    {{Form::textarea('remarks',$data->remarks,['id'=>'remarks','class'=>'form-control', 'placeHolder'=>'','rows'=>'5'])}}
                                </div>
                            </div>
                        </div>
						
						 <div class="row">
                            <div class="col-sm-4 col-md-3">
                                <label class="control-label">Status</label>
                            </div>
                            <div class="col-sm-8 col-md-9">
                                <div class="form-group">
                                    {{Form::select('status', ['Active'=>'Active','Inactive'=>'Inactive'],$data->status,['id'=>'status','required','class'=>'form-control'])}}
                                    @if($errors->has('label_status'))
                                    <div class="error">{{ $errors->first('label_status') }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
						
                        <div class="row radiorow">
                            <div class="col-sm-4 col-md-3">
                                <label class="control-label">Gender</label>
                            </div>
                            <div class="col-sm-8 col-md-9">
                                <div class="form-group">
                                        {!! Form::radio('gender', 'Male', ($data->gender=='Male') ? 'true' : '' ,['id' => 'male']) !!}
                                        {!! Form::label('male', 'Male') !!}
                                    
                                        {!! Form::radio('gender', 'Female', ($data->gender=='Female') ? 'true' : '',['id' => 'female']) !!}
                                        {!! Form::label('female', 'Female') !!}
                                </div>
                            </div>
                        </div>
                    
					</div>
                </div>
                <div class="col-sm-12">
                    <div class="form-actions float-right">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                        <a href="{{ route('admin_user')}}">
                            <button type="button" class="btn btn-danger">Cancel</button>
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    <!--./row-->
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">  
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="{!! asset('js/bootstrap-multiselect.js') !!}"></script>
<script type="text/javascript">
$(function(){
	$("#membershipExpiry").datepicker({ minDate: 0, dateFormat: 'dd-mm-yy' });
});
</script>
@endsection