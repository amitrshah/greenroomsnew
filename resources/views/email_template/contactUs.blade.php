<!DOCTYPE html>
<html lang="">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <title>{{ config('app.name', 'Greenroom') }}</title>
      <link href="http://fonts.googleapis.com/css?family=Raleway:400,900,600" rel="stylesheet" type="text/css">
      <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
   </head>
   <body>
      <table align="center" style="margin:20px 0; padding:0; font-size:18px; color:#686868; width:100%; border:0; border-spacing:0;" cellspacing="0" cellpadding="0">
         <tr>
            <td align="center">
               <table style="width:600px; margin:0 auto; border:1px solid #f1f1f1; padding:0; border-spacing:0;" cellspacing="0" cellpadding="0">
                  <tr>
                     <td align="center" style="padding:20px 0; text-align:center; background:#f1f1f1">
                        
                     </td>
                  </tr>
                  <tr>
                     <td style="padding:20px 20px 0 20px;">
                        <p style="font-size:15px; color:#686868; padding:0 0 15px 0; margin:0;"> Hi {{ $to_name }},</p>
                     </td>
                  </tr>
                  <tr>
                     <td style="padding:20px 20px 0 20px">
                        <p style="font-size:15px; color:#686868; padding:0 0 15px 0; margin:0;"><pre>{{ $message }}</pre></p>
                     </td>
                  </tr>
				  <tr>
                     <td style="padding:20px 20px 0 20px">
                        <p style="font-size:15px; color:#686868; padding:0 0 15px 0; margin:0;"><pre>{{ $name }}</pre></p>
                     </td>
                  </tr>
				  <tr>
                     <td style="padding:20px 20px 0 20px">
                        <p style="font-size:15px; color:#686868; padding:0 0 15px 0; margin:0;"><pre>{{ $email }}</pre></p>
                     </td>
                  </tr>
                 
                  <tr>
                     <td style="padding:0px 20px 30px 20px">
                        <p style="font-size:15px; color:#686868; padding:0 0 15px 0; margin:0;">                                
                           <span style="color:#001c68; font-weight:500; display:block;">{!! env('MAIL_FOOTER') !!}</span>
                        </p>
                     </td>
                  </tr>
                  <tr>
                     <td style="text-align:center; background:#f1f1f1; padding:10px 0;">
                        <p style="font-size:13px; color:#686868; margin:0;">
                           Powered by {!! env('APP_NAME') !!}
                        </p>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
   </body>
</html>