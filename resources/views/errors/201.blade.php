@extends('layouts.backend.app')

@section('content')
        <div class="container alert alert-danger">
            <div class="content">
                <div class="title">You have not authorized to access this page.</div>
            </div>
        </div>
@endsection