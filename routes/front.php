<?php
/**
 * Created by PhpStorm.
 * User: developer2
 * Date: 26/6/19
 * Time: 9:51 AM
 */

//ADD BY CT
Route::get('/', 'frontend\HomeController@index')->name('user_home');
Route::get('dashboard-owner', 'frontend\PageController@dashboardOwner')->name('dashboard_owner');
Route::get('owner-profile', 'frontend\PageController@OwnerProfile')->name('owner_profile');
Route::get('about-us', 'frontend\PageController@aboutUs')->name('about_us');
Route::get('about-us-green-room', 'frontend\PageController@aboutUsGreenRoom')->name('about_us_green_room');
Route::get('post-your-property/{id?}', 'frontend\PageController@postYourProperty')->name('post_your_property');
Route::get('search', 'frontend\PageController@search')->name('search');
Route::post('signup', 'frontend\HomeController@signUp')->name('do_sign_up');
//Route::get('search', 'frontend\PageController@search')->name('search');
Route::post('search-content', 'frontend\PageController@searchContent')->name('search_content');
Route::get('activate-user/{code}', 'frontend\HomeController@activateUser')->name('activate_user');
Route::POST('do-login', 'frontend\HomeController@doLogin')->name('do_login');
Route::get('front-logout', 'frontend\HomeController@logout')->name('front_logout');
Route::get('search-details/{id}', 'frontend\SearchController@searchDetails')->name('search_details');
Route::post('update-owner-profile', 'frontend\HomeController@updateOwnerProfile')->name('update_owner_profile');
Route::get('property-list/{type?}', 'frontend\HomeController@propertyList')->name('property_list');
Route::post('add-property', 'frontend\HomeController@addProperty')->name('add_property');
Route::post('delete_your_property', 'frontend\HomeController@deleteProperty')->name('delete_your_property');
