<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('optimize');
    $exitCode = Artisan::call('cache:clear');
//    $exitCode = Artisan::call('route:cache');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('config:clear');
});
/*Route::get('/', function () {
    return view('welcome');
});*/


//ADD BY CT
@include 'front.php';

//Route::get('/', 'frontend\HomeController@index')->name('user_home');
//Route::get('/search', 'frontend\SearchController@index')->name('search');
//Route::post('/search', 'frontend\SearchController@index')->name('search');
Route::post('/searchajax', 'frontend\SearchController@index')->name('searchajax');
Route::post('/search/property/sidebar', 'frontend\SearchController@propertySidebar');

Route::get('/property/{slug?}', 'frontend\SearchController@propertyDetails')->name('property_details');
//Route::get('/detail/{propertyID?}', 'frontend\SearchController@propertyDetail')->name('property_detail');

Route::get('/password/reset', 'frontend\Auth\ForgotPasswordController@showLinkRequestForm')->name('user_password.request');
Route::post('password/reset', 'frontend\Auth\ResetPasswordController@reset')->name('user_password.request');
Route::get('password/reset/{token}', 'frontend\Auth\ResetPasswordController@showResetForm')->name('user_password.reset');
Route::post('password/email', 'frontend\Auth\ForgotPasswordController@sendResetLinkEmail')->name('user_password.email');

Route::get('/login', 'frontend\Auth\LoginController@showLoginForm')->name('user_login');
Route::post('/authenticate', 'frontend\Auth\LoginController@authenticate')->name('user_authenticate');

Route::get('register', 'frontend\Auth\RegisterController@showRegistrationForm')->name('user_register');
Route::post('register', 'frontend\Auth\RegisterController@register')->name('user_create');
//Route::post('register', 'frontend\Auth\RegisterController@register')->name('user_create');
Route::get('ajax-city', 'AjaxController@getCity');
Route::get('ajax-area', 'AjaxController@getArea');
//Route::get('about-us', 'frontend\PageController@aboutUs')->name('about-us');
Route::get('how-it-works', 'frontend\PageController@howItWorks')->name('how-it-works');
Route::get('refer-and-earn', 'frontend\PageController@referAndEarn')->name('refer-and-earn');
Route::get('owners', 'frontend\PageController@owners')->name('owners');
Route::get('privacy-policy', 'frontend\PageController@privacyPolicy')->name('privacy-policy');
Route::post('contact-us', 'frontend\PageController@storeContactUs')->name('contact-us');
Route::get('contact-us', 'frontend\PageController@contactUs')->name('contact-us');
Route::get('faqs', 'frontend\PageController@faqs')->name('faqs');
Route::get('services', 'frontend\PageController@services')->name('services');
Route::get('terms', 'frontend\PageController@terms')->name('terms');

Route::get('affiliate', 'frontend\PageController@affiliate')->name('affiliate');
Route::get('complaint', 'frontend\PageController@complaint')->name('complaint');
Route::get('tenants', 'frontend\PageController@tenants')->name('tenants');
Route::get('testimonial', 'frontend\PageController@testimonial')->name('testimonial');
Route::get('owners-benefits', 'frontend\PageController@ownersBenefits')->name('owners-benefits');
Route::get('tenants-benefits', 'frontend\PageController@tenantsBenefits')->name('tenants-benefits');
Route::get('why-choose-us', 'frontend\PageController@whyChooseUs')->name('why-choose-us');
Route::get('corporate', 'frontend\PageController@corporate')->name('corporate');
Route::get('careers', 'frontend\PageController@careers')->name('careers');



//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin'], function($admin) {

	$admin->get('/dashboard', ['middleware' => ['admin','user.access.role'],'uses'=> 'backend\DashboardController@index'])->name('admin_home');

	$admin->get('/', 'backend\Auth\LoginController@showLoginForm')->name('admin_login');
	$admin->get('/login', 'backend\Auth\LoginController@showLoginForm')->name('admin_login');
	$admin->post('/authenticate', 'backend\Auth\LoginController@authenticate')->name('admin_authenticate');
	$admin->post('logout', ['middleware' => ['admin','user.access.role'],'uses'=> 'backend\Auth\LoginController@logout'])->name('admin_logout');

    $admin->get('/password/reset', 'backend\Auth\ForgotPasswordController@showLinkRequestForm')->name('admin_password.request');
	$admin->post('password/email', 'backend\Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin_password.email');
	$admin->get('password/reset/{token}', 'backend\Auth\ResetPasswordController@showResetForm')->name('admin_password.reset');
	$admin->post('password/reset', 'backend\Auth\ResetPasswordController@reset')->name('admin_password.request');


    /* User */
    $admin->get('user',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\UserController@index'])->name("admin_user");
    $admin->get('user/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\UserController@create'])->name("admin_add_user");
    $admin->post('user/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\UserController@store'])->name("admin_add_user");
    $admin->get('user/edit/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\UserController@edit'])->name("admin_edit_user");
    $admin->post('user/update',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\UserController@update'])->name("admin_update_user");
    $admin->get('user/update/{id?}&{status?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\UserController@updateStatus'])->name("admin_update_user");
    $admin->post('user/update/{action?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\UserController@bulkUpdate'])->name("admin_update_user");
    $admin->get('user/delete/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\UserController@destroy'])->name("admin_remove_user");
    $admin->get('user/edit/plan/upgrade/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\UserController@planDetailsByUserID'])->name("admin_upgrade_plan");
    $admin->post('user/edit/plan/upgrade/update',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\UserController@upgradeValue'])->name("admin_upgrade_plan");
    /* End User */

    /* Property */

    $admin->get('property',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\PropertyController@index'])->name("admin_property");
    $admin->get('property/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\PropertyController@create'])->name("admin_add_property");
    $admin->post('property/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\PropertyController@store'])->name("admin_add_property");
    $admin->get('property/edit/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\PropertyController@edit'])->name("admin_edit_property");
    $admin->post('property/update',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\PropertyController@update'])->name("admin_update_property");
    $admin->get('property/update/{id?}&{status?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\PropertyController@updateStatus'])->name("admin_update_property");
    $admin->post('property/update/{action?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\PropertyController@bulkUpdate'])->name("admin_update_property");
    $admin->get('property/delete/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\PropertyController@destroy'])->name("admin_remove_property");

    $admin->get('property/getallstate',  ['middleware' => ['admin'], 'uses' => 'backend\PropertyController@getAllState'])->name("admin_property_state");
    $admin->get('property/getallcity/{stateID?}',  ['middleware' => ['admin'], 'uses' => 'backend\PropertyController@getAllCity'])->name("admin_property_city");
    $admin->get('property/getallarea/{stateID?}/{cityID?}',  ['middleware' => ['admin'], 'uses' => 'backend\PropertyController@getAllArea'])->name("admin_property_area");
    $admin->get('property/deleteImage/{delid?}',  ['middleware' => ['admin'], 'uses' => 'backend\PropertyController@deleteImage'])->name("admin_property_area");
    /* End Property */

    /* Room Sharing */
    $admin->get('roomsharing',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\RoomSharingController@index'])->name("admin_roomsharing");
    $admin->get('roomsharing/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\RoomSharingController@create'])->name("admin_add_roomsharing");
    $admin->post('roomsharing/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\RoomSharingController@store'])->name("admin_add_roomsharing");
    $admin->get('roomsharing/edit/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\RoomSharingController@edit'])->name("admin_edit_roomsharing");
    $admin->post('roomsharing/update',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\RoomSharingController@update'])->name("admin_edit_roomsharing");
    $admin->get('roomsharing/delete/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\RoomSharingController@destroy'])->name("admin_remove_roomsharing");
    /* End Room Sharing */

    /* Property Types */
    $admin->get('propertytype',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\PropertyTypeController@index'])->name("admin_propertytype");
    $admin->get('propertytype/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\PropertyTypeController@create'])->name("admin_add_propertytype");
    $admin->post('propertytype/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\PropertyTypeController@store'])->name("admin_add_propertytype");
    $admin->get('propertytype/edit/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\PropertyTypeController@edit'])->name("admin_edit_propertytype");
    $admin->post('propertytype/update',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\PropertyTypeController@update'])->name("admin_edit_propertytype");
    $admin->get('propertytype/delete/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\PropertyTypeController@destroy'])->name("admin_remove_propertytype");
    /* End Property Types */

    /* State */
    $admin->get('state',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\StateController@index'])->name("admin_state");
    $admin->get('state/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\StateController@create'])->name("admin_add_state");
    $admin->post('state/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\StateController@store'])->name("admin_add_state");
    $admin->get('state/edit/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\StateController@edit'])->name("admin_edit_state");
    $admin->post('state/update',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\StateController@update'])->name("admin_edit_state");
    $admin->get('state/delete/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\StateController@destroy'])->name("admin_remove_state");
    /* End State */

    /* city */
    $admin->get('city',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\CityController@index'])->name("admin_city");
    $admin->get('city/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\CityController@create'])->name("admin_add_city");
    $admin->post('city/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\CityController@store'])->name("admin_add_city");
    $admin->get('city/edit/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\CityController@edit'])->name("admin_edit_city");
    $admin->post('city/update',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\CityController@update'])->name("admin_edit_city");
    $admin->get('city/delete/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\CityController@destroy'])->name("admin_remove_city");

    /* End city */

    /* MemberPayment */
    $admin->get('memberpayment',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\MemberPaymentController@index'])->name("admin_memberpayment");
    $admin->get('memberpayment/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\MemberPaymentController@create'])->name("admin_add_memberpayment");
    $admin->post('memberpayment/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\MemberPaymentController@store'])->name("admin_add_memberpayment");
    $admin->get('memberpayment/edit/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\MemberPaymentController@edit'])->name("admin_edit_memberpayment");
    $admin->post('memberpayment/update',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\MemberPaymentController@update'])->name("admin_edit_memberpayment");
    $admin->get('memberpayment/delete/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\MemberPaymentController@destroy'])->name("admin_remove_memberpayment");
    $admin->post('memberpayment/update/{action?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\MemberPaymentController@bulkUpdate'])->name("admin_edit_memberpayment");
    /* End MemberPayment */

	/* MemberRequirement */
    $admin->get('memberrequirement',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\MemberRequirementController@index'])->name("admin_memberrequirement");
    $admin->get('memberrequirement/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\MemberRequirementController@create'])->name("admin_add_memberrequirement");
    $admin->post('memberrequirement/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\MemberRequirementController@store'])->name("admin_add_memberrequirement");
    $admin->get('memberrequirement/edit/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\MemberRequirementController@edit'])->name("admin_edit_memberrequirement");
    $admin->post('memberrequirement/update',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\MemberRequirementController@update'])->name("admin_edit_memberrequirement");
    $admin->get('memberrequirement/delete/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\MemberRequirementController@destroy'])->name("admin_remove_memberrequirement");
    $admin->post('memberrequirement/update/{action?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\MemberRequirementController@bulkUpdate'])->name("admin_edit_memberrequirement");
    /* End MemberRequirement */

    /* area */
    $admin->get('area',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\AreaController@index'])->name("admin_area");
    $admin->get('area/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\AreaController@create'])->name("admin_add_area");
    $admin->post('area/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\AreaController@store'])->name("admin_add_area");
    $admin->get('area/edit/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\AreaController@edit'])->name("admin_edit_area");
    $admin->post('area/update',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\AreaController@update'])->name("admin_edit_area");
    $admin->get('area/delete/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\AreaController@destroy'])->name("admin_remove_area");
    $admin->get('area/getallcity/{stateID}',  ['middleware' => ['admin'], 'uses' => 'backend\AreaController@getAllCity'])->name("admin_property_city");

    /* End area */

    /* User Role */
    $admin->get('userrole',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\UserRoleController@index'])->name("admin_userrole");
    $admin->get('userrole/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\UserRoleController@create'])->name("admin_add_userrole");
    $admin->post('userrole/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\UserRoleController@store'])->name("admin_add_userrole");
    $admin->get('userrole/edit/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\UserRoleController@edit'])->name("admin_edit_userrole");
    $admin->post('userrole/update',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\UserRoleController@update'])->name("admin_edit_userrole");
    $admin->get('userrole/delete/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\UserRoleController@destroy'])->name("admin_remove_userrole");

    $admin->get('userrole/manage/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\UserRoleController@managePermission'])->name("admin_manage_role_permission");
    $admin->post('userrole/manage_update',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\UserRoleController@updatePermission'])->name("admin_update_role_permission");
    /* End User Role */

    /* Plan */
    $admin->get('plan',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\PlanController@index'])->name("admin_plan");
    $admin->get('plan/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\PlanController@create'])->name("admin_add_plan");
    $admin->post('plan/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\PlanController@store'])->name("admin_add_plan");
    $admin->get('plan/edit/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\PlanController@edit'])->name("admin_edit_plan");
    $admin->post('plan/update',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\PlanController@update'])->name("admin_edit_plan");
    $admin->get('plan/update/{id?}&{status?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\PlanController@updateStatus'])->name("admin_update_plan");
    $admin->post('plan/update/{action?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\PlanController@bulkUpdate'])->name("admin_update_plan");
    $admin->get('plan/delete/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\PlanController@destroy'])->name("admin_remove_plan");
    /* End Plan */

    /* Facilities & Furniture */
    $admin->get('facilities',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\FacilitiesController@index'])->name("admin_facilities");
    $admin->get('facilities/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\FacilitiesController@create'])->name("admin_add_facilities");
    $admin->post('facilities/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\FacilitiesController@store'])->name("admin_add_facilities");
    $admin->get('facilities/edit/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\FacilitiesController@edit'])->name("admin_edit_facilities");
    $admin->post('facilities/update',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\FacilitiesController@update'])->name("admin_edit_facilities");
    $admin->get('facilities/delete/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\FacilitiesController@destroy'])->name("admin_remove_facilities");
    /* End Facilities & Furniture */

	/* interiors */
    $admin->get('interiors',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\InteriorsController@index'])->name("admin_interiors");
    $admin->get('interiors/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\InteriorsController@create'])->name("admin_add_interiors");
    $admin->post('interiors/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\InteriorsController@store'])->name("admin_add_interiors");
    $admin->get('interiors/edit/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\InteriorsController@edit'])->name("admin_edit_interiors");
    $admin->post('interiors/update',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\InteriorsController@update'])->name("admin_edit_interiors");
    $admin->get('interiors/delete/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\InteriorsController@destroy'])->name("admin_remove_interiors");
    /* End interiors */

    /* Rules */
    $admin->get('rules',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\RulesController@index'])->name("admin_rules");
    $admin->get('rules/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\RulesController@create'])->name("admin_add_rules");
    $admin->post('rules/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\RulesController@store'])->name("admin_add_rules");
    $admin->get('rules/edit/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\RulesController@edit'])->name("admin_edit_rules");
    $admin->post('rules/update',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\RulesController@update'])->name("admin_edit_rules");
    $admin->get('rules/delete/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\RulesController@destroy'])->name("admin_remove_rules");
    /* End Rules */

    /* Society Amenity */
    $admin->get('societyamenity',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\SocietyAmenityController@index'])->name("admin_societyamenity");
    $admin->get('societyamenity/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\SocietyAmenityController@create'])->name("admin_add_societyamenity");
    $admin->post('societyamenity/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\SocietyAmenityController@store'])->name("admin_add_societyamenity");
    $admin->get('societyamenity/edit/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\SocietyAmenityController@edit'])->name("admin_edit_societyamenity");
    $admin->post('societyamenity/update',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\SocietyAmenityController@update'])->name("admin_edit_societyamenity");
    $admin->get('societyamenity/delete/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\SocietyAmenityController@destroy'])->name("admin_remove_societyamenity");
    /* End Society Amenity */

    /* Furniture */
    $admin->get('furniture',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\FurnitureController@index'])->name("admin_furniture");
    $admin->get('furniture/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\FurnitureController@create'])->name("admin_add_furniture");
    $admin->post('furniture/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\FurnitureController@store'])->name("admin_add_furniture");
    $admin->get('furniture/edit/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\FurnitureController@edit'])->name("admin_edit_furniture");
    $admin->post('furniture/update',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\FurnitureController@update'])->name("admin_edit_furniture");
    $admin->get('furniture/delete/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\FurnitureController@destroy'])->name("admin_remove_furniture");
    /* End Furniture */

	/* availibility type */
    $admin->get('availibilitytype',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\AvailibilityTypeController@index'])->name("admin_availibilitytype");
    $admin->get('availibilitytype/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\AvailibilityTypeController@create'])->name("admin_add_availibilitytype");
    $admin->post('availibilitytype/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\AvailibilityTypeController@store'])->name("admin_add_availibilitytype");
    $admin->get('availibilitytype/edit/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\AvailibilityTypeController@edit'])->name("admin_edit_availibilitytype");
    $admin->post('availibilitytype/update',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\AvailibilityTypeController@update'])->name("admin_edit_availibilitytype");
    $admin->get('availibilitytype/delete/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\AvailibilityTypeController@destroy'])->name("admin_remove_availibilitytype");
    /* End availibility type */

	/* availability */
    $admin->get('availability',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\AvailabilityController@index'])->name("admin_availability");
    $admin->get('availability/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\AvailabilityController@create'])->name("admin_add_availability");
    $admin->post('availability/add',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\AvailabilityController@store'])->name("admin_add_availability");
    $admin->get('availability/edit/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\AvailabilityController@edit'])->name("admin_edit_availability");
    $admin->post('availability/update',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\AvailabilityController@update'])->name("admin_edit_availability");
    $admin->get('availability/delete/{id?}',  ['middleware' => ['admin','user.access.role'], 'uses' => 'backend\AvailabilityController@destroy'])->name("admin_remove_availability");
    /* End availability */


});

Route::group([], function($user) {
    $user->get('logout', ['middleware' => ['user'],'uses'=>'frontend\Auth\LoginController@logout'])->name('user_logout');
    //$user->post('logout', ['middleware' => ['user','user.access.role'],'uses'=> 'frontend\Auth\LoginController@logout'])->name('user_logout');
    $user->get('account/changepassword', ['middleware' => ['user'],'uses'=>'frontend\UserController@showChangePasswordForm'])->name('profile.changePassword');
    $user->post('account/changepassword', ['middleware' => ['user'],'uses'=>'frontend\UserController@changePassword'])->name('profile.changePassword');
    $user->get('account/profile', ['middleware' => ['user'],'uses'=>'frontend\UserController@profile'])->name('profile');
    $user->post('account/profile', ['middleware' => ['user'],'uses'=>'frontend\UserController@profileUpdate'])->name('profile');

    $user->get('plans', ['middleware' => ['user'],'uses'=>'frontend\PaymentController@plans'])->name('user_plans');
    $user->post('getplans', ['middleware' => ['user'],'uses'=>'frontend\PaymentController@getPlansByCityID']);
    $user->post('getplandetails', ['middleware' => ['user'],'uses'=>'frontend\PaymentController@getPlansDatailsByPlanID']);
    $user->post('order', ['middleware' => ['user'],'uses'=>'frontend\PaymentController@order'])->name('user_order');
    $user->post('paymentCallback', ['middleware' => ['user'],'uses'=>'frontend\PaymentController@paymentCallback'])->name('paymentCallback');
    $user->get('payment-history', ['middleware' => ['user'], 'uses'=>'frontend\PaymentController@paymentHistory'])->name('payment-history');

    $user->post('search/property/setfavourite', ['middleware' => ['user'],'uses'=>'frontend\SearchController@setFavourite']);
    $user->get('myfavourite', ['middleware' => ['user'], 'uses'=>'frontend\FavouriteController@index'])->name('myfavourite');
    $user->get('myfavourite/delete/{id?}',  ['middleware' => ['user'], 'uses' => 'frontend\FavouriteController@destroy'])->name("deleteFavourite");
    $user->get('shortlist', ['middleware' => ['user'], 'uses'=>'frontend\FavouriteController@shortlist'])->name('shortlist');
    $user->get('shortlist/removefromshortlist/{propery_id?}', ['middleware' => ['user'], 'uses'=>'frontend\FavouriteController@removefromshortlist'])->name('removefromshortlist');
    $user->get('shortlist/addPrimeMembership', ['middleware' => ['user'], 'uses'=>'frontend\FavouriteController@addPrimeMembership'])->name('addPrimeMembership');
    $user->get('list-your-property', ['middleware' => ['user'], 'uses'=>'frontend\FavouriteController@listProperty'])->name('list-your-property');
    $user->get('list-your-property', ['middleware' => ['user'], 'uses'=>'frontend\FavouriteController@listProperty'])->name('list-your-property');
    $user->post('addproperty', ['middleware' => ['user'], 'uses'=>'frontend\FavouriteController@addProperty'])->name('addproperty');
    $user->get('detail/{propertyID?}', ['middleware' => ['user'], 'uses'=>'frontend\SearchController@propertyDetail'])->name('property_detail');

    $user->post('search/property/getfavourite', ['middleware' => ['user'],'uses'=>'frontend\SearchController@getFavouritePropertyList']);

});

Route::get('images/property/{folder_id}/{filename}', function ($folder, $filename)
{
    //$file = url('/').DIRECTORY_SEPARATOR.'images/property/no_image.jpg';
    $path = storage_path('images/property') . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $filename;

    if(!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});


Route::get('images/property/{filename}', function ( $filename)
{
    //$file = url('/').DIRECTORY_SEPARATOR.'images/property/no_image.jpg';
    $path = storage_path('images/property') . DIRECTORY_SEPARATOR  . $filename;

    if(!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});