<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\backend\AvailibilityType;
use Validator;
use Session;

class AvailibilityTypeController extends Controller
{

    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $availibilityTypeObj = new AvailibilityType();

        $res_arr = $availibilityTypeObj->getRecords($availibilityTypeObj);
        //print_r($res_arr);exit;
        return view('backend.availibilitytype.list')->with(["page_title"=>"Availibility Type - ",'records' => $res_arr]);
        
    }

    /**
     * Show the room sharing add form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.availibilitytype.add')->with(["page_title"=>"Add Availibility Type - "]);
    }

    public function store(Request $request){
        try {
            
            $errors = array();
            $data = array(); 
            

            $rules = array(
                'title'          => 'required',
                'status'         =>'required'
            );

            $messages = [
              'title.required' => 'Please enter title.',
              'status.required' => 'Please select status.',
            ]; 

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){

                //print_r($validator->messages()->getMessages());exit;

                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                return redirect()->route('admin_add_availibilitytype')
                        ->withErrors($errors)
                        ->withInput();
            }
            if (empty($errors)) {
              
              /*$data['errors']  = $errors;

            } else {*/

                $availibilityTypeObj = new AvailibilityType();
                $id = $availibilityTypeObj->saveAvailibilityType($request);
                if($id > 0){

                    //$data['id'] = $id;
                    Session::flash('message', 'Record added successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->route('admin_availibilitytype');

                }
                else{
                    Session::flash('message', 'Record addition error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }
            //return response($data);
        } catch (Exception $e) {
            //echo $e->getMessages();
            Session::flash('message', 'Record addition error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_availibilitytype');
        }
        return redirect()->route('admin_availibilitytype');
    }

    public function edit($encoded_id="")
    {
        if(!empty($encoded_id))
        {
            $availibilityTypeObj = new AvailibilityType();
            $id = base64_decode($encoded_id);
            $data = $availibilityTypeObj->getRecordById($id);
            if(!empty($data)){
                return view('backend.availibilitytype.edit')->with(["page_title"=>"Edit Availibility Type - ",'data' => $data]);
            }else{
                Session::flash('message', 'Data does not exist.');
                Session::flash('msgclass', 'alert-danger');
                return redirect()->route('admin_availibilitytype');
            }
        }else{
            
            Session::flash('message', 'Url does not exist.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_availibilitytype');
        }
    }

    public function update(Request $request){
        try {
            $errors = array();
            $data = array(); 
            

            $rules = array(
                'title'          => 'required',
                'status'         =>'required'
            );

            $messages = [
              'title.required' => 'Please enter title.',
              'status.required' => 'Please select status.',
            ]; 

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){

                //print_r($validator->messages()->getMessages());exit;

                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                return redirect()->back()
                        ->withErrors($errors)
                        ->withInput();
            }
            if(empty($errors)) {
              
              /*$data['errors']  = $errors;

            } else {*/

                $availibilityTypeObj = new AvailibilityType();
                $result = $availibilityTypeObj->updateAvailibilityType($request);
                if($result){
                    Session::flash('message', 'Record updated successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->back();
                }
                else{
                    Session::flash('message', 'Record updation error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }
            //return response($data);
        } catch (Exception $e) {
            //echo $e->getMessages();
            Session::flash('message', 'Record updation error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->back();
        }   
    }

    public function destroy($encoded_id="")
    {
        /*if(!empty($encoded_id))
        {*/
            try {
                $availibilityTypeObj = new AvailibilityType();
                $id = base64_decode($encoded_id);

                $data['data'] = $availibilityTypeObj->getRecordById($id);
                if(!empty($data['data'])){
                    $data['result'] = $availibilityTypeObj->deleteRecordById($id);
                    if($data['result'] == 'true'){
                        Session::flash('message', 'Record is deleted successfully.');
                        Session::flash('msgclass', 'alert-success');
                        //$result['result'] = true;
                        //return response($result);
                        
                    }
                }else{
                    Session::flash('message', 'Record does not exist.');
                    Session::flash('msgclass', 'alert-danger');
                    //$result['result'] = false;
                    //return response($result);
                }
            } catch (Exception $e) {
                Session::flash('message', 'Record could not be deleted.');
                Session::flash('msgclass', 'alert-danger');
                //$result['result'] = false;
                //return response($result);
            }
            return redirect()->back();
            //$result['result'] = false;
            //return response($result);
        /*}else{
            
            Session::flash('message', 'Url does not exist.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_propertytype');
        }*/
    }
}
