<?php
namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\backend\User;
use App\Model\backend\UserRole;
use Validator;
use Session;
class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userObj = new User();
        $userRoleObj = new UserRole();
        $res_arr = $userObj->getRecords($userObj);
        $userRoleDropDown = $userRoleObj->getUserRoleDropDown()->toArray();
        $userStatus = Array('Active'=>'Active','Inactive'=>'Inactive','Delete'=>'Delete');
        //echo "<pre>";print_r($userRoleDropDown);exit;
        return view('backend.user.list')->with(["page_title"=>"User - ",'records' => $res_arr,'userRoleDropDown' => $userRoleDropDown, 'userStatus' => $userStatus]);
    }
    /**
     * Show the user add form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userRoleObj = new UserRole();
        $userRoleDropDown = $userRoleObj->getUserRoleDropDown()->toArray();
        return view('backend.user.add',["page_title"=>"Add User - ",'userRoleDropDown' => $userRoleDropDown]);
    }
    /* Save the user data.*/
    public function store(Request $request){
        try {
            $errors = array();
            $data = array(); 
            $rules = array(
                //'name'          => 'required',
                'firstName'          => 'required',
                //'lastName'          => 'required',
                'email'         => 'required|unique:users',
                'password'         => 'required|min:6',
                'role'        =>'required',
                'mobile'        =>'required|numeric|digits:10',
                //'phone'        =>'nullable|numeric|digits:10',
            );
            $messages = [
                'name.required' => 'Please Enter User Name.',
                'firstName.required' => 'Please Enter First Name.',
                'lastName.required' => 'Please Enter Last Name.',
                'email.required' => 'Please Enter Email.',
                'password.required' => 'Please Enter Password.',
                'password.min' => 'Please Enter Password Minimum 6 Characters.',
                'role.required' => 'Please Select Role.',
                'mobile.required' => 'Please Enter Mobile Number.',
                'mobile.numeric' => 'Please Enter A Valid Number.',
                'mobile.digits' => 'Please Enter A Value 10 digits Long.',
                'phone.number' => "Please Enter A Valid Number.",
                'phone.digits' => "Please Enter A Value 10 digits Long."
            ]; 
            $validator = Validator::make($request->all(), $rules, $messages);
            if($validator->fails()){
                //print_r($validator->messages()->getMessages());exit;
                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                    return redirect()->route('admin_add_user')
                        ->withErrors($errors)
                        ->withInput();
            }
            if (empty($errors)) {
              /*$data['errors']  = $errors;
            } else {*/
                $userObj = new User();
                $id = $userObj->saveUser($request);
                if($id > 0){
                    //$data['id'] = $id;
                    Session::flash('message', 'Record added successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->route('admin_user');
                }
                else{
                    Session::flash('message', 'Record addition error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }
            /*return response($data);*/
        } catch (Exception $e) {
            //echo $e->getMessages();
            Session::flash('message', 'Record addition error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_user');
        }
        return redirect()->route('admin_user');
    }
    /*Show the user edit  form.*/
    public function edit($encoded_id="")
    {
        if(!empty($encoded_id))
        {
            $userObj = new User();
            $id = base64_decode($encoded_id);
            $data['data'] = $userObj->getRecordById($id);
            if(!empty($data['data'])){
                $userRoleObj = new UserRole();
                $data['userRoleDropDown'] = $userRoleObj->getUserRoleDropDown()->toArray();
                return view('backend.user.edit')->with($data)->with(["page_title"=>"Edit User -"]);
            }else{
                Session::flash('message', 'Data does not exist.');
                Session::flash('msgclass', 'alert-danger');
                return redirect()->route('admin_user');
            }
        }else{
            Session::flash('message', 'Url does not exist.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_user');
        }
    }
    /* Update the user data */
    public function update(Request $request){
        try {
            $errors = array();
            $data = array(); 
            $password = ($request->input('password') != "") ? $request->input('password') : null;
            $rules = array(
                //'name'          => 'required|alpha',
                'firstName'          => 'required',
                //'lastName'          => 'required',
                //'email'         => 'required',
                'role'        =>'required',
                'mobile'        =>'required|numeric|digits:10',
                //'phone'        =>'nullable|numeric|digits:10',
            );
            $messages = [
                'name.required' => 'Please Enter User Name..',
                'firstName.required' => 'Please Enter First Name.',
                //'lastName.required' => 'Please Enter Last Name.',
                //'email.required' => 'Please Enter Email.',
                'role.required' => 'Please Select Role.',
                'mobile.required' => 'Please Enter Mobile Number.',
                'mobile.numeric' => 'Please Enter A Valid Number.',
                'mobile.digits' => 'Please Enter A Value 10 digits Long.',
                //'phone.number' => "Please Enter A Valid Number.",
                //'phone.digits' => "Please Enter A Value 10 digits Long."
            ];  
            if(!empty($password)){
                $rules['password'] = 'min:6';
                $messages['password.min'] = 'Please Enter Password Minimum 6 Characters.';
            }
            $validator = Validator::make($request->all(), $rules, $messages);
            if($validator->fails()){
                //print_r($validator->messages()->getMessages());exit;
                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                    return redirect()->back()
                        ->withErrors($errors)
                        ->withInput();
            }
            if(empty($errors)) {
              /*$data['errors']  = $errors;
            } else {*/
                $userObj = new User();
                $result = $userObj->updateUser($request);
                if($result){
                    Session::flash('message', 'Record updated successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->back();
                }
                else{
                    Session::flash('message', 'Record updation error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }
            return redirect()->back();
        } catch (Exception $e) {
            //echo $e->getMessages();
            Session::flash('message', 'Record updation error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->back();
        }  
    }
	public function updateStatus($encoded_id="", $status)
    {
        try {
			$userObj = new User();
			$id = base64_decode($encoded_id);
			$data['result'] = $userObj->updateRecordById($id, $status);
			if($data['result'] == 'true'){
				Session::flash('message', 'Record updated successfully.');
				Session::flash('msgclass', 'alert-success');
			}
        } catch (Exception $e) {
            Session::flash('message', 'Record could not be deleted.');
            Session::flash('msgclass', 'alert-danger');
        }
        return redirect()->back();
    }
	public function bulkUpdate(Request $request)
    {
        try {
			$userObj = new User();
			$data['result'] = $userObj->updateRecordById($request->user_id, $request->action);
			if($data['result'] == 'true'){
				Session::flash('message', 'Record updated successfully.');
				Session::flash('msgclass', 'alert-success');
			}
        } catch (Exception $e) {
            Session::flash('message', 'Record could not be updated.');
            Session::flash('msgclass', 'alert-danger');
        }
        return redirect()->back();
    }
    public function destroy($encoded_id="")
    {
        try {
            $userObj = new User();
            $id = base64_decode($encoded_id);
            $data['data'] = $userObj->getRecordById($id);
            if(!empty($data['data'])){
                $data['result'] = $userObj->deleteRecordById($id);
                if($data['result'] == 'true'){
                    Session::flash('message', 'Record is deleted successfully.');
                    Session::flash('msgclass', 'alert-success');
                }
            }else{
                Session::flash('message', 'Record does not exist.');
                Session::flash('msgclass', 'alert-danger');
            }
        } catch (Exception $e) {
            Session::flash('message', 'Record could not be deleted.');
            Session::flash('msgclass', 'alert-danger');
        }
        return redirect()->back();
    }
    public function planDetailsByUserID($id="")
    {
        if(!empty($id))
        {
            $userObj = new User();
            $decoded_id = base64_decode($id);
            $data['planDetails'] = $userObj->planDetailsByUserID($decoded_id);
            if(empty($data['planDetails'])){
                $data['message'] = 'Data does not exist.';
                $data['msgclass'] =  'alert-danger';
            }
        }else{
            $data['message'] = 'Url does not exist.';
            $data['msgclass'] =  'alert-danger';
        }
        return response($data);
    }
    public function upgradeValue(Request $request)
    {
        try {
            $userObj = new User();
            $userData = $userObj->getRecordById($request->userID);
            $result = $userObj->upgradeValue($request,$userData);
            if($result == true){
                $result1 = "true";
                Session::flash('message', 'Record added successfully.');
                Session::flash('msgclass', 'alert-success');
            }else{
                $result1 = "false";
                Session::flash('message', 'Record addition error.');
                Session::flash('msgclass', 'alert-danger');
            }
            return $result1;
        } catch (Exception $e) {
            echo "string";
            Session::flash('message', 'Record addition error.');
            Session::flash('msgclass', 'alert-danger');
            return 'false';
        }
    }
}
