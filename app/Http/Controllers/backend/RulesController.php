<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\backend\Rules;
use Validator;
use Session;

class RulesController extends Controller
{

    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {                
        $rulesObj = new Rules();        
        $res_arr = $rulesObj->getRecords($rulesObj);
        //print_r($res_arr);exit;
        return view('backend.rules.list')->with(["page_title"=>"Rules - ",'records' => $res_arr]);
        
    }

    /**
     * Show the room sharing add form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.rules.add')->with(["page_title"=>"Add Rules - "]);
    }

    public function store(Request $request){
        try {
            
            $errors = array();
            $data = array(); 
            

            $rules = array(
                'title'          => 'required',
                'status'         =>'required'
            );

            $messages = [
              'title.required' => 'Please enter title.',
              'status.required' => 'Please select status.',
            ]; 

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){

                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                return redirect()->route('admin_add_rules')
                        ->withErrors($errors)
                        ->withInput();
            }
            if (empty($errors)) {
             
                $rulesObj = new Rules();
                $id = $rulesObj->saveRules($request);
                if($id > 0){
                    Session::flash('message', 'Record added successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->route('admin_rules');
                }else{
                    Session::flash('message', 'Record addition error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }            
        } catch (Exception $e) {            
            Session::flash('message', 'Record addition error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_rules');
        }
        return redirect()->route('admin_rules');
    }

    public function edit($encoded_id="")
    {
        if(!empty($encoded_id))
        {
            $rulesObj = new Rules();
            $id = base64_decode($encoded_id);
            $data = $rulesObj->getRecordById($id);
            if(!empty($data)){
                return view('backend.rules.edit')->with(["page_title"=>"Edit Rules - ",'data' => $data]);
            }else{
                Session::flash('message', 'Data does not exist.');
                Session::flash('msgclass', 'alert-danger');
                return redirect()->route('admin_rules');
            }
        }else{
            
            Session::flash('message', 'Url does not exist.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_rules');
        }
    }

    public function update(Request $request){
        try {
            $errors = array();
            $data = array(); 
            

            $rules = array(
                'title'          => 'required',
                'status'         =>'required'
            );

            $messages = [
              'title.required' => 'Please enter title.',
              'status.required' => 'Please select status.',
            ]; 

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){

                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                return redirect()->back()
                        ->withErrors($errors)
                        ->withInput();
            }
            if(empty($errors)) {
              
              
                $rulesObj = new Rules();
                $result = $rulesObj->updateRules($request);
                if($result){
                    Session::flash('message', 'Record updated successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->back();
                }
                else{
                    Session::flash('message', 'Record updation error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }            
        } catch (Exception $e) {            
            Session::flash('message', 'Record updation error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->back();
        }   
    }

    public function destroy($encoded_id="")
    {        
        try {
            $rulesObj = new Rules();
            $id = base64_decode($encoded_id);

            $data['data'] = $rulesObj->getRecordById($id);
            if(!empty($data['data'])){
                $data['result'] = $rulesObj->deleteRecordById($id);
                if($data['result'] == 'true'){
                    Session::flash('message', 'Record is deleted successfully.');
                    Session::flash('msgclass', 'alert-success');
                }
            }else{
                Session::flash('message', 'Record does not exist.');
                Session::flash('msgclass', 'alert-danger');
            }
        } catch (Exception $e) {
            Session::flash('message', 'Record could not be deleted.');
            Session::flash('msgclass', 'alert-danger');
        }
        return redirect()->back();           
    }
}
