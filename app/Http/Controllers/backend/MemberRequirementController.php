<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\backend\MemberRequirement;
use Validator;
use Session;
use App\Libraries\GlobalHelpers;
use App\Model\backend\User;


class MemberRequirementController extends Controller
{

    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $requirementObj = new MemberRequirement();
        $dropdown['planDropdown'] = GlobalHelpers::planDropdown();
        $dropdown['cityDropdown'] = GlobalHelpers::cityDropdown();
        $dropdown['tnxStatsDropdown'] = GlobalHelpers::tnxStatsDropdown();
        //$propertyTypeDropdown = GlobalHelpers::propertyTypeDropdown();
        $noticePeriodDropdown = GlobalHelpers::noticePeriodArr();
        $availablebhk = GlobalHelpers::availablebhkArr();
		
		
		//$dropdown['propertyTypeDropdown'] = $propertyTypeDropdown;
        $dropdown['availablebhkDropdown'] = $availablebhk;
        $dropdown['noticePeriodDropdown'] = $noticePeriodDropdown;

        $res_arr = $requirementObj->getRecords($requirementObj);
        
        return view('backend.memberrequirement.list',["page_title"=>"Member Requirement - ",'records'=>$res_arr, 'dropdown'=> $dropdown]);
        
    }
    
    /**
     * Show the member payment add form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        $stateDropdown = GlobalHelpers::stateDropdown();
        $planDropdown = GlobalHelpers::planDropdown();
        $cityDropdown = GlobalHelpers::cityDropdown();
        $propertyTypeDropdown = GlobalHelpers::propertyTypeDropdown();
        $noticePeriodDropdown = GlobalHelpers::noticePeriodArr();
		
        $constantRols = config('constants.ADMIN_ROLES');
        $propertyOwnerDropdown = User::userDropdown('roleID', $constantRols['Owner']);
        $dropdowns = [
            'stateDropdown' => $stateDropdown,
            'propertyTypeDropdown' => $propertyTypeDropdown,
            'cityDropdown' => $cityDropdown,
            'planDropdown' => $planDropdown,
            'propertyOwnerDropdown' => $propertyOwnerDropdown,
            'noticePeriodDropdown' => $noticePeriodDropdown,
        ];
        return view('backend.memberrequirement.add', ["page_title" => "Add Member Requirement - ", 'dropdowns' => $dropdowns]);
    }
	
	public function edit($encoded_id="")
    {
        if(!empty($encoded_id))
        {
			$requirementObj = new MemberRequirement();
            $id = base64_decode($encoded_id);
            $data = $requirementObj->getRecordById($id);
            if(!empty($data)){
				$stateDropdown = GlobalHelpers::stateDropdown();
				$planDropdown = GlobalHelpers::planDropdown();
				$cityDropdown = GlobalHelpers::cityDropdown();
				$propertyTypeDropdown = GlobalHelpers::propertyTypeDropdown();
				$noticePeriodDropdown = GlobalHelpers::noticePeriodArr();
				
				$constantRols = config('constants.ADMIN_ROLES');
				$propertyOwnerDropdown = User::userDropdown('roleID', $constantRols['Owner']);
				$dropdowns = [
					'stateDropdown' => $stateDropdown,
					'propertyTypeDropdown' => $propertyTypeDropdown,
					'cityDropdown' => $cityDropdown,
					'planDropdown' => $planDropdown,
					'propertyOwnerDropdown' => $propertyOwnerDropdown,
					'noticePeriodDropdown' => $noticePeriodDropdown,
				];
				return view('backend.memberrequirement.edit')->with(["page_title"=>"Edit Requirement - ",'data' => $data, 'dropdowns' => $dropdowns]);
            }else{
                Session::flash('message', 'Data does not exist.');
                Session::flash('msgclass', 'alert-danger');
                return redirect()->route('admin_Interiors');
            }
        }else{
            
            Session::flash('message', 'Url does not exist.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_Interiors');
        }
    }
	
	public function store(Request $request) {

        try {

            $errors = array();
            $data = array();

            $rules = array(
                'name' => 'required',
                'contact_no' => 'required',
                'email' => 'required',
                'gender' => 'required',
                'looking_for' => 'required',
                'final_pg_in' => 'required',
                'executive_name' => 'required'
            );

            $messages = [
                'name.required' => 'Please enter name.',
                'contact_no.required' => 'Please enter contact no.',
                'email.required' => 'Please enter email.',
                'gender.required' => 'Please select gender.',
                'looking_for.required' => 'Please select looking for.',
                'final_pg_in.required' => 'Please select finalize pg in.',
                'executive_name.required' => 'Please enter executive name.',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {

                $messages_arr = $validator->messages()->getMessages();
                if (count($messages_arr) > 0) {
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_" . $key] = $value_arr[0];
                    }
                }
                return redirect()->route('admin_add_memberrequirement')
                                ->withErrors($errors)
                                ->withInput();
            }
            if (empty($errors)) {

                $memberrequirementObj = new MemberRequirement();
                $id = $memberrequirementObj->saveMemberRequirement($request);
                if ($id > 0) {
                    Session::flash('message', 'Record added successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->route('admin_memberrequirement');
                } else {
                    Session::flash('message', 'Record addition error.');
                    Session::flash('msgclass', 'alert-danger');
                }
            }
        } catch (Exception $e) {
            Session::flash('message', 'Record addition error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_memberrequirement');
        }
        return redirect()->route('admin_memberrequirement');
    }

	
}
