<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\backend\MemberPayment;
use Validator;
use Session;
use App\Libraries\GlobalHelpers;
use App\Model\backend\User;


class MemberPaymentController extends Controller
{

    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $paymentObj = new MemberPayment();
        $dropdown['planDropdown'] = GlobalHelpers::planDropdown();
        $dropdown['cityDropdown'] = GlobalHelpers::cityDropdown();
        $dropdown['tnxStatsDropdown'] = GlobalHelpers::tnxStatsDropdown();

        $res_arr = $paymentObj->getRecords($paymentObj);
        
        return view('backend.memberpayment.list',["page_title"=>"Member Payment - ",'records'=>$res_arr, 'dropdown'=> $dropdown]);
        
    }
    
    /**
     * Show the member payment add form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        $stateDropdown = GlobalHelpers::stateDropdown();
        $planDropdown = GlobalHelpers::planDropdown();
        $cityDropdown = GlobalHelpers::cityDropdown();
        $propertyTypeDropdown = GlobalHelpers::propertyTypeDropdown();
        $constantRols = config('constants.ADMIN_ROLES');
        $propertyOwnerDropdown = User::userDropdown('roleID', $constantRols['Owner']);
        $dropdowns = [
            'stateDropdown' => $stateDropdown,
            'propertyTypeDropdown' => $propertyTypeDropdown,
            'cityDropdown' => $cityDropdown,
            'planDropdown' => $planDropdown,
            'propertyOwnerDropdown' => $propertyOwnerDropdown
        ];
        return view('backend.memberpayment.add', ["page_title" => "Add Member Payment - ", 'dropdowns' => $dropdowns]);
    }
}
