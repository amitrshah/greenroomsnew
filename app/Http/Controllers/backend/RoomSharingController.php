<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\backend\RoomSharing;
use Validator;
use Session;

class RoomSharingController extends Controller
{

    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roomSharingObj = new RoomSharing();

        $res_arr = $roomSharingObj->getRecords($roomSharingObj);
        //print_r($res_arr);exit;
        return view('backend.roomsharing.list')->with(["page_title"=>"Room Sharing - ",'records' => $res_arr]);
        
    }

    /**
     * Show the room sharing add form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.roomsharing.add',["page_title"=>"Add Room Sharing - "]);
    }

    public function store(Request $request){
        try {
            
            $errors = array();
            $data = array(); 
            

            $rules = array(
                'title'          => 'required',
                'maximum_beds'   =>'required|numeric',
                'status'         =>'required'
            );

            $messages = [
              'title.required' => 'Please enter title.',
              'maximum_beds.required' => 'Please enter maximum beds.',
              'maximum_beds.numeric' => 'Please enter only numeric value.',
              'status.required' => 'Please select status.',
            ]; 

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){

                //print_r($validator->messages()->getMessages());exit;

                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                return redirect()->route('admin_add_roomsharing')
                        ->withErrors($errors)
                        ->withInput();
            }
            if (empty($errors)) {
              
              /*$data['errors']  = $errors;

            } else {*/

                $roomSharingObj = new RoomSharing();
                $id = $roomSharingObj->saveRoomSharing($request);
                if($id > 0){

                    //$data['id'] = $id;
                    Session::flash('message', 'Record added successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->route('admin_roomsharing');

                }
                else{
                    Session::flash('message', 'Record addition error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }
            //return response($data);
        } catch (Exception $e) {
            //echo $e->getMessages();
            Session::flash('message', 'Record addition error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_roomsharing');
        }
        return redirect()->route('admin_roomsharing');
    }

    public function edit($encoded_id="")
    {
        if(!empty($encoded_id))
        {
            $roomSharingObj = new RoomSharing();
            $id = base64_decode($encoded_id);
            $data = $roomSharingObj->getRecordById($id);
            if(!empty($data)){
                return view('backend.roomsharing.edit')->with(["page_title"=>"Edit Room Sharing - ",'data' => $data]);
            }else{
                Session::flash('message', 'Data does not exist.');
                Session::flash('msgclass', 'alert-danger');
                return redirect()->route('admin_roomsharing');
            }
        }else{
            
            Session::flash('message', 'Url does not exist.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_roomsharing');
        }
    }

    public function update(Request $request){
        try {
            $errors = array();
            $data = array(); 
            

            $rules = array(
                'title'          => 'required',
                'maximum_beds'   =>'required|numeric',
                'status'         =>'required'
            );

            $messages = [
              'title.required' => 'Please enter title.',
              'maximum_beds.required' => 'Please enter maximum beds.',
              'maximum_beds.numeric' => 'Please enter only numeric value.',
              'status.required' => 'Please select status.',
            ]; 

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){

                //print_r($validator->messages()->getMessages());exit;

                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                return redirect()->back()
                        ->withErrors($errors)
                        ->withInput();
            }
            if(empty($errors)) {
              
              /*$data['errors']  = $errors;

            } else {*/

                $roomSharingObj = new RoomSharing();
                $result = $roomSharingObj->updateRoomSharing($request);
                if($result){
                    Session::flash('message', 'Record updated successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->back();
                }
                else{
                    Session::flash('message', 'Record updation error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }
            //return response($data);
        } catch (Exception $e) {
            //echo $e->getMessages();
            Session::flash('message', 'Record updation error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->back();
        }   
    }

    public function destroy($encoded_id="")
    {
        /*if(!empty($encoded_id))
        {*/
            try {
                $roomSharingObj = new RoomSharing();
                $id = base64_decode($encoded_id);

                $data['data'] = $roomSharingObj->getRecordById($id);
                if(!empty($data['data'])){
                    $data['result'] = $roomSharingObj->deleteRecordById($id);
                    if($data['result'] == 'true'){
                        Session::flash('message', 'Record is deleted successfully.');
                        Session::flash('msgclass', 'alert-success');
                        //$result['result'] = true;
                        //return response($result);
                        
                    }
                }else{
                    Session::flash('message', 'Record does not exist.');
                    Session::flash('msgclass', 'alert-danger');
                    //$result['result'] = false;
                    //return response($result);
                }
            } catch (Exception $e) {
                Session::flash('message', 'Record could not be deleted.');
                Session::flash('msgclass', 'alert-danger');
                //$result['result'] = false;
                //return response($result);
            }
            return redirect()->back();
            //$result['result'] = false;
            //return response($result);
        /*}else{
            
            Session::flash('message', 'Url does not exist.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_roomsharing');
        }*/
    }
}
