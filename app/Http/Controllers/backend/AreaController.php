<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\backend\Area;
use Validator;
use Session;
use App\Libraries\GlobalHelpers;


class AreaController extends Controller
{

    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $areaObj = new Area();

        $res_arr = $areaObj->getRecords($areaObj);
        //print_r($res_arr);exit;
        return view('backend.area.list')->with(["page_title"=>"Area - ",'records' => $res_arr]);
        
    }

    /**
     * Show the room sharing add form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = GlobalHelpers::stateDropdown();
        return view('backend.area.add')->with(["page_title"=>"Add Area - ",'stateDropdown' => $data]);
    }

    public function store(Request $request){
        try {
            
            $errors = array();
            $data = array(); 
            

            $rules = array(
                'areaName'          => 'required',
                'pincode'          => 'required',
                'state'          => 'required',
                'city'          => 'required',
                'status'         =>'required'
            );

            $messages = [
              'areaName.required' => 'Please enter title.',
              'pincode.required' => 'Please enter pincode.',
              'state.required' => 'Please select state.',
              'city.required' => 'Please select city.',
              'status.required' => 'Please select status.',
            ]; 

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){

                //print_r($validator->messages()->getMessages());exit;

                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                return redirect()->route('admin_add_area')
                        ->withErrors($errors)
                        ->withInput();
            }
            if (empty($errors)) {
              
              /*$data['errors']  = $errors;

            } else {*/

                $areaObj = new Area();
                $id = $areaObj->saveArea($request);
                if($id > 0){

                    //$data['id'] = $id;
                    Session::flash('message', 'Record added successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->route('admin_area');

                }
                else{
                    Session::flash('message', 'Record addition error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }
            //return response($data);
        } catch (Exception $e) {
            //echo $e->getMessages();
            Session::flash('message', 'Record addition error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_area');
        }
        return redirect()->route('admin_area');
    }

    public function edit($encoded_id="")
    {
        if(!empty($encoded_id))
        {
            $areaObj = new Area();
            $id = base64_decode($encoded_id);
            $data = $areaObj->getRecordById($id);

            //echo "<pre>";print_r($data['data']);exit;
            if(!empty($data)){
                $data->stateDropdown = GlobalHelpers::stateDropdown();
                $data->cityDropdown = GlobalHelpers::cityDropdown($data->stateID);
                return view('backend.area.edit')->with(["page_title"=>"Edit Area - ",'data' => $data]);
            }else{
                Session::flash('message', 'Data does not exist.');
                Session::flash('msgclass', 'alert-danger');
                return redirect()->route('admin_area');
            }
        }else{
            
            Session::flash('message', 'Url does not exist.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_area');
        }
    }

    public function update(Request $request){
        //echo "<pre>";print_r($request->all());exit;
        try {
            $errors = array();
            $data = array(); 
            

            $rules = array(
                'areaName'          => 'required',
                'pincode'          => 'required',
                'state'          => 'required',
                'city'          => 'required',
                'status'         =>'required'
            );

            $messages = [
              'areaName.required' => 'Please enter title.',
              'pincode.required' => 'Please enter pincode.',
              'state.required' => 'Please select state.',
              'city.required' => 'Please select city.',
              'status.required' => 'Please select status.',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){

                //print_r($validator->messages()->getMessages());exit;

                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                return redirect()->back()
                        ->withErrors($errors)
                        ->withInput();
            }
            if(empty($errors)) {
              
              /*$data['errors']  = $errors;

            } else {*/

                $areaObj = new Area();
                $result = $areaObj->updateArea($request);
                if($result){
                    Session::flash('message', 'Record updated successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->back();
                }
                else{
                    Session::flash('message', 'Record updation error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }
            //return response($data);
        } catch (Exception $e) {
            //echo $e->getMessages();
            Session::flash('message', 'Record updation error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->back();
        }   
    }

    public function destroy($encoded_id="")
    {
        /*if(!empty($encoded_id))
        {*/
            try {
                $areaObj = new Area();
                $id = base64_decode($encoded_id);

                $data['data'] = $areaObj->getRecordById($id);
                if(!empty($data['data'])){
                    $data['result'] = $areaObj->deleteRecordById($id);
                    if($data['result'] == 'true'){
                        Session::flash('message', 'Record is deleted successfully.');
                        Session::flash('msgclass', 'alert-success');
                        //$result['result'] = true;
                        //return response($result);
                        
                    }
                }else{
                    Session::flash('message', 'Record does not exist.');
                    Session::flash('msgclass', 'alert-danger');
                    //$result['result'] = false;
                    //return response($result);
                }
            } catch (Exception $e) {
                Session::flash('message', 'Record could not be deleted.');
                Session::flash('msgclass', 'alert-danger');
                //$result['result'] = false;
                //return response($result);
            }
            return redirect()->back();
            //$result['result'] = false;
            //return response($result);
        /*}else{
            
            Session::flash('message', 'Url does not exist.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_area');
        }*/
    }

    public function getAllCity($stateID=null){
        $cityArr = GlobalHelpers::cityDropdown($stateID);
       
        $cityDropdown = "";
        $cityDropdown = "<option value=''>Select City</option>";
        foreach ($cityArr as $key => $value) {
            $cityDropdown .= "<option value='".$key."'>".$value."</option>";
        }
        return $cityDropdown;
    }
}
