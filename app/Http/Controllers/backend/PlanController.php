<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\backend\Plan;
use Validator;
use Session;
use App\Libraries\GlobalHelpers;


class PlanController extends Controller
{

    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $planObj = new Plan();

        $res_arr = $planObj->getRecords($planObj);
        //print_r($res_arr);exit;
        return view('backend.plan.list')->with(["page_title"=>"Plan - ",'records' => $res_arr]);
        
    }

    /**
     * Show the room sharing add form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = GlobalHelpers::cityDropdown();
        //echo "<pre>"; print_r($data['cityDropdown'] );exit;
        return view('backend.plan.add')->with(["page_title"=>"Add Plan - ",'cityDropdown' => $data]);
    }

    public function store(Request $request){
        try {
            
            $errors = array();
            $data = array(); 
            

            $rules = array(
                'planName'          => 'required',
                'favLimit'          => 'required|numeric',
                'price'          => 'required|numeric',
                'duration'          => 'required|numeric',
                'city'          => 'required',
                'status'         =>'required'
            );

            $messages = [
              'planName.required' => 'Please enter title.',
              'price.required' => 'Please enter price.',
              'price.numeric' => 'Please enter price numeric only.',
              'favLimit.required' => 'Please enter favourite property limit.',
              'favLimit.numeric' => 'Please enter favourite property limit numeric only.',
              'duration.required' => 'Please enter duration.',
              'duration.numeric' => 'Please enter duration numeric only.',
              'city.required' => 'Please select city.',
              'status.required' => 'Please select status.',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){

                //print_r($validator->messages()->getMessages());exit;

                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                return redirect()->route('admin_add_plan')
                        ->withErrors($errors)
                        ->withInput();
            }
            if (empty($errors)) {
              
              /*$data['errors']  = $errors;

            } else {*/

                $planObj = new Plan();
                $id = $planObj->savePlan($request);
                if($id > 0){

                    //$data['id'] = $id;
                    Session::flash('message', 'Record added successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->route('admin_plan');

                }
                else{
                    Session::flash('message', 'Record addition error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }
            //return response($data);
        } catch (Exception $e) {
            //echo $e->getMessages();
            Session::flash('message', 'Record addition error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_plan');
        }
        return redirect()->route('admin_plan');
    }

    public function edit($encoded_id="")
    {
        if(!empty($encoded_id))
        {
            $planObj = new Plan();
            $id = base64_decode($encoded_id);
            $data = $planObj->getRecordById($id);
            $data->cityDropdown = GlobalHelpers::cityDropdown();
            //echo "<pre>";print_r($data['data']);exit;
            if(!empty($data)){
                return view('backend.plan.edit')->with(["page_title"=>"Edit Plan - ",'data' => $data]);
            }else{
                Session::flash('message', 'Data does not exist.');
                Session::flash('msgclass', 'alert-danger');
                return redirect()->route('admin_plan');
            }
        }else{
            
            Session::flash('message', 'Url does not exist.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_plan');
        }
    }

    public function update(Request $request){
        
        try {
            $errors = array();
            $data = array(); 
            

            $rules = array(
                'planName'          => 'required',
                'favLimit'          => 'required|numeric',
                'price'          => 'required|numeric',
                'duration'          => 'required|numeric',
                'city'          => 'required',
                'status'         =>'required'
            );

            $messages = [
              'planName.required' => 'Please enter title.',
              'price.required' => 'Please enter price.',
              'price.numeric' => 'Please enter price numeric only.',
              'favLimit.required' => 'Please enter favourite property limit.',
              'favLimit.numeric' => 'Please enter favourite property limit numeric only.',
              'duration.required' => 'Please enter duration.',
              'duration.numeric' => 'Please enter duration numeric only.',
              'city.required' => 'Please select city.',
              'status.required' => 'Please select status.',
            ];
            

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){

                //print_r($validator->messages()->getMessages());exit;

                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                return redirect()->back()
                        ->withErrors($errors)
                        ->withInput();
            }
            if(empty($errors)) {
              
              /*$data['errors']  = $errors;

            } else {*/

                $planObj = new Plan();
                $result = $planObj->updatePlan($request);
                if($result){
                    Session::flash('message', 'Record updated successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->back();
                }
                else{
                    Session::flash('message', 'Record updation error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }
            //return response($data);
        } catch (Exception $e) {
            //echo $e->getMessages();
            Session::flash('message', 'Record updation error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->back();
        }   
    }

    public function destroy($encoded_id="")
    {
        /*if(!empty($encoded_id))
        {*/
            try {
                $planObj = new Plan();
                $id = base64_decode($encoded_id);

                $data['data'] = $planObj->getRecordById($id);
                if(!empty($data['data'])){
                    $data['result'] = $planObj->deleteRecordById($id);
                    if($data['result'] == 'true'){
                        Session::flash('message', 'Record is deleted successfully.');
                        Session::flash('msgclass', 'alert-success');
                        //$result['result'] = true;
                        //return response($result);
                        
                    }
                }else{
                    Session::flash('message', 'Record does not exist.');
                    Session::flash('msgclass', 'alert-danger');
                    //$result['result'] = false;
                    //return response($result);
                }
            } catch (Exception $e) {
                Session::flash('message', 'Record could not be deleted.');
                Session::flash('msgclass', 'alert-danger');
                //$result['result'] = false;
                //return response($result);
            }
            return redirect()->back();
            //$result['result'] = false;
            //return response($result);
        /*}else{
            
            Session::flash('message', 'Url does not exist.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_plan');
        }*/
    }
	
	public function updateStatus($encoded_id="", $status)
    {
        try {
			$planObj = new Plan();
			$id = base64_decode($encoded_id);
			$data['result'] = $planObj->updatePlanById($id, $status);
			if($data['result'] == 'true'){
				Session::flash('message', 'Record updated successfully.');
				Session::flash('msgclass', 'alert-success');
			}
        } catch (Exception $e) {
            Session::flash('message', 'Record could not be deleted.');
            Session::flash('msgclass', 'alert-danger');
        }
        return redirect()->back();
    }
	public function bulkUpdate(Request $request)
    {
        try {
			$planObj = new Plan();
			$data['result'] = $planObj->updatePlanById($request->plan_id, $request->action);
			if($data['result'] == 'true'){
				Session::flash('message', 'Record updated successfully.');
				Session::flash('msgclass', 'alert-success');
			}
        } catch (Exception $e) {
            Session::flash('message', 'Record could not be updated.');
            Session::flash('msgclass', 'alert-danger');
        }
        return redirect()->back();
    }
}
