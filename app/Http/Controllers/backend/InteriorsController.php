<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\backend\Interiors;
use Validator;
use Session;

class InteriorsController extends Controller
{

    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {        
        $interiorsObj = new Interiors();
        
        $res_arr = $interiorsObj->getRecords($interiorsObj);
        //print_r($res_arr);exit;
        return view('backend.interiors.list')->with(["page_title"=>"Interiors - ",'records' => $res_arr]);
        
    }

    /**
     * Show the room sharing add form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.interiors.add')->with(["page_title"=>"Add Interiors - "]);
    }

    public function store(Request $request){
        try {
            
            $errors = array();
            $data = array(); 
            

            $rules = array(
                'title'          => 'required',
                'status'         =>'required'
            );

            $messages = [
              'title.required' => 'Please enter title.',
              'status.required' => 'Please select status.',
            ]; 

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){

                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                return redirect()->route('admin_add_Interiors')
                        ->withErrors($errors)
                        ->withInput();
            }
            if (empty($errors)) {
             
                $interiorsObj = new Interiors();
                $id = $interiorsObj->saveInteriors($request);
                if($id > 0){
                    Session::flash('message', 'Record added successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->route('admin_interiors');
                }else{
                    Session::flash('message', 'Record addition error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }            
        } catch (Exception $e) {            
            Session::flash('message', 'Record addition error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_interiors');
        }
        return redirect()->route('admin_interiors');
    }

    public function edit($encoded_id="")
    {
        if(!empty($encoded_id))
        {
            $interiorsObj = new Interiors();
            $id = base64_decode($encoded_id);
            $data = $interiorsObj->getRecordById($id);
            if(!empty($data)){
                return view('backend.interiors.edit')->with(["page_title"=>"Edit Interiors - ",'data' => $data]);
            }else{
                Session::flash('message', 'Data does not exist.');
                Session::flash('msgclass', 'alert-danger');
                return redirect()->route('admin_Interiors');
            }
        }else{
            
            Session::flash('message', 'Url does not exist.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_Interiors');
        }
    }

    public function update(Request $request){
        try {
            $errors = array();
            $data = array(); 
            

            $rules = array(
                'title'          => 'required',
                'status'         =>'required'
            );

            $messages = [
              'title.required' => 'Please enter title.',
              'status.required' => 'Please select status.',
            ]; 

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){

                //print_r($validator->messages()->getMessages());exit;

                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                return redirect()->back()
                        ->withErrors($errors)
                        ->withInput();
            }
            if(empty($errors)) {
              
              
                $interiorsObj = new Interiors();
                $result = $interiorsObj->updateInteriors($request);
                if($result){
                    Session::flash('message', 'Record updated successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->back();
                }
                else{
                    Session::flash('message', 'Record updation error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }
            //return response($data);
        } catch (Exception $e) {
            //echo $e->getMessages();
            Session::flash('message', 'Record updation error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->back();
        }   
    }

    public function destroy($encoded_id="")
    {
        /*if(!empty($encoded_id))
        {*/
            try {
                $interiorsObj = new Interiors();
                $id = base64_decode($encoded_id);

                $data['data'] = $interiorsObj->getRecordById($id);
                if(!empty($data['data'])){
                    $data['result'] = $interiorsObj->deleteRecordById($id);
                    if($data['result'] == 'true'){
                        Session::flash('message', 'Record is deleted successfully.');
                        Session::flash('msgclass', 'alert-success');
                       
                    }
                }else{
                    Session::flash('message', 'Record does not exist.');
                    Session::flash('msgclass', 'alert-danger');
                   
                }
            } catch (Exception $e) {
                Session::flash('message', 'Record could not be deleted.');
                Session::flash('msgclass', 'alert-danger');
               
            }
            return redirect()->back();
           
    }
}
