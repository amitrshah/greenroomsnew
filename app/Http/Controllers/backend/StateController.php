<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\backend\State;
use Validator;
use Session;

class StateController extends Controller
{

    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $stateObj = new State();

        $res_arr = $stateObj->getRecords($stateObj);
        //print_r($res_arr);exit;
        return view('backend.state.list')->with(["page_title"=>"State - ",'records' => $res_arr]);
        
    }

    /**
     * Show the room sharing add form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.state.add',["page_title"=>"Add State - "]);
    }

    public function store(Request $request){
        try {
            
            $errors = array();
            $data = array(); 
            

            $rules = array(
                'stateName'          => 'required|unique:state,stateName',
                'status'         =>'required'
            );

            $messages = [
              'stateName.required' => 'Please enter title.',
              'status.required' => 'Please select status.',
            ]; 

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){

                //print_r($validator->messages()->getMessages());exit;

                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                return redirect()->route('admin_add_state')
                        ->withErrors($errors)
                        ->withInput();
            }
            if (empty($errors)) {
              
              /*$data['errors']  = $errors;

            } else {*/

                $stateObj = new State();
                $id = $stateObj->saveState($request);
                if($id > 0){

                    //$data['id'] = $id;
                    Session::flash('message', 'Record added successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->route('admin_state');

                }
                else{
                    Session::flash('message', 'Record addition error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }
            //return response($data);
        } catch (Exception $e) {
            //echo $e->getMessages();
            Session::flash('message', 'Record addition error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_state');
        }
        return redirect()->route('admin_state');
    }

    public function edit($encoded_id="")
    {
        if(!empty($encoded_id))
        {
            $stateObj = new State();
            $id = base64_decode($encoded_id);
            $data = $stateObj->getRecordById($id);
            if(!empty($data)){
                return view('backend.state.edit')->with(["page_title"=>"Edit State - ",'data' => $data]);
            }else{
                Session::flash('message', 'Data does not exist.');
                Session::flash('msgclass', 'alert-danger');
                return redirect()->route('admin_state');
            }
        }else{
            
            Session::flash('message', 'Url does not exist.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_state');
        }
    }

    public function update(Request $request){
        try {
            $errors = array();
            $data = array(); 
            

            $rules = array(
                'stateName'          => 'required',
                'status'         =>'required'
            );

            $messages = [
              'stateName.required' => 'Please enter title.',
              'status.required' => 'Please select status.',
            ]; 

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){

                //print_r($validator->messages()->getMessages());exit;

                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                return redirect()->back()
                        ->withErrors($errors)
                        ->withInput();
            }
            if(empty($errors)) {
              
              /*$data['errors']  = $errors;

            } else {*/

                $stateObj = new State();
                $result = $stateObj->updateState($request);
                if($result){
                    Session::flash('message', 'Record updated successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->back();
                }
                else{
                    Session::flash('message', 'Record updation error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }
            //return response($data);
        } catch (Exception $e) {
            //echo $e->getMessages();
            Session::flash('message', 'Record updation error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->back();
        }   
    }

    public function destroy($encoded_id="")
    {
        /*if(!empty($encoded_id))
        {*/
            try {
                $stateObj = new State();
                $id = base64_decode($encoded_id);

                $data['data'] = $stateObj->getRecordById($id);
                if(!empty($data['data'])){
                    $data['result'] = $stateObj->deleteRecordById($id);
                    if($data['result'] == 'true'){
                        Session::flash('message', 'Record is deleted successfully.');
                        Session::flash('msgclass', 'alert-success');
                        //$result['result'] = true;
                        //return response($result);
                        
                    }
                }else{
                    Session::flash('message', 'Record does not exist.');
                    Session::flash('msgclass', 'alert-danger');
                    //$result['result'] = false;
                    //return response($result);
                }
            } catch (Exception $e) {
                Session::flash('message', 'Record could not be deleted.');
                Session::flash('msgclass', 'alert-danger');
                //$result['result'] = false;
                //return response($result);
            }
            return redirect()->back();
            //$result['result'] = false;
            //return response($result);
        /*}else{
            
            Session::flash('message', 'Url does not exist.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_state');
        }*/
    }
}
