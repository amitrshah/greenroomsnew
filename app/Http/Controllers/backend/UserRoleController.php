<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\backend\UserRole;
use Validator;
use Session;

class UserRoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userRoleObj = new UserRole();

        $res_arr = $userRoleObj->getRecords($userRoleObj);
        //print_r($res_arr);exit;
        return view('backend.userrole.list')->with(["page_title"=>"User Role - ",'records' => $res_arr]);
        
    }

    /**
     * Show the user role add form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.userrole.add')->with(["page_title"=>"Add User Role - "]);
    }

    public function store(Request $request){

        try {
            
            $errors = array();
            $data = array(); 
            

            $rules = array(
                'roleName'          => 'required',
                'description'   =>'required',
                'status'         =>'required'
            );

            $messages = [
              'roleName.required' => 'Please enter roleName.',
              'description.required' => 'Please enter description.',
              'status.required' => 'Please select status.',
            ]; 

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){

                //print_r($validator->messages()->getMessages());exit;

                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                return redirect()->route('admin_add_userrole')
                        ->withErrors($errors)
                        ->withInput();
            }
            if (empty($errors)) {
              
              /*$data['errors']  = $errors;

            } else {*/

                $userRoleObj = new UserRole();
                $id = $userRoleObj->saveUserRole($request);
                if($id > 0){

                    //$data['id'] = $id;
                    Session::flash('message', 'Record added successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->route('admin_userrole');

                }
                else{
                    Session::flash('message', 'Record addition error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }
            //return response($data);
        } catch (Exception $e) {
            //echo $e->getMessages();
            Session::flash('message', 'Record addition error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_userrole');
        }
        return redirect()->route('admin_userrole');
    }

    public function edit($encoded_id="")
    {
        if(!empty($encoded_id))
        {
            $userRoleObj = new UserRole();
            $id = base64_decode($encoded_id);
            $data = $userRoleObj->getRecordById($id);
            if(!empty($data)){
                return view('backend.userrole.edit')->with(["page_title"=>"Edit User Role - ",'data' => $data]);
            }else{
                Session::flash('message', 'Data does not exist.');
                Session::flash('msgclass', 'alert-danger');
                return redirect()->route('admin_userrole');
            }
        }else{
            
            Session::flash('message', 'Url does not exist.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_userrole');
        }
    }

    public function update(Request $request){
        try {
            $errors = array();
            $data = array(); 
            

            $rules = array(
                'roleName'          => 'required',
                'description'   =>'required',
                'status'         =>'required'
            );

            $messages = [
              'roleName.required' => 'Please enter roleName.',
              'description.required' => 'Please enter description.',
              'status.required' => 'Please select status.',
            ]; 

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){

                //print_r($validator->messages()->getMessages());exit;

                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                return redirect()->back()
                        ->withErrors($errors)
                        ->withInput();
            }
            if(empty($errors)) {
              
              /*$data['errors']  = $errors;

            } else {*/

                $userRoleObj = new UserRole();
                $result = $userRoleObj->updateUserRole($request);
                if($result){
                    Session::flash('message', 'Record updated successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->back();
                }
                else{
                    Session::flash('message', 'Record updation error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }
            //return response($data);
        } catch (Exception $e) {
            //echo $e->getMessages();
            Session::flash('message', 'Record updation error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->back();
        }   
    }

    public function destroy($encoded_id="")
    {
        /*if(!empty($encoded_id))
        {*/
            try {
                $userRoleObj = new UserRole();
                $id = base64_decode($encoded_id);

                $data['data'] = $userRoleObj->getRecordById($id);
                if(!empty($data['data'])){
                    $data['result'] = $userRoleObj->deleteRecordById($id);
                    if($data['result'] == 'true'){
                        Session::flash('message', 'Record is deleted successfully.');
                        Session::flash('msgclass', 'alert-success');
                        //$result['result'] = true;
                        //return response($result);
                        
                    }
                }else{
                    Session::flash('message', 'Record does not exist.');
                    Session::flash('msgclass', 'alert-danger');
                    //$result['result'] = false;
                    //return response($result);
                }
            } catch (Exception $e) {
                Session::flash('message', 'Record could not be deleted.');
                Session::flash('msgclass', 'alert-danger');
                //$result['result'] = false;
                //return response($result);
            }
            return redirect()->back();
            //$result['result'] = false;
            //return response($result);
        /*}else{
            
            Session::flash('message', 'Url does not exist.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_userrole');
        }*/
    }

     public  function managePermission(Request $request){

        
        $userRole = new UserRole();

        $id = $request->id;
        $decoded_id = base64_decode($id);
        //echo $decoded_id;exit;
        /*if(isset($id) && intval($id) > 0){
            $mode = "edit";
        }else{
            $mode = "create";
        }*/

        $mode = "manage";
       /* print_r($request->all());
echo $decoded_id;exit;*/
        $actions = $userRole->getAction();        
        $modules = $userRole->getModule();        
        $modulePermission = $userRole->getModulePermission($decoded_id);
        $roleData = $userRole->getRecordById($decoded_id);


        $per_sel_arr  = array();

        if($modulePermission){
            foreach ($modulePermission as $key => $permission) {
                $per_sel_arr[$permission->moduleID] = (array) json_decode($permission->actionID);
            }
        }

        $data = array(
            'actions' => $actions,
            'modules' => $modules,
            'per_sel_arr' => $per_sel_arr,
            'roleName' => $roleData->roleName,
            'roleID' => $id,
            'mode' => $mode
        );
        /*echo "<pre>";
print_r($data);exit;*/
        return view('backend.userrole.manage')->with($data)->with(['page_title' => 'Edit User Permission - ']);
    }

    public function updatePermission(Request $request){
        
        $data = array();       

        $user_role = new UserRole();
        $affected = $user_role->updateModulePermission($request);
        
        if($affected > 0){
            
          //  SystemHelper::user_activity_log($request, 'UPDATE_USER_PERMISSION_DETAILS', 'User Permission Updated', $request->input('id'));
            Session::flash('message', 'User role managed successfully.');
            Session::flash('msgclass', 'alert-success');
        }
        else{
            Session::flash('message', 'User role managing error.');
            Session::flash('msgclass', 'alert-danger'); 
        }
        return redirect()->back();
    }
}
