<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\backend\Societyamenity;
use Validator;
use Session;

class SocietyAmenityController extends Controller
{

    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {                       
        $societyamenityObj = new Societyamenity();        
        $res_arr = $societyamenityObj->getRecords($societyamenityObj);       
        return view('backend.societyamenity.list')->with(["page_title"=>"Society Amenity - ",'records' => $res_arr]);        
    }

    /**
     * Show the room sharing add form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.societyamenity.add')->with(["page_title"=>"Add Society Amenity - "]);
    }

    public function store(Request $request){
        try {
            
            $errors = array();
            $data = array(); 
            

            $rules = array(
                'title'          => 'required',
                'status'         =>'required'
            );

            $messages = [
              'title.required' => 'Please enter title.',
              'status.required' => 'Please select status.',
            ]; 

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){

                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                return redirect()->route('admin_add_societyamenity')
                        ->withErrors($errors)
                        ->withInput();
            }
            if (empty($errors)) {
             
                $societyamenityObj = new Societyamenity();
                $id = $societyamenityObj->saveSocietyamenity($request);
                if($id > 0){
                    Session::flash('message', 'Record added successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->route('admin_societyamenity');
                }else{
                    Session::flash('message', 'Record addition error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }            
        } catch (Exception $e) {            
            Session::flash('message', 'Record addition error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_societyamenity');
        }
        return redirect()->route('admin_societyamenity');
    }

    public function edit($encoded_id="")
    {
        if(!empty($encoded_id))
        {
            $societyamenityObj = new Societyamenity();
            $id = base64_decode($encoded_id);
            $data = $societyamenityObj->getRecordById($id);
            if(!empty($data)){
                return view('backend.societyamenity.edit')->with(["page_title"=>"Edit Society Amenity - ",'data' => $data]);
            }else{
                Session::flash('message', 'Data does not exist.');
                Session::flash('msgclass', 'alert-danger');
                return redirect()->route('admin_societyamenity');
            }
        }else{
            
            Session::flash('message', 'Url does not exist.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_societyamenity');
        }
    }

    public function update(Request $request){
        try {
            $errors = array();
            $data = array(); 
            

            $rules = array(
                'title'          => 'required',
                'status'         =>'required'
            );

            $messages = [
              'title.required' => 'Please enter title.',
              'status.required' => 'Please select status.',
            ]; 

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){

                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                return redirect()->back()
                        ->withErrors($errors)
                        ->withInput();
            }
            if(empty($errors)) {
              
              
                $societyamenityObj = new Societyamenity();
                $result = $societyamenityObj->updateSocietyamenity($request);
                if($result){
                    Session::flash('message', 'Record updated successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->back();
                }
                else{
                    Session::flash('message', 'Record updation error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }            
        } catch (Exception $e) {            
            Session::flash('message', 'Record updation error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->back();
        }   
    }

    public function destroy($encoded_id="")
    {        
        try {
            $societyamenityObj = new Societyamenity();
            $id = base64_decode($encoded_id);

            $data['data'] = $societyamenityObj->getRecordById($id);
            if(!empty($data['data'])){
                $data['result'] = $societyamenityObj->deleteRecordById($id);
                if($data['result'] == 'true'){
                    Session::flash('message', 'Record is deleted successfully.');
                    Session::flash('msgclass', 'alert-success');
                }
            }else{
                Session::flash('message', 'Record does not exist.');
                Session::flash('msgclass', 'alert-danger');
            }
        } catch (Exception $e) {
            Session::flash('message', 'Record could not be deleted.');
            Session::flash('msgclass', 'alert-danger');
        }
        return redirect()->back();           
    }
}
