<?php

namespace App\Http\Controllers\backend\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use DB;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin/dashboard';
    protected $guard = 'admin';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //DB::enableQueryLog();
        //$this->middleware('guest')->except('logout');
    }
    protected function guard()
    {
        return Auth::guard('admin');
    }
    public function showLoginForm()
    {
        
        if ( auth()->guard('admin')->check() ) 
        {
            return redirect()->intended('admin/dashboard');
        }
        return view('backend/auth/login')->with(["page_title"=>"Login - "]);
    }

    public function logout(Request $request)
    {
		auth()->guard("admin")->logout();
        //$this->guard('admin')->logout();
        $request->session()->forget('sess_role_name');
        $request->session()->forget('sess_access_module');
        //$request->session()->invalidate();

        return view('backend/auth/login')->with(["page_title"=>"Login - "]);
    }

    public function authenticate(Request $request)
    {
        if ( auth()->guard('admin')->check() ) 
        {
            return redirect()->intended('admin/dashboard');
        }

        //$email = $request->input('email');
        $mobile = $request->input('mobile');
        $password = $request->input('password');
         
        if (auth()->guard('admin')->attempt(['mobile' => $mobile, 'password' => $password /*, 'roleID' => [1,3,4]*/])) 
        {
            $loggedIn = in_array(auth()->guard('admin')->user()->roleID, config('constants.ADMIN_ROLES'));
            if($loggedIn){
                return redirect()->intended('admin/dashboard');
            }else{
                auth()->guard('admin')->logout();
                return redirect()->intended('admin/login')->with('status', 'Invalid Login Credentials !')->withInput();
            }
            //dd(DB::getQueryLog());
        }
        else
        {   
        //dd(DB::getQueryLog());         
            return redirect()->intended('admin/login')->with('status', 'Invalid Login Credentials !')->withInput();
        }
    }
}
