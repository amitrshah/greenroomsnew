<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\backend\Property;
use App\Model\backend\User;
use App\Model\backend\Rules;
use App\Model\backend\Societyamenity;
use App\Model\backend\Facilities;
use App\Model\backend\Interiors;
use App\Model\backend\Furniture;
use Validator;
use Session;
use App\Libraries\GlobalHelpers;
use App\Libraries\ImageHelpers;

class PropertyController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $propertyObj = new Property();
        $cityDropdown = GlobalHelpers::cityDropdown();
        $stateDropdown = GlobalHelpers::stateDropdown();
		
		$propertyTypeDropdown = GlobalHelpers::propertyTypeDropdown();
        //mehul change
        $numberOfRoomDropdown = GlobalHelpers::numberOfRoomArr();
        $facingDropdown = GlobalHelpers::facingArr();
        $noticePeriodDropdown = GlobalHelpers::noticePeriodArr();
        //mehul change over
        $conditionDropdown = GlobalHelpers::conditionArr();
        $mealsIncludedDropdown = GlobalHelpers::mealsIncludedArr();
        $societyamenityIDropdown = GlobalHelpers::societyamenityArr();
        $floorIDropdown = GlobalHelpers::floorArr();
        $availablebhkIDropdown = GlobalHelpers::availablebhkArr();
        $constantRols = config('constants.ADMIN_ROLES');


        $propertyOwnerDropdown = User::userDropdown('roleID', $constantRols['Owner']);
        $roomSharingDropdown = GlobalHelpers::roomSharingDropdown();
        $rulesObj = new Rules();
        $ruleschk = $rulesObj->getAllRecord();
        $societyamenityObj = new Societyamenity();
        $facilitiesObj = new Facilities();
        $interiorsObj = new Interiors();
        $furnitureObj = new Furniture();
        $societyamenitychk = $societyamenityObj->getAllRecord();
        $facilitieschk = $facilitiesObj->getAllRecord();
        $interiorschk = $interiorsObj->getAllRecord();
        $furniturechk = $furnitureObj->getAllRecord();
        $mealschk = array(1=>"Breakfast",2=>"Lunch",3=>"Dinner",4=>"Not Included");
        $suitableFor = array(1=>"Students",2=>"Working Professional",3=>"Student & Working Professional");
		$area = $propertyObj->getAllArea();
        $availableTypeDropdown = GlobalHelpers::availableTypeArr();
        $propertyAge = GlobalHelpers::propertyAge();

        $dropdown = [
            'cityDropdown' => $cityDropdown,
            'stateDropdown' => $stateDropdown,
            'propertyTypeDropdown' => $propertyTypeDropdown,
            'mealsIncludedDropdown' => $mealsIncludedDropdown,
            'conditionDropdown' => $conditionDropdown,
            //mehul change
            'numberOfRoomDropdown' => $numberOfRoomDropdown,
            'facingDropdown' => $facingDropdown,
            'noticePeriodDropdown' => $noticePeriodDropdown,
            //mehul change over
            'propertyOwnerDropdown' => $propertyOwnerDropdown,
            'roomSharingDropdown' => $roomSharingDropdown,
            'societyamenityIDropdown' => $societyamenityIDropdown,
            'floorIDropdown' => $floorIDropdown,
            'availablebhkIDropdown' => $availablebhkIDropdown,
            'ruleschk' => $ruleschk,
            'societyamenitychk' => $societyamenitychk,
            'facilitieschk' => $facilitieschk,
            'interiorschk' => $interiorschk,
            'furniturechk' => $furniturechk,
            'mealschk' => $mealschk,
            'area' => $area,
            'suitableFor' => $suitableFor,
            'availableTypeDropdown' => $availableTypeDropdown,
            'propertyAge' => $propertyAge,
        ];
		
		
		
        $res_arr = $propertyObj->getRecords($propertyObj);
        /* echo "<pre>";
          print_r($res_arr);exit; */
        return view('backend.property.list')->with(["page_title" => "Property - ", 'records' => $res_arr, 'dropdown' => $dropdown]);
    }
	
	function getEnglishOrdinalSuffix($n) 
	{ 
		if (!in_array(($n % 100),array(11,12,13)))
		{
			switch ($n % 10)
			{
			// Only Handle 1st, 2nd, 3rd from Here
			case 1:  return $n .'st';
			case 2:  return $n .'nd';
			case 3:  return $n .'rd';
		  }
		}
		return $num.'th';
	}


    /**
     * Show the property add form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $propertyObj = new Property();

        $stateDropdown = GlobalHelpers::stateDropdown();
        $propertyTypeDropdown = GlobalHelpers::propertyTypeDropdown();
        //mehul change
        $numberOfRoomDropdown = GlobalHelpers::numberOfRoomArr();
        $facingDropdown = GlobalHelpers::facingArr();
        $noticePeriodDropdown = GlobalHelpers::noticePeriodArr();
        //mehul change over
        $conditionDropdown = GlobalHelpers::conditionArr();
        $mealsIncludedDropdown = GlobalHelpers::mealsIncludedArr();
        $societyamenityIDropdown = GlobalHelpers::societyamenityArr();
        $floorIDropdown = GlobalHelpers::floorArr();
        $availablebhkIDropdown = GlobalHelpers::availablebhkArr();
        $availableTypeDropdown = GlobalHelpers::availableTypeArr();
        $constantRols = config('constants.ADMIN_ROLES');


        $propertyOwnerDropdown = User::userDropdown('roleID', $constantRols['Owner']);
        $roomSharingDropdown = GlobalHelpers::roomSharingDropdown();
        $rulesObj = new Rules();
        $ruleschk = $rulesObj->getAllRecord();
        $societyamenityObj = new Societyamenity();
        $facilitiesObj = new Facilities();
        $interiorsObj = new Interiors();
        $furnitureObj = new Furniture();
        $societyamenitychk = $societyamenityObj->getAllRecord();
        $facilitieschk = $facilitiesObj->getAllRecord();
        $interiorschk = $interiorsObj->getAllRecord();
        $furniturechk = $furnitureObj->getAllRecord();
        $mealschk = array(1=>"Breakfast",2=>"Lunch",3=>"Dinner",4=>"Not Included");
        $availableTypeDropdown = GlobalHelpers::availableTypeArr();
		$propertyAge = GlobalHelpers::propertyAge();
		//$area = $propertyObj->getAllArea();
		
		/*$areaData = array();
		foreach ($area as $key => $areaObject) {
			$areaData[$areaObject->areaID] = $areaObject->areaName;
		}*/
        
		$dropdowns = [
            'stateDropdown' => $stateDropdown,
            'propertyTypeDropdown' => $propertyTypeDropdown,
            'mealsIncludedDropdown' => $mealsIncludedDropdown,
            'conditionDropdown' => $conditionDropdown,
            //mehul change
            'numberOfRoomDropdown' => $numberOfRoomDropdown,
            'facingDropdown' => $facingDropdown,
            'noticePeriodDropdown' => $noticePeriodDropdown,
            //mehul change over
            'propertyOwnerDropdown' => $propertyOwnerDropdown,
            'roomSharingDropdown' => $roomSharingDropdown,
            'societyamenityIDropdown' => $societyamenityIDropdown,
            'floorIDropdown' => $floorIDropdown,
            'availablebhkIDropdown' => $availablebhkIDropdown,
            'ruleschk' => $ruleschk,
            'societyamenitychk' => $societyamenitychk,
            'facilitieschk' => $facilitieschk,
            'interiorschk' => $interiorschk,
            'furniturechk' => $furniturechk,
            'mealschk' => $mealschk,
            'availableTypeDropdown' => $availableTypeDropdown,
            'propertyAge' => $propertyAge,
            //'area' => $areaData,
        ];
        return view('backend.property.add', ["page_title" => "Add Property - ", 'dropdowns' => $dropdowns]);
    }

    public function store(Request $request) {

        try {

            $errors = array();
            $data = array();


            $rules = array(
                //'availability' => 'required',
                'propertyType' => 'required',
                'companyName' => 'required',
                'companyNumber' => 'required',
                'address' => 'required',
                'landmark' => 'required',
                'state' => 'required',
                'city' => 'required',
                'area' => 'required',
                'mealsIncludedID' => 'required',
                'status' => 'required',
                //mehul change
                'numberOfRoom' => 'required',
                'facing' => 'required',
                'noticePeriod' => 'required',
                'deposit' => 'required'
                    //mehul change over
            );

            $messages = [
                //'availability.required' => 'Please select availability',
                'propertyType.required' => 'Please select property type',
                'companyName.required' => 'Please enter company name',
                'companyNumber.required' => 'Please enter company number',
                'address.required' => 'Please enter address',
                'landmark.required' => 'Please enter landmark',
                'state.required' => 'Please select state',
                'city.required' => 'Please select city',
                'area.required' => 'Please select area',
                'propertyType.required' => 'Please select property type',
                'mealsIncludedID.required' => 'Please select meals included',
                'status.required' => 'Please select status',
                //mehul change
                'numberOfRoom.required' => 'Please select Number of room',
                'facing.required' => 'Please select Facing',
                'noticePeriod.required' => 'Please select Notice Period',
                'deposit.required' => 'Please enter deposit',
                    //mehul change over
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {

                $messages_arr = $validator->messages()->getMessages();
                if (count($messages_arr) > 0) {
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_" . $key] = $value_arr[0];
                    }
                }
                return redirect()->route('admin_add_property')
                                ->withErrors($errors)
                                ->withInput();
            }
            if (empty($errors)) {

                $propertyObj = new Property();
                $id = $propertyObj->saveProperty($request);
                if ($id > 0) {
                    Session::flash('message', 'Record added successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->route('admin_property');
                } else {
                    Session::flash('message', 'Record addition error.');
                    Session::flash('msgclass', 'alert-danger');
                }
            }
        } catch (Exception $e) {
            Session::flash('message', 'Record addition error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_property');
        }
        return redirect()->route('admin_property');
    }

    public function edit($encoded_id = "") {
        if (!empty($encoded_id)) {
            $propertyObj = new Property();
            $id = base64_decode($encoded_id);
            $data = $propertyObj->getRecordById($id);

            if (!empty($data['data'])) {
                $stateDropdown = GlobalHelpers::stateDropdown();
                $cityDropdown = GlobalHelpers::cityDropdown($data['data']->stateID);
                $areaDropdown = GlobalHelpers::areaDropdown($data['data']->stateID, $data['data']->cityID);
                $propertyTypeDropdown = GlobalHelpers::propertyTypeDropdown();
//mehul change
                $numberOfRoomDropdown = GlobalHelpers::numberOfRoomArr();
                $facingDropdown = GlobalHelpers::facingArr();
                $noticePeriodDropdown = GlobalHelpers::noticePeriodArr();
                //mehul change over
                $conditionDropdown = GlobalHelpers::conditionArr();

                $mealsIncludedDropdown = GlobalHelpers::mealsIncludedArr();
                $societyamenityIDropdown = GlobalHelpers::societyamenityArr();
                $floorIDropdown = GlobalHelpers::floorArr();
                $constantRols = config('constants.ADMIN_ROLES');

                $propertyOwnerDropdown = User::userDropdown('roleID', $constantRols['Owner']);
                $roomSharingDropdown = GlobalHelpers::roomSharingDropdown();
                $availablebhkIDropdown = GlobalHelpers::availablebhkArr();
				$availableTypeDropdown = GlobalHelpers::availableTypeArr();
				$propertyAge = GlobalHelpers::propertyAge();

                $rulesObj = new Rules();
                $ruleschk = $rulesObj->getAllRecord();
                $societyamenityObj = new Societyamenity();
                $facilitiesObj = new Facilities();
                $interiorsObj = new Interiors();
                $furnitureObj = new Furniture();
                $societyamenitychk = $societyamenityObj->getAllRecord();
                $facilitieschk = $facilitiesObj->getAllRecord();
                $interiorschk = $interiorsObj->getAllRecord();
                $furniturechk = $furnitureObj->getAllRecord();
				
				$mealschk = array(1=>"Breakfast",2=>"Lunch",3=>"Dinner",4=>"Not Included");
				$suitableFor = array(1=>"Students",2=>"Working Professional",3=>"Student & Working Professional");
				$area = $propertyObj->getAllArea();

                //$cityDropdown = $propertyObj->getRecordById($data['data']->propertyID);
                $data['data']->propertyID = base64_encode($data['data']->propertyID);
                $data['data']->roomSharingData = $data['roomSharingData'];
                $data['dropdowns'] = [
                    'stateDropdown' => $stateDropdown,
                    'cityDropdown' => $cityDropdown,
                    'areaDropdown' => $areaDropdown,
                    'propertyTypeDropdown' => $propertyTypeDropdown,
                    'mealsIncludedDropdown' => $mealsIncludedDropdown,
                    'conditionDropdown' => $conditionDropdown,
                    //mehul change
                    'numberOfRoomDropdown' => $numberOfRoomDropdown,
                    'facingDropdown' => $facingDropdown,
                    'noticePeriodDropdown' => $noticePeriodDropdown,
                    //mehul change over
                    'propertyOwnerDropdown' => $propertyOwnerDropdown,
                    'roomSharingDropdown' => $roomSharingDropdown,
                    'societyamenityIDropdown' => $societyamenityIDropdown,
                    'floorIDropdown' => $floorIDropdown,
                    'availablebhkIDropdown' => $availablebhkIDropdown,
                    'ruleschk' => $ruleschk,
                    'societyamenitychk' => $societyamenitychk,
                    'facilitieschk' => $facilitieschk,
                    'interiorschk' => $interiorschk,
                    'furniturechk' => $furniturechk,
                    'availableTypeDropdown' => $availableTypeDropdown,
					'mealschk' => $mealschk,
					'area' => $area,
					'suitableFor' => $suitableFor,
					'propertyAge' => $propertyAge,
                ];
//                        echo "<pre>";
//print_r($data['data']);exit;
                return view('backend.property.edit', ["page_title" => "Edit Property - ", 'data' => $data['data'], 'dropdowns' => $data['dropdowns']]);
            } else {
                Session::flash('message', 'Data does not exist.');
                Session::flash('msgclass', 'alert-danger');
                return redirect()->route('admin_property');
            }
        } else {

            Session::flash('message', 'Url does not exist.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_property');
        }
    }

    public function update(Request $request) {
//        echo "<pre>"; print_r($request->all());exit;
        try {
            $errors = array();
            $data = array();


            $rules = array(
//                'title'          => 'required',
                'companyName' => 'required',
                'companyNumber' => 'required',
                'state' => 'required',
                'city' => 'required',
                'area' => 'required',
                'address' => 'required',
                'propertyType' => 'required',
                'condition' => 'required',
                'landmark' => 'required',
                'mealsIncludedID' => 'required',
                'status' => 'required',
                'availability' => 'required',
                //mehul change
                'numberOfRoom' => 'required',
                'facing' => 'required',
                'noticePeriod' => 'required',
                'deposit' => 'required'
                    //mehul change over
            );

            $messages = [
//              'title.required' => 'Please enter title.',
                'companyName.required' => 'Please enter company name.',
                'companyNumber.required' => 'Please enter company number.',
                'state.required' => 'Please select state.',
                'city.required' => 'Please select city.',
                'area.required' => 'Please select area.',
                'address.required' => 'Please enter address.',
                'propertyType.required' => 'Please select property type.',
                'condition.required' => 'Please select condition.',
                'landmark.required' => 'Please enter landmark.',
                'mealsIncludedID.required' => 'Please select meals included.',
                'status.required' => 'Please select status.',
                'availability.required' => 'Please select availability.',
                //mehul change
                'numberOfRoom.required' => 'Please select Number of room',
                'facing.required' => 'Please select Facing',
                'noticePeriod.required' => 'Please select Notice Period',
                'deposit.required' => 'Please enter deposit',
                    //mehul change over
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {

//                print_r($validator->messages()->getMessages());exit;

                $messages_arr = $validator->messages()->getMessages();
                if (count($messages_arr) > 0) {
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_" . $key] = $value_arr[0];
                    }
                }
                return redirect()->back()
                                ->withErrors($errors)
                                ->withInput();
            }

            if (empty($errors)) {

                /* $data['errors']  = $errors;

                  } else { */

                $propertyObj = new Property();
                $result = $propertyObj->updateProperty($request);
                if ($result) {
                    Session::flash('message', 'Record updated successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->back();
                } else {
                    Session::flash('message', 'Record updation error.');
                    Session::flash('msgclass', 'alert-danger');
                }
            }
            //return response($data);
        } catch (Exception $e) {
            //echo $e->getMessages();
            Session::flash('message', 'Record updation error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->back();
        }
    }
	
	public function updateStatus($encoded_id="", $status)
    {
        try {
			$propertyObj = new Property();
			$id = base64_decode($encoded_id);
			$data['result'] = $propertyObj->updateRecordById($id, $status);
			if($data['result'] == 'true'){
				Session::flash('message', 'Record updated successfully.');
				Session::flash('msgclass', 'alert-success');
			}
        } catch (Exception $e) {
            Session::flash('message', 'Record could not be deleted.');
            Session::flash('msgclass', 'alert-danger');
        }
        return redirect()->back();
    }
	public function bulkUpdate(Request $request)
    {
        try {
			$propertyObj = new Property();
			$data['result'] = $propertyObj->updateRecordById($request->property_id, $request->action);
			if($data['result'] == 'true'){
				Session::flash('message', 'Record updated successfully.');
				Session::flash('msgclass', 'alert-success');
			}
        } catch (Exception $e) {
            Session::flash('message', 'Record could not be updated.');
            Session::flash('msgclass', 'alert-danger');
        }
        return redirect()->back();
    }

    public function destroy($encoded_id = "") {
        /* if(!empty($encoded_id))
          { */
        try {
            $propertyObj = new Property();
            $id = base64_decode($encoded_id);

            $data['data'] = $propertyObj->getRecordById($id);
            if (!empty($data['data'])) {
                $data['result'] = $propertyObj->deleteRecordById($id);
                if ($data['result'] == 'true') {
                    Session::flash('message', 'Record is deleted successfully.');
                    Session::flash('msgclass', 'alert-success');
                    //$result['result'] = true;
                    //return response($result);
                }
            } else {
                Session::flash('message', 'Record does not exist.');
                Session::flash('msgclass', 'alert-danger');
                //$result['result'] = false;
                //return response($result);
            }
        } catch (Exception $e) {
            Session::flash('message', 'Record could not be deleted.');
            Session::flash('msgclass', 'alert-danger');
            //$result['result'] = false;
            //return response($result);
        }
        return redirect()->back();
        //$result['result'] = false;
        //return response($result);
        /* }else{

          Session::flash('message', 'Url does not exist.');
          Session::flash('msgclass', 'alert-danger');
          return redirect()->route('admin_property');
          } */
    }

//    mehul change
    public function deleteImage($delid) {
        $propertyObj = new Property();
        $getimagedata = $propertyObj->getphotoRecordById($delid);
        $photoname = $getimagedata->photoName;
        $path = 'images/property';
        $file = ImageHelpers::getMediumImage($path, $photoname);
        $file_headers = @get_headers($file);
        $explodedata = explode('/', $file);
        if (isset($explodedata[6]) && $explodedata[6] != '') {
            $imagepath = $_SERVER['DOCUMENT_ROOT'] . '/storage/' . $explodedata[3] . '/' . $explodedata[4] . '/' . $explodedata[5] . '/' . $explodedata[6];
        } else {
            $imagepath = '';
        }

        if ($delid != '') {
            if ($file_headers[0] != 'HTTP/1.1 404 Not Found') {
                if (isset($explodedata[6]) && $explodedata[6] != '') {
                    unlink($imagepath);
                }
            }
            $propertyObj->deletephotoById($delid);
            return true;
        }
    }

//    mehul change over

    public function getAllCity($stateID = null) {
        //print_r($request->all());

        /* $stateID = ($request->input('stateID') != "") ? $request->input('stateID') : null;
          $cityID = ($request->input('cityID') != "") ? $request->input('cityID') : null; */
        $cityArr = GlobalHelpers::cityDropdown($stateID);

        $cityDropdown = "";
        $cityDropdown = "<option value=''>Select City</option>";
        foreach ($cityArr as $key => $value) {
            $cityDropdown .= "<option value='" . $key . "'>" . $value . "</option>";
        }
        return $cityDropdown;
    }

    public function getAllArea($stateID = null, $cityID = null) {

        /* $stateID = ($request->input('stateID') != "") ? $request->input('stateID') : null;
          $cityID = ($request->input('cityID') != "") ? $request->input('cityID') : null;
          $areaID = ($request->input('areaID') != "") ? $request->input('areaID') : null; */
        $areaArr = GlobalHelpers::areaDropdown($stateID, $cityID);

        $areaDropdown = "";
        $areaDropdown = "<option value=''>Select Area</option>";

        foreach ($areaArr as $key => $value) {

            $areaDropdown .= "<option value='" . $key . "'>" . $value . "</option>";
        }
        return $areaDropdown;
    }

}
