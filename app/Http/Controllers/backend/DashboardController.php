<?php

namespace App\Http\Controllers\backend;

use Auth;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\backend\Dashboard;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $res_arr['error'] = false;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dashboardObj = new Dashboard();
		
        //$data['properties'] = $dashboardObj->getProperty();
		//$data['total_propertyCount'] = $dashboardObj->getPropertyCountByStatus();
        $data['total_userCount'] = $dashboardObj->getUserCountByStatus();

        $data['active_propertyCount'] = $dashboardObj->getPropertyCountByStatus("Active");
        $data['active_userCount'] = $dashboardObj->getUserCountByStatus("Active");
        
        $data['inactive_propertyCount'] = $dashboardObj->getPropertyCountByStatus("Inactive");
        $data['inactive_userCount'] = $dashboardObj->getUserCountByStatus("Inactive");

		$data['deleted_propertyCount'] = $dashboardObj->getPropertyCountByStatus("Delete");
        $data['deleted_userCount'] = $dashboardObj->getUserCountByStatus("Delete");
        
		$data['admin_userCount'] = $dashboardObj->getUserCountByType("1");
		$data['manager_userCount'] = $dashboardObj->getUserCountByType("3");
		$data['owner_userCount'] = $dashboardObj->getUserCountByType("4");
		$data['active_ownerCount'] = $dashboardObj->getUserCountByType("4","Active");
		$data['delete_ownerCount'] = $dashboardObj->getUserCountByType("4","Delete");
		
		
		
		$data['user_userCount'] = $dashboardObj->getUserCountByType("2");
		$data['active_userCount'] = $dashboardObj->getUserCountByType("2","Active");
		$data['delete_userCount'] = $dashboardObj->getUserCountByType("2","Delete");
		
		/*
		$data['villa_propertyCount'] = $dashboardObj->getPropertyCountByType("1");
		$data['pg_propertyCount'] = $dashboardObj->getPropertyCountByType("2");
		$data['service_app_propertyCount'] = $dashboardObj->getPropertyCountByType("3");
		$data['home_propertyCount'] = $dashboardObj->getPropertyCountByType("4");
		*/
		
		/*
		$data['property_icon'][0] = 'fa-home';
		$data['property_icon'][1] = 'fa-building';
		$data['property_icon'][2] = 'fa-tree';
		$data['property_icon'][3] = 'fa-ellipsis-h';
		*/
		
		$data['property_active'] = $dashboardObj->getAllPropertyWithActiveCount();
		$data['property_delete'] = $dashboardObj->getAllPropertyWithDeleteCount();
		
        return view('backend.dashboard')->with(["page_title"=>"Dashboard - ",'data'=>$data]);
    }
}
