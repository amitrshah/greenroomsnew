<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\backend\Furniture;
use Validator;
use Session;

class FurnitureController extends Controller
{

    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {        
        $furnitureObj = new Furniture();
        
        $res_arr = $furnitureObj->getRecords($furnitureObj);
        //print_r($res_arr);exit;
        return view('backend.furniture.list')->with(["page_title"=>"Furniture - ",'records' => $res_arr]);
        
    }

    /**
     * Show the room sharing add form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.furniture.add')->with(["page_title"=>"Add Furniture - "]);
    }

    public function store(Request $request){
        try {
            
            $errors = array();
            $data = array(); 
            

            $rules = array(
                'title'          => 'required',
                'status'         =>'required'
            );

            $messages = [
              'title.required' => 'Please enter title.',
              'status.required' => 'Please select status.',
            ]; 

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){

                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                return redirect()->route('admin_add_furniture')
                        ->withErrors($errors)
                        ->withInput();
            }
            if (empty($errors)) {
             
                $furnitureObj = new Furniture();
                $id = $furnitureObj->saveFurniture($request);
                if($id > 0){
                    Session::flash('message', 'Record added successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->route('admin_furniture');
                }else{
                    Session::flash('message', 'Record addition error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }            
        } catch (Exception $e) {            
            Session::flash('message', 'Record addition error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_furniture');
        }
        return redirect()->route('admin_furniture');
    }

    public function edit($encoded_id="")
    {
        if(!empty($encoded_id))
        {
            $furnitureObj = new Furniture();
            $id = base64_decode($encoded_id);
            $data = $furnitureObj->getRecordById($id);
            if(!empty($data)){
                return view('backend.furniture.edit')->with(["page_title"=>"Edit Furniture - ",'data' => $data]);
            }else{
                Session::flash('message', 'Data does not exist.');
                Session::flash('msgclass', 'alert-danger');
                return redirect()->route('admin_furniture');
            }
        }else{
            
            Session::flash('message', 'Url does not exist.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('admin_furniture');
        }
    }

    public function update(Request $request){
        try {
            $errors = array();
            $data = array(); 
            

            $rules = array(
                'title'          => 'required',
                'status'         =>'required'
            );

            $messages = [
              'title.required' => 'Please enter title.',
              'status.required' => 'Please select status.',
            ]; 

            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){

                //print_r($validator->messages()->getMessages());exit;

                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                return redirect()->back()
                        ->withErrors($errors)
                        ->withInput();
            }
            if(empty($errors)) {
              
              
                $furnitureObj = new Furniture();
                $result = $furnitureObj->updateFurniture($request);
                if($result){
                    Session::flash('message', 'Record updated successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->back();
                }
                else{
                    Session::flash('message', 'Record updation error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }
            //return response($data);
        } catch (Exception $e) {
            //echo $e->getMessages();
            Session::flash('message', 'Record updation error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->back();
        }   
    }

    public function destroy($encoded_id="")
    {
        /*if(!empty($encoded_id))
        {*/
            try {
                $furnitureObj = new Furniture();
                $id = base64_decode($encoded_id);

                $data['data'] = $furnitureObj->getRecordById($id);
                if(!empty($data['data'])){
                    $data['result'] = $furnitureObj->deleteRecordById($id);
                    if($data['result'] == 'true'){
                        Session::flash('message', 'Record is deleted successfully.');
                        Session::flash('msgclass', 'alert-success');
                       
                    }
                }else{
                    Session::flash('message', 'Record does not exist.');
                    Session::flash('msgclass', 'alert-danger');
                   
                }
            } catch (Exception $e) {
                Session::flash('message', 'Record could not be deleted.');
                Session::flash('msgclass', 'alert-danger');
               
            }
            return redirect()->back();
           
    }
}
