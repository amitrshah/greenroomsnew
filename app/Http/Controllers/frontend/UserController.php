<?php

namespace App\Http\Controllers\frontend;

use Auth;
use Hash;
use App\Model\frontend\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Validator;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $res_arr['error'] = false;
    }

    public function showChangePasswordForm()
    {
        $breadcrumbs = Array();
        /*$breadcrumbs = [
                        ['title'=>'Home','url'=>route('user_home'),'last'=>false],
                        ['title'=>'Change Password','last'=>true]
                    ];*/
        return view('frontend.user.change_password')->with(["page_title"=>"Change Password - "]);   
    }

    public function changePassword(Request $request)
    {
    	try {
	        $userObj = new User();
	        $input_data = $request->all();
	        $input_data['password'] = (isset($input_data['password'])) ? $input_data['password'] : "";
	        $input_data['oldPassword'] = (isset($input_data['oldPassword'])) ? $input_data['oldPassword'] : "";
	        $input_data['confirmPassword'] = (isset($input_data['confirmPassword'])) ? $input_data['confirmPassword'] : "";

	        $input_data['email'] = Auth::guard('user')->user()->email;
	        $user_password = Auth::guard('user')->user()->password;
	        

	        //echo "<pre>";print_R($input_data);exit;
	        /*$confirmPassword = isset($input_arr['confirmPassword']) ? $input_arr['confirmPassword'] : '';
	        $oldPassword = isset($input_arr['oldPassword']) ? $input_arr['oldPassword'] : '';
	        $password = isset($input_arr['password']) ? $input_arr['password'] : '';*/
	        //$email = isset($input_arr['email']) ? $input_arr['email'] : '';
	        
	        /*$field_arr = array();
	        $fields['password'] = array('value' => $password, 'rules' => 'required|max:255|min:6');
	        $fields['confirmPassword'] = array('value' => $confirmPassword, 'rules' => 'required|same:password|min:6|max:255');
	        $fields['oldPassword'] = array('value' => $oldPassword, 'rules' => 'required|min:6|max:255');*/

	        Validator::extend('pwdvalidation', function($field, $value, $parameters)
	        {
	            return Hash::check($value, Auth::guard('user')->user()->password);
	        });
	            

	        $rules = array(
	            'password'          => 'required|max:255|min:6',
	            'confirmPassword'   =>'required|same:password|min:6|max:255',
	            'oldPassword'   =>'required|pwdvalidation|min:6|max:255',
	        );

	        /*$messages = [
	          'oldPassword.required' => 'Please enter old password.',
	          'oldPassword.pwdvalidation' => 'Please enter currect old password.',
	          'password.required' => 'Please enter password.',
	          'confirmPassword.required' => 'Please enter confirm password.',
	        ];*/


	        $messages = array('pwdvalidation' => 'The old password is incorrect.');



	            $validator = Validator::make($request->all(), $rules, $messages);

	            if($validator->fails()){
	            	/*echo "<pre>";
	            	print_r($validator->messages()->getMessages());exit;*/
	                //print_r($validator->messages()->getMessages());exit;

	                $messages_arr = $validator->messages()->getMessages();
	                if(count($messages_arr) > 0){
	                    foreach ($messages_arr as $key => $value_arr) {
	                        $errors["label_".$key] = $value_arr[0];
	                    }
	                }
	                return redirect()->route('profile.changePassword')
	                        ->withErrors($errors)
	                        ->withInput();
	            }


	        //$res_arr = $userObj->validateUserChangePassword($input_data);
	        //echo "<pre>";print_R($res_arr);exit;
	        
	        if (empty($errors)) {
	            if($userObj->changePassword($input_data)) {     
	                Session::flash('message', 'Your password has been updated successfully..');
	                Session::flash('msgclass', 'alert-success');
	                return redirect()->back();
	            }
	            else{
	                Session::flash('message', 'Error while updating your password.');
	                Session::flash('msgclass', 'alert-danger');
	            }
	        }
        } catch (Exception $e) {
            //echo $e->getMessages();
            Session::flash('message', 'Error while updating your password.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->back();
        }   
    }

    public function profile()
    {
        $user_id = Auth::guard('user')->user()->id;
        if(!empty($user_id))
        {
            $userObj = new User();
            $data = $userObj->getUserRecordById($user_id);
            if(!empty($data)){
                return view('frontend.user.profile')->with(["page_title"=>"My Favourite - ",'data' => $data]);
            }else{
                Session::flash('message', 'Data does not exist.');
                Session::flash('msgclass', 'alert-danger');
                return redirect()->route('search');
            }
        }else{
            
            Session::flash('message', 'User is not logged in.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('search');
        }
    }


    public function profileUpdate(Request $request){
        try {
            $rules = array(
                'name'          => 'required',
                'firstName'          => 'required',
            );

            $messages = [
              'name.required' => 'Please enter name.',
              'firstName.required' => 'Please enter first name.',
            ];
            
            $validator = Validator::make($request->all(), $rules, $messages);
            if($validator->fails()){
                //print_r($validator->messages()->getMessages());exit;

                $messages_arr = $validator->messages()->getMessages();
                if(count($messages_arr) > 0){
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_".$key] = $value_arr[0];
                    }
                }
                    return redirect()->back()
                        ->withErrors($errors)
                        ->withInput();
            }

            if(empty($errors)) {
              
              /*$data['errors']  = $errors;

            } else {*/

                $userObj = new User();
                $result = $userObj->profileUpdate($request);
                if($result){
                    Session::flash('message', 'Record updated successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->back();
                }
                else{
                    Session::flash('message', 'Record updation error.');
                    Session::flash('msgclass', 'alert-danger'); 
                }
            }
            return redirect()->back();
        } catch (Exception $e) {
            //echo $e->getMessages();
            Session::flash('message', 'Record updation error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->back();
        }  
    }

}
