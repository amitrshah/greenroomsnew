<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\frontend\Search;
use App\Model\frontend\User;
use App\Libraries\GlobalHelpers;
use App\Libraries\ImageHelpers;
use DB;
use Auth;

class SearchController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('user');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        /* $products = DB::table('users')->paginate($record_per_page); */
        $dataArray = array();
        $searchObj = New Search();
        $dataArray['roomSharing'] = GlobalHelpers::roomSharingDropdown('Active');
        $dataArray['propertyType'] = GlobalHelpers::propertyTypeDropdown('Active');
        $dataArray['facilities'] = GlobalHelpers::facilitiesDropdown();
        $dataArray['conditionArr'] = GlobalHelpers::conditionArr();
        $dataArray['mealsIncludedID'] = GlobalHelpers::mealsIncludedArr();
        $record_per_page = env('RECORDS_PER_PAGE_FRONTEND');

        if ($request->ajax()) {

            $query = DB::table('property');
            $query->where('property.status', '=', "Active");
            if (!empty($request->occupancy) || !empty($request->minPrice) || !empty($request->maxPrice)) {
                if (!empty($request->maxPrice)) {

                    $query->where('ps.price', '<=', $request->maxPrice);
                }
                if (!empty($request->minPrice)) {

                    $query->where('ps.price', '>=', $request->minPrice);
                }
                if (!empty($request->occupancy)) {

                    $query->whereIn('ps.roomSharingID', $request->occupancy);
                }
            }
            if (!empty($request->facilities)) {

                $query->leftJoin('room_facilities as rf', 'property.propertyID', '=', 'rf.propertyID');
                foreach ($request->facilities as $key => $value) {
                    $query->where("rf.$value", '=', '1');
                }
            }
            if (!empty($request->propertyType)) {

                $query->whereIn('property.propertyTypeID', $request->propertyType);
            }

            if (!empty($request->mealsIncludedID)) {
                $strMeals = implode(',', $request->mealsIncludedID);
                //if(count($request->mealsIncluded) < 2){

                $query->where('property.mealsIncludedID', 'like', "%" . $strMeals . "%");
                /* }else{

                  foreach ($request->mealsIncluded as $key => $value) {
                  $tempMeals[] = implode(',', $request->mealsIncluded);

                  }
                  $query->whereIn('property.mealsIncluded',$request->mealsIncluded);
                  } */
            }

            if (!empty($request->gender)) {
                /*

                  $query->where('property.gender',"like", "%".$str); */
                $str = implode(',', $request->gender);
                if (count($request->gender) > 1) {
                    $query->where('property.gender', "like", "%" . $str);
                } else {
                    /* $str = implode(',', $request->gender); */
                    $query->whereRaw("FIND_IN_SET('$str',gender)");
                }
            }
            if (!empty($request->searchBarArea)) {
                $query->where('property.areaID', $request->searchBarArea);
                $dataArray['searchBarCity'] = $request->searchBarCity;
                $dataArray['searchBarArea'] = $request->searchBarArea;
            }
            $query->leftJoin('property_sharing as ps', 'property.propertyID', '=', 'ps.propertyID');
            $query->leftjoin('property_photo as pp', 'property.propertyID', '=', 'pp.propertyID');


            if (Auth::guard('user')->check()) {

                $query->select('property.*', DB::raw('MIN(ps.price) AS min_propprice'), DB::raw('group_concat(pp.photoName) AS photo'), DB::raw('group_concat(pp.photoName) AS photo'), DB::raw("(select propertyID from favourite where userID=" . Auth::guard('user')->user()->id . " and propertyID = property.propertyID) as favourite"));
            } else {
                $query->select('property.*', DB::raw('MIN(ps.price) AS min_propprice'), DB::raw('group_concat(pp.photoName) AS photo'));
            }


            //$properties->select('property.title as title')->paginate($record_per_page);
            $query->groupBy('property.propertyID');
            $dataArray['properties'] = $query->paginate($record_per_page);
            foreach ($dataArray['properties'] as $key => $value) {
                $file = '';
                /* if(!empty($value->photo)){
                  $photos = explode(',', $value->photo);
                  $path = 'images/property';
                  $imgName = $photos[0];
                  $file = str_replace("\\", "/",ImageHelpers::getSmallImage($path, $imgName));
                  } */
                if (!empty($value->photo)) {
                    $photos = explode(',', $value->photo);
                    $path = 'images/property';
                    $imgName = $photos[0];
                    $file = str_replace("\\", "/", ImageHelpers::getSmallImage($path, $imgName));
                    $file1 = str_replace("\\", "/", ImageHelpers::getMediumImage($path, $imgName));
                } else {
                    $path = 'images/property';
                    $file = str_replace("\\", "/", ImageHelpers::getSmallImage($path));
                    $file1 = str_replace("\\", "/", ImageHelpers::getMediumImage($path));
                }
                $dataArray['properties']["$key"]->file = $file;
                $dataArray['properties']["$key"]->file1 = $file1;
            }
            if (Auth::guard('user')->check()) {
                $userObj = new User();
                $resultPlanCity = $userObj->userPlanCity(Auth::guard('user')->user()->id);
                if ($resultPlanCity) {
                    foreach ($resultPlanCity as $key => $value) {
                        $dataArray['userPlanCity'][$value->cityID] = $value->cityID;
                    }
                }
            }

            return view('frontend.search.search_list', ['dataArray' => $dataArray]);
        } else {
            $dataArray['favouritePropertyListPopup'] = array();
            $dataArray['test'] = array();
            if (Auth::guard('user')->check()) {

                $dataArray['test'] = $searchObj->getFavourite();

                if (!empty($dataArray['test'])) {
                    foreach ($dataArray['test'] as $key => $value) {
                        $dataArray['favouritePropertyListPopup'][$value->propertyID] = $value;
                        $file = '';
                        if (!empty($value->photo)) {
                            $photos = explode(',', $value->photo);
                            $path = 'images/property';
                            $imgName = $photos[0];
                            $file = str_replace("\\", "/", ImageHelpers::getSmallImage($path, $imgName));
                        } else {
                            $path = 'images/property';
                            $file = str_replace("\\", "/", ImageHelpers::getSmallImage($path));
                        }
                        $dataArray['favouritePropertyListPopup'][$value->propertyID]->file = $file;
                    }
                }
            }
            //echo "<pre>";print_r($dataArray['favouritePropertyListPopup']);exit;
            unset($dataArray['test']);
            /* echo "<pre>";
              print_r($dataArray);exit; */


            if (Auth::guard('user')->check()) {

                $query = DB::table('property')->select('property.*', DB::raw('MIN(ps.price) AS min_propprice'), DB::raw('group_concat(pp.photoName) AS photo'), DB::raw("(select propertyID from favourite where userID=" . Auth::guard('user')->user()->id . " and propertyID = property.propertyID) as favourite"));
            } else {
                $query = DB::table('property')->select('property.*', DB::raw('MIN(ps.price) AS min_propprice'), DB::raw('group_concat(pp.photoName) AS photo'));
            }


            $query->leftJoin('property_sharing as ps', 'property.propertyID', '=', 'ps.propertyID')->leftjoin('property_photo as pp', 'property.propertyID', '=', 'pp.propertyID');
            $query->where('property.status', '=', "Active");
            if (!empty($request->searchBarArea)) {
                $query->where('property.areaID', $request->searchBarArea);
                $dataArray['searchBarCity'] = $request->searchBarCity;
                $dataArray['searchBarArea'] = $request->searchBarArea;
            }
            $query->groupBy('property.propertyID');
            $dataArray['properties'] = $query->paginate($record_per_page);

            foreach ($dataArray['properties'] as $key => $value) {
                $file = '';
                if (!empty($value->photo)) {
                    $photos = explode(',', $value->photo);
                    $path = 'images/property';
                    $imgName = $photos[0];
                    $file = str_replace("\\", "/", ImageHelpers::getSmallImage($path, $imgName));
                    $file1 = str_replace("\\", "/", ImageHelpers::getMediumImage($path, $imgName));
                } else {
                    $path = 'images/property';
                    $file = str_replace("\\", "/", ImageHelpers::getSmallImage($path));
                    $file1 = str_replace("\\", "/", ImageHelpers::getMediumImage($path));
                }
                $dataArray['properties']["$key"]->file = $file;
                $dataArray['properties']["$key"]->file1 = $file1;
            }
            $dataArray['userFavCounter'] = (Auth::guard('user')->check() ? Auth::guard('user')->user()->remainingFavCount != null ? Auth::guard('user')->user()->remainingFavCount : 0 : 0);
            $dataArray['userPlanCityID'] = (Auth::guard('user')->check() ? Auth::guard('user')->user()->planCityID != null ? Auth::guard('user')->user()->planCityID : 0 : 0);
            $dataArray['isPremiumUser'] = (Auth::guard('user')->check() ? Auth::guard('user')->user()->premiumUser != null ? Auth::guard('user')->user()->premiumUser : 0 : 0);
            if (Auth::guard('user')->check()) {
                $userObj = new User();
                $resultPlanCity = $userObj->userPlanCity(Auth::guard('user')->user()->id);
                if ($resultPlanCity) {
                    foreach ($resultPlanCity as $key => $value) {
                        $dataArray['userPlanCity'][$value->cityID] = $value->cityID;
                    }
                }
            }
        }
        /* echo "<pre>";
          print_r($dataArray);exit; */
        return view('frontend.search', ["page_title" => "Search Property - ", 'dataArray' => $dataArray]);
    }

    public function getFavouritePropertyList(Request $request) {
        $searchObj = new Search();
        $dataArray['favouritePropertyListPopup'] = array();
        $result = $searchObj->getFavourite();
        if (!empty($result)) {
            foreach ($result as $key => $value) {
                $dataArray['favouritePropertyListPopup'][$value->propertyID] = $value;
                $file = '';
                if (!empty($value->photo)) {
                    $photos = explode(',', $value->photo);
                    $path = 'images/property';
                    $imgName = $photos[0];
                    $file = str_replace("\\", "/", ImageHelpers::getSmallImage($path, $imgName));
                } else {
                    $path = 'images/property';
                    $file = str_replace("\\", "/", ImageHelpers::getSmallImage($path));
                }
                $dataArray['favouritePropertyListPopup'][$value->propertyID]->file = $file;
            }
        }
        return view('frontend.search.favourite_property_list_popup', ['dataArray' => $dataArray]);
    }

    public function propertySidebar(Request $request) {
        /* $products = DB::table('users')->paginate($record_per_page); */
        $propertyInfo = array();
        $propertySharingInfo = array();
        $propertyPhotoInfo = array();
        $userPlanCity = array();

        if (!empty($request->slug)) {
            $dataArray = array();
            $query = DB::table('property');
            if (Auth::guard('user')->check()) {

                $query->select('property.*', DB::raw("(select propertyID from favourite where userID=" . Auth::guard('user')->user()->id . " and propertyID = property.propertyID) as favourite"));
            } else {
                $query->select('property.*');
            }
            $query->where('slug', '=', $request->slug);
            $propertyInfo = $query->first();

            if (!empty($propertyInfo)) {
                if (!empty($propertyInfo->rules)) {
                    $propertyInfo->rules = explode(PHP_EOL, $propertyInfo->rules);
                }
            }
            $query1 = DB::table('property_sharing as ps');
            $query1->leftJoin('room_facilities as rf', 'rf.propertyRoomSharingID', '=', 'ps.id');
            $query1->leftJoin('room_sharing as rs', 'rf.propertyRoomSharingID', '=', 'rs.id');
            $propertySharingInfo = $query1->where('ps.propertyID', '=', $propertyInfo->propertyID)->get();
            $query2 = DB::table('property_photo')->select('photoName');
            $propertyPhoto = $query2->where('propertyID', '=', $propertyInfo->propertyID)->get();
            /* echo "<pre>";
              print_r($propertyInfo);exit; */
            if ($propertyPhoto->count()) {
                $propertyPhotoInfo = array();
                foreach ($propertyPhoto as $key => $value) {
                    $file = '';
                    /* if(!empty($value->photoName)){
                      $photos = explode(',', $value->photoName);
                      $path = 'images/property';
                      $imgName = $photos[0];
                      $file = str_replace("\\", "/",ImageHelpers::getSmallImage($path, $imgName));
                      } */
                    if (!empty($value->photoName)) {
                        $photos = explode(',', $value->photoName);
                        $path = 'images/property';
                        $imgName = $photos[0];
                        $file = str_replace("\\", "/", ImageHelpers::getSmallImage($path, $imgName));
                    } else {
                        $path = 'images/property';
                        $file = str_replace("\\", "/", ImageHelpers::getSmallImage($path));
                    }
                    $propertyPhotoInfo["$key"] = $file;
                }
                unset($propertyPhoto);
            } else {
                $path = 'images/property';
                $file = str_replace("\\", "/", ImageHelpers::getSmallImage($path));
                $propertyPhotoInfo[0] = $file;
            }
            if (Auth::guard('user')->check()) {
                $userObj = new User();
                $resultPlanCity = $userObj->userPlanCity(Auth::guard('user')->user()->id);
                if ($resultPlanCity) {
                    foreach ($resultPlanCity as $key => $value) {
                        $userPlanCity[$value->cityID] = $value->cityID;
                    }
                }
            }
        }
        /* echo "<pre>";
          print_r($propertyPhotoInfo); */
        return view('frontend.search.property_details_sidebar', ['propertyInfo' => $propertyInfo, 'propertySharingInfo' => $propertySharingInfo, 'propertyPhotoInfo' => $propertyPhotoInfo, "userPlanCity" => $userPlanCity]);
    }

    public function setFavourite(Request $request) {
        $Search = new Search();
        return $Search->setFavourite($request->slug);
    }

    public function propertyDetails($slug = null) {
        if (!empty($slug)) {
            $Search = new Search();
            $dataArray['roomSharing'] = GlobalHelpers::roomSharingDropdown('Active');
            $conditionArray = GlobalHelpers::conditionArr();
            $propertyTypeArray = GlobalHelpers::propertyTypeDropdown('Active');
            $mealsIncludedArray = GlobalHelpers::mealsIncludedArr();
            $dataArray['data'] = $Search->propertyDetails($slug);
            if (!empty($dataArray['data'])) {

                $dataArray['facilities'] = GlobalHelpers::facilitiesDropdown();
                $dataArray['areaDropdown'] = GlobalHelpers::areaDropdown($dataArray['data']['stateID'], $dataArray['data']['cityID'], $dataArray['data']['areaID']);
                $dataArray['cityDropdown'] = GlobalHelpers::cityDropdown($dataArray['data']['stateID'], $dataArray['data']['cityID']);
                $dataArray['stateDropdown'] = GlobalHelpers::stateDropdown($dataArray['data']['stateID']);
                if (!empty($dataArray['data']['cityID'])) {
                    $dataArray['searchBarCity'] = $dataArray['data']['cityID'];
                }
                if (!empty($dataArray['data']['areaID'])) {
                    $dataArray['searchBarArea'] = $dataArray['data']['areaID'];
                }


                if (Auth::guard('user')->check()) {
                    $userObj = new User();
                    $resultPlanCity = $userObj->userPlanCity(Auth::guard('user')->user()->id);
                    if ($resultPlanCity) {
                        foreach ($resultPlanCity as $key => $value) {
                            $dataArray['userPlanCity'][$value->cityID] = $value->cityID;
                        }
                    }
                }
                if (!empty($dataArray['data']['mealsIncludedID'])) {
                    $dataMealsArray = explode(',', $dataArray['data']['mealsIncludedID']);
                    unset($dataArray['data']['mealsIncludedID']);
                    foreach ($dataMealsArray as $key => $value) {

                        $dataArray['data']['mealsIncludedID'][$value] = $mealsIncludedArray[$value];
                    }
                    $dataArray['data']['mealsIncludedID'] = implode(', ', $dataArray['data']['mealsIncludedID']);
                }
                if (!empty($dataArray['data']['propertyTypeID'])) {

                    $dataArray['data']['propertyTypeID'] = $propertyTypeArray[$value];
                }
                if (!empty($dataArray['data']['conditionID'])) {

                    $dataArray['data']['conditionID'] = $conditionArray[$value];
                }
                //echo "<pre>";print_r($dataArray);exit;
                $dataArray['userFavCounter'] = (Auth::guard('user')->check() ? Auth::guard('user')->user()->remainingFavCount != null ? Auth::guard('user')->user()->remainingFavCount : 0 : 0);
                $dataArray['userPlanCityID'] = (Auth::guard('user')->check() ? Auth::guard('user')->user()->planCityID != null ? Auth::guard('user')->user()->planCityID : 0 : 0);
                $dataArray['isPremiumUser'] = (Auth::guard('user')->check() ? Auth::guard('user')->user()->premiumUser != null ? Auth::guard('user')->user()->premiumUser : 0 : 0);
//                echo "<pre>";
//                print_r($dataArray);
//                echo "</pre>";
               
                return view('frontend.search.property_details', ["page_title" => ucfirst($dataArray['data']['title']) . " - ", 'dataArray' => $dataArray]);
            } else {
                Session::flash('message', 'Data does not exist.');
                Session::flash('msgclass', 'alert-danger');
                return redirect()->route('search');
            }
        } else {
            return redirect()->route('search');
        }
    }

    //mehul change
    public function propertyDetail($propertyID) {
        $property = DB::table('property')
                ->leftJoin('city as ct', 'property.cityID', '=', 'ct.cityID')
                ->leftJoin('state as st', 'property.stateID', '=', 'st.stateID')
                ->leftJoin('area as ar', 'property.areaID', '=', 'ar.areaID')
                ->leftJoin('users as u', 'property.createdBy', '=', 'u.id')
                ->leftJoin('property_photo as pp', 'property.propertyId', '=', 'pp.propertyId')
                ->where('property.propertyID', $propertyID)
                ->first();

        $property_img = DB::table('property_photo')->where('propertyID', $propertyID)->get();
       if(!empty($property_img)){
        $path = 'images/property';
        foreach ($property_img as $key => $img) {
            $imgName = $img->photoName;
            $file = ImageHelpers::getOriginalImage($path, $imgName);
            $property->photo_url[$key] = $file;
            //$property->propertyID = $property->propertyID;
        }
       }else{
           $property->photo_url = '';
       }

        $facility_arr = explode(',', $property->facilityID);
        foreach ($facility_arr as $key => $facility_id) {
            $facility = DB::table('property_facilities')->where('facilitiesID', $facility_id)->first();
            $property->facility[$key] = $facility;
        }

        $societyamenityID = explode(',', $property->societyamenityID);
        foreach ($societyamenityID as $key => $society_amenity) {
            $societyamenity = DB::table('property_societyamenity')->where('societyamenityID', $society_amenity)->first();
            $property->societyamenity[$key] = $societyamenity;
        }

        $rulesID = explode(',', $property->rules);
        foreach ($rulesID as $key => $rule_id) {
            $rules = DB::table('property_rules')->where('rulesID', $rule_id)->where('status', 'Active')->first();
            $property->rule[$key] = $rules;
        }

//echo "<pre>"; print_r($property); die;
        return view('frontend.search.property_detail', ["page_title" => "Preperty Detail - ", 'property' => $property]);
    }
    

    //mehul change over

//ADD BY CT
    /**
     * Display details of properties
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchDetails($id){
        $data=[];
        $city_id=\DB::table('area')->select('cityID')->where("stateID","=","7", "AND","cityName","=","Ahmedabad City", "AND", "status","=","Active")->first();
        $ahmedabad_loc_=\DB::table('area')->select('areaID','areaName')->where('cityID',"=",$city_id->cityID,"AND","status","=","Active")->get();
        $society_aminities_title=[];
        $property_facilities=[];
        $property_interiors=[];
        $property_meals_array=[];
        $records=\App\Model\frontend\Property::where('propertyID',"=",$id)->first();
//        dd($records);
        $name=\DB::table("users")->select("name")->where("id","=",$records->createdBy)->first();
        $society_aminities = explode(",",$records->societyamenityID);
        $faciliety = explode(",",$records->facilityID);
        $society_aminities_array=\DB::table("property_societyamenity")->get();
        $society_aminities=\DB::table("property_societyamenity")->select("title")->whereIn("societyamenityID",$society_aminities)->get();

        $faciliety_array=\DB::table("property_facilities")->get();
        $faciliety=\DB::table("property_facilities")->select("title")->whereIn("facilitiesID",$faciliety)->get();

        $property_interiors_array=\DB::table('property_interiors')->get();
        $interiors = explode(",",$records->interiorID);
        $interiors=\DB::table('property_interiors')->select('title')->whereIN("interiorsID",$interiors)->get();

        $meals_array=GlobalHelpers::meals();
//        dd($meals_array);
        $extra_charge_array=GlobalHelpers::Extracharge();
        foreach ($society_aminities as $title){
            $society_aminities_title[]=$title->title;
        }
        foreach ($faciliety as $title){
            $property_facilities[]=$title->title;
        }
        foreach ($interiors as $title){
            $property_interiors[]=$title->title;
        }
        foreach ($meals_array as $title){
            $property_meals_array[]=$title;
        }

//    dd($society_aminities_title);
        $suitable_for= GlobalHelpers::SuitableFor();
//        dd($faciliety);
        $data=[
            "property"=>$records,
            "id"=>$records->propertyID,
            "title"=>$records->title,
            "name"=>$name,
            "landmark"=>$records->landmark,
            "gender"=>$records->gender,
            "description"=>$records->description,
            "price"=>$records->price,
            "numberOfRoom"=>isset($records->availability->title)?$records->availability->title:"-",
            "availabilityType"=>isset($records->availabilityType->title)?$records->availabilityType->title:"-",
            "notice_period"=>$records->noticePeriod,
            "total_square_feet"=>$records->total_square_feet,
            "society_aminities"=>$society_aminities_title,
            "facilities"=>$property_facilities,
            "interiors"=>$property_interiors,
            "Facing"=>$records->Facing,
            "meals"=>$property_meals_array,
            "rules"=>$records->rules,
            "address"=>$records->address,
            "latitude"=>$records->latitude,
            "longitude"=>$records->longitude,
            "suitable_for"=>isset($suitable_for[$records->roomSharingID])? $suitable_for[$records->roomSharingID]:"-",
            "sharingID"=>isset($records->roomsharing->title)?$records->roomsharing->title:"-",
            "property_code"=>$records->property_code,
            "slug"=>$records->slug,
            "created_at"=>\Carbon\Carbon::parse($records->created_at)->format('d,M Y')
        ];
//        dd($data);
        return view('frontend.search_details',compact("ahmedabad_loc_","data","society_aminities_array",
            "faciliety_array","property_interiors_array","meals_array","extra_charge_array"));
    }
}
