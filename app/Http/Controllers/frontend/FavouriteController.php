<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\frontend\Favourite;
use Validator;
use Session;
use App\Libraries\ImageHelpers;
use Auth;
use Illuminate\Support\Facades\DB;
use App\Libraries\GlobalHelpers;
use App\Model\backend\User;
use App\Model\backend\Rules;
use App\Model\backend\Societyamenity;
use App\Model\backend\Facilities;
use App\Model\backend\Furniture;

class FavouriteController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $favouriteObj = new Favourite();

        $res_arr = $favouriteObj->getRecords($favouriteObj);
        //echo "<pre>";print_r($res_arr);exit;
        foreach ($res_arr as $key => $value) {
            $file = '';
            if(!empty($value->photo)){
                $photos = explode(',', $value->photo);
                $path = 'images/property';
                $imgName = $photos[0];
                $file = str_replace("\\", "/",ImageHelpers::getSmallImage($path, $imgName));
            }else{
                $path = 'images/property';
                $file = str_replace("\\", "/",ImageHelpers::getSmallImage($path));
            }

                $res_arr["$key"]->file = $file;
        }
        return view('frontend.user.myfavourite')->with(["page_title"=>"My Favourite - ",'records' => $res_arr]);
    }

    public function destroy($encoded_id="")
    {
       try {
            $favouriteObj = new Favourite();
            $id = base64_decode($encoded_id);

            $data['data'] = $favouriteObj->getRecordById($id);
            if(!empty($data['data']) && (Auth::guard('user')->user()->premiumUser != '1')){
                $data['result'] = $favouriteObj->deleteRecordById($id);
                if($data['result'] == 'true'){
                    Session::flash('message', 'Record is deleted successfully.');
                    Session::flash('msgclass', 'alert-success');
                    //$result['result'] = true;
                    //return response($result);
                    
                }
            }else{
                Session::flash('message', 'Record does not exist.');
                Session::flash('msgclass', 'alert-danger');
                //$result['result'] = false;
                //return response($result);
            }
        } catch (Exception $e) {
            Session::flash('message', 'Record could not be deleted.');
            Session::flash('msgclass', 'alert-danger');
            //$result['result'] = false;
            //return response($result);
        }
        return redirect()->back();      
    }
    
    //mehul change

    public function shortlist() {
        $id = Auth::guard('user')->user()->id;
        $result = DB::table('user_properties')->where('user_id', $id)->first();
        $propertyIds = $result->property_ids;
        if (!empty($propertyIds)) {
            $idsArray = explode(',', $propertyIds);
            foreach ($idsArray as $k => $value) {
                $properties = DB::table('property')
                        ->leftJoin('city as ct', 'property.cityID', '=', 'ct.cityID')
                        ->leftJoin('state as st', 'property.stateID', '=', 'st.stateID')
                        ->leftJoin('area as ar', 'property.areaID', '=', 'ar.areaID')
                        ->leftJoin('users as u', 'property.createdBy', '=', 'u.id')
                        ->leftJoin('property_photo as pp', 'property.propertyId', '=', 'pp.propertyId')
                        ->where('property.propertyID', $value)
                        ->get();
                foreach ($properties as $key => $property) {
                    $path = 'images/property';
                    $imgName = $property->photoName;
                    $file = ImageHelpers::getMediumImage($path, $imgName);
                    $properties[$key]->photo_url = $file;
                    $properties[$key]->propertyID = $value;
                }
                $shortlisted_data[$k] = $properties[$key];
            }
        } else {
            $shortlisted_data = "";
        }

        return view('frontend.user.shortlist')->with(["page_title" => "My Shortlisted - ", 'records' => $shortlisted_data]);
    }

    public function removefromshortlist($propery_id) {
        $result = DB::table('user_properties')->where('user_id', Auth::guard('user')->user()->id)->first();
        $list = explode(',', $result->property_ids);
        $arr = array_diff($list, array($propery_id));
        $newlist = implode(',', $arr);
        DB::table('user_properties')->where('user_id', Auth::guard('user')->user()->id)->update(['property_ids' => $newlist]);
    }
    
    public function addPrimeMembership() {
        DB::table('users')->where('id', Auth::guard('user')->user()->id)->update(['premiumUser' => '1']);
    }
    
    public function listProperty(){
        
        $stateDropdown = GlobalHelpers::stateDropdown();
        $propertyTypeDropdown = GlobalHelpers::propertyTypeDropdown();
        $numberOfRoomDropdown = GlobalHelpers::numberOfRoomArr();
        $facingDropdown = GlobalHelpers::facingArr();
        $noticePeriodDropdown = GlobalHelpers::noticePeriodArr();
        $conditionDropdown = GlobalHelpers::conditionArr();
        $mealsIncludedDropdown = GlobalHelpers::mealsIncludedArr();
        $societyamenityIDropdown = GlobalHelpers::societyamenityArr();
        $floorIDropdown = GlobalHelpers::floorArr();
        $availablebhkIDropdown = GlobalHelpers::availablebhkArr();
        $constantRols = config('constants.ADMIN_ROLES');

        $propertyOwnerDropdown = User::userDropdown('roleID', $constantRols['Owner']);
        $roomSharingDropdown = GlobalHelpers::roomSharingDropdown();
        $rulesObj = new Rules();
        $ruleschk = $rulesObj->getAllRecord();
        $societyamenityObj = new Societyamenity();
        $facilitiesObj = new Facilities();
        $furnitureObj = new Furniture();
        $societyamenitychk = $societyamenityObj->getAllRecord();
        $facilitieschk = $facilitiesObj->getAllRecord();
        $furniturechk = $furnitureObj->getAllRecord();
        
        $dropdowns = [
            'stateDropdown' => $stateDropdown,
            'propertyTypeDropdown' => $propertyTypeDropdown,
            'mealsIncludedDropdown' => $mealsIncludedDropdown,
            'conditionDropdown' => $conditionDropdown,
            'numberOfRoomDropdown' => $numberOfRoomDropdown,
            'facingDropdown' => $facingDropdown,
            'noticePeriodDropdown' => $noticePeriodDropdown,
            'propertyOwnerDropdown' => $propertyOwnerDropdown,
            'roomSharingDropdown' => $roomSharingDropdown,
            'societyamenityIDropdown' => $societyamenityIDropdown,
            'floorIDropdown' => $floorIDropdown,
            'availablebhkIDropdown' => $availablebhkIDropdown,
            'ruleschk' => $ruleschk,
            'societyamenitychk' => $societyamenitychk,
            'facilitieschk' => $facilitieschk,
            'furniturechk' => $furniturechk
        ];
        
        return view('frontend.user.list_your_property')->with(["page_title" => "My Property Listing - ", 'dropdowns' => $dropdowns]);
    }
    
    public function addProperty(Request $request){
//        echo "<pre>";
//        print_r($_POST);
//        die;
        
        $rules = array(
                'availability' => 'required',
                'propertyType' => 'required',
                'companyName' => 'required',
                'companyNumber' => 'required',
                'address' => 'required',
                'landmark' => 'required',
//                'state' => 'required',
//                'city' => 'required',
//                'area' => 'required',
                'mealsIncludedID' => 'required',
                'status' => 'required',
                //mehul change
                'numberOfRoom' => 'required',
                'facing' => 'required',
                'noticePeriod' => 'required',
                'deposit' => 'required'
                    //mehul change over
            );

            $messages = [
                'availability.required' => 'Please select availability',
                'propertyType.required' => 'Please select property type',
                'companyName.required' => 'Please enter company name',
                'companyNumber.required' => 'Please enter company number',
                'address.required' => 'Please enter address',
                'landmark.required' => 'Please enter landmark',
//                'state.required' => 'Please select state',
//                'city.required' => 'Please select city',
//                'area.required' => 'Please select area',
                'propertyType.required' => 'Please select property type',
                'mealsIncludedID.required' => 'Please select meals included',
                'status.required' => 'Please select status',
                //mehul change
                'numberOfRoom.required' => 'Please select Number of room',
                'facing.required' => 'Please select Facing',
                'noticePeriod.required' => 'Please select Notice Period',
                'deposit.required' => 'Please enter deposit',
                    //mehul change over
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            
            if ($validator->fails()) {
                $messages_arr = $validator->messages()->getMessages();
                if (count($messages_arr) > 0) {
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_" . $key] = $value_arr[0];
                    }
                }
                return redirect()->route('list-your-property')
                                ->withErrors($errors)
                                ->withInput();
            }
            
            if (empty($errors)) {

                $propertyObj = new Favourite();
                $id = $propertyObj->saveProperty($request);
                if ($id > 0) {
                    Session::flash('message', 'Record added successfully.');
                    Session::flash('msgclass', 'alert-success');
                    return redirect()->route('list-your-property');
                } else {
                    Session::flash('message', 'Record addition error.');
                    Session::flash('msgclass', 'alert-danger');
                }
            }
            return redirect()->route('list-your-property');
        
    }

    //mehul change over
    
}
