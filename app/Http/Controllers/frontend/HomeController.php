<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Libraries\ImageHelpers;
use App\Libraries\GlobalHelpers;
use Auth;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('user');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$accomodation_for = GlobalHelpers::accomodationFor();
        //ADD BY CT
        $search_property_type=DB::table('property_type')->select('propertyTypeID','title')->where('status',"=",'Active')->get();
        //ADD BY CT
        $city_id=DB::table('area')->select('cityID')->where("stateID","=","7", "AND","cityName","=","Ahmedabad City", "AND", "status","=","Active")->first();
       //ADD BY CT
        $ahmedabad_loc=DB::table('area')->select('areaID','areaName')->where('cityID',"=",$city_id->cityID,"AND","status","=","Active")->get();

        $properties = DB::table('property')
            ->leftJoin('city as ct', 'property.cityID', '=', 'ct.cityID')
            ->leftJoin('state as st', 'property.stateID', '=', 'st.stateID')
            ->leftJoin('users as u', 'property.createdBy', '=', 'u.id')
            ->leftJoin('property_photo as pp', 'property.propertyId', '=', 'pp.propertyId')
            ->orderBy('property.propertyId', 'desc')
            ->limit(10)->get();
        foreach ($properties as $key => $property) {
            $path = 'images/property';
            $imgName = $property->photoName;
            $file = ImageHelpers::getMediumImage($path, $imgName);
            $properties[$key]->photo_url = $file;
        }
        if (isset(Auth::guard('user')->user()->id)) {
            $user_data = DB::table('users')
                ->where('id', Auth::guard('user')->user()->id)->first();
//echo "<pre>"; print_r($user_data); die;
            $id = Auth::guard('user')->user()->id;

            if ($user_data->roleID == 2) {
                $result = DB::table('user_properties')->where('user_id', $id)->first();
                if (!empty($result)) {
                    $propertyIds = $result->property_ids;
                    if (!empty($propertyIds)) {
                        $idsArray = explode(',', $propertyIds);
                        foreach ($idsArray as $k => $value) {
                            $properties = DB::table('property')
                                ->leftJoin('city as ct', 'property.cityID', '=', 'ct.cityID')
                                ->leftJoin('state as st', 'property.stateID', '=', 'st.stateID')
                                ->leftJoin('area as ar', 'property.areaID', '=', 'ar.areaID')
                                ->leftJoin('users as u', 'property.createdBy', '=', 'u.id')
                                ->leftJoin('property_photo as pp', 'property.propertyId', '=', 'pp.propertyId')
                                ->where('property.propertyID', $value)
                                ->get();
                            foreach ($properties as $key => $property) {
                                $path = 'images/property';
                                $imgName = $property->photoName;
                                $file = ImageHelpers::getMediumImage($path, $imgName);
                                $properties[$key]->photo_url = $file;
                                $properties[$key]->propertyID = $value;
                            }
                            $shortlisted_data[$k] = $properties[$key];
                        }
                    } else {
                        $shortlisted_data = "";
                    }
                } else {
                    $shortlisted_data = "";
                }
            } else {
                $result = DB::table('property')->where('createdBy', $id)->get();
                if ($result->count()) {

                    foreach ($result as $k => $value) {
//                    echo "<pre>"; print_r($value); die;
                        $properties = DB::table('property')
                            ->leftJoin('city as ct', 'property.cityID', '=', 'ct.cityID')
                            ->leftJoin('state as st', 'property.stateID', '=', 'st.stateID')
                            ->leftJoin('area as ar', 'property.areaID', '=', 'ar.areaID')
                            ->leftJoin('users as u', 'property.createdBy', '=', 'u.id')
                            ->leftJoin('property_photo as pp', 'property.propertyId', '=', 'pp.propertyId')
                            ->where('property.propertyID', $value->propertyID)
                            ->get();
                        foreach ($properties as $key => $property) {
                            $path = 'images/property';
                            $imgName = $property->photoName;
                            $file = ImageHelpers::getMediumImage($path, $imgName);
                            $properties[$key]->photo_url = $file;
                            $properties[$key]->propertyID = $value->propertyID;
                        }
                        $shortlisted_data[$k] = $properties[$key];
                    }
                } else {
                    $shortlisted_data = '';
                }
            }

            if ($user_data->roleID !== 2) {
                $user_properties = DB::table('property')->where('createdBy', Auth::guard('user')->user()->id)->get();

                if (!empty($user_properties)) {
                    foreach ($user_properties as $key => $property_data) {

                        $user_property = DB::table('user_properties')
                            ->whereRaw('FIND_IN_SET(?,property_ids)', $property_data->propertyID)
                            ->get();
                    }
                    if (!empty($user_property)) {
                        foreach ($user_property as $k => $u_data) {
                            $user = DB::table('users')
                                ->where('id', $u_data->user_id)->first();
                            $response[$k] = $user;
                        }
                        if (empty($response)) {
                            $response = '';
                        }
                    } else {
                        $response = '';
                    }
                } else {
                    $response = '';
                }
            } else {
                $response = '';
            }
//            echo "<pre>"; print_r($response); die;
            return view('frontend.userhome', ['user_data' => $user_data, 'records' => $shortlisted_data, 'response' => $response,"search_property_type"=>$search_property_type,"ahmedabad_loc"=>$ahmedabad_loc,"accomodation_for"=>$accomodation_for]);
        } else {
            //ADD BY CT
            return view('frontend.home', ['records' => $properties,"search_property_type"=>$search_property_type,"ahmedabad_loc"=>$ahmedabad_loc,"accomodation_for"=>$accomodation_for]);
        }
    }

    //mehul search change
    public function searchproperty(Request $request)
    {
//        echo "<pre>";
//        print_r($_REQUEST);
//        exit;
        $city_id = $request->city_id;
        $gender = $request->gender;
        $area_ids = $request->area;
        //$property = '';
        $collection = collect();
        foreach ($area_ids as $area_id) {
//            $properties = DB::table('property')->where('areaID', $area_id)->get();
            $property = DB::table('property')->where('areaID', $area_id)->get();
            $collection->concat($property);
        }

        echo "<pre>";
        print_r($collection);
        die;
    }

    public function getAllCity($stateID = null)
    {
        //print_r($request->all());

        /* $stateID = ($request->input('stateID') != "") ? $request->input('stateID') : null;
          $cityID = ($request->input('cityID') != "") ? $request->input('cityID') : null; */
        $cityArr = GlobalHelpers::cityDropdown($stateID);

        $cityDropdown = "";
        $cityDropdown = "<option value=''>Select City</option>";
        foreach ($cityArr as $key => $value) {
            $cityDropdown .= "<option value='" . $key . "'>" . $value . "</option>";
        }
        return $cityDropdown;
    }

    //mehul search change over


    //ADD BY CT
    public function signUp(Request $request)
    {
        $input = $request->all();
//        dd($input);
        $check_validate = \App\Model\frontend\User::validationCheck($input);

        if ($check_validate) {
            $result = \App\Model\frontend\User::addUser($input);
            if ($result) {
                return response()->json([
                    'status' => true,
                    "message" => "Please Check your mail"
                ]);

            }
        } else {
            $errors = \App\Model\frontend\User::$errors->messages();

            return response()->json([
                'status' => false,
                "message" => $errors
            ]);
        }
    }

    //ADD BY CT
    public static function activateUser($code)
    {
        $flag = \App\Model\frontend\User::activateUser($code);
        if ($flag) {
            //return \Redirect::route("login");
            echo "Your email address is verified successfully. You can login in the app now.";
        } else
            echo "Invalid link provided. Try again!";

    }

    //ADD BY CT

    public function doLogin(Request $request)
    {
        $login_type = filter_var($request->input('login'), FILTER_VALIDATE_EMAIL )
            ? 'email'
            : 'username';

        $request->merge([
            $login_type => $request->input('login')
        ]);

//        $credentials = $request->only('email', 'password');
//        $credentials = $request->only('username', 'password');
//        dd($credentials);

        if (\Auth::attempt($request->only($login_type, 'password')) ) {
            if (\Auth::user()->email_verify_token == NULL) {

                return response()->json([
                    'status' => true,
                    "message" => "login successfully."
                ]);

            } else {

                return response()->json([
                    'status' => false,
                    "message" => "Link are not verified,Please check your email."
                ]);

                \Auth::logout();

            }
        } else {

            return response()->json([
                'status' => false,
                "message" => "Invalid user name and password."
            ]);
            \Auth::logout();

        }

    }


    public static function updateOwnerProfile(Request $request){
        $error=[];
        $data=[];
        $input=$request->all();
//        dd($input);
        $owner_id=\Auth()->user()->id;
        $type=isset($input["type"])?$input["type"]:"";
        $check_validate=\App\Model\frontend\Functions::CheckValidate($input,$type);

        if($check_validate){
            $check_exist=\App\Model\frontend\User::checkExist($input["email"],$owner_id);
            if(!$check_exist) {
                $update_profile=\App\Model\frontend\User::updateOwnerProfile($input,$owner_id,$type);
                $data=["status"=>true,"message"=>"profile Update successfully","result"=>$update_profile];
            }else{
                $error=["Email already exist"];
                $data=["status"=>false,"message"=>$error];
            }
        }else{
            $error=\App\Model\frontend\Functions::$errors->messages();
            $data=["status"=>false,"message"=>$error];
        }

        return response()->json($data);
    }

    //ADD BY CT

    public function propertyList($type=''){
        $data=[];
        $id=\Auth()->user()->id;
        $city_id=\Auth()->user()->id;
        $state_id=\Auth()->user()->id;
        $city_name=\App\Model\frontend\City::getRecordById($city_id);
        $city_name=$city_name["cityName"];
        $state_name=\App\Model\frontend\State::getRecordById($state_id);
        if(!empty($state_name)){
            $state_name=$state_name["stateName"];
        }
//        $state_name=$state_name["cityName"];
        $result=\App\Model\frontend\Property::getProperties($id,$type);
        $total_property=\App\Model\frontend\Property::getProperties($id,$type,true);

        $suitable_for= GlobalHelpers::SuitableFor();
        $availability_type = \App\Model\frontend\Availabillity::availablebhkArr();
        $property_type = \App\Model\frontend\PropertyType::propertyType();
        foreach ($result as $item){
            $title = '';
            $title = isset($availability_type[$item->availability]["title"])? $availability_type[$item->availability]["title"]:"-".'  '.isset($property_type[$item->propertyTypeID]["title"]) ? $property_type[$item->propertyTypeID]["title"]:"-";
            if(!empty($item->areaID)){
                $area_name=\DB::table('area')->select("areaName")->where("areaID","=",$item->areaID)->first();

                $title.=' in '.isset($area_name->areaName)?$area_name->areaName:"-";
            }
            $data[] = [
                "id" => $item->propertyID,
                /*"title" => $item->title,*/
                "title" => $title,
                "user_name" => \Auth()->user()->name,
                "landmark" => $item->landmark,
                "gender" => $item->gender,
                "description" => $item->description,
                "price" => $item->price,
                "slug" => $item->slug,
                "suitable_for"=>isset($suitable_for[$item->roomSharingID])? $suitable_for[$item->roomSharingID]:"-",
                "bed_sharing"=>isset($item->roomsharing->title)?$item->roomsharing->title:"-",
                "created_at" => \Carbon\Carbon::parse($item->created_at)->format('d,M Y')
            ];

        }
//            dd($data);
        $view = view('frontend.search_content', ["property" => $data,"data_array" => $result]);
        $view = $view->render();
//        $total_property=count($data);
//        dd($result);
        return view('frontend.owner_property_list',compact("result","city_name","state_name","total_property","view","type"));
    }

    //ADD BY CT
    public function addProperty(Request $request,$id=''){
        $input=$request->all();
//        dd($input);
        $id=!empty($input["id"])?$input["id"]:"";
        $result=\App\Model\frontend\Property::addProperty($input,$id);
        return \redirect()->route('dashboard_owner');

    }

    //ADD BY CT

    public function deleteProperty(Request $request){
            $input=$request->all();
            $id=$input["id"];
            $delete=\App\Model\frontend\Property::deleteProperty($id);
            return response()->json($id);
    }
    public function logout(){
        \Auth::logout();
        return redirect()->route('user_home');
    }


}
