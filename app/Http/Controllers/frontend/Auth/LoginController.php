<?php

namespace App\Http\Controllers\frontend\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your  screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $guard = 'user';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->redirectTo = url()->previous();
        //$this->middleware('guest')->except('logout');
		//dd(auth());
    }
    protected function guard()
    {
        return Auth::guard('user');
    }
    public function showLoginForm()
    {
        /*if((!session()->has('url.intended')) && (request()->segment(1) != 'login') && (request()->segment(2) != "register"))
        {
            session(['url.intended' => url()->previous()]);
        }*/
        if ( auth()->guard('user')->check() ) 
        {
            return redirect()->intended('/');
        }
        return view('frontend/auth/login')->with(["page_title"=>"Login - "]);
    }

    public function logout(Request $request)
    {
        auth()->guard("user")->logout();
        /*if(!session()->has('url.intended'))
        {
            session(['url.intended' => url()->previous()]);
        }*/
		//$this->guard('user')->logout();

        //$request->session()->invalidate();
        //$request->session()->forget('sess_role_name');
        //$request->session()->forget('sess_access_module');

        return redirect()->intended('/');
    }
    
    public function authenticate(Request $request)
    {

        if ( auth()->guard('user')->check() ) 
        {
            return redirect()->intended('/');
        }
        $email = $request->input('email');
        $password = $request->input('password');
        
        /*
        if (auth()->guard('user')->attempt(['email' => $email, 'password' => $password, 'roleID' => config('constants.USER_ROLES') ])) 
        {
            if($request->ajax()){

                //return redirect('/search')->with('dataArray',$request->all());
              return  response()->json("true");
            }
            return redirect()->intended('/');
        }
        */
        
        if (auth()->guard('user')->attempt(['mobile' => $email, 'password' => $password /*, 'roleID' => config('constants.USER_ROLES')*/])) 
        {
            $loggedIn = in_array(auth()->guard('user')->user()->roleID, config('constants.USER_ROLES'));
            if($loggedIn){
                 return redirect()->intended('/');
            }else{
                auth()->guard('user')->logout();
                return redirect()->intended('/login')->with('status', 'Invalid Login Credentials !')->withInput(); 
            }
           
        }
        else
        {
            /*if($request->ajax()){
              return  response()->json("false");
            }*/
            return redirect()->intended('/login')->with('status', 'Invalid Login Credentials !')->withInput();
        }
    }
}
