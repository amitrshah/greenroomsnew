<?php

namespace App\Http\Controllers\frontend\Auth;

use App\Model\frontend\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use Response;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $formFields
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $formFields)
    {
        return Validator::make($formFields, [
            'name' => 'required|string',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'mobile' => 'required|numeric',
        ]);
    }

    protected function create(array $request)
    {
        $userRolesArray = config('constants.USER_ROLES');
        
        return User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'mobile' => $request['mobile'],
            'roleID' => $userRolesArray["User"],
            'remainingFavCount' => env('FREE_USER_FAVOURITE_LIMIT'),
            'totalFavCount' => env('FREE_USER_FAVOURITE_LIMIT'),
            'status' => "Inactive",
            'roleID' => $request['user_type']
        ]);
    }

    public function showRegistrationForm(Request $request)
    {

        if(Auth::guard('user')->check()){
            return redirect()->route('user_home');
        }
        return view('frontend.auth.register')->with(["page_title"=>"Register - "]);
    }
    
    public function register(Request $request){
        
        $userRolesArray = config('constants.USER_ROLES');
        
        User::create([
            'name' => $request['uname'],
            'firstName' => $request['firstName'],
            'lastName' => $request['lastName'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'mobile' => $request['mobile'],
            'remainingFavCount' => env('FREE_USER_FAVOURITE_LIMIT'),
            'totalFavCount' => env('FREE_USER_FAVOURITE_LIMIT'),
            'status' => "Active",
            'roleID' => $request['user_type']
        ]);
        
        $request->session()->flash('status', 'You have Successfully Registered');
        return redirect('/login');
        
    }
    
    protected function guard()
    {
        return Auth::guard('user');
    }
}
