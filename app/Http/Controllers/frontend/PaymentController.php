<?php
namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\Model\frontend\Payment;
use App\Model\frontend\User;
use Carbon\Carbon;
use PaytmWallet;
use Session;
use App\Libraries\GlobalHelpers;

class PaymentController extends Controller
{
    /**
     * Redirect the user to the Payment Gateway.
     *
     * @return Response
     */
    public function plans()
    {
        //$data['Callback_URL'] = env('PAYTM_CALLBACK_URL');
        $paymentObj = new Payment();
        $dropdown['planCityDropdownArray'] = $paymentObj->planCityDropdown();
        $planCityID = Auth::guard('user')->user()->planCityID;
        if(!empty($planCityID)){
            $dropdown['currentPlan'] = $paymentObj->planCityByPlanCityID($planCityID);
        }
        return view('frontend.membership.plans', ["page_title"=>"Plans - ",'dropdown' => $dropdown]);
    }

    public function getPlansByCityID(Request $request)
    {
        $paymentObj = new Payment();
        $planDropdownArray = $paymentObj->getPlansByCityID($request->city);
        return response($planDropdownArray);
    }

    public function getPlansDatailsByPlanID(Request $request)
    {
        $paymentObj = new Payment();
        $planDetails = $paymentObj->getPlansDatailsByPlanID($request->plan, $request->city);
        return view('frontend.membership.plan_details', ['planDetails' => $planDetails]);
    }

    public function order(Request $request)
    {
        $userObj = new User();
        $userData = $userObj->getUserRecordById(Auth::guard('user')
            ->user()
            ->id);
        $paymentObj = new Payment();
        $planData = $paymentObj->getPlansDatailsByPlanID($request->planID, $request->cityID);
        $orderID = uniqid();

        $data['orderID'] = "GR_" . uniqid();

        $data['userID'] = $userData->id;
        $data['mobile'] = $userData->mobile;
        $data['email'] = $userData->email;

        $data['amount'] = $planData->price;
        $data['planCityID'] = $planData->planCityID;
        $data['cityID'] = $planData->cityID;

        $paymentID = $paymentObj->savePayment($data);
        if ($paymentID)
        {

            /*$payment = PaytmWallet::with('receive');
            $payment->prepare([
            'order' => 'mem_12378978978',//$order->id,
            'user' => 2,
            'mobile_number' => '8866241386',
            'email' => 'devesh.kudosintech@gmail.com',
            'amount' => '12',
            'callback_url' => env('PAYTM_CALLBACK_URL')
            ]);*/
            //return $payment->receive();
            $this->paymentCallback($data);
            return redirect()->route('user_plans');
        }
        else
        {
            Session::flash('message', 'Sorry, Could not process you request.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()
                ->route('user_plans');
        }

        exit;
    }

    /**
     * Obtain the payment information.
     *
     * @return Object
     */
    /* public function paymentCallback()
    {
        $transaction = PaytmWallet::with('receive');
        $response = $transaction->response();
        
        if($transaction->isSuccessful()){
          //Transaction Successful
          Session::flash('message', 'Your membership plan has been upgraded successfully.!!!'); 
          Session::flash('msgclass', 'alert-success'); 
          return redirect('/plans');
        }else if($transaction->isFailed()){
          Session::flash('message', 'Sorry, your transaction is failed.'); 
          Session::flash('msgclass', 'alert-danger'); 
          return redirect('/plans');
          //Transaction Failed
        }else if($transaction->isOpen()){
          //Transaction Open/Processing
        }
        
        //get important parameters via public methods
        $transaction->getOrderId(); // Get order id
        $transaction->getTransactionId(); // Get transaction id
    } */
    public function paymentCallback($data /*Request $Request*/)
    {
        //dd($_POST);
        try
        {

            $paymentObj = new Payment();
            /*$transaction = PaytmWallet::with('receive');
             $response = $transaction->response(); */
            $responseStatus = (isset($response->STATUS) && $response->STATUS != "") ? $response->STATUS : 'TXN_SUCCESS';

            $result = $paymentObj->updatePayment($data);

            if ($responseStatus == "TXN_SUCCESS" && $result /*$transaction->isSuccessful()*/)
            {
                Session::flash('message', 'Your membership plan has been upgraded successfully.!!!');
                Session::flash('msgclass', 'alert-success');
                return redirect()->route('user_plans');

            }
            else if ($responseStatus != "TXN_SUCCESS" /*$transaction->isFailed()*/)
            {
                Session::flash('message', 'Sorry, your transaction is failed.');
                Session::flash('msgclass', 'alert-danger');
                return redirect()->route('user_plans');
            }
        }
        catch(Exception $e)
        {
            Session::flash('message', 'Sorry, Could not process your request.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('user_plans');
        }
    }

    public function paymentHistory(Request $request)
    {
        $paymentObj = new Payment();
        $dropdown['planDropdown'] = GlobalHelpers::planDropdown();
        $dropdown['cityDropdown'] = GlobalHelpers::cityDropdown();
        $dropdown['tnxStatsDropdown'] = GlobalHelpers::tnxStatsDropdown();

        $res_arr = $paymentObj->getRecords($paymentObj);
        
        return view('frontend.payment_history.list',["page_title"=>"Payment History - ",'records'=>$res_arr, 'dropdown'=> $dropdown]);
        
    }
}

