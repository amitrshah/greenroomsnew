<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\frontend\Page;
use App\Libraries\GlobalHelpers;
use Validator;
use Session;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('page');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function aboutUs()
    {
        return view('frontend.aboutus')->with(["page_title" => "About Us - "]);
    }

    public function faqs()
    {
        return view('frontend.faqs')->with(["page_title" => "FAQs - "]);
    }

    public function services()
    {
        return view('frontend.services')->with(["page_title" => "Services - "]);
    }

    public function terms()
    {
        return view('frontend.terms')->with(["page_title" => "Terms & Conditions - "]);
    }

    public function contactUs()
    {
        return view('frontend.contact')->with(["page_title" => "Contact Us - "]);
    }

    public function howItWorks()
    {
        return view('frontend.howitworks')->with(["page_title" => "How it works? - "]);
    }

    public function referAndEarn()
    {
        return view('frontend.refer-and-earn')->with(["page_title" => "Refer And Earn - "]);
    }

    public function owners()
    {
        return view('frontend.owners')->with(["page_title" => "Owners - "]);
    }

    public function privacyPolicy()
    {
        return view('frontend.privacy-policy')->with(["page_title" => "Privacy Policy - "]);
    }

    public function affiliate()
    {
        return view('frontend.affiliate')->with(["page_title" => "Affiliate - "]);
    }

    public function complaint()
    {
        return view('frontend.complaint')->with(["page_title" => "Complaint - "]);
    }

    public function tenants()
    {
        return view('frontend.tenants')->with(["page_title" => "Tenants - "]);
    }

    public function testimonial()
    {
        return view('frontend.testimonial')->with(["page_title" => "Testimonial - "]);
    }

    public function ownersBenefits()
    {
        return view('frontend.owners-benefits')->with(["page_title" => "Owners Benefits - "]);
    }

    public function tenantsBenefits()
    {
        return view('frontend.tenants-benefits')->with(["page_title" => "Tenants Benefits - "]);
    }

    public function whyChooseUs()
    {
        return view('frontend.why-choose-us')->with(["page_title" => "Why Choose Us - "]);
    }

    public function corporate()
    {
        return view('frontend.corporate')->with(["page_title" => "Corporate - "]);
    }

    public function careers()
    {
        return view('frontend.careers')->with(["page_title" => "Careers - "]);
    }

    //ADD BY CT
    public function aboutUsGreenRoom()
    {
        return view('frontend.about_greenroom');
    }

    //ADD BY CT
    public function search(Request $request)
    {
        $data_array = [];
        $data_array["availability"] = GlobalHelpers::availablebhkArr();
        $data_array["condition"] = GlobalHelpers::conditionArr();
        $data_array["bedsharing"] = GlobalHelpers::bedSharing();
        $data_array["availability_type"] = GlobalHelpers::availabilityType();
        $data_array["suitable_for"] = GlobalHelpers::SuitableFor();
        $data_array["accomodation_for"] = GlobalHelpers::accomodationFor();
		$count = \DB::table('property')->count();

//       dd($data_array);
        //ADD BY CT
        $property_id = "";
        $male_female = "";
        $area_id = "";
        $search_property_type = \DB::table('property_type')->select('propertyTypeID', 'title')->where('status', "=", 'Active')->get();
		$property_type = \App\Model\frontend\PropertyType::propertyType();

        //ADD BY CT
        $city_id = \DB::table('area')->select('cityID')->where("stateID", "=", "7", "AND", "cityName", "=", "Ahmedabad City", "AND", "status", "=", "Active")->first();
        //ADD BY CT
        $ahmedabad_loc_ = \DB::table('area')->select('areaID', 'areaName')->where('cityID', "=", $city_id->cityID, "AND", "status", "=", "Active")->get();
        $input = $request->all();
//        dd($input);
        if (!empty($input)) {
			$property_ids = $request->get('propertyType');
			if(!empty($property_ids) && count($property_ids) > 0){
				$property_id = implode($request->get('propertyType'),",");
			}

            $male_female_ids = $request->get('forMaleOrFemale');
			if(!empty($male_female_ids) && count($male_female_ids) > 0){
				$male_female = implode($request->get('forMaleOrFemale'));
			}
            $area_id = $request->get('searchByLoc');
        }

		$title = $string = "";
		
		if(count($input)>0){
			if($male_female!=""){
				$title .= ' for '.$data_array["accomodation_for"][$male_female];
			}
			
			if(!empty($area_id) && count($area_id)!=1){
				$area_name=\DB::table('area')->select("areaName")->where("areaID","=",$area_id)->first();

				$title .=' in '.isset($area_name->areaName)?$area_name->areaName:"-";
			}
			
			if(count($property_ids) == 1){
				$string = $property_type[$property_id[0]]["title"].'&nbsp;'.$title;
			} else {
				for($i=0;$i<count($property_ids);$i++){
					$string .= $property_type[$property_ids[$i]]["title"].$title.", ";
				}
				$string = rtrim($string,", ");
			}
		}

//        $bed_sharing=DB::teable('')
//        dd($request);
        return view('frontend.search',
            ["search_property_" => $search_property_type, "ahmedabad_loc_" => $ahmedabad_loc_, "property_id" => $property_id,
                "male_female" => $male_female, "area_id" => $area_id, "data_array" => $data_array, "count" => $count, "string" => $string]);
    }

    //ADD BY CT

    /**
     * Search content from area
     */
    public function searchContent(Request $request)
    {
        $input = $request->all();
//        dd($input);
        $page = $input['page'];
        $data = [];
        if (!empty($input)) {
//            dd($input);
            $data_array = \App\Model\frontend\Search::getProperties($input["data1"],false,$page);
            $pagination_links = $data_array->appends(['sort' => 'votes'])->links();
//            var_dump($pagination_links);die;


            $data_array_count = \App\Model\frontend\Search::getProperties($input["data1"],true);
//            dd($data_array);
            $suitable_for= GlobalHelpers::SuitableFor();
//            $suitable_for= GlobalHelpers::conditionArr();
//            dd($suitable_for[1]);
            $title="";
            $availability_type = \App\Model\frontend\Availabillity::availablebhkArr();
            $property_type = \App\Model\frontend\PropertyType::propertyType();
            //$property_type = \App\Model\frontend\PropertyType::propertyType();
			
            foreach ($data_array as $item) {
				if($item && !empty($item->availability) && !empty($item->propertyTypeID)){
                $title = isset($availability_type[$item->availability])? $availability_type[$item->availability]["title"]:"-";
				$title .= ' '.(isset($property_type[$item->propertyTypeID]) ? $property_type[$item->propertyTypeID]["title"]:"-");
				} else {
					$title = "-";
				}
                if(!empty($item->areaID)){
                    $area_name=\DB::table('area')->select("areaName")->where("areaID","=",$item->areaID)->first();

                    $title.=' in '.(isset($area_name->areaName)?$area_name->areaName:"-");
                }
//                dd($title);
                //dd($item);
                $data[] = [
					"property"=>$item,
                    "id" => $item->propertyID,
                    //"title" => $item->title,
                    "title" => $title,
                    "user_name" => isset($item->user->name) ? $item->user->name : "-",
                    "landmark" => $item->landmark,
                    "gender" => $item->gender,
                    "description" => $item->description,
                    "price" => $item->price,
                    "slug" => $item->slug,
                    "suitable_for"=>isset($suitable_for[$item->roomSharingID])? $suitable_for[$item->roomSharingID]:"-",
                    "bed_sharing"=>isset($item->roomsharing->title)?$item->roomsharing->title:"-",
                    "created_at" => \Carbon\Carbon::parse($item->created_at)->format('d,M Y')
                ];
            }
//            dd($data);
            $view = view('frontend.search_content', ["property" => $data,"data_array" => $data_array]);
            $view = $view->render();
            return response()->json(["status" => true, "result" => $view,"count"=>$data_array_count]);
        }
    }

    //ADD BY CT
    public function postYourProperty($id='')
    {
        $result=[];

        $bed_sharing = \DB::table('room_sharing')->where('status','<>','Delete')->orderBy('id','ASC')->pluck('title as roomSharingName', 'id as roomSharingID')->toArray();
        $data_array["bedsharing"]=$bed_sharing;
        $city_list=\App\Model\frontend\City::getAllCity();
//        $data_array["bedsharing"] = GlobalHelpers::bedSharing();
        $data_array["availability_type"] = \App\Model\frontend\AvailabillityType::availabilityType();
        $data_array["property_type"] = \App\Model\frontend\PropertyType::propertyType();
        $data_array["notice_period_arr"] = GlobalHelpers::noticePeriodArr();
        $data_array["condition"] = GlobalHelpers::conditionArr();
        $data_array["property_age"] = GlobalHelpers::propertyAge();
        $data_array["availability"] = \App\Model\frontend\Availabillity::availablebhkArr();
        $data_array["property_facilities"] = \App\Model\frontend\PropertyFacilities::propertyFacilities();
        $data_array["property_society_amenity"] = \App\Model\frontend\PropertySocietyamenity::propertySocietyamenity();
        $data_array["property_rule"] = \App\Model\frontend\PropertyRules::propertyRules();
        $data_array["suitable_for"] = GlobalHelpers::SuitableFor();
        $data_array["property_on_floor"] = GlobalHelpers::propertyOnFloor();
        $data_array["total_floor"] = GlobalHelpers::totalFloor();
        $data_array["facing_arr"] = GlobalHelpers::facingArr();
        $data_array["extracharge"] = GlobalHelpers::Extracharge();

        if(!empty($id)){
            $result=\App\Model\frontend\Property::getPropertiesById($id);

        }
//        dd($result);
        return view('frontend.post_your_property', compact("data_array","city_list","result"));
    }

    //ADD BY CT

    public static function dashboardOwner()
    {


        $owner_id = \Auth::user()->id;
        $property_list = \App\Model\frontend\Property::getProperties($owner_id,'',true);
        $available_property_list = \App\Model\frontend\Property::getProperties($owner_id,'Available',true);
        $occupied_property_list = \App\Model\frontend\Property::getProperties($owner_id,'Occupied',true);

        $city_id = \Auth()->user()->city_id;
        $state_id = \Auth()->user()->state_id;
        $city_name = \App\Model\frontend\City::getRecordById($city_id);
        $city_name = $city_name["cityName"];
        $state_name = \App\Model\frontend\State::getRecordById($state_id);
        $state_name = $state_name["stateName"];

        $chartSeries = [];
        $chartSeries[] = [
            "text" => " My Total Property List",
            "values" => [$property_list],
            "lineColor" => "#00BAF2",
            "backgroundColor" => "#00BAF2",
            "lineWidth" => 1,
            "marker" => [
                "backgroundColor" => '#00BAF2'
            ]
        ];
        $chartSeries[] = [
            "text" => " Available Property List",
            "values" => [$available_property_list],
            "lineColor" => "#E80C60",
            "backgroundColor" => "#E80C60",
            "lineWidth" => 1,
            "marker" => [
                "backgroundColor" => '#E80C60'
            ]
        ];
        $chartSeries[] = [
            "text" => " Occupied Property List",
            "values" => [$occupied_property_list],
            "lineColor" => "#9B26AF",
            "backgroundColor" => "#9B26AF",
            "lineWidth" => 1,
            "marker" => [
                "backgroundColor" => '##9B26AF'
            ]
        ];


//        {
//            text:"Available Property List",
//                        values: [60],
//                        lineColor: "#E80C60",
//                        backgroundColor: "#E80C60",
//                        lineWidth: 1,
//                        marker: {
//            backgroundColor: '#E80C60'
//                        }
//                    },
//        {
//            text: Occupied Property List,
//            values: [80],
//                        lineColor: "#9B26AF",
//                        backgroundColor: "#9B26AF",
//                        lineWidth: 1,
//                        marker: {
//            backgroundColor: '#9B26AF'
//                        }
        return view('frontend.dashboard_owner', compact("chartSeries", "property_list", "available_property_list", "occupied_property_list", "city_name", "state_name"));
    }

    //ADD BY CT

    public static function OwnerProfile()
    {
        $owner_id = \Auth()->user()->id;
        $owner_Data = \App\Model\frontend\User::getDataByID($owner_id);
        $city_id = \Auth()->user()->city_id;
        $state_id = \Auth()->user()->state_id;
        $city_name = \App\Model\frontend\City::getRecordById($city_id);
        $city_name = $city_name["cityName"];
        $state_name = \App\Model\frontend\State::getRecordById($state_id);
        $state_name = $state_name["stateName"];
        $state_list = \App\Model\frontend\State::getAllState();
        $city_list = \App\Model\frontend\City::getAllCity();


        return view('frontend.owner_profile', compact("city_name", "state_name", "owner_Data", "state_list", "city_list"));
    }

    public function storeContactUs(Request $request)
    {
        try {

            $errors = array();
            $data = array();

            $rules = array(
                'name' => 'required',
                'email' => 'required',
                'message' => 'required',
            );

            $messages = [
                'name.required' => 'Please enter name.',
                'email.required' => 'Please enter email.',
                'message.required' => 'Please enter message.',
            ];
            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                //print_r($validator->messages()->getMessages());exit;
                $messages_arr = $validator->messages()->getMessages();
                if (count($messages_arr) > 0) {
                    foreach ($messages_arr as $key => $value_arr) {
                        $errors["label_" . $key] = $value_arr[0];
                    }
                }
                return redirect()->route('contact-us')
                    ->withErrors($errors)
                    ->withInput();
            }

            if (empty($errors)) {
                $pageObj = new Page();
                $id = $pageObj->saveContactUs($request);
                if ($id > 0) {
                    //$data['id'] = $id;
                    Session::flash('message', 'Thanks for contacting us!');
                    Session::flash('msgclass', 'alert-success');
                } else {
                    Session::flash('message', 'Record addition error.');
                    Session::flash('msgclass', 'alert-danger');
                }
            }
            return redirect()->route('contact-us');
            /*return response($data);*/
        } catch (Exception $e) {
            //echo $e->getMessages();
            Session::flash('message', 'Record addition error.');
            Session::flash('msgclass', 'alert-danger');
            return redirect()->route('contact-us');
        }
    }

//    public function city()
//    {
//        return $this->hasOne('App\Model\frontend', 'cityID', 'city_id');
//    }

}
