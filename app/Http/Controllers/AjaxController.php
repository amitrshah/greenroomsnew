<?php

namespace App\Http\Controllers;

use Auth;
use DB; // for taking db
use Session;
use Validator;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input; // Input
use Illuminate\Support\Facades\Redirect; // Redirect
use App\Libraries\GlobalHelpers;
use Response;

class AjaxController extends Controller
{
	 
    public function getStates()
    {	
		
		$res_arr = array();
		$states = GlobalHelpers::stateDropdown();
		return $states;
    }
	
	public function getCity()
    {	
		$stateID = Input::get('state');
		$results = array();
		
		$property = DB::table('property')->where('status','<>','Delete')->pluck('cityID')->toArray();

		$results = DB::table('city')->whereIn('cityID',$property)->orderBy('cityName','ASC')->where('status','<>','Delete')->pluck('cityName as cityName', 'cityID')->toArray();
		$results = GlobalHelpers::cityDropdown( $stateID );


		/*foreach ($queries as $query)
			$results[] = [ 'id' => $query->city, 'value' => $query->city ];*/
		return Response::json($results);
    }
	
	public function getArea()
    {	
    	
		$stateID = (!empty(Input::get('state'))) ? Input::get('state') : "";
		$cityID = (!empty(Input::get('city'))) ? Input::get('city') : "";

		/*$countryCode = Input::get('countryCode');
		$stateShortName = Input::get('stateShortName');*/
		$areaArr = array();
		if($stateID != '' && $cityID != '') {
			$areaArr = GlobalHelpers::areaDropdown($stateID,$cityID);
		}elseif($cityID != ''){

			$areaArr = GlobalHelpers::areaDropdown(null,$cityID);
		}
		return Response::json($areaArr);
		//dd($countries);exit;
		//$res_arr['states'] = $states;
    }

    public function getCityByState($stateID = "")
    {	
		/*$stateShortName = Input::get('stateShortName');*/
		$results = array();
		
		$results = GlobalHelpers::cityDropdown( $stateID );
		
		/*foreach ($queries as $query)
			$results[] = [ 'id' => $query->city, 'value' => $query->city ];*/
		return Response::json($results);
    }
}
