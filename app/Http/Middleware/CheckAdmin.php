<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CheckAdmin
{
    public function handle($request, Closure $next)
    {       
        // dd(auth()->guard('admin'));
        if(!empty(auth()->guard('admin')->id()))
        {
            $data = DB::table('users')
                    ->select('users.id','users.name')
                    ->where('users.id',auth()->guard('admin')->id())
                    ->where('users.roleID',auth()->guard('admin')->user()->roleID)
                    ->get();
            
            if (!$data[0]->id)
            {
                return redirect()->intended('admin/login/')->with('status', 'You do not have access to admin side');
            }
            return $next($request);
        }
        else 
        {
            return redirect()->intended('admin/login/')->with('status', 'Please Login to access admin area');
        }
        
         
    }
}
