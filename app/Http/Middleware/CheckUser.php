<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CheckUser
{
    public function handle($request, Closure $next)
    {   
        if(!empty(auth()->guard('user')->id()))
        {
            $data = DB::table('users')
                    ->select('users.id','users.name')
                    ->where('users.id',auth()->guard('user')->id())
                    ->get();
            
            if (!$data[0]->id)
            {
                return redirect()->intended('login/')->with('status', 'You do not have access to user area.');
            }
            return $next($request);
        }
        else 
        {
            return redirect()->intended('login/')->with('status', 'Please Login to access user area.');
        }
    }
}
