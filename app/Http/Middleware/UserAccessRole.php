<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

use DB;
use Session;

class UserAccessRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {     

        if(auth()->guard('admin')->check()){
            
            $logged_user_role_id = auth()->guard('admin')->user()->roleID;

            if($logged_user_role_id){

                /*----------------- Get Access Role for logged in user ---------*/
                if(empty(session('sess_role_name'))){
                    $role = DB::table('user_role')->select('roleName')->where('roleID', $logged_user_role_id)->first();
                    session(['sess_role_name' => $role->roleName]);
                }

                /*----------------- Get Access Role for logged in user ---------*/
                

                /*----------------- Logged in user Access Rights ---------------*/
                if(empty(session('sess_access_module'))){
                    
                    $user_module_permission = DB::table('module_permission AS mp')
                                        ->select('mp.actionID','m.moduleName')
                                        ->leftJoin('module AS m', 'mp.moduleID', '=', 'm.moduleID')
                                        ->leftJoin('action AS a', 'a.actionID', '=', 'mp.actionID')
                                        ->where('mp.roleID', $logged_user_role_id)
                                        ->get();
                    $access_role_arr = array();

                    if($user_module_permission){

                        foreach ($user_module_permission as $key => $value) {
                            //echo "<pre>";print_r(json_decode($value->action_id));exit;
                            //echo $value->action_id->list;exit;
                            $actions = json_decode($value->actionID);
                            
                            if($actions){
                                foreach ($actions as $key => $val) {
                                    if($val == 1)
                                        $access_role_arr[$value->moduleName][$key] = 'Y';
                                    else
                                        $access_role_arr[$value->moduleName][$key] = 'N';
                                }
                            }
                            
                        }
                    }       

                    if(count($access_role_arr) > 0){
                        session(['sess_access_module' => $access_role_arr]);
                    }
                }
                /*----------------- Logged in user Access Rights ---------------*/
            }

            
            $req_path = $request->path();
            $permission =Session::get('sess_access_module');
            if($req_path){
                $path_array = explode('/', $req_path);
                $module_name = (isset($path_array['1']) ? $path_array['1'] : '');
                    $module_action = (isset($path_array['2']) ? $path_array['2'] : '');
                if(!empty($module_name)){
                    if(empty($module_action))
                        $module_action = "list";
                }
            }

            $discard_module_arr = array('logout', 'login','dashboard','image_upload','property_images', 'propertysharing','property_photo');
            $discard_action_arr = array('image_upload','store','update','get_city','get_area','image_upload','manage','manage_update');
         
            //print_r($permission);
                  //  print_r(!array_key_exists(ucfirst($module_action),$permission[ucfirst($module_name)]));exit;
            //echo "<pre>";print_r($module_action);exit;
            if(( empty($permission) || !array_key_exists(ucfirst($module_name),$permission) || !array_key_exists(ucfirst($module_action),$permission[ucfirst($module_name)])) && !in_array($module_name, $discard_module_arr) && !in_array($module_action, $discard_action_arr)){

                    abort(201);
                
            }

        }

        return $next($request);
    }
}
