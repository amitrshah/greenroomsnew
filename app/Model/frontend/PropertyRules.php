<?php

namespace App\Model\frontend;

use Illuminate\Database\Eloquent\Model;

class PropertyRules extends Model
{
    //

    protected $table="property_rules";
    //ADD BY CT

    public static function propertyRules(){
        $data_array=[];
        $records=self::select("rulesID","title")->where("status","=","Active")->get();
        if(!empty($records)){
            foreach ($records as $record){
                $data_array[]=[
                    "id"=>$record["rulesID"],
                    "title"=>$record["title"]
                ];
            }

        }
        return $data_array;
    }
}
