<?php

namespace App\Model\frontend;

use Illuminate\Database\Eloquent\Model;

class EmailModel extends Model
{
    //ADD BY CT
    public static function sendEmail($id){

        $record = \App\Model\frontend\User::find($id);

        $full_name = trim($record->name);
        $active_link=route("activate_user", $record->email_verify_token);
        $parameters = [
            'full_name' => $full_name,
            'url' => $active_link,
            'username' => $record->username,
        ];
        $to = "ct.member2@gmail.com";
        $subject = "Welcome to " . env('APP_NAME');

        \Mail::send(array('html' => 'frontend.email.user_activate'), $parameters, function ($message) use ($to, $subject, $full_name) {
            $message->to($to, $full_name)->subject($subject);
        });
    }
}
