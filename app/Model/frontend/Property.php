<?php

namespace App\Model\frontend;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $primaryKey = 'propertyID'; // or null

    protected $table="property";

    public static function getProperties($owner,$type = '',$count = false){
//        dd($count);
        $records=self::where("propertyOwnerID","=",$owner);

        if($type != '')
            $records = $records->where(["occupancy" => $type]);


        if($count)
            return $records->count();
        else {
            $records = $records->paginate(2);

            foreach($records as &$rec){
                self::updateOwner($rec);
            }
            return $records;
        }
    }



    private static  function updateOwner(&$rec){
        $owner = '-';
        if(isset($rec->owner->name))
            $owner = $rec->owner->name;
        $rec->owner_name = $owner;
    }

    public static  function addProperty($data,$id){

//        dd($data);
        if ($data["property_facilities"] != '' && $data["property_facilities"] !=null) {
            $facilitydata = implode(',', $data["property_facilities"]);
        } else {
            $facilitydata = '';
        }
        if ($data["society_amenity"] != '' && $data["society_amenity"] !=null) {
            $societyamenityIDdata = implode(',', $data["society_amenity"]);
        } else {
            $societyamenityIDdata = '';
        }
        if ($data["rules"] != '' && $data["rules"] !=null) {
            $rulesdata = implode(',', $data["rules"]);
        } else {
            $rulesdata = '';
        }

        if(empty($id))
        {
            $records=new Property();
        }else{
            $records=self::where("propertyID","=",$id)->first();
        }
//        dd($id);
//        dd($records);
		
        $records->ownerName=\Auth()->user()->firstName." ".\Auth()->user()->lastName;
        $records->gender=$data["accommodation"];
        $records->sharingID=$data["suitable_for"];
        $records->companyName=$data["projectName"];
        $records->availability=$data["availability_type"];
        $records->old_building=$data["propertyAge"];
        $records->propertyTypeID=$data["property_type_id"];
        $records->propertyOwnerID=\Auth()->user()->id;
        $records->conditionID=$data["condition"];
        $records->cityID=$data["city_id"];
        $records->areaID=$data["locality"];
        $records->landmark=$data["nearestLandmark"];
        $records->description=$data["description"];
        $records->occupancy=$data["occupancy_status"];
        $records->createdBy=\Auth()->user()->id;
        //$records->created_at=date("y-m-d h:i:s");
        $records->total_square_feet=$data["totalarea"];
        $records->rules=$rulesdata;
        $records->area_unit=$data["total_area"];
        $records->address=$data["projectName"];
        $records->societyamenityID=$societyamenityIDdata;
        $records->floorID=$data["propertyOnFloor"];
        $records->avaialble_from=isset($data["avaialble_from"])?$data["avaialble_from"]:"";
        $records->facilityID=$facilitydata;
        $records->total_floor=$data["totalFloor"];
        $records->price=$data["expectedMontlyRent"];
        $records->numberOfRoom=$data["availability"];
        $records->facing=$data["facing"];
        $records->noticePeriod=$data["notice-period"];
        $records->deposit=$data["securityDeposit"];
        $records->property_code=time();
        $records->roomSharingID=$data["suitable_for"];
        $records->status='Delete';
        $records->save();
        $new_propertyID=$records->propertyID;

//        $new_propertyID=\DB::table('property')->insertGetId([
//            'title' => "",
//            'companyName' => "",
//            'ownerName' => \Auth()->user()->name,
//            'availability' => $data["availability_type"],
//            'old_building' => $data["propertyAge"],
//            'companyNumber' => "",
//            'propertyTypeID' => $data["property_type_id"],
//            'propertyOwnerID' => \Auth()->user()->id,
//            'conditionID' => $data["condition"],
//            'address' => "",
//            'stateID' => "",
//            'cityID' => $data["city_id"],
//            'areaID' => $data["locality"],
//            'latitude' => "",
//            'longitude' => "",
//            'landmark' => $data["nearestLandmark"],
//            'mealsIncludedID' => "",
//            'gender' => "",
//            'description' => $data["description"],
//            'rules' => $rulesdata,
//            'slug' => "",
//            'status' => "",
//            'occupancy' => $data["occupancy_status"],
//            'createdBy' => \Auth()->user()->id,
//            'created_at' => date("y-m-d h:i:s"),
//            'total_square_feet' => $data["totalarea"],
//            'area_unit' => $data["total_area"],
//            'societyamenityID' => $societyamenityIDdata,
//            'floorID' => $data["propertyOnFloor"],
//            'avaialble_from' =>date('Y-m-d', strtotime(isset($data["avaialble_from"])?$data["avaialble_from"]:"")),
//            'furnitureID' => "",
//            'facilityID' => $facilitydata,
//            'interiorID' => "",
//            'total_floor' => $data["totalFloor"],
//            'sharingID' => $data["bed_sharing"],
//            //'unitavailableID' => $unitavailableID,
//            'price' => $data["expectedMontlyRent"],
//            //mehul change
//            'numberOfRoom' => $data["availability"],
//            'facing' => $data["facing"],
//            'noticePeriod' => $data["notice-period"],
//            'deposit' => $data["securityDeposit"],
//            'property_code'=>time(),
//            'roomSharingID'=>$data["suitable_for"],
//            'remarks'=>""
//        ]);


        if ($new_propertyID) {
            if(empty($id)){
                $new_property_sharing_id = \DB::table('property_sharing')->insertGetId([
                    'propertyID' => $new_propertyID,
                    'roomSharingID' => $data["suitable_for"],
                    'availability' => $data["availability"],
                    'price' => $data["expectedMontlyRent"],
                ]);
            }else{
                $new_property_sharing_id=\DB::table('property_sharing')->where('propertyID',"=", $new_propertyID)->update([
                    'propertyID' => $new_propertyID,
                    'roomSharingID' => $data["suitable_for"],
                    'availability' => $data["availability"],
                    'price' => $data["expectedMontlyRent"],
                    ]);
            }
        }
        return $new_propertyID;

    }


    public static function getPropertiesById($id){
        $records=self::where("propertyID","=",$id)->first();
        if(!empty($records)){
            return $records;
        }else{
            return [];
        }
    }

    public static function deleteProperty($id){
            $delete=self::find($id)->delete();
            \DB::table('property_sharing')->where('propertyID', $id)->delete();
            return $delete;
    }
    public function roomsharing()
    {
        return $this->hasOne('\App\Model\frontend\RoomSharing', 'id', 'sharingID');
    }
    public function user(){
        return $this->hasOne('\App\Model\frontend\User','id','createdBy');
    }

    public function availability(){
        return $this->hasOne('\App\Model\frontend\Availabillity','availabilityID','numberOfRoom');
    }
    public function availabilityType(){
        return $this->hasOne('\App\Model\frontend\AvailabillityType','availibilityTypeID','availability');
    }
}
