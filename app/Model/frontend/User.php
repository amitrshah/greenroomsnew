<?php

namespace App\Model\frontend;

use DB;
use Validator;
use Auth;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\FrontResetPassword as ResetPasswordNotification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input; // Input
use App\Libraries\GlobalHelpers;
use App\Libraries\SystemHelpers;
use Carbon\Carbon;
use App\Libraries\ImageHelpers;

class User extends Authenticatable {

    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','roleID','mobile','remainingFavCount','totalFavCount','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    public static $errors;
    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /***************************************************************
     * Method: getRecordById
     * Params: user_id
     * Description: It will give user edit page records
     **************************************************************/
    public function getUserRecordById($user_id){

        $userdata = array();
        if($user_id){           
            $userdata =  DB::table('users')
                    ->where('users.id', $user_id)
//mehul change                    ->where('users.roleID', 2)
                    ->where('users.status', "Active")
                    ->first();
        }
        
        return $userdata;
    }

    /***************************************************************
     * Method: validateUser
     * Params: request post data
     * Description: It will check a valid User data
     **************************************************************/
    /*public static function validateUser($input_arr) {

        $firstName = isset($input_arr['firstName']) ? $input_arr['firstName'] : '';
        $lastName = isset($input_arr['lastName']) ? $input_arr['lastName'] : '';
        $password = isset($input_arr['password']) ? $input_arr['password'] : '';
        $address = isset($input_arr['address']) ? $input_arr['address'] : '';
        $stateShortName = isset($input_arr['stateShortName']) ? $input_arr['stateShortName'] : '';
        $city = isset($input_arr['city']) ? $input_arr['city'] : '';
        $zipcode = isset($input_arr['zipcode']) ? $input_arr['zipcode'] : '';
        $gender = isset($input_arr['gender']) ? $input_arr['gender'] : '';
        
        
        
        $mode = isset($input_arr['mode']) ? $input_arr['mode'] : '';
        
        $field_arr = array();
        
        $fields['firstName'] = array('value' => $firstName, 'rules' => 'required|max:255');
        $fields['lastName'] = array('value' => $lastName, 'rules' => 'required|max:255');
        $fields['address'] = array('value' => $address, 'rules' => 'required');
        $fields['stateShortName'] = array('value' => $stateShortName, 'rules' => 'required');
        $fields['city'] = array('value' => $city, 'rules' => 'required');
        $fields['zipcode'] = array('value' => $zipcode, 'rules' => 'required|max:100');
        $fields['gender'] = array('value' => $gender, 'rules' => 'required');
        $fields['password'] = array('value' => $password, 'rules' => 'max:255|min:6');
        
        return GlobalHelpers::validateField($fields);
    }*/

    /*public function userPlanCity($userID){
        $query =  DB::table('plan_cities as pc')->select('pc.cityID');
        $query->leftJoin('users as u', 'u.planCityID', '=', 'pc.planID');
        $query->where('u.id', '=', $userID);
        $userPlanCity = $query->get()->toArray();
        return $userPlanCity;
    }*/
    public function userPlanCity($userID){
        $query =  DB::table('plan_cities as pc')->select('pc.cityID');
        $query->leftJoin('users as u', 'u.planCityID', '=', 'pc.planCityID');
        $query->where('u.id', '=', $userID);
        $userPlanCity = $query->get()->toArray();
        /*print_r($userPlanCity);exit;*/
        return $userPlanCity;
    }
    
    
    /***************************************************************
     * Method: changePassword
     * Params: request
     * Description: It will Update user Password Information
     **************************************************************/
    public function changePassword($request) 
    {   
        
        $user_email = Auth::guard('user')->user()->email;
        try {
            $user = DB::table('users')
                ->where('email', $user_email)
                ->update([
                    'password' => bcrypt($request['password'])
                ]);     
        }
        catch(\Illuminate\Database\QueryException $ex){                 
            return false;
        }
        return true;
    }

    /***************************************************************
     * Method: profileUpdate
     * Params: request
     * Description: It will Update user Information
     **************************************************************/
    public function profileUpdate($request){
        try {
            $userID = Auth::guard('user')->user()->id;
            $email = Auth::guard('user')->user()->email;

            $name       = ($request->input('name') != "") ? $request->input('name') : null;
            $firstName      = ($request->input('firstName') != "") ? $request->input('firstName') : null;
            $lastName       = ($request->input('lastName') != "") ? $request->input('lastName') : null;
            $mobile         = ($request->input('mobile') != "") ? $request->input('mobile') : null;
            $phone      = ($request->input('phone') != "") ? $request->input('phone') : null;
            $gender = ($request->input('gender') != "") ? $request->input('gender') : null;
            $final_data = [
                            'name' => $name, 
                            'firstName' => $firstName, 
                            'lastName' => $lastName, 
                            'mobile' => $mobile, 
                            'phone' => $phone, 
                            'gender'=> $gender,
                            ];
            if(!empty($password)){
                $final_data['password'] = bcrypt($password);
            }
            $result = DB::table('users')->where('email',$email)->where('id',$userID)->update($final_data);
            return true;
        } catch (Exception $e) {
            return false;
        }
        return false;
    }


    //ADD BY CT
    public static function validationCheck($data){

        $messages=[];
        $validator = \Validator::make($data, [
            'full_name' => 'required',
            'username' => 'sometimes|required|unique:users,username',
            'email' => 'sometimes|required|unique:users,email',
            'password' => 'sometimes|required',
            'cpass' => 'required|same:password',
        ], $messages);


        if ($validator->fails()) {
            self::$errors = $validator;
            return false;
        } else
            return true;
    }

    //ADD BY CT
    public static function addUser($data){
        $records=new User();

        if(!empty($records)){
            $role_id=\DB::table('user_role')->select('roleID')->where('roleName', 'Client')->first();
            $email_verify_token = sha1(rand());
            $records->name=$data["full_name"];
            $records->username=$data["username"];
            $records->email=$data["email"];
            $records->password=\Hash::make($data["password"]);
            $records->roleID=$role_id->roleID;
            $records->email_verify_token=$email_verify_token;
            $records->created_at=date("Y-m-d H:i:s");
        }
        $records->save();

         EmailModel::sendEmail($records->id);
        return $records->id;

    }
    //ADD BY CT
    public static function activateUser($code)
    {
        $user = self::where(['email_verify_token' => $code])->first();
        if (empty($user))
            return false;
        else {
            $user->email_verified_at = date("Y-m-d H:i:s", time());
            $user->email_verify_token = '';
            $user->save();

            return true;
        }
    }

    //ADD BY CT
    public static  function getDataByID($owner_id){
        $result=self::where("id","=",$owner_id)->first();
        if(!empty($result)){
            return $result;
        }else{
            return [];
        }

    }

    //ADD BY CT
    public static function checkExist($email,$id=''){
        $result=self::where("email","=",$email);
        if(!empty($id)){
            $result=$result->where("id","!=",$id);
        }
        $result=$result->count();
        if($result>0){
            return true;
        }else{
            return false;
        }

    }

    //ADD BY CT
    public static function updateOwnerProfile($data,$owner_id,$type){
//        dd($data);
        $record=self::where("id","=",$owner_id)->first();
        if(!empty($record)){
            $record->name=$data["fname"];
            $record->firm_name=$data["cname"];
            $record->phone=$data["phone"];
            if(!empty($data["password"])){
                $record->password=\Hash::make($data["password"]);
            }
            $record->email=$data["email"];
            if(!empty($type)){
                $record->state_id=$data["state_id"];
                $record->city_id=$data["city_id"];
                $record->gender=$data["gender"];
            }
            $record->save();
        }
        return $record;
    }
}
