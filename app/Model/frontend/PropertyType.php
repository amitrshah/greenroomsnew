<?php

namespace App\Model\frontend;

use Illuminate\Database\Eloquent\Model;

class PropertyType extends Model
{
    protected $table="property_type";
    //ADD BY CT

    public static function propertyType(){
        $data_array=[];
        $records=self::select("propertyTypeID","title")->where("status","=","Active")->orderBy("title","ASC")->get();
        if(!empty($records)){
            foreach ($records as $record)
            $data_array[$record["propertyTypeID"]]=[
                "id"=>$record["propertyTypeID"],
                "title"=>$record["title"]
            ];
        }
        return $data_array;
    }
}
