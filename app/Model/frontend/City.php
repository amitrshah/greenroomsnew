<?php

namespace App\Model\frontend;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{

    //ADD BY CT
    protected $table="city";

    public static function getRecordById($cityID){
        $data = self::select("cityName")->where("cityID",$cityID)->first();
        return $data;
    }

    public static function getAllCity(){
        $city_list=[];
        $data=self::select("cityID","cityName")->where("status","=","Active")->get();
        foreach ($data as $state){
            $city_list[]=[
                "id"=>$state["cityID"],
                "name"=>$state["cityName"]
            ];
        }
        return $city_list;
    }
}
