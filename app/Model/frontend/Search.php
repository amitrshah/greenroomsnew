<?php

namespace App\Model\frontend;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input; // Input
use Carbon\Carbon;
use Auth;
use DB;
use App\Libraries\GlobalHelpers;
use App\Libraries\ImageHelpers;

class Search extends Model
{

    public function getFavourite()
    {
        //$query['count'] =  DB::table('favourite')->select(DB::raw('count(propertyID) AS favouriteCount'))->where('userID', '=', Auth::guard('user')->user()->id )->first();

        $query = DB::table('favourite')
            ->join('property as p', 'favourite.propertyID', '=', 'p.propertyID')
            //->leftjoin('property_photo as pp', 'p.propertyID', '=', 'pp.propertyID')
            ->select(DB::raw('(select MIN(property_sharing.price) AS min_propprice from property_sharing where property_sharing.propertyID = p.propertyID) as min_propprice'), 'p.gender', 'p.propertyID', 'p.slug', 'p.title', DB::raw('(select group_concat(property_photo.photoName) AS photo from property_photo where property_photo.propertyID = p.propertyID) as photo'))->where('favourite.userID', '=', Auth::guard('user')->user()->id)->get()->toArray();

        return $query;
    }

    public function setFavourite($slug)
    {
        //DB::enableQueryLog();
        if (Auth::guard('user')->check()) {
            $property = DB::table('property')->select('propertyID')->where('slug', $slug)->first();

            $query = DB::table('favourite');
            $favouriteID = $query->select('favouriteID')->where('userID', Auth::guard('user')->user()->id)->where('propertyID', $property->propertyID)->first();
            if (!empty($favouriteID->favouriteID)) {
                $delete = DB::table('favourite')->where('favouriteID', $favouriteID->favouriteID)->delete();
                if ($delete) {
                    DB::table("users")->where('id', Auth::guard('user')->user()->id)->increment('remainingFavCount');
                    return 'deleted';
                }
            } else {
                $favouriteID = DB::table('favourite')->insertGetId(['userID' => Auth::guard('user')->user()->id,
                    'propertyID' => $property->propertyID]);
                if ($favouriteID) {
                    DB::table("users")->where('id', Auth::guard('user')->user()->id)->decrement('remainingFavCount');
                }
                return "inserted";
            }
        } else {
            return "not_auth";
        }
        //dd(DB::getQueryLog());
    }

    public function propertyDetails($slug)
    {

        $facilitiesArray = GlobalHelpers::facilitiesDropdown();
        if (Auth::guard('user')->check()) {
            $query = DB::table('property')->select('property.*', 'ps.id as propertyRoomSharingID', 'ps.roomSharingID', 'ps.price', 'ps.availability', 'pp.propertyPhotoID', 'pp.photoName', 'rf.*', DB::raw("(select propertyID from favourite where userID=" . Auth::guard('user')->user()->id . " and propertyID = property.propertyID) as favourite"));

        } else {

            $query = DB::table('property')->select('property.*', 'ps.id as propertyRoomSharingID', 'ps.roomSharingID', 'ps.price', 'ps.availability', 'pp.propertyPhotoID', 'pp.photoName', 'rf.*');
        }
        $query->leftJoin('property_sharing as ps', 'property.propertyID', '=', 'ps.propertyID');
        $query->leftjoin('property_photo as pp', 'ps.id', '=', 'pp.propertyRoomSharingID');
        $query->leftJoin('room_facilities as rf', 'rf.propertyRoomSharingID', '=', 'ps.id');
        $query->where('property.status', '=', "Active");
        /*foreach ($facilitiesArray as $key => $value) {
            $query->where("rf.$key", '=','1');
        }*/
        $propertyDetails = $query->where('property.slug', '=', $slug)->get();
        $propertyDetailsArray = array();
        /*echo "<pre>";
        print_r($propertyDetails);exit;*/
        foreach ($propertyDetails as $key => $value) {
            $propertyRoomSharingID = '';
            foreach ($value as $k => $v) {

                //echo "<br>$k. ======= .$v";
                if ($k == 'propertyRoomSharingID') {
                    $propertyRoomSharingID = $v;
                    //$propertyDetailsArray[$v] = $v;
                }

                if ($k == 'propertyPhotoID') {
                    $propertyPhotoID = $v;
                }
                if ($k == 'photoName') {
                    $file = '';
                    if (!empty($v)) {
                        $path = 'images/property';
                        $imgName = $v;
                        $file = str_replace("\\", "/", ImageHelpers::getOriginalImage($path, $imgName));
                        $propertyDetailsArray['sharing'][$propertyRoomSharingID]['file'][$propertyPhotoID] = $file;
                    } else {
                        $path = 'images/property';
                        $file = str_replace("\\", "/", ImageHelpers::getOriginalImage($path));
                        $propertyDetailsArray['sharing'][$propertyRoomSharingID]['file'][0] = $file;
                    }
                }
                if ($k == 'availability' || $k == 'price' || $k == 'propertyRoomSharingID') {
                    $propertyDetailsArray['sharing'][$propertyRoomSharingID][$k] = $v;
                } else {
                    $propertyDetailsArray[$k] = $v;
                }

                //print_r($propertyDetailsArray);
                if ($k == 'roomFacilitiesID') {
                    $roomFacilitiesID = $v;
                }
                if (isset($facilitiesArray[$k]) && $v == 1) {
                    $propertyDetailsArray['sharing'][$propertyRoomSharingID]['facilities'][$k] = $v;
                }
                //$propertyDetailsArray
                if ($k == 'rules' && !empty($v)) {
                    $propertyDetailsArray[$k] = explode(PHP_EOL, $v);
                }
            }
        }
        /*echo "<pre>";
                print_r($propertyDetailsArray);exit;*/
        return $propertyDetailsArray;
    }

//ADD BY CT
    /*
     * This function is used to get properties data by search
     */
    public static function getProperties($input,$count=false,$page = 1)
    {
//        dd($input);
//        DB::enableQueryLog();
        $records = new \App\Model\frontend\Property();
        if (isset($input["propertyType"])) {
			$records = $records->where('propertyTypeID', "=", $input["propertyType"]);
		} else {
			$query = DB::table('property_type')->select(DB::raw('GROUP_CONCAT(propertyTypeID) as propertyType') )->where('status','Active')->get();
			$records = $records->whereIN('propertyTypeID', explode(",",$query[0]->propertyType));
		}
        if (isset($input["availability"])) {
            $records = $records->whereIN("numberOfRoom", $input["availability"]);
        }
        if (isset($input["bed_sharing"])) {
            $records = $records->whereIN("sharingID", $input["bed_sharing"]);
        }
        if (isset($input["accommodation"])) {
            $records = $records->whereIN("gender", $input["accommodation"]);
        }
		if (isset($input["forMaleOrFemale"])) {
			$genderDropdown = GlobalHelpers::accomodationFor();
            $records = $records->where("gender", $genderDropdown[$input["forMaleOrFemale"]]);
        }
        if (isset($input["condition"])) {
            $records = $records->whereIN("conditionID", $input["condition"]);
        }
        if (isset($input["availability_type"])) {
            $records = $records->whereIN("availability", $input["availability_type"]);
        }
        if (isset($input["searchByLoc"])) {
            $records = $records->whereIN("areaID", $input["searchByLoc"]);
        }
        if (isset($input["suitable"])) {
            $records = $records->whereIN("roomSharingID", $input["suitable"]);
        }

		$records->orderBy('updated_at', 'desc');

        if($count)
            return $records->count();
        else
            return $records = $records->paginate(5, ['*'], 'page', $page);
//        dd(DB::getQueryLog());
     }


}
