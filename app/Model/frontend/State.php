<?php

namespace App\Model\frontend;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{

    //ADD BY CITY
    protected $table="state";

    public static function getRecordById($state_id){
        $data=self::select("stateName")->where("stateID","=",$state_id)->first();
        return $data;
    }

    public static function getAllState(){
        $state_list=[];
        $data=self::select("stateID","stateName")->where("status","=","Active")->get();
        foreach ($data as $state){
            $state_list[]=[
                "id"=>$state["stateID"],
                "name"=>$state["stateName"]
            ];
        }
        return $state_list;
    }

}
