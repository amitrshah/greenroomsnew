<?php
namespace App\Model\frontend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Carbon\Carbon;
use Mail;
use App\Libraries\GlobalHelpers;

Class Page extends Model{

	public function saveContactUs($request){

		$name 		= ($request->input('name') != "") ? $request->input('name') : null;
		$email 		= ($request->input('email') != "") ? $request->input('email') : null;
		$message 	= ($request->input('message') != "") ? $request->input('message') : null;
		
		$final_data = [
						'name' => $name, 
						'email'=> $email,
						'message' => $message, 
						'created_at' => Carbon::now()
						];
		$id = DB::table('contactus')->insertGetId($final_data);
		if($id){
			
            $to_email = env('CONTACT_US_EMAIL');
            $subject = "Contact Us";
            $from_email = env('MAIL_USERNAME');
            $template = 'email_template.contactUs';
            $params['to_name'] = "Admin";
            $params['message'] = $message;
            $params['name'] = $name;
            $params['email'] = $email;

			//GlobalHelpers::sendMail($to_email, $from_email, $subject, $template, array(), $params, $path = array());
		}
		return $id;
	}
}