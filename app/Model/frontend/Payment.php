<?php

namespace App\Model\frontend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input; // Input
use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use DB;
use App\Libraries\GlobalHelpers;
use App\Libraries\ImageHelpers;
use Kyslik\ColumnSortable\Sortable;

class Payment extends Model {

    use Sortable;
    protected $table = "payment_details";
    public $sortable = [
                        'orderID',
                        'amount',
                        'txnDate',
                        'gatewayName',
                        'responseStatus',
                        'resposeMessage',
                        'status'
                    ];
    public $sortableAs = [
                        'userName',
                        'planName',
                        'cityName',
                        ];

    public function getRecords(Payment $payment){
        $record_per_page = env('RECORDS_PER_PAGE_FRONTEND');
        $payment = $payment->join('plan_cities as pc', 'pc.planCityID', '=', 'payment_details.planCityID')
                            ->join('city', 'city.cityID', '=', 'pc.cityID')
                            ->join('plan as p', 'pc.planID', '=', 'p.planID')
                            ->select('payment_details.*', 'p.planName','city.cityName' );
        $payment = $payment->where('payment_details.userID', '=', Auth::guard('user')->user()->id);
        
        if(empty($_GET['sort']) || empty($_GET['order']) ){
            $payment = $payment->orderBy('paymentDetailsID', 'desc');
        }

        if( !empty($_GET['plan']) ){
            $payment = $payment->where('p.planID', '=', $_GET['plan']);
        }

        if( !empty($_GET['city']) ){
            $payment = $payment->where('pc.cityID', '=', $_GET['city']);
        }

        if( !empty($_GET['orderID']) ){
            $payment = $payment->where('payment_details.orderID', 'like', '%'.$_GET['orderID'].'%');
        }

        if( !empty($_GET['status']) ){
            $payment = $payment->where('payment_details.responseStatus', '=', $_GET['status']);
        }

        /*if( !empty($_GET['txnDate']) ){
            $txnDateRangeArray = explode(" - ",$_GET['txnDate']);
            $payment = $payment->whereBetween('payment_details.txnDate',$txnDateRangeArray);
        }*/

        $data = $payment->sortable()->paginate($record_per_page);
        //$data = $payment->sortable()->paginate(10);
        return $data;
    }



    public function planCityDropdown(){
        $query =  DB::table('plan_cities as pc');
        $query->join('city', 'city.cityID', '=', 'pc.cityID');
        $query->join('plan', 'plan.planID', '=', 'pc.planID');
        $query->where('city.status', '=', 'Active');
        $query->where('plan.status', '=', 'Active');
        $query->groupBy('pc.cityID');
        $planCity = $query->pluck('city.cityName','pc.cityID')->toArray();
        return $planCity;
    }

    public function planCityByPlanCityID($planCityID){
        $query =  DB::table('plan_cities as pc');
        $query->join('city', 'city.cityID', '=', 'pc.cityID');
        $query->join('plan', 'plan.planID', '=', 'pc.planID');
        $query->where('plan.status', '<>', 'Delete');
        $query->where('pc.planCityID','=',$planCityID);
        $planCity = $query->select('plan.*','city.cityName')->first();
        return $planCity;
    }

    public function getPlansByCityID($cityID){
        $query =  DB::table('plan_cities as pc');
        $query->join('plan', 'plan.planID', '=', 'pc.planID');
        $query->where('plan.status', '=', 'Active');
        $query->where('pc.cityID', '=', $cityID);
        $query->groupBy('plan.planID');
        $plan = $query->pluck('plan.planName','plan.planID')->toArray();
        return $plan;
    }

    public function getPlansDatailsByPlanID($planID,$cityID){
       
        $query =  DB::table('plan')->select('plan.*','city.cityName','city.cityID','pc.planCityID');
        $query->join('plan_cities as pc', 'plan.planID', '=', 'pc.planID');
        $query->join('city', 'city.cityID', '=', 'pc.cityID');
        $query->where('plan.status', '=', 'Active');
        $query->where('pc.planID', '=', $planID);
        $query->where('pc.cityID', '=', $cityID);
        $query->groupBy('plan.planID');
        $plan = $query->first();

        return $plan;
    }

    public function getPaymentDetailsByOrderID($orderID){
        $query =  DB::table('payment_details as pd')->select('p.*','pd.*','pc.cityID');
        $query->join('plan_cities as pc', 'pc.planCityID', '=', 'pd.planCityID');
        $query->join('plan as p', 'pc.planID', '=', 'p.planID');
        $query->where('pd.orderID', '=', $orderID);
        $paymentDetails = $query->first();
        return $paymentDetails;
    }

    public function savePayment($data){
        try {
            $insertDataArray = [
                                    'orderID' => $data['orderID'],
                                    'userID' => $data['userID'],
                                    'amount' => $data['amount'],
                                    'planCityID' => $data['planCityID'],
                                ]; 
            $id =  DB::table('payment_details')->insertGetId($insertDataArray);
            return $id;
        } catch (Exception $e) {
            return false;
        }
            return false;
    }


    public function updatePayment($data){
        try {
            
            $now = Carbon::now();
            $orderID   = $data['orderID'];//($response->ORDERID != "") ? $response->ORDERID : '';
            $txnID     = (isset($response->TXNID) && $response->TXNID != ""  ) ? $response->TXNID : uniqid();
            $banktxnID     = (isset($response->BANKTXNID) && $response->BANKTXNID != "") ? $response->BANKTXNID : uniqid();
            $responseStatus    = (isset($response->STATUS) && $response->STATUS != "") ? $response->STATUS : 'TXN_SUCCESS';
            $responseCode  = (isset($response->RESPCODE) && $response->RESPCODE != "") ? $response->RESPCODE : '01';
            $responseMessage    = (isset($response->RESPMSG) && $response->RESPMSG != "") ? $response->RESPMSG : 'Txn Successful.';
            $txnDate   = (isset($response->TXNDATE) && $response->TXNDATE != "") ? $response->TXNDATE : $now;
            $gatewayname    = (isset($response->GATEWAYNAME) && $response->GATEWAYNAME != "") ? $response->GATEWAYNAME : 'WALLET';
            $bankName  = ( isset($response->BANKNAME) && $response->BANKNAME != "") ? $response->BANKNAME : '';
            $paymentMode   = (isset($response->PAYMENTMODE) && $response->PAYMENTMODE != "") ? $response->PAYMENTMODE : 'PPI';
            $CHECKSUMHASH   = (isset($response->CHECKSUMHASH) && $response->CHECKSUMHASH != "") ? $response->CHECKSUMHASH : uniqid();   

            $dataUpdate = [
                            'txnID' => $txnID,
                            'banktxnID' => $banktxnID, 
                            'responseStatus' => $responseStatus, 
                            'responseCode' => $responseCode, 
                            'responseMessage' => $responseMessage, 
                            'txnDate' => $txnDate, 
                            'gatewayname' => $gatewayname, 
                            'bankName' => $bankName, 
                            'paymentMode' => $paymentMode, 
                            'CHECKSUMHASH' => $CHECKSUMHASH, 
                        ];
            $result = DB::table('payment_details')->where('orderID','=',$orderID)->update($dataUpdate);

            $paymentDetails = $this->getPaymentDetailsByOrderID($orderID);
                //return $id;
            if($responseStatus == 'TXN_SUCCESS'){

                $planCityFavourite = $this->planCityFavourite($paymentDetails->planCityID,$paymentDetails->cityID);
                $planCityFavouriteArray = [];
                
                foreach ($planCityFavourite as $key => $value) {
                    $planCityFavouriteArray[$key] = $value->favouriteID;
                }
                
                $count = count($planCityFavourite);
                
                $remainingFavCount = $paymentDetails->favLimit;
                if($paymentDetails->favLimit > $count){
                    $remainingFavCount = $remainingFavCount - $count;
                }

                $updateUser = DB::table('users')
                ->where('id', Auth::guard('user')->user()->id)
                ->update([
                    'premiumUser' => 1, 
                    'planCityID' => $paymentDetails->planCityID,
                    'membershipExpiry' => Carbon::now()->addDay($paymentDetails->duration),
                    'remainingFavCount' => $remainingFavCount,
                    'totalFavCount' => $paymentDetails->favLimit,
                    ]);

                //$allFavouritePropertyNotInPlan = 

                $deleteOtherCityFavourite = DB::table('favourite')
                ->whereNotIn('favouriteID', $planCityFavouriteArray)
                ->where('userID', Auth::guard('user')->user()->id)
                ->delete();

            }
            return $result;
        } catch (Exception $e) {
            return false;
        }
    }

    public function planCityFavourite($planCityID,$cityID){
            $result = DB::table('favourite')->select('favouriteID')
                    ->join('property as p', 'favourite.propertyID', '=', 'p.propertyID')
                    ->join('plan_cities as pc', 'pc.cityID', '=', 'p.cityID')
                    ->where('pc.planCityID', "=",$planCityID)
                    ->where('pc.cityID', "=",$cityID)
                    ->where('favourite.userID', "=", Auth::guard('user')->user()->id)->get()->toArray();
            //print_r($result);exit;
            return $result;
    }
}
