<?php
namespace App\Model\frontend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Kyslik\ColumnSortable\Sortable;
use DB;
use Auth;
use Carbon\Carbon;
use App\Libraries\ImageHelpers;
use App\Libraries\GlobalHelpers;

Class Favourite extends Model{
    use Sortable;
	protected $table = "favourite";
	public $sortable = ['favouriteID',
						'gender',
                        ];
	public $sortableAs = ['propertyID',
						'title',
						'min_propprice',
						'photo'
						];	

	public function getRecords(Favourite $favourite){

        $record_per_page = env('RECORDS_PER_PAGE_FRONTEND');
		if(empty($_GET['sort']) || empty($_GET['order']) ){
			$favourite = $favourite->orderBy('title', 'desc');
		}
		if( !empty($_GET['title']) ){
			$favourite = $favourite->where('p.title', 'like', '%'.$_GET['title'].'%');
		}


        $favourite = $favourite->join('property as p', 'favourite.propertyID', '=', 'p.propertyID')
                    //->leftjoin('property_photo as pp', 'p.propertyID', '=', 'pp.propertyID')
                    ->select('favourite.favouriteID',DB::raw('(select MIN(property_sharing.price) AS min_propprice from property_sharing where property_sharing.propertyID = p.propertyID) as min_propprice'),'p.gender','p.propertyID','p.title',DB::raw('(select group_concat(property_photo.photoName) AS photo from property_photo where property_photo.propertyID = p.propertyID) as photo'))->where('favourite.userID', '=', Auth::guard('user')->user()->id );
        $data = $favourite->where('p.status', '<>', 'Delete')->sortable()->paginate($record_per_page);
        
        /*$favourite = $favourite->leftJoin('property', 'property.propertyID', '=', 'ar.areaID')
					->leftJoin('city as ct', 'property.cityID', '=', 'ct.cityID')
					->leftJoin('state as st', 'property.stateID', '=', 'st.stateID')
					->leftJoin('users as u', 'property.createdBy', '=', 'u.id')
                    ->select('property.propertyID','property.companyName','property.address','property.description','property.status as status','st.stateName','ct.cityName','ar.areaName','u.name as createdBy','u.id as userID');*/
		
		/*

		if( !empty($_GET['status']) ){
			$favourite = $favourite->where('status', 'like', '%'.$_GET['status'].'%');
		}*/

		//$data = $favourite->where('status', '<>', 'Delete')->sortable()->paginate($record_per_page);
		//$data = $favourite->sortable()->paginate(10);
		return $data;
	}

	public function getRecordById($id){

		
		$data = DB::table('favourite')->where('favouriteID',$id)->where('userID','=',Auth::guard('user')->user()->id)->first();

		return $data;
	}

	public function deleteRecordById($id){
		
		try {
			$result = DB::table('favourite')->where('favouriteID',$id)->delete();
			DB::table("users")->where('id',Auth::guard('user')->user()->id)->increment('remainingFavCount');
			return 'true';
		} catch (Exception $e) {
			return 'false';
		}
		return 'false';
	}
        
        
        public function saveProperty($request) {

        try {

            $title = ($request->input('companyName') != "") ? $request->input('companyName') : null; //mehul change
            $companyName = ($request->input('companyName') != "") ? $request->input('companyName') : null;
            $companyNumber = ($request->input('companyNumber') != "") ? $request->input('companyNumber') : null;
            $propertyType = ($request->input('propertyType') != "") ? $request->input('propertyType') : null;
            $propertyOwnerID = ($request->input('propertyOwnerID') != "") ? $request->input('propertyOwnerID') : null;
            $condition = ($request->input('condition') != "") ? $request->input('condition') : null;
            $address = ($request->input('address') != "") ? $request->input('address') : null;
//            $stateID = ($request->input('state') != "") ? $request->input('state') : null;
//            $cityID = ($request->input('city') != "") ? $request->input('city') : null;
//            $areaID = ($request->input('area') != "") ? $request->input('area') : null;
            $landmark = ($request->input('landmark') != "") ? $request->input('landmark') : null;
            $latitude = ($request->input('latitude') != "") ? $request->input('latitude') : null;
            $longitude = ($request->input('longitude') != "") ? $request->input('longitude') : null;
            $gender = ($request->input('gender') != "") ? $request->input('gender') : null;
            $mealsIncludedID = ($request->input('mealsIncludedID') != "") ? $request->input('mealsIncludedID') : null;
            $description = ($request->input('description') != "") ? $request->input('description') : null;
            $totalsquarefeet = ($request->input('total_square_feet') != "") ? $request->input('total_square_feet') : null;
            $societyamenityID = ($request->input('societyamenityID') != "") ? $request->input('societyamenityID') : null;
            $floorID = ($request->input('floorID') != "") ? $request->input('floorID') : null;
            $avaialble_from = ($request->input('avaialble_from') != "") ? $request->input('avaialble_from') : null;
            $availability = ($request->input('availability') != "") ? $request->input('availability') : null;
            $old_building = ($request->input('old_building') != "") ? $request->input('old_building') : null;

            $rules = ($request->input('rulesID') != "") ? $request->input('rulesID') : null;
            $furnitureID = ($request->input('furnitureID') != "") ? $request->input('furnitureID') : null;
            $facilityID = ($request->input('facilityID') != "") ? $request->input('facilityID') : null;
            $status = ($request->input('status') != "") ? $request->input('status') : null;
            $createdBy = Auth::guard('user')->user()->id;
            $created_at = Carbon::now();
            $strGender = implode(',', $gender);

            if ($mealsIncludedID != '') {
                $strmealsIncluded = implode(',', $mealsIncludedID);
            } else {
                $strmealsIncluded = '';
            }
            $societyamenityIDdata = implode(',', $societyamenityID);
            $avaialbledate = date('Y-m-d', strtotime($avaialble_from));

//            $stateName = GlobalHelpers::stateDropdown($stateID);
//            $cityName = GlobalHelpers::cityDropdown($stateID, $cityID);
//            $areaName = GlobalHelpers::areaDropdown($stateID, $cityID, $areaID);
//            $slug = $stateName[$stateID] . "-" . $cityName[$cityID] . "-" . $areaName[$areaID] . "-" . $title . "-" . time();
//            $slug = str_slug($slug, "-");
            $rulesdata = implode(',', $rules);
            $furnituredata = implode(',', $furnitureID);
            $facilitydata = implode(',', $facilityID);
            $sharingID = ($request->input('sharingID') != "") ? $request->input('sharingID') : null;
            $unitavailableID = ($request->input('unitavailableID') != "") ? $request->input('unitavailableID') : null;
            $rent = ($request->input('rent') != "") ? $request->input('rent') : null;
            //mehul change
            $numberOfRoom = ($request->input('numberOfRoom') != "") ? $request->input('numberOfRoom') : null;
            $facing = ($request->input('facing') != "") ? $request->input('facing') : null;
            $noticePeriod = ($request->input('noticePeriod') != "") ? $request->input('noticePeriod') : null;
            $deposit = ($request->input('deposit') != "") ? $request->input('deposit') : null;
            //mehul change over

            $new_propertyID = DB::table('property')->insertGetId([
                'title' => $title,
                'companyName' => $companyName,
                'availability' => $availability,
                'old_building' => $old_building,
                'companyNumber' => $companyNumber,
                'propertyTypeID' => $propertyType,
                'propertyOwnerID' => $propertyOwnerID,
                'conditionID' => $condition,
                'address' => $address,
//                'stateID' => $stateID,
//                'cityID' => $cityID,
//                'areaID' => $areaID,
                'latitude' => $latitude,
                'longitude' => $longitude,
                'landmark' => $landmark,
                'mealsIncludedID' => $strmealsIncluded,
                'gender' => $strGender,
                'description' => $description,
                'rules' => $rulesdata,
//                'slug' => $slug,
                'status' => $status,
                'createdBy' => $createdBy,
                'created_at' => $created_at,
                'total_square_feet' => $totalsquarefeet,
                'societyamenityID' => $societyamenityIDdata,
                'floorID' => $floorID,
                'avaialble_from' => $avaialbledate,
                'furnitureID' => $furnituredata,
                'facilityID' => $facilitydata,
                'sharingID' => $sharingID,
                'unitavailableID' => $unitavailableID,
                'price' => $rent,
                //mehul change
                'numberOfRoom' => $numberOfRoom,
                'facing' => $facing,
                'noticePeriod' => $noticePeriod,
                'deposit' => $deposit
                //mehul change over
            ]);

            if ($new_propertyID) {
                //mehul change
                $new_property_sharing_id = DB::table('property_sharing')->insertGetId([
                    'propertyID' => $new_propertyID,
                    'roomSharingID' => $sharingID,
                    'availability' => $unitavailableID,
                    'price' => $rent,
                ]);
                //mehul change over
                $sharing = $request['sharing'];

                foreach ($sharing as $key1 => $val) {
                    $propertySharingID = "";
                    foreach ($val as $key => $value) {

                        if (isset($value['file'])) {
                            $new_files = $value['file'];
                            unset($value['file']);
                        }

                        if (isset($new_files)) {
                            foreach ($new_files as $imagekey => $imageValue) {
                                $fileObj = $imageValue;
                                $file = '';
                                $imgCount = 0;

                                if (!empty($fileObj)) {

                                    $propertyImageSizeArr = \Config::get('filesystems.propertyImageSizeArr');
                                    $savePath = storage_path('images/property');
                                    $imgName = rand() . "_" . time() . '.jpg';
                                    $convertFormat = 'jpg';
                                    $convertQuality = '100';

                                    $imgCount = ImageHelpers::uploadResizeImage($fileObj, $propertyImageSizeArr, $savePath, $imgName, $convertFormat, $convertQuality);

                                    if ($imgCount) {
                                        $id = DB::table('property_photo')->insertGetId([
                                            'propertyID' => $new_propertyID,
                                            'photoName' => $imgName,
                                            'propertyRoomSharingID' => $new_property_sharing_id //mehul change
                                        ]);
                                    }
                                }
                            }
                            unset($new_files);
                        }
                    }
                }
            }
            //return $id;
            return $new_propertyID;
        } catch (Exception $e) {
            return false;
        }
    }
        
        
}