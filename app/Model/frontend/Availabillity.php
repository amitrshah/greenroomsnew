<?php

namespace App\Model\frontend;

use Illuminate\Database\Eloquent\Model;

class Availabillity extends Model
{
    protected $table="availability";

    //ADD BY CT
    public static function availablebhkArr(){

        $records=self::select("availabilityID","title")->where("status","=","Active")->get();
        $data_array=[];
        if(!empty($records)){
            foreach ($records as $record){
                $data_array[$record["availabilityID"]]=[
                    "id"=>$record["availabilityID"],
                    "title"=>$record["title"]
                ];
            }
        }
        return $data_array;
    }



}
