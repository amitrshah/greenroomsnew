<?php

namespace App\Model\frontend;

use Illuminate\Database\Eloquent\Model;

class PropertySocietyamenity extends Model
{
    protected $table="property_societyamenity";
    //ADD BY CT

    public static function propertySocietyamenity(){
        $data_array=[];
        $records=self::select("societyamenityID","title")->where("status","=","Active")->get();
        if(!empty($records)){
            foreach ($records as $record){
                $data_array[]=[
                    "id"=>$record["societyamenityID"],
                    "title"=>$record["title"]
            ];
            }

        }
        return $data_array;
    }
}
