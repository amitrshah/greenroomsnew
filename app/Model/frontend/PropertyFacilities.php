<?php

namespace App\Model\frontend;

use Illuminate\Database\Eloquent\Model;

class PropertyFacilities extends Model
{
    protected $table = "property_facilities";

    //ADD BY CT
    public static function propertyFacilities()
    {
        $data_array=[];
        $records=self::select("facilitiesID","title")->where("status","=","Active")->orderBy("title","ASC")->get();
        if(!empty($records)){
            foreach ($records as $value){
                $data_array[]=[
                    "id"=>$value["facilitiesID"],
                    "title"=>$value["title"]
                ];
            }
        }
        return $data_array;
    }
}
