<?php

namespace App\Model\frontend;

use Illuminate\Database\Eloquent\Model;

class AvailabillityType extends Model
{
    protected $table="availibility_type";
    //ADD BY CT

    public static function availabilityType(){
        $data_array=[];
        $records=self::select("availibilityTypeID","title")->where("status","=","Active")->orderBy("title","ASC")->get();
        if(!empty($records)){
            foreach ($records as $record) {
                $data_array[]=[
                    "id"=>$record["availibilityTypeID"],
                    "title"=>$record["title"]
                ];
            }
        }
        return $data_array;
    }
}
