<?php

namespace App\Model\frontend;

use Illuminate\Database\Eloquent\Model;

class Functions extends Model
{
    public static $errors;
    public static function CheckValidate($data,$type){
//        dd($data);
        $messages=[];
        if(!empty($type)){
            $validator = \Validator::make($data, [
                'fname' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'cname' => 'required',
                'password' => !empty($data["password"])?'sometimes|required':"",
                'state_id'=>"required",
                'city_id'=>"required"
            ], $messages);
        }else{
            $validator = \Validator::make($data, [
                'fname' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'cname' => 'required',
                'password' => !empty($data["password"])?'sometimes|required':"",
                'cpassword' => !empty($data["password"])?'required|same:password':"",
            ], $messages);

        }

        if ($validator->fails()) {
            self::$errors = $validator;
            return false;
        } else
            return true;

    }
}
