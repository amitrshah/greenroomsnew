<?php
namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon;

Class City extends Model{
    use Sortable;
	protected $table = "city";
	public $sortable = ['cityID',
                        'cityName',
                        'status'];
    public $sortableAs = [
                        'stateName',
                        ];

	public function getRecords(City $city){

        $record_per_page = env('RECORDS_PER_PAGE_BACKEND');
		$city = $city->join('state as st', 'st.stateID', '=', 'city.stateID')
					->select('city.cityID','city.cityName','st.stateName','city.status');
		if(empty($_GET['sort']) || empty($_GET['order']) ){
			$city = $city->orderBy('cityID', 'desc');
		}
		if( !empty($_GET['cityName']) ){
			$city = $city->where('city.cityName', 'like', '%'.$_GET['cityName'].'%');
		}

		if( !empty($_GET['stateName']) ){
			$city = $city->where('st.stateName', 'like', '%'.$_GET['stateName'].'%');
		}

		if( !empty($_GET['status']) ){
			$city = $city->where('city.status', '=', $_GET['status']);
		}

		$data = $city->where('city.status', '<>', 'Delete')->sortable()->paginate($record_per_page);
		//$data = $city->sortable()->paginate(10);
		return $data;
	}

	public function getRecordById($cityID){

		
		$data = DB::table('city')->join('state as st', 'st.stateID', '=', 'city.stateID')->where('city.cityID',$cityID)->where('city.status','<>','Delete')->select('city.cityID','city.cityName','city.status','st.stateID')->first();

		return $data;
	}

	public function saveCity($request){

		$cityName 		= ($request->input('cityName') != "") ? $request->input('cityName') : null;
		$stateID 		= ($request->input('state') != "") ? $request->input('state') : null;
		$status 	= ($request->input('status') != "") ? $request->input('status') : null;
		$addedBy = auth()->guard('admin')->user()->id;
		$created_at = Carbon::now();
		
		$cityID = DB::table('city')->insertGetId([
			'cityName' => $cityName, 
			'stateID' => $stateID, 
			'status' => $status, 
			'addedBy'=> $addedBy,
			'created_at' => $created_at

		]);

		return $cityID;
	}

	public function updateCity($request){
		try {
			$cityID 		= ($request->input('cityID') != "") ? $request->input('cityID') : null;
			$decoded_cityID = base64_decode($cityID);
			$cityName 		= ($request->input('cityName') != "") ? $request->input('cityName') : null;
			$stateID 		= ($request->input('state') != "") ? $request->input('state') : null;
			$status 	= ($request->input('status') != "") ? $request->input('status') : null;
			$updated_at = Carbon::now();
			
			$result = DB::table('city')->where('cityID',$decoded_cityID)->update([
				'cityName' => $cityName, 
				'stateID' => $stateID, 
				'status' => $status, 
				'updated_at' => $updated_at
				
			]);
			return true;
		} catch (Exception $e) {
			return false;
		}
		return false;
	}

	public function deleteRecordById($cityID){
		
		try {
			$result = DB::table('city')->where('cityID',$cityID)->update(['status'=>'Delete']);
			return 'true';
		} catch (Exception $e) {
			return 'false';
		}
		return 'false';
	}
}