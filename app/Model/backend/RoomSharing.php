<?php
namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Kyslik\ColumnSortable\Sortable;

Class RoomSharing extends Model{
    use Sortable;
	protected $table = "room_sharing";
	public $sortable = ['id',
                        'title',
                        'maximum_beds',
                        'status'];

	public function getRecords(RoomSharing $room){

        $record_per_page = env('RECORDS_PER_PAGE_BACKEND');
		
		if(empty($_GET['sort']) || empty($_GET['order']) ){
			$room = $room->orderBy('id', 'desc');
		}
		if( !empty($_GET['title']) ){
			$room = $room->where('title', 'like', '%'.$_GET['title'].'%');
		}

		if( !empty($_GET['status']) ){
			$room = $room->where('status', 'like', '%'.$_GET['status'].'%');
		}

		$data = $room->where('status', '<>', 'Delete')->sortable()->paginate($record_per_page);
		//$data = $room->sortable()->paginate(10);
		return $data;
	}

	public function getRecordById($id){

		
		$data = DB::table('room_sharing')->where('id',$id)->where('status','<>','Delete')->first();

		return $data;
	}

	public function saveRoomSharing($request){

		$title 		= ($request->input('title') != "") ? $request->input('title') : null;
		$status 	= ($request->input('status') != "") ? $request->input('status') : null;
		$maximum_beds = ($request->input('maximum_beds') != "") ? $request->input('maximum_beds') : null;

		$id = DB::table('room_sharing')->insertGetId([
			'title' => $title, 
			'status' => $status, 
			'maximum_beds'=> $maximum_beds
		]);

		return $id;
	}

	public function updateRoomSharing($request){
		try {
			$id 		= ($request->input('id') != "") ? $request->input('id') : null;
			$decoded_id = base64_decode($id);
			$title 		= ($request->input('title') != "") ? $request->input('title') : null;
			$status 	= ($request->input('status') != "") ? $request->input('status') : null;
			$maximum_beds = ($request->input('maximum_beds') != "") ? $request->input('maximum_beds') : null;

			$result = DB::table('room_sharing')->where('id',$decoded_id)->update([
				'title' => $title, 
				'status' => $status, 
				'maximum_beds'=> $maximum_beds
			]);
			return true;
		} catch (Exception $e) {
			return false;
		}
		return false;
	}

	public function deleteRecordById($id){
		
		try {
			$result = DB::table('room_sharing')->where('id',$id)->update(['status'=>'Delete']);
			return 'true';
		} catch (Exception $e) {
			return 'false';
		}
		return 'false';
	}
}