<?php
namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon;
Class Facilities extends Model{
    use Sortable;
	protected $table = "property_facilities";
	public $sortable = ['facilitiesID',
                        'title',
                        'status'];

	public function getRecords(Facilities $facilities){

                $record_per_page = env('RECORDS_PER_PAGE_BACKEND');
		
		if(empty($_GET['sort']) || empty($_GET['order']) ){
			$facilities = $facilities->orderBy('facilitiesID', 'desc');
		}
		if( !empty($_GET['title']) ){
			$facilities = $facilities->where('title', 'like', '%'.$_GET['title'].'%');
		}

		if( !empty($_GET['status']) ){
			$facilities = $facilities->where('status', 'like', '%'.$_GET['status'].'%');
		}

		$data = $facilities->where('status', '<>', 'Delete')->sortable()->paginate($record_per_page);
		//$data = $propertyType->sortable()->paginate(10);
		return $data;
	}

	public function getRecordById($facilitiesID){

		
		$data = DB::table('property_facilities')->where('facilitiesID',$facilitiesID)->where('status','<>','Delete')->first();

		return $data;
	}
        
        public function getAllRecord(){
            $data = DB::table('property_facilities')->where('status','<>','Delete')->get();
            return $data;
	}
        
	public function saveFacilities($request){

		$title 		= ($request->input('title') != "") ? $request->input('title') : null;
		$status 	= ($request->input('status') != "") ? $request->input('status') : null;
		$created_at = Carbon::now();
		
		$facilitiesID = DB::table('property_facilities')->insertGetId([
			'title' => $title, 
			'status' => $status, 
			'created_at' => $created_at

		]);

		return $facilitiesID;
	}

	public function updateFacilities($request){
		try {
			$facilitiesID 		= ($request->input('facilitiesID') != "") ? $request->input('facilitiesID') : null;
			$decoded_facilitiesID = base64_decode($facilitiesID);
			$title 		= ($request->input('title') != "") ? $request->input('title') : null;
			$status 	= ($request->input('status') != "") ? $request->input('status') : null;
			$updated_at = Carbon::now();
			
			$result = DB::table('property_facilities')->where('facilitiesID',$decoded_facilitiesID)->update([
				'title' => $title, 
				'status' => $status, 
				'updated_at' => $updated_at
				
			]);
			return true;
		} catch (Exception $e) {
			return false;
		}
		return false;
	}

	public function deleteRecordById($facilitiesID){
		
		try {
			$result = DB::table('property_facilities')->where('facilitiesID',$facilitiesID)->update(['status'=>'Delete']);
			return 'true';
		} catch (Exception $e) {
			return 'false';
		}
		return 'false';
	}
}