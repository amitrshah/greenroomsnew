<?php
namespace App\Model\backend;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Kyslik\ColumnSortable\Sortable;
use Hash;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;
use Auth;
Class User extends Model{
	use Notifiable;
	protected $primary_key = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','roleID'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'roleID'
    ];
    use Sortable;
	//protected $table = "user";
/*	public $sortable = ['id',
                        'name',
                        'email',
                        'role_name'];*/
    public $sortableAs = ['id',
                        'name',
                        'email',
                        'premiumUser',
                        'planCityID',
                        'remainingFavCount',
                        'totalFavCount',
                        'membershipExpiry',
                        'role',
                        'status'];
	public function getRecords(User $user){
        $record_per_page = env('RECORDS_PER_PAGE_BACKEND');
		$user = $user->join('user_role as ur', 'users.roleID', '=', 'ur.roleID')
                    ->select('ur.roleName as role', 'users.*', 'users.id as id','users.name as name','users.email as email','users.premiumUser','users.remainingFavCount','users.totalFavCount','users.membershipExpiry','users.membershipExpiry','users.status','users.planCityID','users.remarks');
					
		if(Auth::guard('admin')->user()->roleID == 3){
			$user = $user->whereNotIn('users.roleID', [1,3]);
			//echo $user->toSql();
		}			
					
		if(empty($_GET['sort']) || empty($_GET['order']) ){
			$user = $user->orderBy('id', 'desc');
		}
		if( !empty($_GET['name']) ){
			$user = $user->where('users.name', 'like', '%'.$_GET['name'].'%');
		}
		if( !empty($_GET['email']) ){
			$user = $user->where('users.email', 'like', '%'.$_GET['email'].'%');
		}
		if( !empty($_GET['role']) ){
			$user = $user->where('ur.roleID', 'like', '%'.$_GET['role'].'%');
		}
		if( !empty($_GET['mobile']) ){
			$user = $user->where('users.mobile', 'like', '%'.$_GET['mobile'].'%');
		}
		if( !empty($_GET['status']) ){
			$data = $user->where('users.status', '=', $_GET['status'])->sortable()->paginate($record_per_page);
		} else {			
			//$data = $user->where('users.status', '=', 'Active')->sortable()->paginate($record_per_page);
			$data = $user->sortable()->paginate($record_per_page);
		}
		return $data;
	}
	public function getRecordById($id){
		//$data = DB::table('users')->where('id',$id)->where('status','<>','Delete')->first();
		$data = DB::table('users')->where('id',$id)->first();
		return $data;
	}
	public function saveUser($request){
		$name 		= ($request->input('name') != "") ? $request->input('name') : null;
		$firstName 	= ($request->input('firstName') != "") ? $request->input('firstName') : null;
		$lastName 	= ($request->input('lastName') != "") ? $request->input('lastName') : null;
		$mobile 	= ($request->input('mobile') != "") ? $request->input('mobile') : null;
		$phone 		= ($request->input('phone') != "") ? $request->input('phone') : null;
		$status 	= ($request->input('status') != "") ? $request->input('status') : null;
		$email 		= ($request->input('email') != "") ? $request->input('email') : null;
		$password 	= ($request->input('password') != "") ? $request->input('password') : null;
		$roleID 	= ($request->input('role') != "") ? $request->input('role') : null;
		$gender 	= ($request->input('gender') != "") ? $request->input('gender') : null;
		$remarks 	= ($request->input('remarks') != "") ? $request->input('remarks') : null;
		$membershipExpiry 	= ($request->input('membershipExpiry') != "") ? $request->input('membershipExpiry') : null;
		$final_data = ['name'=>$name,
						'firstName'=>$firstName,
						'lastName'=>$lastName,
						'mobile'=>$mobile,
						'phone'=>$phone,
						'status'=>$status,
						'email'=>$email,
						'password'=>bcrypt($password),
						'roleID'=>$roleID,
						'remainingFavCount'=>env('FREE_USER_FAVOURITE_LIMIT'),
						'totalFavCount'=>env('FREE_USER_FAVOURITE_LIMIT'),
						'gender'=>$gender,'created_at'=>Carbon::now(),
						'membershipExpiry'=>date('Y-m-d h:i:s',strtotime($membershipExpiry)),
						'user_code'=>time(),
						'remarks'=>$remarks
						];
		$id = DB::table('users')->insertGetId($final_data);
		return $id;
	}
	public function updateUser($request){
		try {
			$id 		= ($request->input('id') != "") ? $request->input('id') : null;
			$decoded_id = base64_decode($id);
			$name 		= ($request->input('name') != "") ? $request->input('name') : null;
			$firstName 		= ($request->input('firstName') != "") ? $request->input('firstName') : null;
			$lastName 		= ($request->input('lastName') != "") ? $request->input('lastName') : null;
			$mobile 		= ($request->input('mobile') != "") ? $request->input('mobile') : null;
			$phone 		= ($request->input('phone') != "") ? $request->input('phone') : null;
			$status 	= ($request->input('status') != "") ? $request->input('status') : null;
			//$email = ($request->input('email') != "") ? $request->input('email') : null;
			$password = ($request->input('password') != "") ? $request->input('password') : null;
			$roleID = ($request->input('role') != "") ? $request->input('role') : null;
			$gender = ($request->input('gender') != "") ? $request->input('gender') : null;
			$membershipExpiry 	= ($request->input('membershipExpiry') != "") ? $request->input('membershipExpiry') : null;
			$remarks 	= ($request->input('remarks') != "") ? $request->input('remarks') : null;

			$final_data = [
							'name' => $name, 
							'firstName' => $firstName, 
							'lastName' => $lastName, 
							'mobile' => $mobile, 
							'phone' => $phone, 
							'status' => $status, 
							//'email'=> $email,
							'roleID'=> $roleID,
							'gender'=> $gender,
							'updated_at'=>Carbon::now(),
							'membershipExpiry'=>date('Y-m-d h:i:s',strtotime($membershipExpiry)),
							'remarks'=>$remarks
							];
			if(!empty($password)){
				$final_data['password'] = bcrypt($password);
			}
			$result = DB::table('users')->where('id',$decoded_id)->update($final_data);
			return true;
		} catch (Exception $e) {
			return false;
		}
		return false;
	}
	public function deleteRecordById($id){
		try {
			$result = DB::table('users')->where('id',$id)->update(['status'=>'Delete']);
			return 'true';
		} catch (Exception $e) {
			return 'false';
		}
		return 'false';
	}
	public function updateRecordById($id, $status){
		if($status == 'Delete'){
			$status = "Delete";
		} else if($status == 0) {
			$status = "Inactive";
		} else if($status == 1) {
			$status = "Active";
		}
		try {
			$result = DB::table('users')->whereIn('id',explode(",",$id))->update(['status'=>$status]);
			return 'true';
		} catch (Exception $e) {
			return 'false';
		}
		return 'false';
	}
	public function planDetailsByUserID($userID){
		$query =  DB::table('users as u')->select('pc.cityID','u.id as userID','u.name as name','u.remainingFavCount','u.totalFavCount','u.membershipExpiry','city.cityName','pc.planCityID');
	    $query->join('plan_cities as pc', 'u.planCityID', '=', 'pc.planCityID');
	    $query->join('city', 'city.cityID', '=', 'pc.cityID');
	    $query->where('u.id', '=', $userID);
	    $data = $query->first();
	    return $data;
	}
	static public function userDropdown($field, $id){
		$data = DB::table('users')->where("$field",$id)->where('status','<>','Delete')->pluck('name as userName', 'id as UserID')->toArray();
		return $data;
	}
	public function upgradeValue($request,$data){
		try {
			$userID 	= ($request->input('userID') != "") ? $request->input('userID') : null;
			$createdBy	= Auth::guard('admin')->user()->id;
			$planCityID	= $data->planCityID;
			$upgradeValue	= ($request->input('upgradeValue') != "") ? $request->input('upgradeValue') : null;
			$final_data = [
							'userID' => $userID, 
							'planCityID' => $planCityID, 
							'upgradeValue' => $upgradeValue, 
							'createdBy' => $createdBy, 
							'created_at'=>Carbon::now()
							];
			$request = DB::table('user_plan_upgrade_history')->insert($final_data);
            DB::table("users")->where('id',$userID)->increment('remainingFavCount',$upgradeValue);
            DB::table("users")->where('id',$userID)->increment('totalFavCount',$upgradeValue);
			return true;
		} catch (Exception $e) {
			//return $e->getMessage();
			return false;
		}
		return false;
	}
}