<?php
namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon;
Class Plan extends Model{
    use Sortable;
	protected $table = "plan";
	public $sortable = ['planID',
                        'planName',
                        'tier',
                        'price',
                        'duration',
                        'favLimit',
                        'status',
                    	'created_at'];
    public $sortableAs = [
                        'cityName',
                        ];
	
	public function getRecords(Plan $plan){

        $record_per_page = env('RECORDS_PER_PAGE_BACKEND');
        /*
		$plan = $plan->leftJoin('plan_cities as pct', 'pct.planID', '=', 'pct.planID')
						->leftJoin('city as ct', 'ct.cityID', '=', 'pct.cityID')
						
						->select('plan.*','ct.cityName as cityName');*/
		$plan = $plan->select('plan.*', DB::raw("(select GROUP_CONCAT( cityName) from city left join plan_cities as pct on city.cityID= pct.cityID where pct.planID = plan.planID) as cityName"));						
		if( empty($_GET['sort']) || empty($_GET['order']) ){
			$plan = $plan->orderBy('plan.planID', 'desc');
		}
		if( !empty($_GET['planName']) ){
			$plan = $plan->where('plan.planName', 'like', '%'.$_GET['planName'].'%');
		}

		if( !empty($_GET['status']) ){
			$plan = $plan->where('plan.status', '=', $_GET['status']);
		}

		$data = $plan->where('plan.status', '<>', 'Delete')->sortable()->paginate($record_per_page);
		//$data = $plan->sortable()->paginate(10);

		return $data;
	}

	public function getRecordById($planID){

		$data = DB::table('plan')->select('plan.*', DB::raw("(select GROUP_CONCAT( cityID) as cityID from plan_cities as pct where pct.planID = plan.planID) as cityID"))->where('plan.planID',$planID)->where('plan.status','<>','Delete')->first();

		return $data;
	}

	public function savePlan($request){

		$planName 		= ($request->input('planName') != "") ? $request->input('planName') : null;
		$price 		= ($request->input('price') != "") ? $request->input('price') : null;
		$favLimit 		= ($request->input('favLimit') != "") ? $request->input('favLimit') : null;
		$duration 		= ($request->input('duration') != "") ? $request->input('duration') : null;
		$cityID 		= ($request->input('city') != "") ? $request->input('city') : null;
		$tier 		= ($request->input('tier') != "") ? $request->input('tier') : null;
		$status 	= ($request->input('status') != "") ? $request->input('status') : null;
		$created_at = Carbon::now();
		
		$planID = DB::table('plan')->insertGetId([
			'planName' => $planName, 
			'price' => $price, 
			'favLimit' => $favLimit, 
			'duration' => $duration, 
			'tier' => $tier, 
			'status' => $status, 
			'created_at' => $created_at

		]);
		$insertPlanCity = [];

		foreach ($cityID as $key => $value) {
			if(!empty($value)){

				$insertPlanCity[$key]['cityID'] = $value;
				$insertPlanCity[$key]['planID'] = $planID;
			}
		}
		
		DB::table('plan_cities')->insert($insertPlanCity);

		return $planID;
	}

	public function updatePlan($request){
		try {
			$planID 		= ($request->input('planID') != "") ? $request->input('planID') : null;
			$decoded_planID = base64_decode($planID);
			$planName 		= ($request->input('planName') != "") ? $request->input('planName') : null;
			$price 			= ($request->input('price') != "") ? $request->input('price') : null;
			$favLimit 		= ($request->input('favLimit') != "") ? $request->input('favLimit') : null;
			$duration 		= ($request->input('duration') != "") ? $request->input('duration') : null;
			$cityID 		= ($request->input('city') != "") ? $request->input('city') : null;
			$tier 			= ($request->input('tier') != "") ? $request->input('tier') : null;
			$status 		= ($request->input('status') != "") ? $request->input('status') : null;
			$updated_at 	= Carbon::now();
			
			$result = DB::table('plan')->where('planID',$decoded_planID)->update([
				'planName' => $planName, 
				'price' => $price, 
				'favLimit' => $favLimit, 
				'duration' => $duration, 
				'tier' => $tier,
				'status' => $status, 
				'updated_at' => $updated_at
				
			]);

	        $oldCity = DB::table('plan_cities')/*->whereIn('cityID',$cityID)*/->where('planID',$decoded_planID)->select('cityID')->get()->toArray();
	        $oldCityArry = array();
	        foreach ($oldCity as $key => $value) {
	        	$oldCityArry[$value->cityID] = $value->cityID;
	        }
	        $delCities = array_diff($oldCityArry,$cityID);
	        $newCities = array_diff($cityID,$oldCityArry);
	        if(!empty($delCities)){
	        	DB::table('plan_cities')->whereIn('cityID',$delCities)->where('planID',$decoded_planID)->delete();
	        }
			if(!empty($newCities)){

				$insertPlanCity = [];
				foreach ($newCities as $key => $value) {
					if(!empty($value)){
					
						$insertPlanCity[$key]['cityID'] = $value;
						$insertPlanCity[$key]['planID'] = $decoded_planID;
					}
				}
				
				DB::table('plan_cities')->insert($insertPlanCity);
			}
			return true;
		} catch (Exception $e) {
			return false;
		}
		return false;
	}

	public function deleteRecordById($planID){
		
		try {
			$result = DB::table('plan')->where('planID',$planID)->update(['status'=>'Delete']);
			return 'true';
		} catch (Exception $e) {
			return 'false';
		}
		return 'false';
	}
	
	public function updatePlanById($id, $status){
		if($status === 0) {
			$status = "Inactive";
		} else if($status === 1) {
			$status = "Active";
		}
		try {
			$result = DB::table('plan')->whereIn('planID',explode(",",$id))->update(['status'=>$status]);
			return 'true';
		} catch (Exception $e) {
			return 'false';
		}
		return 'false';
	}
}