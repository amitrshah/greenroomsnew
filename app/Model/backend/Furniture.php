<?php
namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon;
Class Furniture extends Model{
    use Sortable;
	protected $table = "property_furniture";
	public $sortable = ['furnitureID',
                        'title',
                        'status'];

	public function getRecords(Furniture $furniture){

                $record_per_page = env('RECORDS_PER_PAGE_BACKEND');
		
		if(empty($_GET['sort']) || empty($_GET['order']) ){
			$furniture = $furniture->orderBy('furnitureID', 'desc');
		}
		if( !empty($_GET['title']) ){
			$furniture = $furniture->where('title', 'like', '%'.$_GET['title'].'%');
		}

		if( !empty($_GET['status']) ){
			$furniture = $furniture->where('status', 'like', '%'.$_GET['status'].'%');
		}

		$data = $furniture->where('status', '<>', 'Delete')->sortable()->paginate($record_per_page);		
		return $data;
	}

	public function getRecordById($furnitureID){

		
		$data = DB::table('property_furniture')->where('furnitureID',$furnitureID)->where('status','<>','Delete')->first();

		return $data;
	}
        
        public function getAllRecord(){
            $data = DB::table('property_furniture')->where('status','<>','Delete')->get();
            return $data;
	}
        
	public function saveFurniture($request){

		$title 		= ($request->input('title') != "") ? $request->input('title') : null;
		$status 	= ($request->input('status') != "") ? $request->input('status') : null;
		$created_at = Carbon::now();
		
		$furnitureID = DB::table('property_furniture')->insertGetId([
			'title' => $title, 
			'status' => $status, 
			'created_at' => $created_at

		]);

		return $furnitureID;
	}

	public function updateFurniture($request){
		try {
			$furnitureID 		= ($request->input('furnitureID') != "") ? $request->input('furnitureID') : null;
			$decoded_furnitureID = base64_decode($furnitureID);
			$title 		= ($request->input('title') != "") ? $request->input('title') : null;
			$status 	= ($request->input('status') != "") ? $request->input('status') : null;
			$updated_at = Carbon::now();
			
			$result = DB::table('property_furniture')->where('furnitureID',$decoded_furnitureID)->update([
				'title' => $title, 
				'status' => $status, 
				'updated_at' => $updated_at
				
			]);
			return true;
		} catch (Exception $e) {
			return false;
		}
		return false;
	}

	public function deleteRecordById($furnitureID){
		
		try {
			$result = DB::table('property_furniture')->where('furnitureID',$furnitureID)->update(['status'=>'Delete']);
			return 'true';
		} catch (Exception $e) {
			return 'false';
		}
		return 'false';
	}
}