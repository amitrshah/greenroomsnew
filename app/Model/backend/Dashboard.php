<?php
namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Carbon\Carbon;
use Auth;

Class Dashboard extends Model{
    
	public function getProperty(){

        $record_per_page = env('RECORDS_PER_PAGE_BACKEND');
		$property = DB::table('property')->leftJoin('area as ar', 'property.areaID', '=', 'ar.areaID')
					->leftJoin('city as ct', 'property.cityID', '=', 'ct.cityID')
					->leftJoin('state as st', 'property.stateID', '=', 'st.stateID')
					->leftJoin('users as u', 'property.createdBy', '=', 'u.id')
                    ->select('property.title','property.propertyID','property.companyNumber','property.companyName','property.address','property.description','property.status as status','st.stateName','ct.cityName','ar.areaName','u.name as createdBy','u.id as userID');
		$property = $property->orderBy('propertyID', 'desc');
		
		$data = $property->where('property.status', '<>', 'Delete')->paginate(5);
		//$data = $property->sortable()->paginate(10);
		return $data;
	}

	public function getPropertyCount(){

        $property = DB::table('property')
                    ->select(DB::raw("count('1') as propertyCount"));
		
		$data = $property->where('status', '<>', 'Delete')->first();

		return $data->propertyCount;
	}

	public function getUserCount(){

        $query = DB::table('users')
                    ->select(DB::raw("count('1') as userCount"));
		
		$query->where('roleID', '=', config('constants.USER_ROLES'));
		$data = $query->where('status', '<>', 'Delete')->first();

		return $data->userCount;
	}

	public function getPropertyCountByStatus($status=""){

		if($status!=""){
        $property = DB::table('property')
                    ->select(DB::raw("count('1') as propertyCount"))->join('property_type as pt', 'property.propertyID', '=', 'pt.propertyTypeID');
		
		$data = $property->where('property.status', '=', $status)->where('pt.status', '=', $status)->first();
		} else {
			$property = DB::table('property')
                    ->select(DB::raw("count('1') as propertyCount"))->join('property_type as pt', 'property.propertyID', '=', 'pt.propertyTypeID');
			$data = $property->where('property.status', '=', $status)->where('pt.status', '=', $status)->first();
		}
		return $data->propertyCount;
	}
	
	public function getPropertyCountByType($type=""){

		if($type!=""){
        $property = DB::table('property')
                    ->select(DB::raw("count('1') as propertyCount"));
		
		$data = $property->where('propertyTypeID', '=', $type)->first();
		} else {
			$data = DB::table('property')
                    ->select(DB::raw("count('1') as propertyCount"))->first();
		}
		return $data->propertyCount;
	}

	public function getUserCountByStatus($status=""){
		if($status!=""){
			$query = DB::table('users')
						->select(DB::raw("count('1') as userCount"));
			
			//$query->where('roleID', '=', config('constants.USER_ROLES'));
			$data = $query->where('status', '=', $status)->first();
		} else {
			$data = DB::table('users')
						->select(DB::raw("count('1') as userCount"))->first();
		}
		return $data->userCount;
	}

	public function getUserCountByType($type="", $status=""){
		if($type!="" && $status!=""){
			$query = DB::table('users')
						->select(DB::raw("count('1') as userCount"));
			
			//$query->where('roleID', '=', config('constants.USER_ROLES'));
			$data = $query->where('roleID', '=', $type)->where('status', '=', $status)->first();
		} else if($status!=""){
			$query = DB::table('users')
						->select(DB::raw("count('1') as userCount"));
			
			//$query->where('roleID', '=', config('constants.USER_ROLES'));
			$data = $query->where('status', '=', $status)->first();
		}
		else if($type!=""){
			$query = DB::table('users')
						->select(DB::raw("count('1') as userCount"));
			
			//$query->where('roleID', '=', config('constants.USER_ROLES'));
			$data = $query->where('roleID', '=', $type)->first();
		} else {
			$data = DB::table('users')
						->select(DB::raw("count('1') as userCount"))->first();
		}
		return $data->userCount;
	}
	
	public function getAllProperty(){
		$data = DB::table('property_type')
						->select(DB::raw("*"))->where('property_type.status', '<>', 'Delete')->paginate(10);
	
		return $data;
	}

	public function getAllPropertyWithActiveCount(){
		
		$property = DB::table('property_type')->leftJoin('property', function($join){
            $join->on('property_type.propertyTypeID', '=', 'property.propertyTypeID')->whereIn('property.status', array('Available'));
        })->select(DB::raw("count(property.propertyID) as typeCount"), 'property_type.title', 'property_type.propertyTypeID');
		$property = $property->groupBy('property_type.propertyTypeID');

		$property->where('property_type.status', '=', 'Active');
		$data = $property->orderBy('property_type.propertyTypeID', 'asc')->get();
		//echo $property->toSql();

		
		//$data = $property->sortable()->paginate(10);
		return $data;
	}
	
	public function getAllPropertyWithDeleteCount(){
		
		$property = DB::table('property_type')->leftJoin('property', function($join){
            $join->on('property_type.propertyTypeID', '=', 'property.propertyTypeID')->where('property.status', '=', 'Occupied');
        })->select(DB::raw("count(property.propertyID) as typeCount"), 'property_type.title', 'property_type.propertyTypeID');
		$property = $property->groupBy('property_type.propertyTypeID');

		$property->where('property_type.status', '=', 'Active');
		$data = $property->orderBy('property_type.propertyTypeID', 'asc')->get();
		//echo $property->toSql();

		
		//$data = $property->sortable()->paginate(10);
		
		return $data;
	}

}