<?php
namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon;
Class PropertyType extends Model{
    use Sortable;
	protected $table = "property_type";
	public $sortable = ['propertyTypeID',
                        'title',
                        'status'];

	public function getRecords(PropertyType $propertyType){

        $record_per_page = env('RECORDS_PER_PAGE_BACKEND');
		
		if(empty($_GET['sort']) || empty($_GET['order']) ){
			$propertyType = $propertyType->orderBy('propertyTypeID', 'desc');
		}
		if( !empty($_GET['title']) ){
			$propertyType = $propertyType->where('title', 'like', '%'.$_GET['title'].'%');
		}

		if( !empty($_GET['status']) ){
			$propertyType = $propertyType->where('status', 'like', '%'.$_GET['status'].'%');
		}

		$data = $propertyType->where('status', '<>', 'Delete')->sortable()->paginate($record_per_page);
		//$data = $propertyType->sortable()->paginate(10);
		return $data;
	}

	public function getRecordById($propertyTypeID){

		
		$data = DB::table('property_type')->where('propertyTypeID',$propertyTypeID)->where('status','<>','Delete')->first();

		return $data;
	}

	public function savePropertyType($request){

		$title 		= ($request->input('title') != "") ? $request->input('title') : null;
		$status 	= ($request->input('status') != "") ? $request->input('status') : null;
		$created_at = Carbon::now();
		
		$propertyTypeID = DB::table('property_type')->insertGetId([
			'title' => $title, 
			'status' => $status, 
			'created_at' => $created_at

		]);

		return $propertyTypeID;
	}

	public function updatePropertyType($request){
		try {
			$propertyTypeID 		= ($request->input('propertyTypeID') != "") ? $request->input('propertyTypeID') : null;
			$decoded_propertyTypeID = base64_decode($propertyTypeID);
			$title 		= ($request->input('title') != "") ? $request->input('title') : null;
			$status 	= ($request->input('status') != "") ? $request->input('status') : null;
			$updated_at = Carbon::now();
			
			$result = DB::table('property_type')->where('propertyTypeID',$decoded_propertyTypeID)->update([
				'title' => $title, 
				'status' => $status, 
				'updated_at' => $updated_at
				
			]);
			return true;
		} catch (Exception $e) {
			return false;
		}
		return false;
	}

	public function deleteRecordById($propertyTypeID){
		
		try {
			$result = DB::table('property_type')->where('propertyTypeID',$propertyTypeID)->update(['status'=>'Delete']);
			return 'true';
		} catch (Exception $e) {
			return 'false';
		}
		return 'false';
	}
}