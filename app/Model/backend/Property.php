<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon;
use Auth;
use App\Libraries\ImageHelpers;
use App\Libraries\GlobalHelpers;

Class Property extends Model {

    use Sortable;

    protected $table = "property";
    public $sortable = ['propertyID',
        'companyName',
        'companyNumber',
        'description',
        'address',
    ];
    public $sortableAs = [
        'title',
        'cityName',
        'stateName',
        'areaName',
        'createdBy',
        'userID',
        'status'];

    public function getRecords(Property $property) {

        $record_per_page = env('RECORDS_PER_PAGE_BACKEND');
        $property = $property->leftJoin('area as ar', 'property.areaID', '=', 'ar.areaID')
                ->leftJoin('city as ct', 'property.cityID', '=', 'ct.cityID')
                ->leftJoin('state as st', 'property.stateID', '=', 'st.stateID')
                ->leftJoin('users as u', 'property.createdBy', '=', 'u.id')
                ->leftJoin('property_photo as pp', 'property.propertyID', '=', 'pp.propertyID')
                ->leftJoin('property_type as pt', 'pt.propertyTypeID', '=', 'property.propertyTypeID')
                ->select('property.property_code as property_code','property.title as title', 'property.propertyID', 'property.companyNumber', 'property.companyName', 'property.address', 'property.description', 'property.status as status', 'st.stateName', 'ct.cityName', 'ar.areaName', 'u.name as createdBy', 'u.id as userID','property.*',DB::raw('count(pp.propertyPhotoID) as image_count'))->groupBy('property.propertyID');
					
		$property = $property->where('pt.status','=','Active');
        
		if (!empty($_GET['propertyTypeID'])) {
            $property = $property->where('property.propertyTypeID',$_GET['propertyTypeID']);
        }
		
		if (empty($_GET['sort']) || empty($_GET['order'])) {
            $property = $property->orderBy('propertyID', 'desc');
        }
        if (!empty($_GET['title'])) {
            $property = $property->where('property.title', 'like', '%' . $_GET['title'] . '%');
        }

        if (!empty($_GET['status'])) {
            $property = $property->where('property.status', 'like', '%' . $_GET['status'] . '%');
        }

        if (!empty($_GET['city'])) {
            $property = $property->where('property.cityID', '=', $_GET['city']);
        }

        if (!empty($_GET['state'])) {
            $property = $property->where('property.stateID', 'like', $_GET['state']);
        }

        if (!empty($_GET['contact_no'])) {
            $property = $property->where('property.companyNumber', 'like', $_GET['contact_no']);
        }
		
		if (!empty($_GET['location'])) {
            $property = $property->where('property.address', 'like', '%' . $_GET['location'] . '%');
        }
		
        $roleID = Auth::guard('admin')->user()->roleID;
		
		$roles = config('constants.ADMIN_ROLES');

        if ($roleID != $roles["Administrator"]) {
            if ($roleID == 5) {
                $property->where('property.propertyOwnerID', '=', Auth::guard('admin')->user()->id);
            } else {
                //$property->where('property.createdBy', '=', Auth::guard('admin')->user()->id);
            }
        }

		if (!empty($_GET['status'])) {
			$data = $property->where('property.status', '=', $_GET['status'])->sortable()->paginate($record_per_page);
		} else {
			$data = $property->where('property.status', '=', 'Available')->sortable()->paginate($record_per_page);
		}
		
		$data = $property->sortable()->paginate($record_per_page);
        //$data = $property->sortable()->paginate(10);
        return $data;
    }

    public function getRecordById($id) {


        $propertyQuery = DB::table('property')->where('propertyID', $id)->where('status', '<>', 'Delete');
        $roleID = Auth::guard('admin')->user()->roleID;
        if ($roleID != config('constants.ADMIN_ROLES["Administrator"]')) {
            if ($roleID == 5) {
                $propertyQuery->where('property.propertyOwnerID', '=', Auth::guard('admin')->user()->id);
            } else {
                $propertyQuery->where('property.createdBy', '=', Auth::guard('admin')->user()->id);
            }
        }
        $data['data'] = $propertyQuery->first();

        if ($data['data']) {

            $data['roomSharingData'] = DB::table('property_sharing')->where('propertyID', $id)->get();
            foreach ($data['roomSharingData'] as $value) {

                $data['roomPhotoData'] = DB::table('property_photo')->where('propertyRoomSharingID', $value->id)->get();
                $roomPhotoData = [];
                $photocount = 0;
                foreach ($data['roomPhotoData'] as $keyPhoto => $valuePhoto) {
                    /* print_r($data['roomPhotoData']); */
                    //echo $valuePhoto->photoName;
                    $path = 'images/property';
                    $imgName = $valuePhoto->photoName;
                    $file = ImageHelpers::getMediumImage($path, $imgName);
                    $roomPhotoData[$photocount]['file'] = $file;
                    $roomPhotoData[$photocount]['propertyPhotoID'] = $valuePhoto->propertyPhotoID;
                    $photocount++;
                }
                if (!empty($roomPhotoData)) {
                    $value->roomPhotoData = $roomPhotoData;
                }

                // get room facilities

                $data['roomFacilitiesData'] = DB::table('room_facilities')->where('propertyRoomSharingID', $value->id)->first();
                $value->roomFacilitiesData = $data['roomFacilitiesData'];
                unset($data['roomFacilitiesData']);
            }
        }
        /* echo "<pre>";
          print_r($data);
          exit; */
//exit;
        return $data;
    }

    public function saveProperty($request) {

        try {

            $title = ($request->input('companyName') != "") ? $request->input('companyName') : null; //mehul change
            $companyName = ($request->input('companyName') != "") ? $request->input('companyName') : null;
            $companyNumber = ($request->input('companyNumber') != "") ? $request->input('companyNumber') : null;
            $ownerName = ($request->input('ownerName') != "") ? $request->input('ownerName') : null;
            $propertyType = ($request->input('propertyType') != "") ? $request->input('propertyType') : null;
            $propertyOwnerID = ($request->input('propertyOwnerID') != "") ? $request->input('propertyOwnerID') : null;
            $condition = ($request->input('condition') != "") ? $request->input('condition') : null;
            $address = ($request->input('address') != "") ? $request->input('address') : null;
            $stateID = ($request->input('state') != "") ? $request->input('state') : null;
            $cityID = ($request->input('city') != "") ? $request->input('city') : null;
            $areaID = ($request->input('area') != "") ? $request->input('area') : null;
            $landmark = ($request->input('landmark') != "") ? $request->input('landmark') : null;
            $latitude = ($request->input('latitude') != "") ? $request->input('latitude') : null;
            $longitude = ($request->input('longitude') != "") ? $request->input('longitude') : null;
            $gender = ($request->input('gender') != "") ? $request->input('gender') : null;
            $mealsIncludedID = ($request->input('mealsIncludedID') != "") ? $request->input('mealsIncludedID') : null;
            $description = ($request->input('description') != "") ? $request->input('description') : null;
            $totalsquarefeet = ($request->input('total_square_feet') != "") ? $request->input('total_square_feet') : null;
            $sqlftdrp = ($request->input('sqlftdrp') != "") ? $request->input('sqlftdrp') : null;
            $societyamenityID = ($request->input('societyamenityID') != "") ? $request->input('societyamenityID') : null;
            $floorID = ($request->input('floorID') != "") ? $request->input('floorID') : null;
            $avaialble_from = ($request->input('avaialble_from') != "") ? $request->input('avaialble_from') : null;
            $availability = ($request->input('availability') != "") ? $request->input('availability') : null;
            $old_building = ($request->input('old_building') != "") ? $request->input('old_building') : null;

            $rules = ($request->input('rulesID') != "") ? $request->input('rulesID') : null;
            $furnitureID = ($request->input('furnitureID') != "") ? $request->input('furnitureID') : null;
            $facilityID = ($request->input('facilityID') != "") ? $request->input('facilityID') : null;
            $interiorID = ($request->input('interiorID') != "") ? $request->input('interiorID') : null;
            $status = ($request->input('status') != "") ? $request->input('status') : null;
            //$oc_status = ($request->input('oc_status') != "") ? $request->input('oc_status') : null;
            $createdBy = auth()->guard('admin')->user()->id;
            $created_at = Carbon::now();
			if($gender!="" && $gender!=null){
				$genderString[0] = $gender;
				$strGender = implode(',', $genderString);
			} else {
				$strGender = "";
			}

            if ($mealsIncludedID != '' && $mealsIncludedID!=null) {
                $strmealsIncluded = implode(',', $mealsIncludedID);
            } else {
                $strmealsIncluded = '';
            }
			if ($societyamenityID != '' && $societyamenityID!=null) {
                $societyamenityIDdata = implode(',', $societyamenityID);
            } else {
                $societyamenityIDdata = '';
            }
			
			
			
            $avaialbledate = date('Y-m-d', strtotime($avaialble_from));

            $stateName = GlobalHelpers::stateDropdown($stateID);
            $cityName = GlobalHelpers::cityDropdown($stateID, $cityID);
            $areaName = GlobalHelpers::areaDropdown($stateID, $cityID, $areaID);
            $slug = $stateName[$stateID] . "-" . $cityName[$cityID] . "-" . $areaName[$areaID] . "-" . $title . "-" . time();
            $slug = str_slug($slug, "-");
			
			if ($rules != '' && $rules!=null) {
                $rulesdata = implode(',', $rules);
            } else {
                $rulesdata = '';
            }
			
			if ($furnitureID != '' && $furnitureID!=null) {
                $furnituredata = implode(',', $furnitureID);
            } else {
                $furnituredata = '';
            }
			
			if ($facilityID != '' && $facilityID!=null) {
                $facilitydata = implode(',', $facilityID);
            } else {
                $facilitydata = '';
            }
			
			if ($interiorID != '' && $interiorID!=null) {
                $interiordata = implode(',', $interiorID);
            } else {
                $interiordata = '';
            }
			
            $sharingID = ($request->input('sharingID') != "") ? $request->input('sharingID') : null;
            $bedSharingId = ($request->input('bedSharingId') != "") ? $request->input('bedSharingId') : null;
            $unitavailableID = ($request->input('unitavailableID') != "") ? $request->input('unitavailableID') : null;
            $rent = ($request->input('rent') != "") ? $request->input('rent') : null;
            //mehul change
            $numberOfRoom = ($request->input('numberOfRoom') != "") ? $request->input('numberOfRoom') : null;
            $facing = ($request->input('facing') != "") ? $request->input('facing') : null;
            $noticePeriod = ($request->input('noticePeriod') != "") ? $request->input('noticePeriod') : null;
            $deposit = ($request->input('deposit') != "") ? $request->input('deposit') : null;
            $totalFloor = ($request->input('totalFloor') != "") ? $request->input('totalFloor') : null;
            $remarks = ($request->input('remarks') != "") ? $request->input('remarks') : null;
            //mehul change over

            $new_propertyID = DB::table('property')->insertGetId([
                'title' => $title,
                'companyName' => $companyName,
                'ownerName' => $ownerName,
                'availability' => $availability,
                'old_building' => $old_building,
                'companyNumber' => $companyNumber,
                'propertyTypeID' => $propertyType,
                'propertyOwnerID' => $propertyOwnerID,
                'conditionID' => $condition,
                'address' => $address,
                'stateID' => $stateID,
                'cityID' => $cityID,
                'areaID' => $areaID,
                'latitude' => $latitude,
                'longitude' => $longitude,
                'landmark' => $landmark,
                'mealsIncludedID' => $strmealsIncluded,
                'gender' => $strGender,
                'description' => $description,
                'rules' => $rulesdata,
                'slug' => $slug,
                'status' => $status,
                //'occupancy' => $oc_status,
                'createdBy' => $createdBy,
                'created_at' => $created_at,
                'total_square_feet' => $totalsquarefeet,
                'area_unit' => $sqlftdrp,
                'societyamenityID' => $societyamenityIDdata,
                'floorID' => $floorID,
                'avaialble_from' => $avaialbledate,
                'furnitureID' => $furnituredata,
                'facilityID' => $facilitydata,
                'interiorID' => $interiordata,
                'total_floor' => $totalFloor,
                'sharingID' => $sharingID,
                //'unitavailableID' => $unitavailableID,
                'price' => $rent,
                //mehul change
                'numberOfRoom' => $numberOfRoom,
                'facing' => $facing,
                'noticePeriod' => $noticePeriod,
                'deposit' => $deposit,
				'property_code'=>time(),
				'roomSharingID'=>$bedSharingId,
				'remarks'=>$remarks,
                //mehul change over
            ]);

            if ($new_propertyID) {
                //mehul change
                $new_property_sharing_id = DB::table('property_sharing')->insertGetId([
                    'propertyID' => $new_propertyID,
                    'roomSharingID' => $bedSharingId,
                    'availability' => $availability,
                    'price' => $rent,
                ]);
                //mehul change over
                $sharing = $request['sharing'];

				if($sharing != "") {	
                
				foreach ($sharing as $key1 => $val) {
                    $propertySharingID = "";
                    foreach ($val as $key => $value) {

                        if (isset($value['file'])) {
                            $new_files = $value['file'];
                            unset($value['file']);
                        }

                        if (isset($new_files)) {
                            foreach ($new_files as $imagekey => $imageValue) {
                                $fileObj = $imageValue;
                                $file = '';
                                $imgCount = 0;

                                if (!empty($fileObj)) {

                                    $propertyImageSizeArr = \Config::get('filesystems.propertyImageSizeArr');
                                    $savePath = storage_path('images/property');
                                    $imgName = rand() . "_" . time() . '.jpg';
                                    $convertFormat = 'jpg';
                                    $convertQuality = '100';



                                    $imgCount = ImageHelpers::uploadResizeImage($fileObj, $propertyImageSizeArr, $savePath, $imgName, $convertFormat, $convertQuality);


                                    if ($imgCount) {
                                        $id = DB::table('property_photo')->insertGetId([
                                            'propertyID' => $new_propertyID,
                                            'photoName' => $imgName,
                                            'propertyRoomSharingID' => $new_property_sharing_id //mehul change
                                        ]);
                                    }
                                }
                            }
                            unset($new_files);
                        }
                    }
                }
			 }
            }
            //return $id;
            return $new_propertyID;
        } catch (Exception $e) {
            return false;
        }
    }

    public function updateProperty($request) {
//		echo "<pre>";print_r($request->all());exit;
        try {
            $id = ($request->input('propertyID') != "") ? $request->input('propertyID') : null;
            $decoded_id = base64_decode($id);
            $title = ($request->input('companyName') != "") ? $request->input('companyName') : null;  //mehul change
            $companyName = ($request->input('companyName') != "") ? $request->input('companyName') : null;
            $companyNumber = ($request->input('companyNumber') != "") ? $request->input('companyNumber') : null;
            $propertyType = ($request->input('propertyType') != "") ? $request->input('propertyType') : null;
            $propertyOwnerID = ($request->input('propertyOwnerID') != "") ? $request->input('propertyOwnerID') : null;
            $condition = ($request->input('condition') != "") ? $request->input('condition') : null;
            $address = ($request->input('address') != "") ? $request->input('address') : null;
            $stateID = ($request->input('state') != "") ? $request->input('state') : null;
            $cityID = ($request->input('city') != "") ? $request->input('city') : null;
            $areaID = ($request->input('area') != "") ? $request->input('area') : null;
            $landmark = ($request->input('landmark') != "") ? $request->input('landmark') : null;
            $latitude = ($request->input('latitude') != "") ? $request->input('latitude') : null;
            $longitude = ($request->input('longitude') != "") ? $request->input('longitude') : null;
            $gender = ($request->input('gender') != "") ? $request->input('gender') : null;
            $mealsIncludedID = ($request->input('mealsIncludedID') != "") ? $request->input('mealsIncludedID') : null;
            $bedSharingId = ($request->input('bedSharingId') != "") ? $request->input('bedSharingId') : null;
            $description = ($request->input('description') != "") ? $request->input('description') : null;
            $rules = ($request->input('rules') != "") ? $request->input('rules') : null;
            $status = ($request->input('status') != "") ? $request->input('status') : null;
            //$oc_status = ($request->input('oc_status') != "") ? $request->input('oc_status') : null;
            $updated_at = Carbon::now();
            $totalsquarefeet = ($request->input('total_square_feet') != "") ? $request->input('total_square_feet') : null;
            $sqlftdrp = ($request->input('sqlftdrp') != "") ? $request->input('sqlftdrp') : null;
            $societyamenityID = ($request->input('societyamenityID') != "") ? $request->input('societyamenityID') : null;
            $floorID = ($request->input('floorID') != "") ? $request->input('floorID') : null;
            $avaialble_from = ($request->input('avaialble_from') != "") ? $request->input('avaialble_from') : null;
            $availability = ($request->input('availability') != "") ? $request->input('availability') : null;
            $old_building = ($request->input('old_building') != "") ? $request->input('old_building') : null;

            $rules = ($request->input('rulesID') != "") ? $request->input('rulesID') : null;
            $furnitureID = ($request->input('furnitureID') != "") ? $request->input('furnitureID') : null;
            $facilityID = ($request->input('facilityID') != "") ? $request->input('facilityID') : null;
            $interiorID = ($request->input('interiorID') != "") ? $request->input('interiorID') : null;
            $status = ($request->input('status') != "") ? $request->input('status') : null;
            $createdBy = auth()->guard('admin')->user()->id;
            $created_at = Carbon::now();
            
			$strGender = $gender;
			//$strGender = implode(',', $gender);
//			$strmealsIncluded = implode(',', $mealsIncludedID);
            if ($mealsIncludedID != '') {
                $strmealsIncluded = implode(',', $mealsIncludedID);
            } else {
                $strmealsIncluded = '';
            }
			
			if ($societyamenityID != '') {
                $societyamenityIDdata = implode(',', $societyamenityID);
            } else {
                $societyamenityIDdata = '';
            }
			
            $avaialbledate = date('Y-m-d', strtotime($avaialble_from));
            $rulesdata = ($rules!='')?implode(',', $rules):'';
            //$furnituredata = implode(',', $furnitureID);
            $facilitydata = ($facilityID!='')?implode(',', $facilityID):'';
            $interiordata = ($interiorID!='')?implode(',', $interiorID):'';
            $sharingID = ($request->input('sharingID') != "") ? $request->input('sharingID') : null;
            $unitavailableID = ($request->input('unitavailableID') != "") ? $request->input('unitavailableID') : null;
            $rent = ($request->input('rent') != "") ? $request->input('rent') : null;
            //mehul change
            $numberOfRoom = ($request->input('numberOfRoom') != "") ? $request->input('numberOfRoom') : null;
            $facing = ($request->input('facing') != "") ? $request->input('facing') : null;
            $noticePeriod = ($request->input('noticePeriod') != "") ? $request->input('noticePeriod') : null;
            $deposit = ($request->input('deposit') != "") ? $request->input('deposit') : null;
            $remarks = ($request->input('remarks') != "") ? $request->input('remarks') : null;
            //mehul change over

            $result = DB::table('property')->where('propertyID', $decoded_id)->update([
                'title' => $title,
                'availability' => $availability,
                'old_building' => $old_building,
                'companyName' => $companyName,
                'companyNumber' => $companyNumber,
                'propertyTypeID' => $propertyType,
                'propertyOwnerID' => $propertyOwnerID,
                'conditionID' => $condition,
                'address' => $address,
                'stateID' => $stateID,
                'cityID' => $cityID,
                'areaID' => $areaID,
                'latitude' => $latitude,
                'longitude' => $longitude,
                'landmark' => $landmark,
                'mealsIncludedID' => $strmealsIncluded,
                'gender' => $strGender,
                'description' => $description,
                'rules' => $rulesdata,
                'status' => $status,
                //'occupancy' => $oc_status,
                'updated_at' => $updated_at,
                'total_square_feet' => $totalsquarefeet,
                'area_unit' => $sqlftdrp,
                'societyamenityID' => $societyamenityIDdata,
                'floorID' => $floorID,
                'avaialble_from' => $avaialbledate,
                //'furnitureID' => $furnituredata,
                'facilityID' => $facilitydata,
                'interiorID' => $interiordata,
                'sharingID' => $sharingID,
                'unitavailableID' => $unitavailableID,
                'price' => $rent,
                //mehul change
                'numberOfRoom' => $numberOfRoom,
                'facing' => $facing,
                'noticePeriod' => $noticePeriod,
                'deposit' => $deposit,
				'roomSharingID'=>$bedSharingId,
				'remarks'=>$remarks,
                //mehul change over
            ]);

            //echo "<pre>";
            //$all_input = $request->all();
            $sharing = $request['sharing'];
            
            //mehul change
                $new_property_sharing_id = DB::table('property_sharing')->where('propertyID', $decoded_id)->update([
                    'roomSharingID' => $sharingID,
                    'availability' => $unitavailableID,
                    'price' => $rent,
                ]);
                //mehul change over

            // get all old data from property sharing.
            $oldRoomSharing = DB::table('property_sharing')->where('propertyID', $decoded_id)->select('id')->get()->toArray();

            // get all old data from property room facilities.
            $oldRoomFacilities = DB::table('room_facilities')->where('propertyID', $decoded_id)->select('roomFacilitiesID')->get()->toArray();
            $oldRoomSharingArry = array();
            foreach ($oldRoomSharing as $key => $value) {
                $oldRoomSharingArry[$value->id] = $value->id;
            }

            $oldRoomFacilitiesArry = array();
            foreach ($oldRoomFacilities as $key => $value) {
                $oldRoomFacilitiesArry[$value->roomFacilitiesID] = $value->roomFacilitiesID;
            }

            /* echo "<pre>";
              print_r($oldRoomFacilitiesArry);
              print_r($request->all());exit; */
            /* echo "<pre>";
              print_r($sharing);exit;
             */
            if (!empty($sharing)) {
                // delete photos
                $deletePhotosArr = explode(",", $request['deletePhotos']);
                if (!empty($request['deletePhotos'])) {
                    $photoNames = DB::table('property_photo')->whereIn('propertyPhotoID', $deletePhotosArr)->pluck('photoName');

                    DB::table('property_photo')->whereIn('propertyPhotoID', $deletePhotosArr)->delete();
                }
                //DB::table('room_facilities')->where('propertyID',$decoded_id)->delete();
                //DB::table('room_facilities')->where('propertyID',$decoded_id)->delete();
                foreach ($sharing as $key1 => $val) {
                    /* echo "<pre>";
                      print_r($val); */
                    $propertySharingID = "";
                    foreach ($val as $key => $value) {
                        //if(isset($value['roomSharingID']) && $value['roomSharingID']=="") continue ;
                        $new_property = false;
                        if ($key == "rs") {
                            if (!empty($value['id']) && in_array($value['id'], $oldRoomSharingArry)) {
                                $result = DB::table('property_sharing')->where('id', $value['id'])->update(['roomSharingID' => $value['roomSharingID'], 'availability' => $value['availability'], 'price' => $value['price']]);
                                $propertySharingID = $value['id'];
                                unset($oldRoomSharingArry[$value['id']]);
                            }
                            $value['propertyID'] = $decoded_id;
                            if (isset($value['file'])) {
                                $new_files = $value['file'];
                                unset($value['file']);
                            }
                            if (empty($value['id'])) {
                                $propertySharingID = DB::table('property_sharing')->insertGetId(
                                        $value
                                );
                                if ($propertySharingID) {
                                    $new_property = true;
                                }
                            }
                            //echo "<pre>";
                            if (isset($new_files)) {
                                foreach ($new_files as $imagekey => $imageValue) {
                                    $fileObj = $imageValue;
                                    $file = '';
                                    $imgCount = 0;
                                    //print_r($fileObj);
                                    if (!empty($fileObj)) {

                                        $propertyImageSizeArr = \Config::get('filesystems.propertyImageSizeArr');
                                        $savePath = storage_path('images/property');
                                        $imgName = rand() . "_" . time() . '.jpg';
                                        $convertFormat = 'jpg';
                                        $convertQuality = '100';
                                        /* $uploadImage = true;

                                          if($uploadImage) { */
                                        $imgCount = ImageHelpers::uploadResizeImage($fileObj, $propertyImageSizeArr, $savePath, $imgName, $convertFormat, $convertQuality);
                                        /* } */

                                        /* echo $imgName.'<br>';
                                          echo "$propertySharingID.<br>"; */
                                        // Resize Image
                                        if ($imgCount) {
                                            $id = DB::table('property_photo')->insertGetId([

                                                'propertyID' => $decoded_id,
                                                'photoName' => $imgName,
                                                'propertyRoomSharingID' => $propertySharingID,
                                            ]);
                                            /* $path = "images/property";
                                              $file = ImageHelpers::getMediumImage($path, $imgName)."?".time();
                                              $filepath = storage_path('images/property').DIRECTORY_SEPARATOR.'2'.DIRECTORY_SEPARATOR.$file; */
                                        }
                                        /* else {
                                          $filepath = storage_path().DIRECTORY_SEPARATOR.'images/property/no_image.jpg';
                                          } */
                                    }
                                }
                                unset($new_files);
                            }
                        }
                        if (!empty($propertySharingID) && ($key == "rf")) {
                            /* echo "$key";
                              print_r($value); */
                            $value['propertyRoomSharingID'] = $propertySharingID;
                            foreach ($value as $k => $v) {
                                if (empty($value[$k]) && $k != 'roomFacilitiesID') {
                                    $value[$k] = 1;
                                }
                                if (empty($value[$k]) && $k == 'roomFacilitiesID') {
                                    unset($value[$k]);
                                }
                            }
                            $defaultArr = ['AC' => 0, 'westernToilet' => 0, 'parking' => 0, 'security' => 0, 'housekeeping' => 0, 'laundry' => 0, 'wifi' => 0, 'attachedBathroom' => 0, 'geyser' => 0, 'powerBackup' => 0, 'roPurified' => 0];
                            //echo "<pre>";
                            $value = array_merge($defaultArr, $value);
                            unset($defaultArr);
                            //print_r($value);exit;
                            $value['propertyID'] = $decoded_id;
                            if (!empty($value['roomFacilitiesID'])) {

                                $result = DB::table('room_facilities')->where('roomFacilitiesID', $value['roomFacilitiesID'])->update($value);

                                unset($oldRoomFacilitiesArry[$value['roomFacilitiesID']]);
                            } else {

                                if (!empty($value)) {
                                    //print_r($value);exit;		        			
                                    $id = DB::table('room_facilities')->insert(
                                            $value
                                    );
                                }
                            }
                        }
                    }
                }

                //exit;
                if (!empty($oldRoomSharingArry)) {
                    DB::table('property_sharing')->whereIn('id', $oldRoomSharingArry)->delete();
                    DB::table('property_photo')->whereIn('propertyRoomSharingID', $oldRoomSharingArry)->delete();
                }
                if (!empty($oldRoomFacilitiesArry)) {
                    DB::table('room_facilities')->whereIn('roomFacilitiesID', $oldRoomFacilitiesArry)->delete();
                }
            }
            return true;
        } catch (Exception $e) {
            return false;
        }
        return false;
    }

    public function deleteRecordById($id) {
        try {
            $result = DB::table('property')->where('propertyID', $id)->update(['status' => 'Delete']);
            return 'true';
        } catch (Exception $e) {
            return 'false';
        }
        return 'false';
    }
	
	public function updateRecordById($id, $status){
		if($status == 0) {
			$status = "Occupied";
		} else if($status == 1) {
			$status = "Available";
		}
		try {
			$result = DB::table('property')->whereIn('propertyID',explode(",",$id))->update(['status'=>$status]);
			return 'true';
		} catch (Exception $e) {
			return 'false';
		}
		return 'false';
	}
    
    public function getphotoRecordById($id) {

        $propertyQuery = DB::table('property_photo')->where('propertyPhotoID', $id);
        $data = $propertyQuery->first();
        return $data;
    }
    
    public function deletephotoById($id) {
        try {
            $result = DB::table('property_photo')->where('propertyPhotoID', $id)->delete();
            return 'true';
        } catch (Exception $e) {
            return 'false';
        }
        return 'false';
    }

	public function getAllArea(){
		$data = DB::table('area')->join('city as ct', 'ct.cityID', '=', 'area.cityID')->join('state as st', 'st.stateID', '=', 'area.stateID')->where('area.status','<>','Delete')->select('area.areaID','area.areaName','area.pincode','area.status','st.stateID','ct.cityID')->get();
		return $data;
	}
}
