<?php
namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon;
Class Rules extends Model{
    use Sortable;
	protected $table = "property_rules";
	public $sortable = ['rulesID',
                        'title',
                        'status'];

	public function getRecords(Rules $rules){

        $record_per_page = env('RECORDS_PER_PAGE_BACKEND');
		
		if(empty($_GET['sort']) || empty($_GET['order']) ){
			$rules = $rules->orderBy('rulesID', 'desc');
		}
		if( !empty($_GET['title']) ){
			$rules = $rules->where('title', 'like', '%'.$_GET['title'].'%');
		}

		if( !empty($_GET['status']) ){
			$rules = $rules->where('status', 'like', '%'.$_GET['status'].'%');
		}

		$data = $rules->where('status', '<>', 'Delete')->sortable()->paginate($record_per_page);
		//$data = $propertyType->sortable()->paginate(10);
		return $data;
	}

	public function getRecordById($rulesID){
		$data = DB::table('property_rules')->where('rulesID',$rulesID)->where('status','<>','Delete')->first();
		return $data;
	}
        
        public function getAllRecord(){
		$data = DB::table('property_rules')->where('status','<>','Delete')->get();
		return $data;
	}
        
	public function saveRules($request){

		$title 		= ($request->input('title') != "") ? $request->input('title') : null;
		$status 	= ($request->input('status') != "") ? $request->input('status') : null;
		$created_at = Carbon::now();
		
		$facilitiesID = DB::table('property_rules')->insertGetId([
			'title' => $title, 
			'status' => $status, 
			'created_at' => $created_at

		]);

		return $facilitiesID;
	}

	public function updateRules($request){
		try {
			$rulesID 		= ($request->input('rulesID') != "") ? $request->input('rulesID') : null;
			$decoded_rulesID = base64_decode($rulesID);
			$title 		= ($request->input('title') != "") ? $request->input('title') : null;
			$status 	= ($request->input('status') != "") ? $request->input('status') : null;
			$updated_at = Carbon::now();
			
			$result = DB::table('property_rules')->where('rulesID',$decoded_rulesID)->update([
				'title' => $title, 
				'status' => $status, 
				'updated_at' => $updated_at
				
			]);
			return true;
		} catch (Exception $e) {
			return false;
		}
		return false;
	}

	public function deleteRecordById($rulesID){
		
		try {
			$result = DB::table('property_rules')->where('rulesID',$rulesID)->update(['status'=>'Delete']);
			return 'true';
		} catch (Exception $e) {
			return 'false';
		}
		return 'false';
	}
}