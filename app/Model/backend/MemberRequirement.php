<?php
namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon;
Class MemberRequirement extends Model{
    use Sortable;
	protected $table = "requirement_details";
	public $sortable = [
                        'client_id',
                        'name'
                    ];
    public $sortableAs = [
                        'userName',
                        'planName',
                        'cityName',
                        ];

	public function getRecords(MemberRequirement $requirement){
        $record_per_page = env('RECORDS_PER_PAGE_BACKEND');
		$requirement = $requirement->select('requirement_details.*');
		$data = $requirement->sortable()->paginate($record_per_page);
		return $data;
	}
	
	public function getRecordById($id) {
        $requirementQuery = DB::table('requirement_details')->where('id', $id);
        $data['data'] = $requirementQuery->first();
        if ($data['data']) {
            $data['roomSharingData'] = DB::table('property_sharing')->where('propertyID', $id)->get();
        }
        return $data;
    }

    public function saveMemberRequirement($request) {
        try {
            $name = ($request->input('name') != "") ? $request->input('name') : null;
            $contact_no = ($request->input('contact_no') != "") ? $request->input('contact_no') : null;
            $email = ($request->input('email') != "") ? $request->input('email') : null;
            $gender = ($request->input('gender') != "") ? $request->input('gender') : null;
            $looking_for = ($request->input('looking_for') != "") ? $request->input('looking_for') : null;
            $final_pg_in = ($request->input('final_pg_in') != "") ? $request->input('final_pg_in') : null;
            $requirment_description = ($request->input('requirment_description') != "") ? $request->input('requirment_description') : null;
            $total_fav_count = ($request->input('total_fav_count') != "") ? $request->input('total_fav_count') : null;
            $remaining_count = ($request->input('remaining_count') != "") ? $request->input('remaining_count') : null;
            $total_visited_count = ($request->input('total_visited_count') != "") ? $request->input('total_visited_count') : null;
            $visited_count = ($request->input('visited_count') != "") ? $request->input('visited_count') : null;
            $executive_name = ($request->input('executive_name') != "") ? $request->input('executive_name') : null;
            $remarks = ($request->input('remarks') != "") ? $request->input('remarks') : null;
            $status = ($request->input('status') != "") ? $request->input('status') : null;

            $new_requirementID = DB::table('requirement_details')->insertGetId([
                'name' => $name,
                'contact_no' => $contact_no,
                'email' => $email,
                'gender' => $gender,
                'looking_for' => $looking_for,
                'final_pg_in' => $final_pg_in,
                'requirment_description' => $requirment_description,
                'total_fav_count' => $total_fav_count,
                'remaining_count' => $remaining_count,
                'total_visited_count' => $total_visited_count,
                'visited_count' => $visited_count,
                'executive_name' => $executive_name,
                'remarks' => $remarks,
                'status' => $status,
            ]);
            return $new_requirementID;
        } catch (Exception $e) {
            return false;
        }
    }

    public function updateProperty($request) {
        try {
				$id = ($request->input('id') != "") ? $request->input('id') : null;
				$decoded_id = base64_decode($id);
				
				$name = ($request->input('name') != "") ? $request->input('name') : null;
				$contact_no = ($request->input('contact_no') != "") ? $request->input('contact_no') : null;
				$email = ($request->input('email') != "") ? $request->input('email') : null;
				$gender = ($request->input('gender') != "") ? $request->input('gender') : null;
				$looking_for = ($request->input('looking_for') != "") ? $request->input('looking_for') : null;
				$final_pg_in = ($request->input('final_pg_in') != "") ? $request->input('final_pg_in') : null;
				$requirment_description = ($request->input('requirment_description') != "") ? $request->input('requirment_description') : null;
				$total_fav_count = ($request->input('total_fav_count') != "") ? $request->input('total_fav_count') : null;
				$remaining_count = ($request->input('remaining_count') != "") ? $request->input('remaining_count') : null;
				$total_visited_count = ($request->input('total_visited_count') != "") ? $request->input('total_visited_count') : null;
				$visited_count = ($request->input('visited_count') != "") ? $request->input('visited_count') : null;
				$executive_name = ($request->input('executive_name') != "") ? $request->input('executive_name') : null;
				$remarks = ($request->input('remarks') != "") ? $request->input('remarks') : null;
				$status = ($request->input('status') != "") ? $request->input('status') : null;


				$result = DB::table('requirement_details')->where('id', $decoded_id)->update([
					'name' => $name,
					'contact_no' => $contact_no,
					'email' => $email,
					'gender' => $gender,
					'looking_for' => $looking_for,
					'final_pg_in' => $final_pg_in,
					'requirment_description' => $requirment_description,
					'total_fav_count' => $total_fav_count,
					'remaining_count' => $remaining_count,
					'total_visited_count' => $total_visited_count,
					'visited_count' => $visited_count,
					'executive_name' => $executive_name,
					'remarks' => $remarks,
					'status' => $status,
					'date_updated'=>date('Y-m-d H:i:s'),
				]);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function deleteRecordById($id) {
        try {
            $result = DB::table('requirement_details')->where('id', $id)->update(['status' => 'Delete']);
            return 'true';
        } catch (Exception $e) {
            return 'false';
        }
        return 'false';
    }
	
	public function updateRecordById($id, $status){
		if($status == 0) {
			$status = "Occupied";
		} else if($status == 1) {
			$status = "Available";
		}
		try {
			$result = DB::table('property')->whereIn('propertyID',explode(",",$id))->update(['status'=>$status]);
			return 'true';
		} catch (Exception $e) {
			return 'false';
		}
		return 'false';
	}
 
}