<?php
namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon;
Class ResidenceType extends Model{
    use Sortable;
	protected $table = "residence_type";
	public $sortable = ['residenceTypeID',
                        'title',
                        'status'];

	public function getRecords(ResidenceType $residenceType){

        $record_per_page = env('RECORDS_PER_PAGE_BACKEND');
		
		if(empty($_GET['sort']) || empty($_GET['order']) ){
			$residenceType = $residenceType->orderBy('residenceTypeID', 'desc');
		}
		if( !empty($_GET['title']) ){
			$residenceType = $residenceType->where('title', 'like', '%'.$_GET['title'].'%');
		}

		if( !empty($_GET['status']) ){
			$residenceType = $residenceType->where('status', 'like', '%'.$_GET['status'].'%');
		}

		$data = $residenceType->where('status', '<>', 'Delete')->sortable()->paginate($record_per_page);
		//$data = $residenceType->sortable()->paginate(10);
		return $data;
	}

	public function getRecordById($residenceTypeID){

		
		$data = DB::table('residence_type')->where('residenceTypeID',$residenceTypeID)->where('status','<>','Delete')->first();

		return $data;
	}

	public function saveResidenceType($request){

		$title 		= ($request->input('title') != "") ? $request->input('title') : null;
		$status 	= ($request->input('status') != "") ? $request->input('status') : null;
		$created_at = Carbon::now();
		
		$residenceTypeID = DB::table('residence_type')->insertGetId([
			'title' => $title, 
			'status' => $status, 
			'created_at' => $created_at

		]);

		return $residenceTypeID;
	}

	public function updateResidenceType($request){
		try {
			$residenceTypeID 		= ($request->input('residenceTypeID') != "") ? $request->input('residenceTypeID') : null;
			$decoded_residenceTypeID = base64_decode($residenceTypeID);
			$title 		= ($request->input('title') != "") ? $request->input('title') : null;
			$status 	= ($request->input('status') != "") ? $request->input('status') : null;
			$updated_at = Carbon::now();
			
			$result = DB::table('residence_type')->where('residenceTypeID',$decoded_residenceTypeID)->update([
				'title' => $title, 
				'status' => $status, 
				'updated_at' => $updated_at
				
			]);
			return true;
		} catch (Exception $e) {
			return false;
		}
		return false;
	}

	public function deleteRecordById($residenceTypeID){
		
		try {
			$result = DB::table('residence_type')->where('residenceTypeID',$residenceTypeID)->update(['status'=>'Delete']);
			return 'true';
		} catch (Exception $e) {
			return 'false';
		}
		return 'false';
	}
}