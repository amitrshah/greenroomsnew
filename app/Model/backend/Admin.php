<?php

namespace App\Model\backend;

use DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\AdminResetPassword as ResetPasswordNotification;
use Illuminate\Support\Facades\Input; // Input

class Admin extends Authenticatable
{
    use Notifiable;
    protected $table = "users";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'roleID'/*, 'first_name', 'last_name'*/
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'roleID'
    ];
    
    public static function registeruser($input = array()) {
            return Admin::create([
                    'name' => $input['name'],
                    'email' => $input['email'],
                    'password' => bcrypt($input['password']),
                    'roleID'   => 2
                ]);
    }
    
    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
    /***************************************************************
     * Method: getAdminRecord
     * Params: admin id
     * Description: It will get data by admin id
     **************************************************************/
    public function getAdminRecord($admin_id)
    {
        $res_admin  = DB::table('admins')->where('id', $admin_id)->select('id', 'name', 'email')->first();
        if(!empty($res_admin)){
            $path = 'images/admin';
            $imgName = $admin_id .".jpg";
            $file = ImageHelpers::getMediumImage($path, $imgName);
            $res_admin->profile_pic = $file;
        }

        return $res_admin;
    }
}
