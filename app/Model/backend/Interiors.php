<?php
namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon;
Class Interiors extends Model{
    use Sortable;
	protected $table = "property_interiors";
	public $sortable = ['interiorsID',
                        'title',
                        'status'];

	public function getRecords(Interiors $interiors){

                $record_per_page = env('RECORDS_PER_PAGE_BACKEND');
		
		if(empty($_GET['sort']) || empty($_GET['order']) ){
			$interiors = $interiors->orderBy('interiorsID', 'desc');
		}
		if( !empty($_GET['title']) ){
			$interiors = $interiors->where('title', 'like', '%'.$_GET['title'].'%');
		}

		if( !empty($_GET['status']) ){
			$interiors = $interiors->where('status', 'like', '%'.$_GET['status'].'%');
		}

		$data = $interiors->where('status', '<>', 'Delete')->sortable()->paginate($record_per_page);
		//$data = $propertyType->sortable()->paginate(10);
		return $data;
	}

	public function getRecordById($interiorsID){

		
		$data = DB::table('property_interiors')->where('interiorsID',$interiorsID)->where('status','<>','Delete')->first();

		return $data;
	}
        
        public function getAllRecord(){
            $data = DB::table('property_interiors')->where('status','<>','Delete')->get();
            return $data;
	}
        
	public function saveInteriors($request){

		$title 		= ($request->input('title') != "") ? $request->input('title') : null;
		$status 	= ($request->input('status') != "") ? $request->input('status') : null;
		$created_at = Carbon::now();
		
		$interiorsID = DB::table('property_interiors')->insertGetId([
			'title' => $title, 
			'status' => $status, 
			'created_at' => $created_at

		]);

		return $interiorsID;
	}

	public function updateInteriors($request){
		try {
			$interiorsID 		= ($request->input('interiorsID') != "") ? $request->input('interiorsID') : null;
			$decoded_facilitiesID = base64_decode($interiorsID);
			$title 		= ($request->input('title') != "") ? $request->input('title') : null;
			$status 	= ($request->input('status') != "") ? $request->input('status') : null;
			$updated_at = Carbon::now();
			
			$result = DB::table('property_interiors')->where('interiorsID',$decoded_facilitiesID)->update([
				'title' => $title, 
				'status' => $status, 
				'updated_at' => $updated_at
				
			]);
			return true;
		} catch (Exception $e) {
			return false;
		}
		return false;
	}

	public function deleteRecordById($interiorsID){
		
		try {
			$result = DB::table('property_interiors')->where('interiorsID',$interiorsID)->update(['status'=>'Delete']);
			return 'true';
		} catch (Exception $e) {
			return 'false';
		}
		return 'false';
	}
}