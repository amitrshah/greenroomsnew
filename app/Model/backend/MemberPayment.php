<?php
namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon;
Class MemberPayment extends Model{
    use Sortable;
	protected $table = "payment_details";
	public $sortable = [
                        'orderID',
                        'amount',
                        'txnDate',
                        'gatewayName',
                        'responseStatus',
                        'resposeMessage',
                        'status'
                    ];
    public $sortableAs = [
                        'userName',
                        'planName',
                        'cityName',
                        ];

	public function getRecords(MemberPayment $payment){
        $record_per_page = env('RECORDS_PER_PAGE_BACKEND');
		$payment = $payment->join('users as u', 'payment_details.userID', '=', 'u.id')
							->join('plan_cities as pc', 'pc.planCityID', '=', 'payment_details.planCityID')
							->join('city', 'city.cityID', '=', 'pc.cityID')
        					->join('plan as p', 'pc.planID', '=', 'p.planID')
							->select('payment_details.*', 'u.name as userName', 'p.planName','city.cityName' );
		if(empty($_GET['sort']) || empty($_GET['order']) ){
			$payment = $payment->orderBy('paymentDetailsID', 'desc');
		}
		if( !empty($_GET['plan']) ){
			$payment = $payment->where('p.planID', '=', $_GET['plan']);
		}

		if( !empty($_GET['city']) ){
			$payment = $payment->where('pc.cityID', '=', $_GET['city']);
		}

		if( !empty($_GET['orderID']) ){
			$payment = $payment->where('payment_details.orderID', 'like', '%'.$_GET['orderID'].'%');
		}

		if( !empty($_GET['userName']) ){
			$payment = $payment->where('u.name', 'like', '%'.$_GET['userName'].'%');
		}

		if( !empty($_GET['status']) ){
			$payment = $payment->where('payment_details.responseStatus', '=', $_GET['status']);
		}

		/*if( !empty($_GET['txnDate']) ){
			$txnDateRangeArray = explode(" - ",$_GET['txnDate']);
			$payment = $payment->whereBetween('payment_details.txnDate',$txnDateRangeArray);
		}*/

		$data = $payment->sortable()->paginate($record_per_page);
		//$data = $payment->sortable()->paginate(10);
		return $data;
	}
}