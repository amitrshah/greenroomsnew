<?php
namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon;
Class Area extends Model{
    use Sortable;
	protected $table = "area";
	public $sortable = ['areaID',
                        'areaName',
                        'pincode',
                        'status'];

    public $sortableAs = [
                        'stateName',
                        'cityName',
                        ];

	public function getRecords(Area $area){

        $record_per_page = env('RECORDS_PER_PAGE_BACKEND');
		$area = $area->join('state as st', 'st.stateID', '=', 'area.stateID')
					->join('city as ct', 'ct.cityID', '=', 'area.cityID')
					->select('area.areaID','area.areaName','st.stateName','ct.cityName','area.pincode','area.status');
		if(empty($_GET['sort']) || empty($_GET['order']) ){
			$area = $area->orderBy('areaID', 'desc');
		}
		if( !empty($_GET['areaName']) ){
			$area = $area->where('area.areaName', 'like', '%'.$_GET['areaName'].'%');
		}

		if( !empty($_GET['cityName']) ){
			$area = $area->where('ct.cityName', 'like', '%'.$_GET['cityName'].'%');
		}

		if( !empty($_GET['stateName']) ){
			$area = $area->where('st.stateName', 'like', '%'.$_GET['stateName'].'%');
		}

		if( !empty($_GET['pincode']) ){
			$area = $area->where('area.pincode', 'like', '%'.$_GET['pincode'].'%');
		}

		if( !empty($_GET['status']) ){
			$area = $area->where('area.status', 'like', '%'.$_GET['status'].'%');
		}

		$data = $area->where('area.status', '<>', 'Delete')->sortable()->paginate($record_per_page);
		//$data = $area->sortable()->paginate(10);
		return $data;
	}

	public function getRecordById($areaID){

		
		$data = DB::table('area')->join('city as ct', 'ct.cityID', '=', 'area.cityID')->join('state as st', 'st.stateID', '=', 'area.stateID')->where('area.areaID',$areaID)->where('area.status','<>','Delete')->select('area.areaID','area.areaName','area.pincode','area.status','st.stateID','ct.cityID')->first();

		return $data;
	}

	public function saveArea($request){

		$areaName 		= ($request->input('areaName') != "") ? $request->input('areaName') : null;
		$pincode 		= ($request->input('pincode') != "") ? $request->input('pincode') : null;
		$cityID 		= ($request->input('city') != "") ? $request->input('city') : null;
		$stateID 		= ($request->input('state') != "") ? $request->input('state') : null;
		$status 	= ($request->input('status') != "") ? $request->input('status') : null;
		$addedBy = auth()->guard('admin')->user()->id;
		$created_at = Carbon::now();
		
		$areaID = DB::table('area')->insertGetId([
			'areaName' => $areaName, 
			'pincode' => $pincode, 
			'cityID' => $cityID, 
			'stateID' => $stateID, 
			'status' => $status, 
			'addedBy'=> $addedBy,
			'created_at' => $created_at

		]);

		return $areaID;
	}

	public function updateArea($request){
		try {
			$areaID 		= ($request->input('areaID') != "") ? $request->input('areaID') : null;
			$decoded_areaID = base64_decode($areaID);
			$areaName 		= ($request->input('areaName') != "") ? $request->input('areaName') : null;
			$pincode 		= ($request->input('pincode') != "") ? $request->input('pincode') : null;
			$cityID 		= ($request->input('city') != "") ? $request->input('city') : null;
			$stateID 		= ($request->input('state') != "") ? $request->input('state') : null;
			$status 	= ($request->input('status') != "") ? $request->input('status') : null;
			$updated_at = Carbon::now();
			
			$result = DB::table('area')->where('areaID',$decoded_areaID)->update([
				'areaName' => $areaName, 
				'pincode' => $pincode, 
				'cityID' => $cityID, 
				'stateID' => $stateID, 
				'status' => $status, 
				'updated_at' => $updated_at
				
			]);
			return true;
		} catch (Exception $e) {
			return false;
		}
		return false;
	}

	public function deleteRecordById($areaID){
		
		try {
			$result = DB::table('area')->where('areaID',$areaID)->update(['status'=>'Delete']);
			return 'true';
		} catch (Exception $e) {
			return 'false';
		}
		return 'false';
	}
}