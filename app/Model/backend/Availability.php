<?php
namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon;
Class Availability extends Model{
    use Sortable;
	protected $table = "availability";
	public $sortable = ['availabilityID',
                        'title',
                        'status'];

	public function getRecords(Availability $availability){

        $record_per_page = env('RECORDS_PER_PAGE_BACKEND');
		
		if(empty($_GET['sort']) || empty($_GET['order']) ){
			$availability = $availability->orderBy('availabilityID', 'desc');
		}
		if( !empty($_GET['title']) ){
			$availability = $availability->where('title', 'like', '%'.$_GET['title'].'%');
		}

		if( !empty($_GET['status']) ){
			$availability = $availability->where('status', 'like', '%'.$_GET['status'].'%');
		}

		$data = $availability->where('status', '<>', 'Delete')->sortable()->paginate($record_per_page);
		//$data = $availability->sortable()->paginate(10);
		return $data;
	}

	public function getRecordById($availabilityID){

		
		$data = DB::table($this->table)->where('availabilityID',$availabilityID)->where('status','<>','Delete')->first();

		return $data;
	}

	public function saveAvailability($request){

		$title 		= ($request->input('title') != "") ? $request->input('title') : null;
		$status 	= ($request->input('status') != "") ? $request->input('status') : null;
		$created_at = Carbon::now();
		
		$availabilityID = DB::table('availability')->insertGetId([
			'title' => $title, 
			'status' => $status, 
			'created_at' => $created_at

		]);

		return $availabilityID;
	}

	public function updateAvailability($request){
		try {
			$availabilityID 		= ($request->input('availabilityID') != "") ? $request->input('availabilityID') : null;
			$decoded_propertyTypeID = base64_decode($availabilityID);
			$title 		= ($request->input('title') != "") ? $request->input('title') : null;
			$status 	= ($request->input('status') != "") ? $request->input('status') : null;
			$updated_at = Carbon::now();
			
			$result = DB::table('availability')->where('availabilityID',$decoded_propertyTypeID)->update([
				'title' => $title, 
				'status' => $status, 
				'updated_at' => $updated_at
				
			]);
			return true;
		} catch (Exception $e) {
			return false;
		}
		return false;
	}

	public function deleteRecordById($availabilityID){
		
		try {
			$result = DB::table('availability')->where('availabilityID',$availabilityID)->update(['status'=>'Delete']);
			return 'true';
		} catch (Exception $e) {
			return 'false';
		}
		return 'false';
	}
}