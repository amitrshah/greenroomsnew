<?php
namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon;
Class State extends Model{
    use Sortable;
	protected $table = "state";
	public $sortable = ['stateID',
                        'stateName',
                        'status'];

	public function getRecords(State $state){

        $record_per_page = env('RECORDS_PER_PAGE_BACKEND');
		
		if(empty($_GET['sort']) || empty($_GET['order']) ){
			$state = $state->orderBy('stateID', 'desc');
		}
		if( !empty($_GET['stateName']) ){
			$state = $state->where('stateName', 'like', '%'.$_GET['stateName'].'%');
		}

		if( !empty($_GET['status']) ){
			$state = $state->where('status', 'like', '%'.$_GET['status'].'%');
		}

		$data = $state->where('status', '<>', 'Delete')->sortable()->paginate($record_per_page);
		//$data = $state->sortable()->paginate(10);
		return $data;
	}

	public function getRecordById($stateID){

		
		$data = DB::table('state')->where('stateID',$stateID)->where('status','<>','Delete')->first();

		return $data;
	}

	public function saveState($request){

		$stateName 		= ($request->input('stateName') != "") ? $request->input('stateName') : null;
		$status 	= ($request->input('status') != "") ? $request->input('status') : null;
		$addedBy = auth()->guard('admin')->user()->id;
		$created_at = Carbon::now();
		
		$stateID = DB::table('state')->insertGetId([
			'stateName' => $stateName, 
			'status' => $status, 
			'addedBy'=> $addedBy,
			'created_at' => $created_at

		]);

		return $stateID;
	}

	public function updateState($request){
		try {
			$stateID 		= ($request->input('stateID') != "") ? $request->input('stateID') : null;
			$decoded_stateID = base64_decode($stateID);
			$stateName 		= ($request->input('stateName') != "") ? $request->input('stateName') : null;
			$status 	= ($request->input('status') != "") ? $request->input('status') : null;
			$updated_at = Carbon::now();
			
			$result = DB::table('state')->where('stateID',$decoded_stateID)->update([
				'stateName' => $stateName, 
				'status' => $status, 
				'updated_at' => $updated_at
				
			]);
			return true;
		} catch (Exception $e) {
			return false;
		}
		return false;
	}

	public function deleteRecordById($stateID){
		
		try {
			$result = DB::table('state')->where('stateID',$stateID)->update(['status'=>'Delete']);
			return 'true';
		} catch (Exception $e) {
			return 'false';
		}
		return 'false';
	}
}