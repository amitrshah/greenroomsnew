<?php
namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Kyslik\ColumnSortable\Sortable;
use Auth;

Class UserRole extends Model{
    use Sortable;
	protected $table = "user_role";
	public $sortable = ['roleID',
                        'roleName',
                        'description',
                        'status'];

	public function getRecords(UserRole $userRole){

        $record_per_page = env('RECORDS_PER_PAGE_BACKEND');
		
		if(empty($_GET['sort']) || empty($_GET['order']) ){
			$userRole = $userRole->orderBy('roleID', 'desc');
		}
		if( !empty($_GET['roleName']) ){
			$userRole = $userRole->where('roleName', 'like', '%'.$_GET['roleName'].'%');
		}

		if( !empty($_GET['status']) ){
			$userRole = $userRole->where('status', 'like', '%'.$_GET['status'].'%');
		}

		$data = $userRole->where('status', '<>', 'Delete')->sortable()->paginate($record_per_page);
		//$data = $userRole->sortable()->paginate(10);
		return $data;
	}

	public function getRecordById($roleID){

		
		$data = DB::table('user_role')->where('roleID',$roleID)->where('status','<>','Delete')->first();

		return $data;
	}

	public function saveUserRole($request){

		$roleName 		= ($request->input('roleName') != "") ? $request->input('roleName') : null;
		$status 	= ($request->input('status') != "") ? $request->input('status') : null;
		$description = ($request->input('description') != "") ? $request->input('description') : null;

		$roleID = DB::table('user_role')->insertGetId([
			'roleName' => $roleName, 
			'description'=> $description,
			'status' => $status, 
		]);

		return $roleID;
	}

	public function updateUserRole($request){
		try {
			$roleID 		= ($request->input('roleID') != "") ? $request->input('roleID') : null;
			$decoded_roleID = base64_decode($roleID);
			$roleName 		= ($request->input('roleName') != "") ? $request->input('roleName') : null;
			$description = ($request->input('description') != "") ? $request->input('description') : null;
			$status 	= ($request->input('status') != "") ? $request->input('status') : null;

			$result = DB::table('user_role')->where('roleID',$decoded_roleID)->update([
				'roleName' => $roleName, 
				'description'=> $description,
				'status' => $status, 
			]);
			return true;
		} catch (Exception $e) {
			return false;
		}
		return false;
	}

	public function deleteRecordById($roleID){
		
		try {
			$result = DB::table('user_role')->where('roleID',$roleID)->update(['status'=>'Delete']);
			return 'true';
		} catch (Exception $e) {
			return 'false';
		}
		return 'false';
	}

	public function getUserRoleDropDown(){
		
		if(Auth::guard('admin')->user()->roleID == 3){
			$data = DB::table('user_role')->whereNotIn('roleID', [1,3])->pluck('roleName as role', 'roleID');
		} else {
			$data = DB::table('user_role')->pluck('roleName as role', 'roleID');
		}			

		return $data;
	}

	public function getAction(){		
		$action =  DB::table('action')->get();			
		return $action;
	}

	public function getModule(){		
		$user_role =  DB::table('module')->orderBy('moduleName')->get();			
		return $user_role;
	}
	public function getModulePermission($id){
		$query = DB::table('module_permission')->where('roleID', '=', $id);
		return $query->get();
	}

	public function updateModulePermission($request){
		try {
			$all_inputs = $request->all();
			$roleID = base64_decode($all_inputs['roleID']);

			$list_arr = isset($all_inputs['List']) ? $all_inputs['List'] : array();
			$add_arr = isset($all_inputs['Add']) ? $all_inputs['Add'] : array();
			$edit_arr = isset($all_inputs['Edit']) ? $all_inputs['Edit'] : array();
			$delete_arr = isset($all_inputs['Delete']) ? $all_inputs['Delete'] : array();
			
			$final_arr = array();

			/*$list_arr = explode(",", $list);*/
			$li = count($list_arr);
			if($li > 0){
				for ($l=0; $l < $li; $l++) {
					if($list_arr[$l]){
						$arr = explode("_", $list_arr[$l]);
						//$final_arr[$arr[0]][] = intval($arr[1]);
						$final_arr[$arr[0]]['List'] = true;
					}
				}
			}

			/*$add_arr = explode(",", $add);*/
			$ai = count($add_arr);
			if($ai > 0){
				for ($a=0; $a < $ai; $a++) {
					if($add_arr[$a]){
						$arr = explode("_", $add_arr[$a]);
						//$final_arr[$arr[0]][] = intval($arr[1]);
						$final_arr[$arr[0]]['Add'] = true;
					}
				}
			}

			/*$edit_arr = explode(",", $edit);*/
			$ei = count($edit_arr);
			if($ei > 0){
				for ($e=0; $e < $ei; $e++) {
					if($edit_arr[$e]){
						$arr = explode("_", $edit_arr[$e]);
						//$final_arr[$arr[0]][] = intval($arr[1]);
						$final_arr[$arr[0]]['Edit'] = true;
					}
				}
			}

			/*$delete_arr = explode(",", $delete);*/
			$di = count($delete_arr);
			if($di > 0){
				for ($d=0; $d < $di; $d++) {
					if($delete_arr[$d]){
						$arr = explode("_", $delete_arr[$d]);
						//$final_arr[$arr[0]][] = intval($arr[1]);
						$final_arr[$arr[0]]['Delete'] = true;
					}
				}
			}

			$data = array();

			if(count($final_arr) > 0){

				foreach ($final_arr as $key => $action_arr) {
					$data[] = array('roleID'=>$roleID, 'moduleID'=>$key, 'actionID'=>json_encode($action_arr));		
				}
			}

			//print_r($data);exit;

			//Delete Using Rule id
			DB::table('module_permission')->where('roleID', '=', $roleID)->delete();

			//Insert Into Rule
			$affected = DB::table('module_permission')->insert($data);

			return $affected;
		} catch (Exception $e) {
			return false;
		}
	}
}