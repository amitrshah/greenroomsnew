<?php
namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon;
Class AvailibilityType extends Model{
    use Sortable;
	protected $table = "availibility_type";
	public $sortable = ['availibilityTypeID',
                        'title',
                        'status'];

	public function getRecords(AvailibilityType $availibilityType){

        $record_per_page = env('RECORDS_PER_PAGE_BACKEND');
		
		if(empty($_GET['sort']) || empty($_GET['order']) ){
			$availibilityType = $availibilityType->orderBy('availibilityTypeID', 'desc');
		}
		if( !empty($_GET['title']) ){
			$availibilityType = $availibilityType->where('title', 'like', '%'.$_GET['title'].'%');
		}

		if( !empty($_GET['status']) ){
			$availibilityType = $availibilityType->where('status', 'like', '%'.$_GET['status'].'%');
		}

		$data = $availibilityType->where('status', '<>', 'Delete')->sortable()->paginate($record_per_page);
		//$data = $availibilityType->sortable()->paginate(10);
		return $data;
	}

	public function getRecordById($availibilityTypeID){

		
		$data = DB::table('availibility_type')->where('availibilityTypeID',$availibilityTypeID)->where('status','<>','Delete')->first();

		return $data;
	}

	public function saveAvailibilityType($request){

		$title 		= ($request->input('title') != "") ? $request->input('title') : null;
		$status 	= ($request->input('status') != "") ? $request->input('status') : null;
		$created_at = Carbon::now();
		
		echo $availibilityTypeID = DB::table('availibility_type')->insertGetId([
			'title' => $title, 
			'status' => $status, 
			'created_at' => $created_at

		]);

		return $availibilityTypeID;
	}

	public function updateAvailibilityType($request){
		try {
			$availibilityTypeID 		= ($request->input('availibilityTypeID') != "") ? $request->input('availibilityTypeID') : null;
			$decoded_propertyTypeID = base64_decode($availibilityTypeID);
			$title 		= ($request->input('title') != "") ? $request->input('title') : null;
			$status 	= ($request->input('status') != "") ? $request->input('status') : null;
			$updated_at = Carbon::now();
			
			$result = DB::table('availibility_type')->where('availibilityTypeID',$decoded_propertyTypeID)->update([
				'title' => $title, 
				'status' => $status, 
				'updated_at' => $updated_at
				
			]);
			return true;
		} catch (Exception $e) {
			return false;
		}
		return false;
	}

	public function deleteRecordById($availibilityTypeID){
		
		try {
			$result = DB::table('availibility_type')->where('availibilityTypeID',$availibilityTypeID)->update(['status'=>'Delete']);
			return 'true';
		} catch (Exception $e) {
			return 'false';
		}
		return 'false';
	}
}