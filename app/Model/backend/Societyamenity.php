<?php
namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon;
Class Societyamenity extends Model{
    use Sortable;
	protected $table = "property_societyamenity";
	public $sortable = ['societyamenityID',
                        'title',
                        'status'];

	public function getRecords(Societyamenity $societyamenity){

                $record_per_page = env('RECORDS_PER_PAGE_BACKEND');
		
		if(empty($_GET['sort']) || empty($_GET['order']) ){
			$societyamenity = $societyamenity->orderBy('societyamenityID', 'desc');
		}
		if( !empty($_GET['title']) ){
			$societyamenity = $societyamenity->where('title', 'like', '%'.$_GET['title'].'%');
		}

		if( !empty($_GET['status']) ){
			$societyamenity = $societyamenity->where('status', 'like', '%'.$_GET['status'].'%');
		}

		$data = $societyamenity->where('status', '<>', 'Delete')->sortable()->paginate($record_per_page);		
		return $data;
	}

	public function getRecordById($rulesID){
		$data = DB::table('property_societyamenity')->where('societyamenityID',$rulesID)->where('status','<>','Delete')->first();
		return $data;
	}
        
        public function getAllRecord(){
		$data = DB::table('property_societyamenity')->where('status','<>','Delete')->get();
		return $data;
	}
        
	public function saveSocietyamenity($request){

		$title 		= ($request->input('title') != "") ? $request->input('title') : null;
		$status 	= ($request->input('status') != "") ? $request->input('status') : null;
		$created_at = Carbon::now();
		
		$facilitiesID = DB::table('property_societyamenity')->insertGetId([
			'title' => $title, 
			'status' => $status, 
			'created_at' => $created_at

		]);

		return $facilitiesID;
	}

	public function updateSocietyamenity($request){
		try {
			$societyamenityID 		= ($request->input('societyamenityID') != "") ? $request->input('societyamenityID') : null;
			$decoded_societyamenityID = base64_decode($societyamenityID);
			$title 		= ($request->input('title') != "") ? $request->input('title') : null;
			$status 	= ($request->input('status') != "") ? $request->input('status') : null;
			$updated_at = Carbon::now();
			
			$result = DB::table('property_societyamenity')->where('societyamenityID',$decoded_societyamenityID)->update([
				'title' => $title, 
				'status' => $status, 
				'updated_at' => $updated_at
				
			]);
			return true;
		} catch (Exception $e) {
			return false;
		}
		return false;
	}

	public function deleteRecordById($societyamenityID){
		
		try {
			$result = DB::table('property_societyamenity')->where('societyamenityID',$societyamenityID)->update(['status'=>'Delete']);
			return 'true';
		} catch (Exception $e) {
			return 'false';
		}
		return 'false';
	}
}