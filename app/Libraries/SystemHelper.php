<?php

namespace App\Libraries;

use Auth;
use DB;
use DateTime;
use URL;
use Session;

class SystemHelper
{
	public  function __construct() {

	}

	public static function is_module_permission($module_name,$module_action){
		$permission =Session::get('sess_access_module');
		if(!empty($permission)){
	        if(array_key_exists($module_name,$permission) && array_key_exists($module_action,$permission[$module_name])){
	        	return true;
	        }else{
	        	return false;
	        }
    	}
	}
}