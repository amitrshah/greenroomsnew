<?php
namespace App\Libraries;
use DB;
use Auth;
use Mail;
use Illuminate\Support\Facades\Input; // Input

class GlobalHelpers
{

	public  function __construct() {

	}


	//ADD BY CT
    public static function propertyOnFloor(){
        $property_on_floor=['1'=>'basement','2'=>'Lower Ground','3'=>'Ground','4'=>'1','5'=>'2','6'=>'40+'];
        return $property_on_floor;
    }

    //ADD BY CT
    public static function totalFloor(){
        $total_floor=['1'=>'1','2'=>'2','3'=>'Ground','4'=>'40+'];
        return $total_floor;
    }

    //ADD BY CT
    public static function SuitableFor(){
        $suitabl_for=['1'=>'Student','2'=>'Working professionals','3'=>'Both student & working professionals'];
        return $suitabl_for;
    }
//ADD BY CT
    public static function bedSharing(){
        $bed_sharing = ['1'=>'Single','2'=>'two' ,'3' => 'four', '4' => 'five'];
        return $bed_sharing;
    }
//ADD BY CT
    public static function availabilityType(){
        $availability_type=['1'=>'Low Rise Apartment','2'=>'High Rise Apartment','3'=>'Bungalow','4'=>'Tenement'];
        return $availability_type;
    }

    //ADD BY CT
    public static function meals(){
        $meals=['1'=>'Lunch','2'=>'Breakfast','3'=>'Dinner','4'=>'Not Included
'];
        return $meals;
    }

    //ADD BY CT
    public static function Extracharge(){
        $extra_charge=['1'=>'Maintenance','2'=>'Included All Charges','3'=>'AC Charge
'];
        return $extra_charge;
    }

	/* meals included for property */
	public static function mealsIncludedArr(){
		$meals_included = ['1' => 'Breakfast','2' => 'Lunch','3' => 'Dinner','4' => 'Not Included'];
        return $meals_included;
	}

	/* condition type for property */
	public static function conditionArr(){
		$conditionArr = ['1'=>'Furnished','2'=>'Semi Furnished' ,'3' => 'Full Furnished'];
        return $conditionArr;
	}
        
        //mehul change
        public static function numberOfRoomArr(){
		// $numberOfRoomArr = ['1'=>'1BHK','2'=>'2BHK' ,'3' => '3BHK', '4' => '4BHK', '5' => '5BHK'];
        // return $numberOfRoomArr;
		return $numberOfRoomArr = DB::table('availability')->where('status','<>','Delete')->orderBy('title','ASC')->pluck('title as title', 'availabilityID')->toArray();
		
	} 
        
        public static function facingArr(){
		$facingArr = ['north'=>'North','south'=>'South' ,'east' => 'East', 'west' => 'West'];
        return $facingArr;
	} 
        
        public static function noticePeriodArr(){
		$noticePeriodArr = ['1'=>'15 Days','2'=>'30 Days' ,'3' => '60 Days'];
        return $noticePeriodArr;
	} 
        //mehul change over
        
        public static function availablebhkArr(){
            $availablebhkArr = ['1'=>'1 BHK','2'=>'2 BHK' ,'3' => '3 BHK','4' => '4 BHK','5' => '5 BHK','6' => '6 BHK','7' => '7 BHK','8' => '8 BHK','9' => '9 BHK','10' => '10 BHK','11' => 'Above 10 BHK'];
            return $availablebhkArr;
	}
        
        /* floor */
	public static function floorArr(){
            $floorArr = ['1'=>'2 out of 10 floor','2'=>'2 out of 12 floor'];
            return $floorArr;
	}
        
        /* society amenity */
	public static function societyamenityArr(){
            $societyamenityArr = ['1' => '24 hour water supply', '2' => 'Security' , '3' =>'Park', '4' => 'Coverd car parking', '5' => 'Open car parking', '6' => 'Two wheeler parking'];
            return $societyamenityArr;
	}
        
	/* property type for property */
	public static function propertyTypeDropdown($status = null){
		if($status != null){
			$propertyTypeDropdown = DB::table('property_type')->where('status','=',$status)->where('status','<>','Delete')->orderBy('title','ASC')->pluck('title as propertyTypeName', 'propertyTypeID')->toArray();
		}else{
			$propertyTypeDropdown = DB::table('property_type')->where('status','<>','Delete')->orderBy('title','ASC')->pluck('title as propertyTypeName', 'propertyTypeID')->toArray();
		}
        return $propertyTypeDropdown;
	}

	public static function roomSharingDropdown($status = null){
		if($status != null){
			$propertyTypeDropdown = DB::table('room_sharing')->where('status','=',$status)->where('status','<>','Delete')->orderBy('title','ASC')->pluck('title as roomSharingName', 'id as roomSharingID')->toArray();

		}else{
			$propertyTypeDropdown = DB::table('room_sharing')->where('status','<>','Delete')->orderBy('id','ASC')->pluck('title as roomSharingName', 'id as roomSharingID')->toArray();
		}
        return $propertyTypeDropdown;
	}

	public function getRecordById($id){

		
		$data = DB::table('room_sharing')->where('id',$id)->where('status','<>','Delete')->first();

		return $data;
	}

	/* get state dropdown */
	public static function stateDropdown($stateID = null){
		if(empty($stateID)){

			$stateDropdown = DB::table('state')->where('status','<>','Delete')->orderBy('stateName','ASC')->pluck('stateName as stateName', 'stateID')->toArray();
		}else{

			$stateDropdown = DB::table('state')->where('stateID','=',$stateID)->where('status','<>','Delete')->pluck('stateName as stateName', 'stateID')->toArray();
		}
        return $stateDropdown;
	}

	/* get city dropdown */
	public static function cityDropdown($stateID = null, $cityID = null){
		$cityDropdown = [];
		if(!empty($stateID) && !empty($cityID)){
			$cityDropdown = DB::table('city')->where('stateID','=',$stateID)->where('cityID','=',$cityID)->orderBy('cityName','ASC')->where('status','<>','Delete')->pluck('cityName as cityName', 'cityID')->toArray();
		}elseif(!empty($stateID)){
			$cityDropdown = DB::table('city')->where('stateID','=',$stateID)->orderBy('cityName','ASC')->where('status','<>','Delete')->pluck('cityName as cityName', 'cityID')->toArray();

		}elseif(empty($stateID)){

			$cityDropdown = DB::table('city')->where('status','<>','Delete')->orderBy('cityName','ASC')->pluck('cityName as cityName', 'cityID')->toArray();
		}
        return $cityDropdown;
	}


	/* get area dropdown */
	public static function areaDropdown($stateID = null,$cityID = null,$areaID=null){
		$areaDropdown = [];

			/*$areaDropdown = DB::table('area')->where('stateID','=',$stateID)->where('cityID','=',$cityID)->where('status','<>','Delete')->pluck('areaName as areaName', 'areaID')->toArray();*/
		if(!empty($cityID) && !empty($stateID) && !empty($areaID)){

			$areaDropdown = DB::table('area')->where('stateID','=',$stateID)->where('cityID','=',$cityID)->where('areaID','=',$areaID)->orderBy('areaName','ASC')->where('status','<>','Delete')->pluck('areaName as areaName', 'areaID')->toArray();
		}
		elseif(!empty($cityID) && !empty($stateID)){
			$areaDropdown = DB::table('area')->where('stateID','=',$stateID)->where('cityID','=',$cityID)->orderBy('areaName','ASC')->where('status','<>','Delete')->pluck('areaName as areaName', 'areaID')->toArray();
		}elseif(empty($stateID) && !empty($cityID)){

			$areaDropdown = DB::table('area')->where('cityID','=',$cityID)->where('status','<>','Delete')->orderBy('areaName','ASC')->pluck('areaName as areaName', 'areaID')->toArray();
		}
        return $areaDropdown;
	}

	public static function planDropdown(){
		$planDropdown = DB::table('plan')->where('status','<>','Delete')->orderBy('planName','ASC')->pluck('planName as planName', 'planID as planID')->toArray();
        return $planDropdown;
	}
	
	public static function facilitiesDropdown(){
		$FacilitiesArr = ['westernToilet'=>'Western toilet','AC'=>'AC' ,'parking' => 'Parking','security' => 'Security','housekeeping' => 'Housekeeping','laundry' => 'Laundry','wifi' =>'WiFi','attachedBathroom' => 'Attached bathroom','geyser' => 'Geyser','powerBackup' => 'Power backup','roPurified' =>'RO purified drinking water'];
		return $FacilitiesArr;
	}
	/* transaction status */
	public static function availableTypeArr(){
		$availableType = DB::table('availibility_type')->where('status','<>','Delete')->orderBy('title','ASC')->pluck('title as title', 'availibilityTypeID as availibilityTypeID')->toArray();
        return $availableType;
	}
	
	public static function propertyAge(){
            $propertyAge = ['1'=>'0 to 3 years','2'=>'3 to 5 years','3'=>'5 to 10 years','4'=>'10 to 20 years','5'=>'more than 20 years'];
            return $propertyAge;
	}
	
	public static function finalizeAge(){
            $propertyAge = ['1'=>'0 to 15 days','2'=>'3 to 5 years','3'=>'5 to 10 years','4'=>'10 to 20 years','5'=>'more than 20 years'];
            return $propertyAge;
	}
	
	/* transaction status */
	public static function tnxStatsDropdown(){
		$tnxStatsDropdown = DB::table('payment_details')->pluck('responseStatus as responseStatus', 'responseStatus as responseStatus')->toArray();
        return $tnxStatsDropdown;
	}
	
	/* accomodation for */
	public static function accomodationFor(){
		$accomodationFor = [1=>'Male',2=>'Female'];
		return $accomodationFor;
	}

	/*******************************************************
	 * Method: sendMail
	 * Params: to | from | subject | template (name of email template) | additional_headers (for cc, bcc) | params (data for replace array)
	 * Description: It will send an email
	 * return: boolean
	 *******************************************************/
    public static function sendMail($to, $from, $subject, $template, $additional_headers = array(), $params = array(), $attachment = array(), $attachmentName= false) {
		if(!empty($params)) {
			$data = array();
			$data['to'] = $to;
			$data['from'] = $from;
			$data['subject'] = $subject;
			if(isset($additional_headers['cc']))
				$data['cc'] = $additional_headers['cc'];
			if(isset($additional_headers['bcc']))
				$data['bcc'] = $additional_headers['bcc'];
			if(isset($additional_headers['reply_to_email']))
				$data['replyto'] = $additional_headers['reply_to_email'];

			$send = false;
			try {
				$send = Mail::send($template, $params, function ($message) use ($data, $attachment,$attachmentName) {
					$pathToFile = '';
				 	$message->to($data['to'])->subject($data['subject']);
				 	if(isset($data['cc']) && !empty($data['cc']))
				 		$message->cc($data['cc']);
				 	if(isset($data['bcc']) && !empty($data['bcc']))
						$message->bcc($data['bcc']);
					if(isset($data['replyto']) && !empty($data['replyto']) )
						$message->replyTo($data['replyto']);

					if(!empty($attachment)) {

						if(empty($attachmentName)){
							$attachmentName = "greenroom.pdf";
						}
						$message->attach($attachment, ['as' => $attachmentName, 'mime' => 'application/pdf']);
					}
				});
			}
			catch (Exception $e) {
				echo $e->getMessage();exit;
				return false;
			}

			if($send) return true;
			else return false;
		}
		else return false;
    }
}
