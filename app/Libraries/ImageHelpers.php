<?php 
namespace App\Libraries;

//use Intervention\Image\Facades\Image; // Use this if you want facade style code
//use Intervention\Image\ImageManager; // Use this if you don't want facade style code
use Intervention\Image\ImageManagerStatic as Image;
use Storage;
use File;
use DB;
use FFMpeg;
use Carbon\Carbon;


class ImageHelpers
{	
	/*******************************************************
	 * Method: uploadResizeImage
	 * Params: fileObj | propertyImageSizeArr | savePath | imgName | convertFormat = 'jpg' | convertQuality = 85
	 * Description: Upload image in sizes
	 * return: Count
	 *******************************************************/    
	public static function uploadResizeImage($fileObj, $propertyImageSizeArr, $savePath, $imgName, $convertFormat='jpg', $convertQuality=85) {
		$counter = 0;
		/* $extension 		= 	$fileObj->getClientOriginalExtension();
		echo $imageRealPath 	= 	$fileObj->getRealPath();
		$imageName 		= 	$fileObj->getClientOriginalName(); */
		
		
		if(is_object($fileObj)) {
			$realPath = $fileObj->getRealPath();
		}
		else {
			$realPath = $fileObj;
		}
		
		
		foreach ($propertyImageSizeArr as $key => $image_arr) {
			if(!empty($image_arr['w']) && !empty($image_arr['h'])) {
				$success = Image::make( $realPath )->encode($convertFormat, $convertQuality)->fit($image_arr['w'], $image_arr['h'])->save($savePath . DIRECTORY_SEPARATOR . $key . DIRECTORY_SEPARATOR . $imgName )->destroy();
			}
			else {
				$success = Image::make( $realPath )->encode($convertFormat, $convertQuality)->save($savePath . DIRECTORY_SEPARATOR . $key . DIRECTORY_SEPARATOR . $imgName )->destroy();
			}
			if($success)
				$counter++;
		}
		return $counter;
	}
			
	/*******************************************************
	 * Method: getSmallImage
	 * Params: path | imgName
	 * Description: get small size image url
	 * return: File URL
	 *******************************************************/    
	public static function getSmallImage($path, $imgName="") {
		$file = '';
		$url = url('/').DIRECTORY_SEPARATOR.$path;
		$imgBase = storage_path($path);
		$imagePath = $imgBase . DIRECTORY_SEPARATOR . 2 . DIRECTORY_SEPARATOR . $imgName;
		if(\File::exists($imagePath) && !empty($imgName)) {
			//$file = $url . DIRECTORY_SEPARATOR . 2 . DIRECTORY_SEPARATOR . $imgName."?".time();
			$file = $url . DIRECTORY_SEPARATOR . 2 . DIRECTORY_SEPARATOR . $imgName;
		}
		else {
			
			$file = $url . DIRECTORY_SEPARATOR . "no_image_2.jpg";			
		}
		return $file;
	}

	/*******************************************************
	 * Method: getMediumImage
	 * Params: path | imgName
	 * Description: get medium size image url
	 * return: File URL
	 *******************************************************/    
	public static function getMediumImage($path, $imgName="") {
		$file = '';
		$url = url('/').DIRECTORY_SEPARATOR.$path;
		$imgBase = storage_path($path);
		$imagePath = $imgBase . DIRECTORY_SEPARATOR . 3 . DIRECTORY_SEPARATOR . $imgName;
		if(\File::exists($imagePath) && !empty($imgName)) {
			//$file = $url . DIRECTORY_SEPARATOR. 3 . DIRECTORY_SEPARATOR . $imgName."?".time();
			$file = $url . DIRECTORY_SEPARATOR. 3 . DIRECTORY_SEPARATOR . $imgName;
		}
		else {
			$file = $url . DIRECTORY_SEPARATOR . "no_image_1.jpg";			
		}
		return $file;
	}
		
	/*******************************************************
	 * Method: getOriginalImage
	 * Params: path | imgName
	 * Description: get original size image url
	 * return: File URL
	 *******************************************************/    
	public static function getOriginalImage($path, $imgName="") {
		$file = '';
		$url = url('/').DIRECTORY_SEPARATOR.$path;
		$imgBase = storage_path($path);
		$imagePath = $imgBase . DIRECTORY_SEPARATOR . 1 . DIRECTORY_SEPARATOR . $imgName;
		if(\File::exists($imagePath) && !empty($imgName)) {
			$file = $url . DIRECTORY_SEPARATOR. 1 . DIRECTORY_SEPARATOR . $imgName;
		}
		else {
			$file = $url . DIRECTORY_SEPARATOR . "no_image.jpg";
		}
		return $file;
	}
	
	/*******************************************************
	 * Method: getOriginalImagePath
	 * Params: url | path | imgName
	 * Description: get original size image url
	 * return: File URL
	 *******************************************************/    
	public static function getOriginalImagePath($path, $imgName) {		
		$imagePath = $path . DIRECTORY_SEPARATOR . '1' . DIRECTORY_SEPARATOR . $imgName;
		if(!\File::exists($imagePath)) {
			$imagePath = '';
		}
		return $imagePath;
	}
	
	/*******************************************************
	 * Method: getLargeImagePath
	 * Params: url | path | imgName
	 * Description: get original size image url
	 * return: File URL
	 *******************************************************/    
	public static function getLargeImagePath($path, $imgName) {
		$imagePath = $path . DIRECTORY_SEPARATOR . '3' . DIRECTORY_SEPARATOR . $imgName;
		if(!\File::exists($imagePath)) {
			$imagePath = '';
		}
		return $imagePath;
	}

	/*******************************************************
	 * Method: getImage
	 * Params: url | path | imgName
	 * Description: get original size image url
	 * return: File URL
	 *******************************************************/    
	public static function getImage($url, $path, $imgName) {
		$file = '';
		$imagePath = $path . DIRECTORY_SEPARATOR . $imgName;
		if(\File::exists($imagePath)) {
			$file = $url . DIRECTORY_SEPARATOR. $imgName;
		}
		else {
			$file = $url . DIRECTORY_SEPARATOR . "no_image.jpg";
		}
		return $file;
	}

	/*******************************************************
	 * Method: deleteResizeImage
	 * Params: id | sizeArr | path | imgName
	 * Description: remove multiple size images
	 * return: boolean
	 *******************************************************/    
	public static function deleteResizeImage($path, $imgName, $sizeArr ) {
		$imgBase = storage_path($path);
		$return['status'] = false;
		foreach ($sizeArr as $key => $image_arr) {			
			if(File::isFile($imgBase . DIRECTORY_SEPARATOR . $key . DIRECTORY_SEPARATOR . $imgName)){
				\File::delete($imgBase . DIRECTORY_SEPARATOR . $key . DIRECTORY_SEPARATOR . $imgName);
				$return['status'] = true;
			} else {
				$return['files'][] = $imgBase . DIRECTORY_SEPARATOR . $key . DIRECTORY_SEPARATOR . $imgName;
				$return['status'] = false;
			}
		}
		return $return;
	}
}
